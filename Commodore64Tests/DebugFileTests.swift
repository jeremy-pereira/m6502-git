//
//  DebugFileTests.swift
//  Commodore64Tests
//
//  Created by Jeremy Pereira on 16/12/2018.
//

import XCTest
@testable import Commodore64


class DebugFileTests: XCTestCase
{

    override func setUp()
    {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

	let testInfoString =
    """
    segment	"DATA",start=0x001300,size=0x0108,addrsize=absolute,type=rw
    segment	"CODE",start=0x0007FF,size=0x0AD2,addrsize=absolute,type=rw
    file	"hardware-test.s",size=17507,mtime=0x5C114E70
    file	"stackmachine.inc",size=8893,mtime=0x5C0FFAAA
    file	"characters.inc",size=1076,mtime=0x5C027B62
    sym	".size",value=0x00000000,addrsize=zeropage,type=equate
    sym	".size",value=0x00000011,addrsize=zeropage,type=equate
    sym	"switchVicBank",value=0x00000CC8,addrsize=absolute,type=label
    sym	"switchScreen",value=0x00000CB7,addrsize=absolute,type=label
    sym    "dataStackPtr",value=0x0000006C,addrsize=zeropage,type=label
    sym    "dataStackAlt",value=0x0000006C,addrsize=absolute,type=label
    sym    "baseRegister",value=0x0000006A,addrsize=zeropage,type=label
    """

    func testCreateInfo()
    {
        do
        {
            let debugInfo = try DebugInfo(string: testInfoString)
            let switchScreen = debugInfo["switchScreen"]
            if switchScreen.count == 1
            {
                XCTAssert(switchScreen[0] == 0xcb7)
            }
            else
            {
                XCTFail("switchScreen: expected one element: \(switchScreen)")
            }
            XCTAssert(debugInfo["doesNotExist"].count == 0)

        }
        catch
        {
            XCTFail("\(error)")
        }
    }

    func testResolveLabels()
    {
        do
        {
            let debugInfo = try DebugInfo(string: testInfoString)
            let foundZpLabels = debugInfo.labels(address: 0x6c, type: .zeroPage)
            XCTAssert(foundZpLabels.count == 1)
            let foundAbsoluteLabels = debugInfo.labels(address: 0x6c, type: .absolute)
            XCTAssert(foundAbsoluteLabels.count == 1)
            let foundAllLabels = debugInfo.labels(address: 0x6c, type: nil)
            XCTAssert(foundAllLabels.count == 2)
        }
        catch
        {
            XCTFail("\(error)")
        }

    }
}
