//
//  Commodore64.swift
//  Commodore64
//
//  Created by Jeremy Pereira on 29/09/2018.
//
import AppKit
import Swift6502

class Commodore64
{
    let programStart: S65Address = 0x0200
    let romImageLoad = "c64-load"
    let loadExtension = "6502load"

    public static let cia1Location: S65Address = 0xDC00 - C64RamPla.ioBase
    public static let cia2Location: S65Address = 0xDD00 - C64RamPla.ioBase
    public static let vicLocation: S65Address = 0xD000 - C64RamPla.ioBase
    public static let sidLocation: S65Address = 0xD400 - C64RamPla.ioBase
    public static let colourRAMLocation: S65Address = 0xD800 - C64RamPla.ioBase

    var cpu: Mos6510
    var pia1 = PIA6520()
    var pia2 = PIA6520()
    var via = VIA6522()
    var pla = C64RamPla()
    let loRamWire = Wire()
    let hiRamWire = Wire()
    let charEnWire = Wire()
    let cia1 = CIA6526(baseAddress: cia1Location)
    let cia2 = CIA6526(baseAddress: cia2Location)
    let vicII: VicII
    let colourRAM = ColourRAM(baseAddress: colourRAMLocation)
    let keyboard = C64Keyboard()

    /// IRQ line
    var irqAnd = AndGate()
    fileprivate var cpuTraceStack = Stack<Bool>()

    init(vduController: VDUController) throws
    {
        cpu = make6510Bus(memory: pla)
        vicII = VicII(pla: pla)
        vicII.baseAddress = Commodore64.vicLocation
        pla.map(device: vicII)
        pla.repeatMapping(target: vicII,
                          range: (vicII.baseAddress + 0x40 ..< Commodore64.sidLocation))

        // Connect the IO port to the RAM banks
        cpu.ioPort.connect(target: TriStatePort.NotificationTarget(mask: 0x7, notifier: {
            [unowned self] (bits) in
            var newMode = self.pla.mode
			newMode.set(bits: .loRam, on: bits & 1 != 0)
            newMode.set(bits: .hiRam, on: bits & (1 << 1) != 0)
            newMode.set(bits: .charEn, on: bits & (1 << 2) != 0)
            self.pla.mode = newMode
        }))
        pla.map(device: colourRAM)
        pla.map(device: cia1)
        pla.map(device: cia2)
        pla.repeatMapping(target: cia1,
                           range: (cia1.baseAddress + S65Address(cia1.portCount)) ..< Commodore64.cia2Location)
        pla.repeatMapping(target: cia2,
                           range: (cia2.baseAddress + S65Address(cia2.portCount)) ..< (0xDE00 - C64RamPla.ioBase))
		cpu.addClockDriver(cia1)
        cpu.addClockDriver(cia2)
        cia1.connectPortA(mask: 0xff)
        {
            [unowned self] in
            self.keyboard.selectedColumn = $0
        }
        cia2.connectPortA(mask: 0x03)
        {
            [unowned self] in
            // The two bits of the data port are the inverted bank number
            self.pla.vicBankNumber = Int($0 ^ 0b11)
        }
        keyboard.selectedRow.connect(cia1, mask: 0xff)
        {
            [unowned self] in
            self.cia1.portBPins = $0
        }
        cia1.notIRQ.pipeToWire(cpu.irqWire)
        // TODO: We haven't implemented NMI yet
        try loadROMImages()
        vduController.vicII = vicII
        vduController.colourRAM = colourRAM

//        cpu.set(address: 0x09b8)
//        {
//            [unowned self] in
//            self.cpu.trace = true
//            self.traceJSR = true
//        } // drawp pattern
//        cpu.set(address: 0x0a13)
//        {
//            [unowned self] in
//            self.cpu.trace = false
//            self.traceJSR = false
//        } // drawp pattern end
//        cpu.set(address: 0x0a14)
//        {
//            [unowned self] in
//            self.cpu.trace = true
//            print("draw diagonal line")
//        } // draw diagonal line
//        cpu.jsrTrace =
//        {
//            [unowned self] (pc, address) in
//            if let smState = self.stackMachineStateAsString(),
//                self.traceJSR && pc > 0x800 && pc < 0x8000
//            {
//                print("\(pc.hexString): JSR to \(self.resolveSymbol(address: address) ?? ("$" + address.hexString))")
//                   print(smState)
//            }
//            self.cpuTraceStack.push(self.cpu.trace)
//            self.cpu.trace = false
//        }
//        cpu.rtsTrace =
//        {
//            [unowned self] pc in
//            if let smState = self.stackMachineStateAsString(),
//                self.traceJSR && pc > 0x800 && pc < 0x8000
//            {
//                print("\(pc.hexString): RTS")
//                print(smState)
//            }
//            if !self.cpuTraceStack.isEmpty
//            {
//                self.cpu.trace = self.cpuTraceStack.pop()
//            }
//        }
    }

    private func loadROMImages() throws
    {
        let thisBundle = Bundle(for: type(of: self))
        if let fileURL = thisBundle.url(forResource: romImageLoad, withExtension: loadExtension)
        {
            try cpu.loadImagesFromURL(fileURL)
        }
        else
        {
            throw Error6502.fileLoad(message: "No resource for \(romImageLoad).\(loadExtension)")
        }
    }

    func loadProgram(bytes: [S65Byte]) throws
    {
        guard bytes.count >= 2 else { throw Error.invalidProgram }

        var loadAddress: S65Address = S65Address(bytes[0])
        loadAddress.highByte = bytes[1]
        cpu.set(bytes: bytes[2 ..< bytes.endIndex], at: loadAddress)
    }

    private var debugInfo: DebugInfo?

    func load(debugInfo: DebugInfo)
    {
        self.debugInfo = debugInfo
    }

    private var traceJSR = false

    private func resolveSymbol(address: S65Address) -> String?
    {
        guard let debugInfo = debugInfo else { return nil }
        let labels = debugInfo.labels(address: address)
        guard !labels.isEmpty else { return nil }
        return labels.reduce("")
        {
            (result, nextLabel) -> String in
            return result.isEmpty ? nextLabel : (result + "|" + nextLabel)
        }
    }
}

extension Commodore64
{
    enum Error: Swift.Error
    {
		case invalidProgram
    }
}


// MARK: - Debugging the stack machine
fileprivate extension Commodore64
{

    /// Writre the stack machine state a sa string.
    ///
    /// This will only work if a debug fiule is loaded with a zero page
    /// label called `dataStackPtr` and another one called `baseRegister`.
    ///
    /// - Returns: A string representing the stack machine or nil if we cannot
    ///            be sure this is a stack machine.
	func stackMachineStateAsString() -> String?
    {
        // If there's no debug info, we can't find the stack
        guard let debugInfo = debugInfo  else { return nil }
        let possibleDataStacks = debugInfo["dataStackPtr"].filter { $0 < 0x100 }
        guard let dataStackPtr = possibleDataStacks.first else { return nil }
        let possibleBaseRegisters = debugInfo["baseRegister"].filter { $0 < 0x100 }
        guard let baseRegister = possibleBaseRegisters.first else { return nil }
		var stackTop = cpu.word(at: dataStackPtr)
        var currentBase = cpu.word(at: baseRegister)
        var ret = "data stack: \(stackTop.hexString), base: $\(currentBase.hexString)"
        for offset in 0 ..< 32
        {
            if offset % 8 == 0
            {
                ret += "\n\t\(stackTop.hexString):"
            }
            if stackTop == currentBase
            {
                ret += " B:"
                currentBase = cpu.word(at: stackTop)
            }
            else
            {
                ret += "   "
            }
            ret += "\(cpu.byte(at: stackTop).hexString)"
            stackTop &+= 1
        }
        return ret
    }


}
