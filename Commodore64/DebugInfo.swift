//
//  DebugInfo.swift
//  Commodore64
//
//  Created by Jeremy Pereira on 14/12/2018.
//

import Swift6502


/// Provides debug indo for a program that is loaded into the C64.
///
/// The debug file is the sort that is compatible with `ld65`. A debug file
/// looks something like this:
///
/// ```
/// segment    "DATA",start=0x001300,size=0x0108,addrsize=absolute,type=rw
/// segment    "CODE",start=0x0007FF,size=0x0AD2,addrsize=absolute,type=rw
/// file    "hardware-test.s",size=17507,mtime=0x5C114E70
/// file    "stackmachine.inc",size=8893,mtime=0x5C0FFAAA
/// file    "characters.inc",size=1076,mtime=0x5C027B62
/// sym    ".size",value=0x00000000,addrsize=zeropage,type=equate
/// sym    ".size",value=0x00000011,addrsize=zeropage,type=equate
/// sym    "switchVicBank",value=0x00000CC8,addrsize=absolute,type=label
/// sym    "switchScreen",value=0x00000CB7,addrsize=absolute,type=label
/// sym    ".size",value=0x0000000D,addrsize=zeropage,type=equate
/// sym    "useLetter",value=0x00000CB3,addrsize=absolute,type=label
/// sym    "makeAHexDigit",value=0x00000CAA,addrsize=absolute,type=label
/// sym    ".size",value=0x00000015,addrsize=zeropage,type=equate
/// sym    "printHex",value=0x00000C95,addrsize=absolute,type=label
/// ```
///
/// We ignore anything that is not a symbol and not of type `label`. The
/// initialiser will throw an error if the file can't be parsed.
struct DebugInfo
{
    init(string: String) throws
    {
		let lines = string.split(separator: "\n")
        let objects = try lines.compactMap
        {
            try Object.objectFrom(string: String($0))
        }
        var labels: [String : [S65Address]] = [:]
        var zeroPage: [S65Address : [String]] = [:]
        var absolute: [S65Address : [String]] = [:]
        objects.forEach
        {
            (object) in
            switch object
            {
            case .label(name: let name, address: let address, size: let size):
				if let addressesSoFar = labels[name]
                {
                    labels[name] = addressesSoFar + [address]
                }
                else
                {
                    labels[name] = [address]
                }
				switch size
                {
                case .zeroPage:
                    if let namesSoFar = zeroPage[address]
                    {
                        zeroPage[address] = namesSoFar + [name]
                    }
                    else
                    {
                        zeroPage[address] = [name]
                    }
                case .absolute:
                    if let namesSoFar = absolute[address]
                    {
                        absolute[address] = namesSoFar + [name]
                    }
                    else
                    {
                        absolute[address] = [name]
                    }
                }
            default:
                break
            }
        }
        labelToAddresses = labels
        zeroPageToLabels = zeroPage
        absoluteToLabels = absolute
    }


    /// Label type
    ///
    /// - zeroPage: A zero page label (one byte address)
    /// - absolute: An absolute label (two byte address)
    enum LabelType: String
    {
        case zeroPage = "zeropage"
        case absolute = "absolute"
    }

    fileprivate enum Object
    {
        case segment
        case file
        case label(name: String, address: S65Address, size: LabelType)
        case equate(name: String, value: S65Address, size: LabelType)

        static func objectFrom(string: String) throws -> Object?
        {
            let parts = string.split(maxSplits: 2)
            {
                $0 == " " || $0 == "\t"
            }
            guard parts.count == 2 else { return nil } // Eliminate blank lines
            switch parts[0]
            {
            case "segment", "file":
                return nil
            case "sym":
                let nameAndRest = parts[1].split(separator: "\"", maxSplits: 2)
                guard nameAndRest.count == 2 else { throw Error.failedToParseName(string) }
                let name = nameAndRest[0]
                let properties = try parseProperties(subString: nameAndRest[1])
                guard let type = properties["type"] else { throw Error.invalidSymbolType(string) }
                guard type == "label" else { return nil } // Ignore if not a label
                guard let sizeString = properties["addrsize"]
                    else { throw Error.symbolWithoutSize(string) }
                guard let size = LabelType(rawValue: sizeString)
                    else { throw Error.invalidAddressSize(string) }
                guard let valueString = properties["value"]
                    else { throw Error.symbolWithoutValue(string) }
                guard let value = addressFromCHex(valueString)
                    else { throw Error.invalidHexNumber(string) }
                return .label(name: String(name), address: value, size: size)
            default:
                throw Error.unknownDebugObject(string)
            }
        }

        private static func parseProperties(subString: Substring) throws -> [String : String]
        {
			let propertyPairs = subString.split(separator: ",")
            return try propertyPairs.reduce(into: [:])
            {
                (result, pair) in
                let keyAndValue = pair.split(separator: "=", maxSplits: 2)
                guard keyAndValue.count == 2 else { throw Error.invalidKeyValuePair(String(pair)) }
                result[String(keyAndValue[0])] = String(keyAndValue[1])
            }
        }

        private static func addressFromCHex(_ string: String) -> S65Address?
        {
            guard string.hasPrefix("0x") else { return nil }
            let conversionString = string.suffix(from: string.index(string.startIndex, offsetBy: 2))
            return S65Address(conversionString, radix: 16)
        }
    }

    enum Error: Swift.Error
    {
        case unknownDebugObject(String)
        case failedToParseName(String)
        case invalidKeyValuePair(String)
        case invalidSymbolType(String)
        case symbolWithoutSize(String)
        case symbolWithoutValue(String)
        case invalidAddressSize(String)
        case invalidHexNumber(String)
    }

    private let labelToAddresses: [String : [S65Address]]
    private let zeroPageToLabels: [S65Address: [String]]
    private let absoluteToLabels: [S65Address: [String]]

    /// Get a list of the addresses defined for the label index.
    ///
    /// - Parameter index: The label for which to get the address
    subscript(index: String) -> [S65Address]
	{
        guard let addresses = labelToAddresses[index] else { return [] }
        return addresses
    }

    /// The labels for a particular address
    ///
    /// - Parameters:
    ///   - address: address for which to get labels.
    ///   - type: Filter on zero page or absolute label, if `nil` (the default)
    ///           no filter is applied.
    /// - Returns: The labels defined for the given address
    func labels(address: S65Address, type: LabelType? = nil) -> [String]
    {
        let zpLabels = zeroPageToLabels[address] ?? []
        let absoluteLabels = absoluteToLabels[address] ?? []
        if let type = type
        {
            return type == .absolute ? absoluteLabels : zpLabels
        }
        else
        {
            return zpLabels + absoluteLabels
        }
    }
}
