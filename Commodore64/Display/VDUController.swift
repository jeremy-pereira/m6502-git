//
//  VDUController.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/06/2015.
//
//

import Cocoa
import Swift6502

let reverseBit = 7

class VDUController: NSWindowController
{
    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }

    @IBOutlet weak var vduView: MetalVDUView!
    fileprivate var viewNeedsDisplay = false

    public var keyboard: C64Keyboard?

    override func windowDidLoad()
    {
        super.windowDidLoad()
    }

    var vSyncThread: Thread?

    @IBOutlet var myTouchbar: NSObject!

    @available(OSX 10.12.2, *)
    override func makeTouchBar() -> NSTouchBar?
    {
        let touchBar = myTouchbar

        return touchBar as? NSTouchBar
    }

    @IBAction func runStop(_ sender: AnyObject)
    {
        guard let _ = sender as? NSSegmentedControl
        else
        {
            fatalError("Sender must be a segmented control")
        }
//        emulateKeyPress(key: .CtrlC, shifted: sender.selectedSegment == 0)
    }

    @IBAction func offRevs(_ sender: AnyObject)
    {
        guard let _ = sender as? NSSegmentedControl
            else
        {
            fatalError("Sender must be a segmented control")
        }
//        emulateKeyPress(key: .CtrlR, shifted: sender.selectedSegment == 0)
    }
    
    @IBAction func clearHome(_ sender: AnyObject)
    {
        guard let _ = sender as? NSSegmentedControl
        else
        {
            fatalError("Sender must be a segmented control")
        }
        emulateKeyPress(key: .Home, shifted: sender.selectedSegment == 0)
    }

    private func emulateKeyPress(key: VirtualKeyCode, shifted: Bool = false)
    {
        guard let keyboard = keyboard else { return }
        if shifted
        {
            keyboard.setMatrixForKeyCode(.LeftShift)
        }
        keyboard.setMatrixForKeyCode(key)
        let delay: UInt64 = 100_000_000 // nonoseconds == 0.1 seconds
        let triggerTime = DispatchTime.init(uptimeNanoseconds: DispatchTime.now().uptimeNanoseconds + delay)

        DispatchQueue.main.asyncAfter(deadline: triggerTime)
        {
            keyboard.resetMatrixForKeyCode(key)
            if shifted
            {
                keyboard.resetMatrixForKeyCode(.LeftShift)
            }
        }
    }

    private var syncs = 0
    private var syncStartTime: DispatchTime = DispatchTime.now()

    func vSync()
    {
        if let vSyncWire = vSyncWire
        {
            vSyncWire.value = false
            vSyncWire.value = true
        }

        if syncs == 0
        {
            syncStartTime = DispatchTime.now()
        }
        syncs += 1
        if syncs % (60 * 30) == 0
        {
            let currentTime = DispatchTime.now()
            let nanoSeconds = currentTime.uptimeNanoseconds - syncStartTime.uptimeNanoseconds
            let frequency = (Double(syncs) * 1_000_000_000) / Double(nanoSeconds)
            print("Vertical sync frequency = \(frequency) Hz")
        }
   }

    var geometry: VDUGeometry
    {
        set
        {
            guard geometry.width == 40 || geometry.height == 25
                else { fatalError("Currently we only support the 40 x 25 sceen") }
			vduView.geometry = newValue
        }

        get
        {
            return vduView.geometry
        }
    }
    var vicII: VicII?
    var colourRAM: ColourRAM?

    var vSyncWire: Wire?

    override func keyDown(with theEvent: NSEvent)
    {
        if let keyboard = keyboard
        {
            keyboard.setKeyCode(theEvent.keyCode, modifierFlags: theEvent.modifierFlags)
        }
    }
    override func keyUp(with theEvent: NSEvent)
    {
        if let keyboard = keyboard
        {
            keyboard.resetKeyCode(theEvent.keyCode, modifierFlags: theEvent.modifierFlags)
        }
    }
}

// MARK: VicII access functions

extension VDUController
{
    /// Copy the content of the normal screen (40 x 25 characters) to a pointer
    ///
    /// - Parameter to: Where to copy the screen to
    func copyMemory(to: UnsafeMutableRawPointer)
    {
        if let vicII = vicII
        {
            vicII.copyMemory(to: to)
        }
    }

    func copyVicRegisters(to: UnsafeMutableRawPointer)
    {
        if let vicII = vicII
        {
            vicII.copyRegisters(to: to)
        }
    }

    func copyColourRAM(to: UnsafeMutableRawPointer)
    {
        if let colourRAM = colourRAM
        {
            colourRAM.copy(to: to)
        }
    }

    var characterSize: NSSize { return NSSize(width: 8, height: 8) }
}
