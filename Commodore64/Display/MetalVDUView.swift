//
//  MetalVDUView.swift
//  PET2
//
//  Created by Jeremy Pereira on 22/09/2018.
//

import Cocoa
import MetalKit
import Swift6502

class MetalVDUView: MTKView
{
    private let commandQueue: MTLCommandQueue
    private let computePipelineState: MTLComputePipelineState
    private var vicMemoryBuffer: MTLBuffer?
    private var vicRegisterBuffer: MTLBuffer?
    private var colourRAMBuffer: MTLBuffer?

    required init(coder: NSCoder)
    {
        guard let device = MTLCreateSystemDefaultDevice()
            else { fatalError("Cannot obtain the GPU device") }
        do
        {
            guard let commandQueue = device.makeCommandQueue()
                else { throw MetalVDUView.Error.unexpectedNil("command queue") }
            self.commandQueue = commandQueue
            computePipelineState = try MetalVDUView.registerShaders(device: device)
        }
        catch
        {
            fatalError("\(error)")
        }
        super.init(coder: coder)
        self.device = device
        framebufferOnly = false
    }

    override init(frame frameRect: CGRect, device: MTLDevice?)
    {
        let notNilDevice = device ?? MTLCreateSystemDefaultDevice()!
        do
        {
            guard let commandQueue = notNilDevice.makeCommandQueue()
                else { throw MetalVDUView.Error.unexpectedNil("command queue") }
            self.commandQueue = commandQueue
            computePipelineState = try MetalVDUView.registerShaders(device: notNilDevice)
        }
        catch
        {
            fatalError("\(error)")
        }
        super.init(frame: frameRect, device: notNilDevice)
        framebufferOnly = false
    }

    static func registerShaders(device: MTLDevice) throws -> MTLComputePipelineState
    {
        let library = device.makeDefaultLibrary()!
        guard let kernel = library.makeFunction(name: "petVDURender")
            else { throw MetalVDUView.Error.unexpectedNil("kernel") }

        return try device.makeComputePipelineState(function: kernel)
    }

    private func makeSureBuffersAreAllocated(device: MTLDevice) throws
    {
        if vicMemoryBuffer == nil
        {
            guard let newBuffer = device.makeBuffer(length: VicII.memorySize * MemoryLayout<UInt8>.size,
                                                    options: [])
                else { throw MetalVDUView.Error.unexpectedNil("command encoder") }
            vicMemoryBuffer = newBuffer
        }
		if vicRegisterBuffer == nil
        {
            guard let newBuffer = device.makeBuffer(length: VicII.registerCount * MemoryLayout<UInt8>.size, options: [])
                else { throw MetalVDUView.Error.unexpectedNil("command encoder") }
            vicRegisterBuffer = newBuffer
        }
        if colourRAMBuffer == nil
        {
            guard let newBuffer = device.makeBuffer(length: ColourRAM.size * MemoryLayout<UInt8>.size, options: [])
                else { throw MetalVDUView.Error.unexpectedNil("command encoder") }
            colourRAMBuffer = newBuffer
        }
    }

    override func draw(_ dirtyRect: NSRect)
    {
    	controller.vSync()
       	super.draw(dirtyRect)

        guard let drawable = currentDrawable, let device = device
            else { return }
        do
        {
            guard let commandBuffer = commandQueue.makeCommandBuffer()
                else { throw MetalVDUView.Error.unexpectedNil("command buffer") }
            guard let commandEncoder = commandBuffer.makeComputeCommandEncoder()
                else { throw MetalVDUView.Error.unexpectedNil("command encoder") }
            commandEncoder.setComputePipelineState(computePipelineState)
            commandEncoder.setTexture(drawable.texture, index: 0)

            try makeSureBuffersAreAllocated(device: device)
            // We need to pass the bitmaps for the characters to display to
            // the shader
            controller.copyMemory(to: vicMemoryBuffer!.contents())
			commandEncoder.setBuffer(vicMemoryBuffer!, offset: 0, index: 1)

            controller.copyVicRegisters(to: vicRegisterBuffer!.contents())
            commandEncoder.setBuffer(vicRegisterBuffer!, offset: 0, index: 2)

            controller.copyColourRAM(to: colourRAMBuffer!.contents())
            commandEncoder.setBuffer(colourRAMBuffer!, offset: 0, index: 3)

            let w = computePipelineState.threadExecutionWidth
            let h = computePipelineState.maxTotalThreadsPerThreadgroup / w
            let threadsPerGroup = MTLSizeMake(w, h, 1)
            commandEncoder.dispatchThreads(MTLSizeMake(drawable.texture.width,
                                                       drawable.texture.height,
                                                       1),
                                           threadsPerThreadgroup: threadsPerGroup)
            commandEncoder.endEncoding()
            commandBuffer.present(currentDrawable!)
            commandBuffer.commit()
        }
        catch
        {
            print("\(error)")
        }
    }

    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!

    @IBOutlet weak var controller: VDUController!

    let borderWidth: CGFloat = 50
    let borderHeight: CGFloat = 50
    /// The geometry of the view i.e. number of rows and columns
    var geometry: VDUGeometry = VDUGeometry.defaultGeometry
    {
        didSet
        {
            let widthInC64Pixels = controller.characterSize.width * CGFloat(geometry.width) + borderWidth
            let heightInC64Pixels = controller.characterSize.height * CGFloat(geometry.height) + borderHeight
            self.widthConstraint.constant = 2 * widthInC64Pixels
            self.heightConstraint.constant = 2 * heightInC64Pixels
        }
    }
}

extension MetalVDUView
{
    public enum Error: Swift.Error
    {
        case createVertexBufferFailed
        case unexpectedNil(String)
    }
}

