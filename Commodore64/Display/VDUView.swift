//
//  VDUView.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/06/2015.
//
//

import Cocoa
import Swift6502

struct VDUGeometry
{
    let width: Int
    let height: Int

    /// The default geometry of the VSDU screen in characters
    static let defaultGeometry = VDUGeometry(width: 40, height: 25)
}

struct VDUAttribute: OptionSet
{
    let rawValue: Int

    static let Reverse = VDUAttribute(rawValue: 1)
}


/// The delegate is what actually does the drawing. This makes it easy to swap
/// in different methods of drawing e.g. using bitmaps for characters to get an 
/// authentic look.
protocol VDUViewDelegate
{

    /// The background colour of characters. This is usually black.
    var backgroundColour:  NSColor { get set }
    /// The size of a character on screen.
    var characterSize: NSSize { get }

    /// Switches bewtween the graphics and lower case character sets.
    ///
    /// - Parameter lowerCase: If true, uses lower case, if false, uses graphics.
    func setCharacterSet(lowerCase: Bool)


    /// Get the character set in one go as a bit map. Thecharacters are arranged
    /// sequentially top row of bits to bottom row of bits
    var characterSetBitmap: [UInt8] { get }
}
