//
//  VDU.metal
//  PET2
//
//  Created by Jeremy Pereira on 22/09/2018.
//

#include <metal_stdlib>
using namespace metal;

struct Geometry
{
    uint2 charDims;
    uint2 screenDims;
};

constant struct Geometry pet40Geometry = { uint2(8, 8), uint2(40, 25) };
constant uint2 borderDims = uint2(50, 50);		// (width, height)

constant int memoryPtrIndex = 0x18;				// Vic memory pointer register
constant uint8_t videoMatrixMask = 0xf0;		// Mask for the Video matrix pointer
constant uint8_t characterBufferMask = 0x0e;    // Mask for the character buffer pointer
constant uint8_t bitmapMask = 0x08;    			// Mask for the bitmap pointer
constant int control1Index = 0x11;              // Control1 register
constant uint8_t control1ECMMask = 0x40;        // The bit in cotrol1 that selects
                                                // extended colour mode
constant uint8_t bitmapModeMask = 0x20;			// Controls if bitmap mode is on.
constant int control2Index = 0x16;				// Control2 register
//constant uint8_t control2CSelMask = 0x08;     // The bit in cotrol2 that selects
//                                              // Something
constant uint8_t control2CMCMMask = 0x10;       // The bit in cotrol2 that selects
                                                // multicolout mode
constant int borderColourIndex = 0x20;			// Bordser colour register
constant int backgroundColour0Index = 0x21;		// Backgoround colour 0 register
constant int backgroundColour1Index = 0x22;     // Backgoround colour 1 register
constant int backgroundColour2Index = 0x23;     // Backgoround colour 2 register
//constant int backgroundColour3Index = 0x24;     // Backgoround colour 3 register

constant float3 colourPalette[16] =
{
    float3(0, 0, 0) / float3(255, 255, 255),		// Black
    float3(255, 255, 255) / float3(255, 255, 255),	// White
    float3(137, 78, 67) / float3(255, 255, 255),	// Red allegedly
    float3(146, 195, 203) / float3(255, 255, 255),	// Cyan

    float3(138, 87, 177) / float3(255, 255, 255),	// Purple
    float3(128, 174, 89) / float3(255, 255, 255),	// Green
    float3(69, 62, 164) / float3(255, 255, 255),	// Dark blue allegedly
    float3(215, 222, 137) / float3(255, 255, 255),	// Yellowish

    float3(146, 56, 106) / float3(255, 255, 255),	// Orange, you must be joking
    float3(100, 82, 23) / float3(255, 255, 255),	// Brown
    float3(184, 132, 122) / float3(255, 255, 255),	// Light red
    float3(96, 96, 96) / float3(255, 255, 255),		// Dark grey

    float3(138, 138, 138) / float3(255, 255, 255),	// Grey
    float3(191, 233, 155) / float3(255, 255, 255),	// Light green
    float3(131, 125, 216) / float3(255, 255, 255),  // Light blue allegedly
    float3(179, 179, 179) / float3(255, 255, 255),  // Light grey
};

void renderBorder(texture2d<float, access::write> output, uint8_t colourIndex, uint2 gid)
{
    float3 colour = colourPalette[colourIndex];
    output.write(float4(colour, 1), gid);
}

void standardCharacterRender(texture2d<float, access::write> output,
                             constant uint8_t *characterMaps,
                             const uint2 c64Pixel,
                             const uint charStartIndex,
                             const uint8_t backgroundColour,
                             const uint8_t foregroundColour,
                             const uint2 gid
                             )
{
    uint2 bitIndexes = c64Pixel % pet40Geometry.charDims;
    bool isOn = (characterMaps[charStartIndex + bitIndexes.y] & (0x80 >> bitIndexes.x)) != 0;

    float3 colour = isOn ? (colourPalette[foregroundColour]) : (colourPalette[backgroundColour]);
    output.write(float4(colour, 1), gid);
}

void multiColourRender(texture2d<float, access::write> output,
                       constant uint8_t *characterMaps,
                       const uint2 c64Pixel,
                       const uint charStartIndex,
                       uchar4 colours,
                       const uint2 gid
                       )
{
    uint2 bitIndexes = c64Pixel % pet40Geometry.charDims;
    bitIndexes.x &= 0xfe;				// The colour is made from two bits of the char
    // This gets the two bits specified by x and x + 1 in the lowest two bits
    // of a byte.
    uint8_t colourIndex = (characterMaps[charStartIndex + bitIndexes.y] >> (6 - bitIndexes.x)) & 3;
    float3 colour = colourPalette[colours[colourIndex]];
    output.write(float4(colour, 1), gid);
}

void characterRender(texture2d<float, access::write> output,
                             uint2 c64Pixel,
                             constant uint8_t *screenBuffer,
                             constant uint8_t *characterMaps,
                             constant uint8_t *colourRAM,
                             constant uint8_t *vicRegisters,
                             uint2 gid
                             )
{

    // Calculate the index of the character
    uint2 charPos = c64Pixel / pet40Geometry.charDims;
    uint screenBufferIndex = charPos.x + charPos.y * pet40Geometry.screenDims.x;
    uint8_t foregroundColour = colourRAM[screenBufferIndex] & 0xf;
    uint8_t backgroundColour;
    uint8_t charNum;
    if ((vicRegisters[control1Index] & control1ECMMask) == 0)
    {
        backgroundColour = vicRegisters[backgroundColour0Index] & 0xf;
        charNum = screenBuffer[screenBufferIndex];
    }
    else
    {
        // In ECM the top two bits of the character tell us what the background
        // colour is.
        charNum = screenBuffer[screenBufferIndex] & 0x3f;
        uint8_t colourIndex = screenBuffer[screenBufferIndex] >> 6;
        backgroundColour = vicRegisters[backgroundColour0Index + colourIndex];
    }
    uint charStartIndex = charNum * pet40Geometry.charDims.y;
    // charStartIndex now points at the byte of the first line of the character

    if ((vicRegisters[control2Index] & control2CMCMMask) != 0)
    {
		if ((foregroundColour & 0x08) == 0)
        {
            standardCharacterRender(output, characterMaps, c64Pixel, charStartIndex,
                                    backgroundColour, foregroundColour, gid);
        }
        else
        {
            uchar4 colours;
            colours[0] = backgroundColour;
            colours[1] = vicRegisters[backgroundColour1Index] & 0xf;
            colours[2] = vicRegisters[backgroundColour2Index] & 0xf;
            colours[3] = foregroundColour & 0x7;
            multiColourRender(output, characterMaps, c64Pixel, charStartIndex,
                              colours, gid);

        }
    }
    else
    {
        standardCharacterRender(output, characterMaps, c64Pixel, charStartIndex,
                                backgroundColour, foregroundColour, gid);
    }
}

void bitmapRender(texture2d<float, access::write> output,constant
                  uint8_t* colourMatrix, constant uint8_t * bitmap,
                  uint2 c64Pixel, uint2 gid)
{
    // First work out whether the bit is on or off. Which way it is will
    // determine whether we use the uppoer or lower four bits of the colour
    // matrix.
    int linearBit = (c64Pixel[1] * pet40Geometry.screenDims[0] * pet40Geometry.charDims[0] + c64Pixel[0]);
    int byteNumber = linearBit / 8;
    int bitShift = linearBit % 8;
    int colourshift = (bitmap[byteNumber] & (0x80 >> bitShift)) != 0 ? 4 : 0;
    // Now figure out the value in the colour matrix.
    uint2 colourPos = c64Pixel / pet40Geometry.charDims;
    uint colourBufferIndex = colourPos.x + colourPos.y * pet40Geometry.screenDims.x;
    uint theColour = (colourMatrix[colourBufferIndex] >> colourshift) & 0xf;
    // Finally, draw the pixel
    float3 colour = colourPalette[theColour];
    output.write(float4(colour, 1), gid);
}

kernel void petVDURender(texture2d<float, access::write> output [[texture(0)]],
                     	constant uint8_t *vicMemory [[buffer(1)]],
                        constant uint8_t *vicRegisters [[buffer(2)]],
                        constant uint8_t *colourRAM [[buffer(3)]],
                     	uint2 gid [[thread_position_in_grid]])
{
    uint2 screenDims = uint2(output.get_width(), output.get_height());
    uint2 petPixelDims = pet40Geometry.charDims * pet40Geometry.screenDims + 2 * borderDims;
    uint2 c64Pixel = gid * petPixelDims / screenDims;

    constant uint8_t* screenBuffer = vicMemory + (((int)(vicRegisters[memoryPtrIndex]) & videoMatrixMask) << 6);
    constant uint8_t* characterMaps = vicMemory + (((int)(vicRegisters[memoryPtrIndex]) & characterBufferMask) << 10);

    // First check if we are in the border

    if (c64Pixel[0] < borderDims[0]
        || c64Pixel[0] >= petPixelDims[0] - borderDims[0]
        || c64Pixel[1] < borderDims[1]
        || c64Pixel[1] >= petPixelDims[1] - borderDims[1])
    {
        renderBorder(output, vicRegisters[borderColourIndex], gid);
    }
    else
    {
        c64Pixel -= borderDims; // Adjust for the non border

        if ((vicRegisters[control1Index] & bitmapModeMask) != 0)
        {
            constant uint8_t* bitmapPointer = vicMemory + (((int)(vicRegisters[memoryPtrIndex]) & bitmapMask) << 10);
            bitmapRender(output, screenBuffer, bitmapPointer, c64Pixel, gid);
        }
        else
        {
            characterRender(output, c64Pixel, screenBuffer, characterMaps,
                            colourRAM, vicRegisters, gid);
        }
    }
}
