//
//  AppDelegate.swift
//  Commodore64
//
//  Created by Jeremy Pereira on 29/09/2018.
//

import Cocoa
import Swift6502

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{
    var fpController: FPWindowController!

    func applicationDidFinishLaunching(_ aNotification: Notification)
    {
        Logger.setLevel(.debug, forName: "Swift6502.CommodoreKeyboard")
        Logger.setLevel(.debug, forName: "Swift6502.VicII")
        //Logger.setLevel(.debug, forName: "Swift6502.CIA6526")
        fpController = FPWindowController(windowNibName: "FPWindowController")
        _ = fpController.window
    }

    func applicationWillTerminate(_ aNotification: Notification)
    {
        // Insert code here to tear down your application
    }

    var debugInfo: DebugInfo?

    @IBAction func openFile(_ sender: AnyObject?)
    {
        let openFile = NSOpenPanel()
        openFile.allowsMultipleSelection = false
        openFile.canChooseDirectories = false
        openFile.canChooseFiles = true
        openFile.title = "Load Program"
        openFile.message = "Pick a file to load"
        openFile.prompt = "Load"
        openFile.allowedFileTypes = ["bin", "prg"]
        openFile.allowsOtherFileTypes = true

        openFile.beginSheetModal(for: fpController.window!)
        {
            [weak self] response in
            switch response
            {
            case NSApplication.ModalResponse.OK:
                let urls = openFile.urls
                do
                {
                    // Open the program
                    let programUrl = urls[0]
                    try self?.loadProgram(url: programUrl)
                    // Save to the recently used menu
                    NSDocumentController.shared.noteNewRecentDocumentURL(programUrl)
                }
                catch
                {
                    self?.fpController.window?.presentError(error)
                }
            case NSApplication.ModalResponse.cancel:
                break
            default:
                fatalError("Invalid open panel response")
            }
        }
    }

    private func loadProgram(url: URL) throws
    {
        let data = try Data(contentsOf: url)
		let program = data.withUnsafeBytes
		{
			(bytes: UnsafeRawBufferPointer) in
			return Array(bytes)
		}
        // Try to load the program and debug symbols
        try fpController.load(program: program)
    }

    @IBAction func selectDebugFile(_ sender: AnyObject?)
    {
        let openFile = NSOpenPanel()
        openFile.allowsMultipleSelection = false
        openFile.canChooseDirectories = false
        openFile.canChooseFiles = true
        openFile.title = "Load Debug Info"
        openFile.message = "Pick a debug file to load"
        openFile.prompt = "Load"
        openFile.allowedFileTypes = ["dbg"]
        openFile.allowsOtherFileTypes = true

        openFile.beginSheetModal(for: fpController.window!)
        {
            [weak self] response in
            switch response
            {
            case NSApplication.ModalResponse.OK:
                let urls = openFile.urls
                do
                {
                    // Open the program
                    let debugUrl = urls[0]
                    let debugString = try String(contentsOf: debugUrl, encoding: String.Encoding.utf8)
                    let debugInfo = try DebugInfo(string: debugString)
                    self?.fpController.load(debugInfo: debugInfo)
                }
                catch
                {
                    self?.fpController.window?.presentError(error)
                }
            case NSApplication.ModalResponse.cancel:
                break
            default:
                fatalError("Invalid open panel response")
            }
        }
    }

    func application(_ sender: NSApplication, openFile fileName: String) -> Bool
    {
        let ret: Bool
        let url = URL(fileURLWithPath: fileName)
        do
        {
			try loadProgram(url: url)
            ret = true
        }
        catch
        {
            fpController.window?.presentError(error)
            ret = false
        }
        return ret
    }
}

