//
//  FPWindowController.swift
//  M6502
//
//  Created by Jeremy Pereira on 18/12/2016.
//
//

import Cocoa
import M65xxUI

private let characters40x25 = VDUGeometry(width: 40, height: 25)


class FPWindowController: NSWindowController
{
    private var theC64: Commodore64!
    private var cpuController: CPUController?
    private var frontPanelControllers: [OperationUpdatable] = []
    private var vduController: VDUController!


    @IBOutlet var frontPanel: FrontPanelView!
    @IBOutlet var myTouchbar: NSObject!

    override func windowDidLoad()
    {
        super.windowDidLoad()

        guard let window = self.window
        else
        {
            return
        }
        vduController = VDUController(windowNibName: "VDUController")
        _ = vduController.window

        buildCommodore64()
        vduController.keyboard = theC64.keyboard

        let views = frontPanelControllers.map { controller in return controller.view }
        frontPanel.views = views
        let contentRect = NSRect(origin: window.frame.origin, size: self.frontPanel.frame.size)
        let frameRect = window.frameRect(forContentRect: contentRect)
		window.setFrame(frameRect, display: true)
        window.makeKeyAndOrderFront(self)
    }

    @available(OSX 10.12.2, *)
    override func makeTouchBar() -> NSTouchBar?
    {
        let touchBar = myTouchbar

        return touchBar as? NSTouchBar
    }

    private func buildCommodore64()
    {
        vduController.geometry = characters40x25

        do
        {
            theC64 = try Commodore64(vduController: vduController)
        }
        catch
        {
            fatalError("Failed to build the Commodore 64, reason: \(error)")
        }
        cpuController = CPUController(cpu: theC64.cpu, uberController: self)
        frontPanelControllers.append(cpuController!)
    }
    

    @IBAction func startCPU(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.startCPU(sender)
        }
    }

    @IBAction func stopCPU(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.stopCPU(sender)
        }
    }

    @IBAction func goAtFullSpeed(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.goAtFullSpeed(sender)
        }
    }

    @IBAction func goAtFullUpdates(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.goAtFullSpeed(sender)
        }
    }

    func load(program: [UInt8]) throws
    {
        cpuController?.stopCPU
        {
            [weak self] wasRunning in
            guard let controller = self else { return }
            do
            {
                try controller.theC64.loadProgram(bytes: program)
            }
            catch
            {
				controller.frontPanel.presentError(error)
            }
            if wasRunning
            {
                // We are still in the middle of stopping, so start again by 
                // dispatching to the main queue.
                DispatchQueue.main.async
                {
                    controller.cpuController?.startCPU()
                }
            }
        }
    }

    func load(debugInfo: DebugInfo)
    {
        cpuController?.stopCPU
        {
            [weak self] wasRunning in
            guard let controller = self else { return }
            controller.theC64.load(debugInfo: debugInfo)
            if wasRunning
            {
                // We are still in the middle of stopping, so start again by
                // dispatching to the main queue.
                DispatchQueue.main.async
                    {
                        controller.cpuController?.startCPU()
                }
            }
        }
    }
}

extension FPWindowController: OperationUpdatable
{
    func update(operation: ExecutionOperation)
    {
        DispatchQueue.main.async
        {
            for controller in self.frontPanelControllers
            {
                controller.update(operation: operation)
            }
        }
    }

    var view: NSView
    {
        return frontPanel
    }


}
