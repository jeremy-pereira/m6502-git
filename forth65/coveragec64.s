;
; This is a copy of the coverage tests for the 6502. The main difference is 
; That we are aware of the IO port, which on the C64 is used to bank switch the
; memory. The IO port is accessed through zp locations 0 and 1, so we have moved
; the success flag to location 2.
;
; The point is to exercise every single opcode except EMU 	scFail.  If jumps go wrong EMU 	scFail is used
; to signal failure
;
		.include "defs.s"
		
		ORIG      		= $0200     ; program start.
		ZPLOC	  		= $14		; A zero page location
		successFlag		= $02       ; Where we put the success flag
		altBasePage		= $4000		; Page we will use for testing relocatable
									; base page addressing
		allRAMSelect    = $00		; Selects RAM on all banks as long as there
									; is no cartridge
		portDDR			= $07		; Make sure the RAM select are outputs
		
		.ORG   ORIG
;-------------------------------------------------------------------------------
; First set the IO port to all RAM. This is necessary because we will be setting
; our own interrupt vector later. Before doing that, we copy the reset vector
; into RAM from ROM by using the shadowing feature of the Commodore 64. 
; 
		LDA		resetVector			; Get the reset vector low byte
		STA     resetVector			; store to shadow RAM
		LDA		resetVector+1		; Get the reset vector high byte
		STA     resetVector+1		; store to shadow RAM
		
		LDA		#allRAMSelect		; The bit pattern to switch out the ROM and IO
		STA		$01					; Store in the IO port
		LDA		#portDDR			; Direction for IO port
		STA		$00				
		
		LDA		#$DE				; Put a flag at the bottom or RAM which 
		STA		successFlag+1		; is cleared only if the tests work
		LDA		#$AD				
		STA		successFlag				
;-------------------------------------------------------------------------------
;		Set up the stack
;
START:	LDX		#$FF
		LDY		#01
		TYS							; This is the high byte of SP
		TXS							; The low byte of SP		
		JSR		RESETCPU			; MAke sure the rest of the CPU is tidy
;-------------------------------------------------------------------------------
;	BRK
;
T00:
		LDA		#$0				; Clear the interrupted flag
		STA		interrupted
;
;		Set the interrupt vector
;
		LDA		#<TestBRK
		STA		interruptVector
		LDA		#>TestBRK
		STA		interruptVector+1
;
;		Preserve the current stack pointer to make sure it is still right at
;		the end
;
		SEE						; Make sure we are in the right mode
		LDY		#$1				; and on the right page
		TYS
		TSX						; Save the stack pointer
		STX		ZPLOC
		LDX		#$AA			; Put a random number in X to trigger a fail on 
								; stack test if returning we skip the TSX
;
;	We are ready to go...
;
		BRK
		TXS						; BRK padding will break stack if executed
;
;	Check the stack pointer is as it was before
;
		TSX
		CPX		ZPLOC
		BEQ		T00_checkFlag
		EMU 	scFail	
T00_checkFlag:
		LDA		interrupted
		CMP		#$2
		BEQ		T00_success
		EMU 	scFail	
T00_success:

;-------------------------------------------------------------------------------
;		ORA(bp,X)
;
		LDA		#<ABYTE				; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>ABYTE
		STA		ZPLOC+1
		LDX		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		ORA		(ZPLOC-2,X)
		CMP		#$FA				; Test against the expected value
		BEQ		CLETEST	
		EMU		scFail
;-------------------------------------------------------------------------------
; Test CLE and SEE
;
CLETEST:
		TSY							; make sure we are still on p1
		CPY		#$01
		BEQ		CLEOK
		EMU 	scFail
CLEOK:	CLE
		LDX		#$00				; Clearing E means that the stackk is 16 bit, see if 
		TXS							; it extends in to the zero page
		PHA
		PHA
		TSY							; should be 0	
		BEQ		CLEFIX				; restore the stack to 6502 mode
		EMU 	scFail
CLEFIX:	SEE
		LDX		#$FF
		LDY		#$1
		TXS
		TYS	
;-------------------------------------------------------------------------------
;  TSB bp
;
TEST04:	LDA		#$F0				; Set up the base page operand
		STA		ZPLOC
		LDA		#$AA				; The A operand
		LDX		#$0					; make sure negative flag is clear, Z set
		.BYTE  TSB_bp, ZPLOC			; Perform the operation
		BPL		T04MINUS			; negative should be unaffected
		EMU 	scFail
T04MINUS:
		BNE		T04NONZERO			; zeros should not be set
		EMU 	scFail
T04NONZERO:
		LDA		#$FA				; Check the result is right
		CMP		ZPLOC
		BEQ		T04SUCCESS
		EMU 	scFail
T04SUCCESS:
;-------------------------------------------------------------------------------
; ORA bp
;
T05:
		LDA		#$F0				; Load up the first operand
		STA		ZPLOC				;
		LDA		#$AA				; and the second operand
		ORA		ZPLOC				; Do the or
		BNE		T05ZSET				; Test the N and Z flags are right
		EMU 	scFail
T05ZSET:
		BMI		T05NSET
		EMU 	scFail
T05NSET:	
		CMP		#$FA				; Test the result is right
		BEQ		T05SUCCESS
		EMU 	scFail	
T05SUCCESS:
;-------------------------------------------------------------------------------
;	ASL bp
;
T06:
		LDA		#$AA				; An appropriate value for shifting
		STA		ZPLOC
		ASL		ZPLOC
		BCS		T06CARRY			; Make sure the carry is set
		EMU 	scFail
T06CARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T06V1CORRECT
		EMU 	scFail
T06V1CORRECT:
		ASL		ZPLOC				; Do it again	
		BCC		T06CARRY2			; Make sure the carry is clear
		EMU 	scFail
T06CARRY2:
		LDA		#$A8				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T06SUCCESS
		EMU 	scFail
T06SUCCESS:
;-------------------------------------------------------------------------------
;	RMB bp (do all of them)
;
T07:
		LDA		#$FF				; Test value with all bits set
		STA		ZPLOC
		.BYTE	RMB0, ZPLOC			; Bit 0
		LDA		#$FE				; Make sure only bit 0 is reset
		CMP		ZPLOC	
		BEQ		T0700SUCCESS
		EMU 	scFail
T0700SUCCESS:
		.BYTE	RMB1, ZPLOC			; Bit 1
		LDA		#$FC				; Make sure only bit 1 is reset
		CMP		ZPLOC	
		BEQ		T0701SUCCESS
		EMU 	scFail
T0701SUCCESS:
		.BYTE	RMB2, ZPLOC			; Bit 2
		LDA		#$F8				; Make sure only bit 2 is reset
		CMP		ZPLOC	
		BEQ		T0702SUCCESS
		EMU 	scFail
T0702SUCCESS:
		.BYTE	RMB3, ZPLOC			; Bit 3
		LDA		#$F0				; Make sure only bit 3 is reset
		CMP		ZPLOC	
		BEQ		T0703SUCCESS
		EMU 	scFail
T0703SUCCESS:
		.BYTE	RMB4, ZPLOC			; Bit 4
		LDA		#$E0				; Make sure only bit 4 is reset
		CMP		ZPLOC	
		BEQ		T0704SUCCESS
		EMU 	scFail
T0704SUCCESS:
		.BYTE	RMB5, ZPLOC			; Bit 5
		LDA		#$C0				; Make sure only bit 5 is reset
		CMP		ZPLOC	
		BEQ		T0705SUCCESS
		EMU 	scFail
T0705SUCCESS:
		.BYTE	RMB6, ZPLOC			; Bit 6
		LDA		#$80				; Make sure only bit 6 is reset
		CMP		ZPLOC	
		BEQ		T0706SUCCESS
		EMU 	scFail
T0706SUCCESS:
		.BYTE	RMB7, ZPLOC			; Bit 7
		LDA		#$00				; Make sure only bit 7 is reset
		CMP		ZPLOC	
		BEQ		T07SUCCESS
		EMU 	scFail
T07SUCCESS:
;-------------------------------------------------------------------------------
;  test PHP
;
T08:
		JSR		RESETCPU			; Get a known state E and I should already
									; be 1 if the test runs from reset
		PHP							; Push the status flags
		PLA							; Pop the top of the stack to A
		AND		#$DB				; Reset E and I so the comparison ignores them
		CMP		#$02				; Everything should be 0 except Z
		BEQ		T08SUCCESS
		EMU 	scFail
T08SUCCESS:
;-------------------------------------------------------------------------------
;  ORA immediate
;
T09:
		LDA		#$F0				; Load up the first operand
		ORA		#$AA				; Do the or
		BNE		T09ZSET				; Test the N and Z flags are right
		EMU 	scFail
T09ZSET:
		BMI		T09NSET
		EMU 	scFail
T09NSET:	
		CMP		#$FA				; Test the result is right
		BEQ		T09SUCCESS
		EMU 	scFail	
T09SUCCESS:
;-------------------------------------------------------------------------------
;  ASL A
;
T0A:
		LDA		#$AA				; An appropriate value for shifting
		ASL		A
		BCS		T0ACARRY			; Make sure the carry is set
		EMU 	scFail
T0ACARRY:
		CMP		#$54				; Make sure the value is correct
		BEQ		T0AV1CORRECT
		EMU 	scFail
T0AV1CORRECT:
		ASL		A					; Do it again	
		BCC		T0ACARRY2			; Make sure the carry is clear
		EMU 	scFail
T0ACARRY2:
		CMP		#$A8				; Make sure the value is correct
		BEQ		T0ASUCCESS
		EMU 	scFail
T0ASUCCESS:
;-------------------------------------------------------------------------------
;  (0B) TSY covered by CLE/SEE test 
;-------------------------------------------------------------------------------
;  TSB abs
T0C:	LDA		#$F0				; Set up the base page operand
		STA		ABSLOC
		LDA		#$AA				; The A operand
		LDX		#$0					; make sure negative flag is clear, Z set
		.BYTE 	TSB_abs 
		.WORD	ABSLOC				; Perform the operation
		BPL		T0CMINUS			; negative should be unaffected
		EMU 	scFail
T0CMINUS:
		BNE		T0CNONZERO			; zeros should not be set
		EMU 	scFail
T0CNONZERO:
		LDA		#$FA				; Check the result is right
		CMP		ABSLOC
		BEQ		T0CSUCCESS
		EMU 	scFail
T0CSUCCESS:
;-------------------------------------------------------------------------------
; ORA abs
;
T0D:
		LDA		#$F0				; Load up the first operand
		STA		ABSLOC				;
		LDA		#$AA				; and the second operand
		ORA		ABSLOC				; Do the or
		BNE		T0DZSET				; Test the N and Z flags are right
		EMU 	scFail
T0DZSET:
		BMI		T0DNSET
		EMU 	scFail
T0DNSET:	
		CMP		#$FA				; Test the result is right
		BEQ		T0DSUCCESS
		EMU 	scFail	
T0DSUCCESS:
;-------------------------------------------------------------------------------
;	ASL abs
;
T0E:
		LDA		#$AA				; An appropriate value for shifting
		STA		ABSLOC
		ASL		ABSLOC
		BCS		T0ECARRY			; Make sure the carry is set
		EMU 	scFail
T0ECARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T0EV1CORRECT
		EMU 	scFail
T0EV1CORRECT:
		ASL		ABSLOC				; Do it again	
		BCC		T0ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T0ECARRY2:
		LDA		#$A8				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T0ESUCCESS
		EMU 	scFail
T0ESUCCESS:
;-------------------------------------------------------------------------------
;	BBR bp
;
T0F:
	LDA		#$FE					; Bit 0 for a jump, all the others failed
									; jump
	STA		ZPLOC
	.byte	BBR0,ZPLOC,T0F0SUCCESS-*-1	; Bit 0 is not set so branch should happen
	EMU 	scFail
T0F0SUCCESS:
	.byte	BBR1,ZPLOC,T0FFAIL-*-1		; Bit 1 is set so branch should not happen
	.byte	BBR2,ZPLOC,T0FFAIL-*-1		; Bit 1 is set so branch should not happen
	.byte	BBR3,ZPLOC,T0FFAIL-*-1		; Bit 1 is set so branch should not happen
	.byte	BBR4,ZPLOC,T0FFAIL-*-1		; Bit 1 is set so branch should not happen
	.byte	BBR5,ZPLOC,T0FFAIL-*-1		; Bit 1 is set so branch should not happen
	.byte	BBR6,ZPLOC,T0FFAIL-*-1		; Bit 1 is set so branch should not happen
	.byte	BBR7,ZPLOC,T0FFAIL-*-1		; Bit 1 is set so branch should not happen
	CLC
	BCC		T0FSUCCESS
T0FFAIL:
	EMU 	scFail
T0FSUCCESS:
;-------------------------------------------------------------------------------
;  (10) BPL covered bu T0C
;-------------------------------------------------------------------------------
;  ORA (bp),Y
;
T11:
		LDA		#<ABYTE				; Load the zp location with the target address
		SEC							; Offset ZPLOC so Y must be non zero
		SBC		#2					; by -2
		STA		ZPLOC
		LDA		#>ABYTE
		SBC		#0					; In case the first subtract had a carry
		STA		ZPLOC+1
		
		LDY		#2					; The zp loc is 2 too low.
		LDA		#$F0				; The other operand
		ORA		(ZPLOC),Y
		CMP		#$FA				; Test against the expected value
		BEQ		T11SUCCESS			
		EMU 	scFail
T11SUCCESS:
;-------------------------------------------------------------------------------
;  ORA (bp),Z
;
T12:
		LDA		#<ABYTE				; Load the zp location with the target address
		SEC							; Offset ZPLOC so Y must be non zero
		SBC		#2					; by -2
		STA		ZPLOC
		LDA		#>ABYTE
		SBC		#0					; In case the first subtract had a carry
		STA		ZPLOC+1
		
		LDZ_imm $2					; The zp loc is 2 too low.
		LDA		#$F0				; The other operand
		.byte	ORA_bp_ind_z, ZPLOC
		CMP		#$FA				; Test against the expected value
		BEQ		T12SUCCESS			
		EMU 	scFail
T12SUCCESS:
		LDZ_imm $0			; Set Z back to zero for normal 65C02 mode
;-------------------------------------------------------------------------------
;  BPL word rel
;
T13:
		LDA		#$1					; Make sure N is not set
		.byte	BPL_word
		.word	T13SUCCESS-*-2		; Offset of success label
		EMU 	scFail
T13SUCCESS:
;-------------------------------------------------------------------------------
;  TRB bp
;
T14:	LDA		#$F0				; Set up the base page operand
		STA		ZPLOC
		LDA		#$AA				; The A operand
		LDX		#$0					; make sure negative flag is clear, Z set
		.BYTE  TRB_bp, ZPLOC		; Perform the operation
		BPL		T14MINUS			; negative should be unaffected
		EMU 	scFail
T14MINUS:
		BNE		T14NONZERO			; zeros should not be set
		EMU 	scFail
T14NONZERO:
		LDA		#$A0				; Check the result is right
		CMP		ZPLOC
		BEQ		T14SUCCESS
		EMU 	scFail
T14SUCCESS:
;-------------------------------------------------------------------------------
;		ORA bp,X
;
T15:
		LDA		#$AA				; Load the zp location with the target address
		STA		ZPLOC
		LDX		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		ORA		ZPLOC-2,X
		CMP		#$FA				; Test against the expected value
		BEQ		T15Success	
		EMU 	scFail
T15Success:
;-------------------------------------------------------------------------------
;	ASL bp,X
;
T16:
		LDA		#$AA				; An appropriate value for shifting
		STA		ZPLOC
		LDX		#3					; We'll offset from a different zp location
		ASL		ZPLOC-3,X
		BCS		T16CARRY			; Make sure the carry is set
		EMU 	scFail
T16CARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T16V1CORRECT
		EMU 	scFail
T16V1CORRECT:
		ASL		ZPLOC-3,X			; Do it again	
		BCC		T16CARRY2			; Make sure the carry is clear
		EMU 	scFail
T16CARRY2:
		LDA		#$A8				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T16SUCCESS
		EMU 	scFail
T16SUCCESS:
;-------------------------------------------------------------------------------
;		ORA abs,Y
;
T19:
		LDA		#$AA				; Load the zp location with the target address
		STA		ABSLOC
		LDY		#5					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		ORA		ABSLOC-5,Y
		CMP		#$FA				; Test against the expected value
		BEQ		T19Success	
		EMU 	scFail
T19Success:
;-------------------------------------------------------------------------------
;		INC A
;
T1A:
		LDA		#$7E
		INC		A
		BNE		T1ANE1				; Z flag not set
		EMU 	scFail
T1ANE1:
		BPL		T1APL1				; N flag clear
		EMU 	scFail
T1APL1:
		CMP		#$7F				; is the value right?
		BEQ		T1AValue1
		EMU 	scFail
T1AValue1:
		INC		A
		BNE		T1ANE2				; Z flag not set
		EMU 	scFail
T1ANE2:
		BMI		T1AMI1				; N flag clear
		EMU 	scFail
T1AMI1:
		CMP		#$80				; is the value right?
		BEQ		T1ASuccess
		EMU 	scFail
T1ASuccess:		
;-------------------------------------------------------------------------------
;		INZ
;
T1B:
		LDZ_imm	$7E
		.byte	INZ				; Should be 7F now
		BNE		T1BNE1				; Z flag not set
		EMU 	scFail
T1BNE1:
		BPL		T1BPL1				; N flag clear
		EMU 	scFail
T1BPL1:
		CPZ_imm	$7F					; is the value right?
		BEQ		T1BValue1
		EMU 	scFail
T1BValue1:
		.byte 	INZ					; Now 80
		BNE		T1BNE2				; Z flag not set
		EMU 	scFail
T1BNE2:
		BMI		T1BMI1				; N flag clear
		EMU 	scFail
T1BMI1:
		CPZ_imm	$80					; is the value right?
		BEQ		T1BSuccess
		EMU 	scFail
T1BSuccess:
		LDZ_imm	0					; Restore to 65C02 mode
;-------------------------------------------------------------------------------
;  TRB abs
;
T1C:	LDA		#$F0				; Set up the base page operand
		STA		ABSLOC
		LDA		#$AA				; The A operand
		LDX		#$0					; make sure negative flag is clear, Z set
		.BYTE  	TRB_abs
		.WORD	ABSLOC				; Perform the operation
		BPL		T1CMINUS			; negative should be unaffected
		EMU 	scFail
T1CMINUS:
		BNE		T1CNONZERO			; zeros should not be set
		EMU 	scFail
T1CNONZERO:
		LDA		#$A0				; Check the result is right
		CMP		ABSLOC
		BEQ		T1CSUCCESS
		EMU 	scFail
T1CSUCCESS:
;-------------------------------------------------------------------------------
; ORA abs,X
;
T1D:
		LDA		#$F0				; Load up the first operand
		STA		ABSLOC				;
		LDX		#3					; X offset
		LDA		#$AA				; and the second operand
		ORA		ABSLOC-3,X			; Do the or
		BNE		T1DZSET				; Test the N and Z flags are right
		EMU 	scFail
T1DZSET:
		BMI		T1DNSET
		EMU 	scFail
T1DNSET:	
		CMP		#$FA				; Test the result is right
		BEQ		T1DSUCCESS
		EMU 	scFail	
T1DSUCCESS:
;-------------------------------------------------------------------------------
;	ASL abs,X
;
T1E:
		LDA		#$AA				; An appropriate value for shifting
		STA		ABSLOC
		LDX		#4					; Set the X offset
		ASL		ABSLOC-4,X
		BCS		T1ECARRY			; Make sure the carry is set
		EMU 	scFail
T1ECARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T1EV1CORRECT
		EMU 	scFail
T1EV1CORRECT:
		DEX
		DEX
		DEX
		ASL		ABSLOC-1,X
		BCC		T1ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T1ECARRY2:
		LDA		#$A8				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T1ESUCCESS
		EMU 	scFail
T1ESUCCESS:
;-------------------------------------------------------------------------------
;		AND (bp,X)
;
T21:
		LDA		#<ABYTE				; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>ABYTE
		STA		ZPLOC+1
		LDX		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		AND		(ZPLOC-2,X)
		CMP		#$A0				; Test against the expected value
		BEQ		T21Success	
		EMU 	scFail
T21Success:
;-------------------------------------------------------------------------------
;		JSR (abs)
;
T22:
		LDA		#<IncAcc			; Load the subroutine address into the 
		STA		ABSIND				; Location we will jump through
		LDA		#>IncAcc
		STA		ABSIND+1
		LDA		#0					; Set a known value
		.byte	JSR_abs_ind		
		.word	ABSIND				; Hopefully increment
		BNE		T22Success			; A should no longer be zero
		EMU 	scFail
T22Success:
;-------------------------------------------------------------------------------
;		JSR (abs,X)
;
T23:
		LDA		#<IncAcc			; Load the subroutine address into the 
		STA		ABSIND				; Location we will jump through
		LDA		#>IncAcc
		STA		ABSIND+1
		LDA		#0					; Set a known value
		LDX		#2
		.byte	JSR_absx_ind		
		.word	ABSIND-2			; Hopefully increment
		BNE		T23Success			; A should no longer be zero
		EMU 	scFail
T23Success:
;-------------------------------------------------------------------------------
;		BIT bp
;
T24:
		LDA		#$7A				; Set up zp loc
		STA		ZPLOC
		CLV							; Clear the overflow flag (it will be set after)
		LDA		#$FC				; Second operand (sets N)
		BMI		T24NegSet			; Check the flags re correct
		EMU 	scFail
T24NegSet:
		BVC		T24VClear
		EMU 	scFail
T24VClear:
		BIT		ZPLOC
		BPL		T24NOK				; Test the negative bit is clear
		EMU 	scFail
T24NOK:
		BVS		T24VOK				; Test the overflow is clear
		EMU 	scFail
T24VOK:
		BNE		T24ZeroTest			; Check zero is still not set
		EMU 	scFail
T24ZeroTest:
		LDA		#$85				; Should clear the result
		BIT		ZPLOC
		BEQ		T24Success			; Test the result was zero
		EMU 	scFail
T24Success:
;-------------------------------------------------------------------------------
;		AND bp
;
T25:
		LDA		#$AA				; First operand
		STA		ZPLOC
		LDA		#$F0				; The other operand
		AND		ZPLOC
		CMP		#$A0				; Test against the expected value
		BEQ		T25Success	
		EMU 	scFail
T25Success:
;-------------------------------------------------------------------------------
;	ROL bp
;
T26:
		CLC							; Make sure carry is known
		LDA		#$AA				; An appropriate value for shifting
		STA		ZPLOC
		ROL		ZPLOC
		BCS		T26CARRY			; Make sure the carry is set
		EMU 	scFail
T26CARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T26V1CORRECT
		EMU 	scFail
T26V1CORRECT:
		ROL		ZPLOC				; Do it again	
		BCC		T26CARRY2			; Make sure the carry is clear
		EMU 	scFail
T26CARRY2:
		LDA		#$A9				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T26SUCCESS
		EMU 	scFail
T26SUCCESS:
;-------------------------------------------------------------------------------
;   RMB2 covered
;-------------------------------------------------------------------------------
;	PLP
;
T28:
		SEI							; Don't want interrupts for this test
		LDA		#$00				; All flags clear
		PHA							; Put it on the stack
		PLP
;
;	Test that all the flags are clear
;
		BCC		T28CClear
		EMU 	scFail
T28CClear:
		BNE		T28ZClear
		EMU 	scFail
T28ZClear:
		BVC		T28VClear
		EMU 	scFail
T28VClear:
		BPL		T28NClear
		EMU 	scFail
T28NClear:
;
;	Set the flags and repeat the tests
;
		LDA		#$FF
		PHA
		PLP
		CLD							; Don't want decimal for now
;
;	Test that all the flags are clear
;
		BCS		T28CSet
		EMU 	scFail
T28CSet:
		BEQ		T28ZSet
		EMU 	scFail
T28ZSet:
		BVS		T28VSet
		EMU 	scFail
T28VSet:
		BMI		T28NSet
		EMU 	scFail
T28NSet:
T28Success:
;-------------------------------------------------------------------------------
;		AND immediate
;
T29:
		LDA		#$F0				; The other operand
		AND		#$AA
		CMP		#$A0				; Test against the expected value
		BEQ		T29Success	
		EMU 	scFail
T29Success:
;-------------------------------------------------------------------------------
;	ROL A
;
T2A:
		CLC							; Make sure carry is known
		LDA		#$AA				; An appropriate value for shifting
		ROL		A
		BCS		T2ACARRY			; Make sure the carry is set
		EMU 	scFail
T2ACARRY:
		CMP		#$54
		BEQ		T2AV1CORRECT
		EMU 	scFail
T2AV1CORRECT:
		ROL		A					; Do it again	
		BCC		T2ACARRY2			; Make sure the carry is clear
		EMU 	scFail
T2ACARRY2:
		CMP		#$A9
		BEQ		T2ASUCCESS
		EMU 	scFail
T2ASUCCESS:
;-------------------------------------------------------------------------------
;		BIT abs
;
T2C:
		LDA		#$7A				; Set up zp loc
		STA		ABSLOC
		CLV							; Clear the overflow flag (it will be set after)
		LDA		#$FC				; Second operand (sets N)
		BMI		T2CNegSet			; Check the flags re correct
		EMU 	scFail
T2CNegSet:
		BVC		T2CVClear
		EMU 	scFail
T2CVClear:
		BIT		ABSLOC
		BPL		T2CNOK				; Test the negative bit is clear
		EMU 	scFail
T2CNOK:
		BVS		T2CVOK				; Test the overflow is clear
		EMU 	scFail
T2CVOK:
		BNE		T2CZeroTest			; Check zero is still not set
		EMU 	scFail
T2CZeroTest:
		LDA		#$85				; Should clear the result
		BIT		ABSLOC
		BEQ		T2CSuccess			; Test the result was zero
		EMU 	scFail
T2CSuccess:
;-------------------------------------------------------------------------------
;		AND abs
;
T2D:
		LDA		#$AA				; First operand
		STA		ABSLOC
		LDA		#$F0				; The other operand
		AND		ABSLOC
		CMP		#$A0				; Test against the expected value
		BEQ		T2DSuccess	
		EMU 	scFail
T2DSuccess:
;-------------------------------------------------------------------------------
;	ROL abs
;
T2E:
		CLC							; Make sure carry is known
		LDA		#$AA				; An appropriate value for shifting
		STA		ABSLOC
		ROL		ABSLOC
		BCS		T2ECARRY			; Make sure the carry is set
		EMU 	scFail
T2ECARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T2EV1CORRECT
		EMU 	scFail
T2EV1CORRECT:
		ROL		ABSLOC				; Do it again	
		BCC		T2ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T2ECARRY2:
		LDA		#$A9				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T2ESUCCESS
		EMU 	scFail
T2ESUCCESS:
;-------------------------------------------------------------------------------
;		AND (bp),Y
;
T31:
		LDA		#<ABYTE-2				; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>ABYTE
		STA		ZPLOC+1
		LDY		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		AND		(ZPLOC),Y
		CMP		#$A0				; Test against the expected value
		BEQ		T31Success	
		EMU 	scFail
T31Success:
;-------------------------------------------------------------------------------
;		AND (bp),Z
;
T32:
		LDA		#<ABYTE-2				; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>ABYTE
		STA		ZPLOC+1
		LDZ_imm	2						; We'll offset from a different zp location
		LDA		#$F0					; The other operand
		.byte	AND_bp_ind_z, ZPLOC
		CMP		#$A0					; Test against the expected value
		BEQ		T32Success	
		EMU 	scFail
T32Success:	
		LDZ_imm	0						; Reset Z to 6502 mode
;-------------------------------------------------------------------------------
;  BMI word rel
;
T33:
		LDA		#$F1					; Make sure N is set
		.byte	BMI_word
		.word	T33Minus-*-2			; Should branch
		EMU 	scFail
T33Minus:
		LDA		#$1						; N is cleared
		.byte	BMI_word
		.word	T33Fail-*-2				; Should branch
		JMP		T33SUCCESS
T33Fail:
		EMU 	scFail
T33SUCCESS:
;-------------------------------------------------------------------------------
;		BIT bp,X
;
T34:
		LDA		#$7A				; Set up zp loc
		LDX		#$6					; Set up the X index
		STA		ZPLOC
		CLV							; Clear the overflow flag (it will be set after)
		LDA		#$FC				; Second operand (sets N)
		BMI		T34NegSet			; Check the flags re correct
		EMU 	scFail
T34NegSet:
		BVC		T34VClear
		EMU 	scFail
T34VClear:
		.byte	BIT_bpx,ZPLOC-6
		BPL		T34NOK				; Test the negative bit is clear
		EMU 	scFail
T34NOK:
		BVS		T34VOK				; Test the overflow is clear
		EMU 	scFail
T34VOK:
		BNE		T34ZeroTest			; Check zero is still not set
		EMU 	scFail
T34ZeroTest:
		LDA		#$85				; Should clear the result
		.byte	BIT_bpx, ZPLOC-6
		BEQ		T34Success			; Test the result was zero
		EMU 	scFail
T34Success:
;-------------------------------------------------------------------------------
;		AND bp,X
;
T35:
		LDA		#$AA				; First operand
		LDX		#$02
		STA		ZPLOC
		LDA		#$F0				; The other operand
		AND		ZPLOC-2,X
		CMP		#$A0				; Test against the expected value
		BEQ		T35Success	
		EMU 	scFail
T35Success:
;-------------------------------------------------------------------------------
;	ROL bp,X
;
T36:
		CLC							; Make sure carry is known
		LDA		#$AA				; An appropriate value for shifting
		STA		ZPLOC
		LDX		#$01				; Set up the index
		ROL		ZPLOC-1,X
		BCS		T36CARRY			; Make sure the carry is set
		EMU 	scFail
T36CARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T36V1CORRECT
		EMU 	scFail
T36V1CORRECT:
		DEX							; Different X
		ROL		ZPLOC,X				; Do it again	
		BCC		T36CARRY2			; Make sure the carry is clear
		EMU 	scFail
T36CARRY2:
		LDA		#$A9				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T36SUCCESS
		EMU 	scFail
T36SUCCESS:
;-------------------------------------------------------------------------------
;		AND abs,Y
;
T39:
		LDA		#$AA				; First operand
		STA		ABSLOC
		LDA		#$F0				; The other operand
		LDY		#$6
		AND		ABSLOC-6,Y
		CMP		#$A0				; Test against the expected value
		BEQ		T39Success	
		EMU 	scFail
T39Success:
;-------------------------------------------------------------------------------
;		DEC A
;
T3A:
		LDA		#$1					; Initial value
		DEC		A					; first decrement - should be 0
		BEQ		T3A_zero			; Check the result is zero
		EMU 	scFail
T3A_zero:
		BPL		T3A_positive		; Check the result is positive
		EMU 	scFail
T3A_positive:
		DEC		A					; Decrement again, should be -1
		BNE		T3A_notZero			; Check non zero
		EMU 	scFail
T3A_notZero:
		BMI		T3A_negative		; Check less than 0
		EMU 	scFail
T3A_negative:
		CMP		#$FF				; Check is -1
		BEQ		T3A_success
		EMU 	scFail
T3A_success:
;-------------------------------------------------------------------------------
;		DEZ
;
T3B:
		LDZ_imm	$1					; Initial value
		.byte	DEZ					; first decrement - should be 0
		BEQ		T3B_zero			; Check the result is zero
		EMU 	scFail
T3B_zero:
		BPL		T3B_positive		; Check the result is positive
		EMU 	scFail
T3B_positive:
		.byte	DEZ					; Decrement again, should be -1
		BNE		T3B_notZero			; Check non zero
		EMU 	scFail
T3B_notZero:
		BMI		T3B_negative		; Check less than 0
		EMU 	scFail
T3B_negative:
		CPZ_imm	$FF					; Check is -1
		BEQ		T3B_success
		EMU 	scFail
T3B_success:
;-------------------------------------------------------------------------------
;		BIT abs,X
;
T3C:
		LDX		#$02
		LDA		#$7A				; Set up zp loc
		STA		ABSLOC
		CLV							; Clear the overflow flag (it will be set after)
		LDA		#$FC				; Second operand (sets N)
		BMI		T3CNegSet			; Check the flags re correct
		EMU 	scFail
T3CNegSet:
		BVC		T3CVClear
		EMU 	scFail
T3CVClear:
		.byte 	BIT_absx		
		.word	ABSLOC-2
		BPL		T3CNOK				; Test the negative bit is clear
		EMU 	scFail
T3CNOK:
		BVS		T3CVOK				; Test the overflow is clear
		EMU 	scFail
T3CVOK:
		BNE		T3CZeroTest			; Check zero is still not set
		EMU 	scFail
T3CZeroTest:
		LDA		#$85				; Should clear the result
		.byte	BIT_absx		
		.word	ABSLOC-2
		BEQ		T3CSuccess			; Test the result was zero
		EMU 	scFail
T3CSuccess:
;-------------------------------------------------------------------------------
;		AND abs,X
;
T3D:
		LDA		#$AA				; First operand
		STA		ABSLOC
		LDA		#$F0				; The other operand
		LDX		#$6
		AND		ABSLOC-6,X
		CMP		#$A0				; Test against the expected value
		BEQ		T3DSuccess	
		EMU 	scFail
T3DSuccess:
;-------------------------------------------------------------------------------
;	ROL abs,X
;
T3E:
		CLC							; Make sure carry is known
		LDX		#$10
		LDA		#$AA				; An appropriate value for shifting
		STA		ABSLOC
		ROL		ABSLOC-$10,X
		BCS		T3ECARRY			; Make sure the carry is set
		EMU 	scFail
T3ECARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T3EV1CORRECT
		EMU 	scFail
T3EV1CORRECT:
		ROL		ABSLOC-$10,X		; Do it again	
		BCC		T3ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T3ECARRY2:
		LDA		#$A9				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T3ESUCCESS
		EMU 	scFail
T3ESUCCESS:
;-------------------------------------------------------------------------------
;	RTI
;
;	We'll enter a loop that can only be exited if the interrupt routine has run
;	Then after a number of iterations, we simulate an interrupt by calling a
;   "BIOS" routine
;
T40:
		SEI						; Make sure no interrupts during set up
		LDA		#$0				; Clear the interrupted flag
		STA		interrupted
;
;		Set the interrupt vector
;
		LDA		#<TestInterrupt
		STA		interruptVector
		LDA		#>TestInterrupt
		STA		interruptVector+1
;
;		Preserve the current stack pointer to make sure it is still right at
;		the end
;
		SEE						; Make sure we are in the right mode
		LDY		#$1				; and on the right page
		TYS
		TSX						; Save the stack pointer
		STX		ZPLOC
;
;	We are ready to go...
;
		CLI						; Enable interrupts
		LDX		#$00			; Load our countdown timer
		LDY		#$FF			; Y will limit the loop, if it gets to 0 error
T40_loop:
		LDA		interrupted		; Test the flag to see if it has been set
		BNE		T40_loopExit	; Exit the loop if it is
		DEX
		BNE		T40_missInterrupt	; Only simulate interrupt on X = 0
;
;		Check we don't go round the loop indefinitely
;
		DEY
		BNE		T40_noLimit
		EMU 	scFail						; A EMU 	scFail here means the interrupt didn't happen
T40_noLimit:
		EMU		simulateInterrupt
T40_missInterrupt:
		BRA 	T40_loop			
T40_loopExit:
;
;	Check the stack pointer is as it was before
;
		TSX
		CPX		ZPLOC
		BEQ		T40_success
		EMU 	scFail	
T40_success:
		SEI						; Disable interrupts again
;-------------------------------------------------------------------------------
;	Non maskable interrupt
;
;	We'll enter a loop that can only be exited if the NMI routine has run
;	Then after a number of iterations, we simulate an interrupt by calling a
;   "BIOS" routine
;
TNMI:
		SEI						; Make sure no interrupts 
		LDA		#$0				; Clear the interrupted flag
		STA		interrupted
;
;		Set the interrupt vector
;
		LDA		#<TestInterrupt
		STA		nmiVector
		LDA		#>TestInterrupt
		STA		nmiVector+1
;
;		Preserve the current stack pointer to make sure it is still right at
;		the end
;
		SEE						; Make sure we are in the right mode
		LDY		#$1				; and on the right page
		TYS
		TSX						; Save the stack pointer
		STX		ZPLOC
;
;	We are ready to go...
;
		LDX		#$00			; Load our countdown timer
		LDY		#$FF			; Y will limit the loop, if it gets to 0 error
TNMI_loop:
		LDA		interrupted		; Test the flag to see if it has been set
		BNE		TNMI_loopExit	; Exit the loop if it is
		DEX
		BNE		TNMI_missInterrupt	; Only simulate interrupt on X = 0
;
;		Check we don't go round the loop indefinitely
;
		DEY
		BNE		TNMI_noLimit
		EMU 	scFail						; scFail here means the interrupt didn't happen
TNMI_noLimit:
		EMU		pullUpNMI					; NMI is edge triggered, so first 
		EMU		pullDownNMI					; make it high, then make it low
		EMU		pullUpNMI					; make it high again
TNMI_missInterrupt:
		BRA 	TNMI_loop			
TNMI_loopExit:
;
;	Check the stack pointer is as it was before
;
		TSX
		CPX		ZPLOC
		BEQ		TNMI_success
		EMU 	scFail	
TNMI_success:
;-------------------------------------------------------------------------------
;		EOR (bp,X)
;
T41:
		LDA		#<ABYTE				; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>ABYTE
		STA		ZPLOC+1
		LDX		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		EOR		(ZPLOC-2,X)
		BNE		T41_notZero
		EMU 	scFail
T41_notZero:
		BPL		T41_positive
		EMU 	scFail
T41_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T41_success	
		EMU 	scFail
T41_success:
;-------------------------------------------------------------------------------
;		NEG A
;
T42:
		LDA		#$64				; An operand
		.byte	NEG_A				; Negate it
		BNE		T42_notZero			; Check Z flag
		EMU 	scFail
T42_notZero:
		BMI		T42_negative		; Check it is less than 0
		EMU 	scFail
T42_negative:
		CMP		#(0-$64) & $ff		; Make sure it is the right answer
		BEQ		T42_success
		EMU 	scFail
T42_success:
;-------------------------------------------------------------------------------
;  ASR A
;
T43:
		LDA		#$AB				; An appropriate value for shifting
		.byte	ASR_A
		BCS		T43CARRY			; Make sure the carry is set
		EMU 	scFail
T43CARRY:
		CMP		#$D5				; Make sure the value is correct
		BEQ		T43V1CORRECT
		EMU 	scFail
T43V1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		.byte	ASR_A				; Do the shift again	
		BCC		T43CARRY2			; Make sure the carry is clear
		EMU 	scFail
T43CARRY2:
		CMP		#$EA				; Make sure the value is correct
		BEQ		T43SUCCESS
		EMU 	scFail
T43SUCCESS:
;-------------------------------------------------------------------------------
;  ASR bp
;
T44:
		LDA		#$AB				; An appropriate value for shifting
		STA		ZPLOC
		.byte	ASR_bp, ZPLOC
		BCS		T44CARRY			; Make sure the carry is set
		EMU 	scFail
T44CARRY:
		LDA		ZPLOC
		CMP		#$D5				; Make sure the value is correct
		BEQ		T44V1CORRECT
		EMU 	scFail
T44V1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		STA		ZPLOC
		.byte	ASR_bp, ZPLOC
		BCC		T44CARRY2			; Make sure the carry is clear
		EMU 	scFail
T44CARRY2:
		LDA		ZPLOC
		CMP		#$EA				; Make sure the value is correct
		BEQ		T44SUCCESS
		EMU 	scFail
T44SUCCESS:
;-------------------------------------------------------------------------------
;		EOR bp
;
T45:
		LDA		#$AA				; Load the zp location with the operand
		STA		ZPLOC
		LDA		#$F0				; The other operand
		EOR		ZPLOC
		BNE		T45_notZero
		EMU 	scFail
T45_notZero:
		BPL		T45_positive
		EMU 	scFail
T45_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T45_success	
		EMU 	scFail
T45_success:
;-------------------------------------------------------------------------------
;  LSR bp
;
T46:
		LDA		#$AB				; An appropriate value for shifting
		STA		ZPLOC
		LSR		ZPLOC
		BCS		T46CARRY			; Make sure the carry is set
		EMU 	scFail
T46CARRY:
		BPL		T46_positive		; Make sure the N flag is clear
		EMU 	scFail
T46_positive:
		LDA		ZPLOC
		CMP		#$55				; Make sure the value is correct
		BEQ		T46V1CORRECT
		EMU 	scFail
T46V1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		STA		ZPLOC
		LSR		ZPLOC
		BCC		T46CARRY2			; Make sure the carry is clear
		EMU 	scFail
T46CARRY2:
		LDA		ZPLOC
		CMP		#$2A				; Make sure the value is correct
		BEQ		T46SUCCESS
		EMU 	scFail
T46SUCCESS:
;-------------------------------------------------------------------------------
;		EOR imm
;
T49:
		LDA		#$F0				; The other operand
		EOR		#$AA
		BNE		T49_notZero
		EMU 	scFail
T49_notZero:
		BPL		T49_positive
		EMU 	scFail
T49_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T49_success	
		EMU 	scFail
T49_success:
;-------------------------------------------------------------------------------
;  LSR A
;
T4A:
		LDA		#$AB				; An appropriate value for shifting
		LSR		A
		BCS		T4ACARRY			; Make sure the carry is set
		EMU 	scFail
T4ACARRY:
		CMP		#$55				; Make sure the value is correct
		BEQ		T4AV1CORRECT
		EMU 	scFail
T4AV1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		LSR		A				; Do the shift again	
		BCC		T4ACARRY2			; Make sure the carry is clear
		EMU 	scFail
T4ACARRY2:
		CMP		#$2A				; Make sure the value is correct
		BEQ		T4ASUCCESS
		EMU 	scFail
T4ASUCCESS:
;-------------------------------------------------------------------------------
;  TAZ
;
T4B:
		LDZ_imm	$0					; Make sure we have a known state
		LDA		#$AA				; Byte to transfer
		LDX		#$0					; Clear N and set Z
		.byte	TAZ					; Do the transfer
		BMI		T4B_negative		; N should be set
		EMU 	scFail
T4B_negative:
		BNE		T4B_notZ			; Z should be cleared
		EMU 	scFail
T4B_notZ:
		CPZ_imm	$AA					; Make sure the value is right
		BEQ		T4B_success
		EMU 	scFail
T4B_success:
;-------------------------------------------------------------------------------
;		EOR abs
;
T4D:
		LDA		#$AA				; Load the zp location with the operand
		STA		ABSLOC
		LDA		#$F0				; The other operand
		EOR		ABSLOC
		BNE		T4D_notZero
		EMU 	scFail
T4D_notZero:
		BPL		T4D_positive
		EMU 	scFail
T4D_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T4D_success	
		EMU 	scFail
T4D_success:
;-------------------------------------------------------------------------------
;  LSR abs
;
T4E:
		LDA		#$AB				; An appropriate value for shifting
		STA		ABSLOC
		LSR		ABSLOC
		BCS		T4ECARRY			; Make sure the carry is set
		EMU 	scFail
T4ECARRY:
		BPL		T4E_positive		; Make sure the N flag is clear
		EMU 	scFail
T4E_positive:
		LDA		ABSLOC
		CMP		#$55				; Make sure the value is correct
		BEQ		T4EV1CORRECT
		EMU 	scFail
T4EV1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		STA		ABSLOC
		LSR 	ABSLOC
		BCC		T4ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T4ECARRY2:
		LDA		ABSLOC
		CMP		#$2A				; Make sure the value is correct
		BEQ		T4ESUCCESS
		EMU 	scFail
T4ESUCCESS:
;-------------------------------------------------------------------------------
;  BVC
;
T50:
		LDA		#$7A				; Force overflow to be set
		CLC
		ADC		#$10
		BVC		T50_fail
		BRA		T50_noFail
T50_fail:
		EMU 	scFail
T50_noFail:
		LDA		#$01				; Force overflow clear
		CLC	
		ADC		#$10
		BVC		T50_success
		EMU 	scFail
T50_success:
;-------------------------------------------------------------------------------
;		EOR (bp),Y
;
T51:
		LDA		#<(ABYTE-2)			; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>(ABYTE-2)
		STA		ZPLOC+1
		LDY		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		EOR		(ZPLOC),Y
		BNE		T51_notZero
		EMU 	scFail
T51_notZero:
		BPL		T51_positive
		EMU 	scFail
T51_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T51_success	
		EMU 	scFail
T51_success:
;-------------------------------------------------------------------------------
;		EOR (bp),Z
;
T52:
		LDA		#<(ABYTE-2)			; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>(ABYTE-2)
		STA		ZPLOC+1
		LDZ_imm	$2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		.byte	EOR_bp_ind_z, ZPLOC
		BNE		T52_notZero
		EMU 	scFail
T52_notZero:
		BPL		T52_positive
		EMU 	scFail
T52_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T52_success	
		EMU 	scFail
T52_success:
;-------------------------------------------------------------------------------
;  BVC word
;
T53:
		LDA		#$7A				; Force overflow to be set
		CLC
		ADC		#$10
		.byte 	BVC_word		
		.word	T53_fail-*-2
		BRA		T53_noFail
T53_fail:
		EMU 	scFail
T53_noFail:
		LDA		#$01				; Force overflow clear
		CLC	
		ADC		#$10
		.byte	BVC_word		
		.word	T53_success-*-2
		EMU 	scFail
T53_success:
;-------------------------------------------------------------------------------
;  ASR bp,X
;
T54:
		LDA		#$AB				; An appropriate value for shifting
		STA		ZPLOC
		LDX		#$2
		.byte	ASR_bp_x, ZPLOC-2
		BCS		T54CARRY			; Make sure the carry is set
		EMU 	scFail
T54CARRY:
		LDA		ZPLOC
		CMP		#$D5				; Make sure the value is correct
		BEQ		T54V1CORRECT
		EMU 	scFail
T54V1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		STA		ZPLOC
		.byte	ASR_bp_x, ZPLOC-2
		BCC		T54CARRY2			; Make sure the carry is clear
		EMU 	scFail
T54CARRY2:
		LDA		ZPLOC
		CMP		#$EA				; Make sure the value is correct
		BEQ		T54SUCCESS
		EMU 	scFail
T54SUCCESS:
;-------------------------------------------------------------------------------
;		EOR bp,X
;
T55:
		LDA		#$AA				; Load the zp location with the operand
		STA		ZPLOC
		LDX		#$2
		LDA		#$F0				; The other operand
		EOR		ZPLOC-2,X
		BNE		T55_notZero
		EMU 	scFail
T55_notZero:
		BPL		T55_positive
		EMU 	scFail
T55_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T55_success	
		EMU 	scFail
T55_success:
;-------------------------------------------------------------------------------
;  LSR bp,X
;
T56:
		LDA		#$AB				; An appropriate value for shifting
		STA		ZPLOC
		LDX		#$10
		LSR		ZPLOC-$10,X
		BCS		T56CARRY			; Make sure the carry is set
		EMU 	scFail
T56CARRY:
		BPL		T56_positive		; Make sure the N flag is clear
		EMU 	scFail
T56_positive:
		LDA		ZPLOC
		CMP		#$55				; Make sure the value is correct
		BEQ		T56V1CORRECT
		EMU 	scFail
T56V1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		STA		ZPLOC
		LSR		ZPLOC-$10,X
		BCC		T56CARRY2			; Make sure the carry is clear
		EMU 	scFail
T56CARRY2:
		LDA		ZPLOC
		CMP		#$2A				; Make sure the value is correct
		BEQ		T56SUCCESS
		EMU 	scFail
T56SUCCESS:
;-------------------------------------------------------------------------------
;		EOR abs,Y
;
T59:
		LDA		#$AA				; Load the zp location with the operand
		STA		ABSLOC
		LDY		#$2
		LDA		#$F0				; The other operand
		EOR		ABSLOC-2,Y
		BNE		T59_notZero
		EMU 	scFail
T59_notZero:
		BPL		T59_positive
		EMU 	scFail
T59_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T59_success	
		EMU 	scFail
T59_success:
;-------------------------------------------------------------------------------
;		PHY
;
T5A:
		LDA		#$0					; Zero accumulator for later
		LDY		#$AA				; Something to put on the stack
		TSX							; Save the current stack pointer to test
		STX		ZPLOC
		PHY
		PLA							; Pull the saved Y
		CMP		#$AA				; Make sure we have the right value
		BEQ		T5A_valueOK
		EMU 	scFail
T5A_valueOK:
		TSX
		CPX		ZPLOC				; Check sp is right
		BEQ		T5A_success
		EMU 	scFail
T5A_success:
;-------------------------------------------------------------------------------
; TAB Relocate the base page
;
T5B:
		LDA		#>altBasePage		; We only need the high byte for the B reg
		TAB							; Relocate the base page
		STA		ZPLOC				; This should store in the new base page
		LDA		altBasePage+ZPLOC	; Let's find out
		CMP		#>altBasePage
		BEQ		T5B_bpSuccess
		EMU 	scFail
T5B_bpSuccess:
T5B_success:
		LDA		#$0					; Reset the base page as per 6502
		TAB
;-------------------------------------------------------------------------------
; AUG is a 4 byte 4 cycle noop
;
T5C:
		.byte	AUG
		.byte	0,0,0				; Padding
T5C_success:
;-------------------------------------------------------------------------------
;		EOR abs,X
;
T5D:
		LDA		#$AA				; Load the zp location with the operand
		STA		ABSLOC
		LDA		#$F0				; The other operand
		LDX		#$3					; An index
		EOR		ABSLOC-3,X
		BNE		T5D_notZero
		EMU 	scFail
T5D_notZero:
		BPL		T5D_positive
		EMU 	scFail
T5D_positive:
		CMP		#$5A				; Test against the expected value
		BEQ		T5D_success	
		EMU 	scFail
T5D_success:
;-------------------------------------------------------------------------------
;  LSR abs,X
;
T5E:
		LDA		#$AB				; An appropriate value for shifting
		STA		ABSLOC
		LDX		#$2
		LSR		ABSLOC-2,X
		BCS		T5ECARRY			; Make sure the carry is set
		EMU 	scFail
T5ECARRY:
		BPL		T5E_positive		; Make sure the N flag is clear
		EMU 	scFail
T5E_positive:
		LDA		ABSLOC
		CMP		#$55				; Make sure the value is correct
		BEQ		T5EV1CORRECT
		EMU 	scFail
T5EV1CORRECT:
		AND		#$FE				; Make it so the carry should be clear
		STA		ABSLOC
		LSR 	ABSLOC-2,X
		BCC		T5ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T5ECARRY2:
		LDA		ABSLOC
		CMP		#$2A				; Make sure the value is correct
		BEQ		T5ESUCCESS
		EMU 	scFail
T5ESUCCESS:
;-------------------------------------------------------------------------------
;		ADC (bp,X)
;
T61:
		LDA		#<ABYTE				; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>ABYTE
		STA		ZPLOC+1
		LDX		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		CLC
		ADC		(ZPLOC-2,X)
		BCS		T61_carrySet		; MAke sure carry is set
		EMU		scFail
T61_carrySet:
		BVC		T61_vClear			; Overflow should be clear
		EMU		scFail
T61_vClear:
		CMP		#$9A				; Test against the expected value
		BEQ		T61Success	
		EMU 	scFail
T61Success:
;-------------------------------------------------------------------------------
;		RTN Return from kernel.  I think this adds the arg to the sp before
;           returning.
;
T62:
		TSX							; Save the stack pointer
		STX		ZPLOC
		JSR		T62_rtnTest
		TSX							; Make sure the stack pointer is still right
		CPX		ZPLOC
		BEQ		T62_spOK
		EMU		scFail
T62_spOK:
		BRA		T62_success
;
; Subroutine with RTN at the end of it
;		
.proc	T62_rtnTest
		PHX							; Adjust the stack pointer
		PHX
		PHX
		RTN		$03					; Adjust stack pointer 3?
.endproc

T62_success:

;-------------------------------------------------------------------------------
;		BSR word rel
;
T63:
		TSX							; Save the stack pointer
		STX		ZPLOC
		BSR		T63_testSub
		TSX							; Make sure the stack pointer is still right
		CPX		ZPLOC
		TSX							; Make sure the stack pointer is still right
		CPX		ZPLOC
		BEQ		T63_spOK
		EMU		scFail
T63_spOK:
		BRA		T63_success
.proc	T63_testSub
		RTS
.endproc
T63_success:
;-------------------------------------------------------------------------------
;		STZ bp
;
T64:
		LDZ_imm	$64
		STZ_bp	ZPLOC
		LDX		ZPLOC
		CPX		#$64
		BEQ		T64_success
		EMU		scFail
T64_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;		ADC bp
;
T65:
		LDA		#$6A				; Load the zp location with the target address
		STA		ZPLOC
		LDA		#$50				; The other operand
		CLC
		ADC		ZPLOC
		BCC		T65_carryClear		; MAke sure carry is set
		EMU		scFail
T65_carryClear:
		BVS		T65_vSet			; Overflow should be clear
		EMU		scFail
T65_vSet:
		CMP		#$BA				; Test against the expected value
		BEQ		T65Success	
		EMU 	scFail
T65Success:
;-------------------------------------------------------------------------------
;	ROR bp
;
T66:
		CLC							; Make sure carry is known
		LDA		#$A9				; An appropriate value for shifting
		STA		ZPLOC
		ROR		ZPLOC
		BCS		T66CARRY			; Make sure the carry is set
		EMU 	scFail
T66CARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T66V1CORRECT
		EMU 	scFail
T66V1CORRECT:
		ROR		ZPLOC				; Do it again	
		BCC		T66CARRY2			; Make sure the carry is clear
		EMU 	scFail
T66CARRY2:
		LDA		#$AA				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T66SUCCESS
		EMU 	scFail
T66SUCCESS:
;-------------------------------------------------------------------------------
;		ADC imm
;
T69:
		LDA		#$6A				; Load the zp location with the target address
		CLC
		ADC		#$50
		BCC		T69_carryClear		; MAke sure carry is set
		EMU		scFail
T69_carryClear:
		BVS		T69_vSet			; Overflow should be clear
		EMU		scFail
T69_vSet:
		CMP		#$BA				; Test against the expected value
		BEQ		T69Success	
		EMU 	scFail
T69Success:
;-------------------------------------------------------------------------------
;	ROR acc
;
T6A:
		CLC							; Make sure carry is known
		LDA		#$A9				; An appropriate value for shifting
		ROR		A
		BCS		T6ACARRY			; Make sure the carry is set
		EMU 	scFail
T6ACARRY:
		CMP		#$54
		BEQ		T6AV1CORRECT
		EMU 	scFail
T6AV1CORRECT:
		ROR		A					; Do it again	
		BCC		T6ACARRY2			; Make sure the carry is clear
		EMU 	scFail
T6ACARRY2:
		CMP		#$AA
		BEQ		T6ASUCCESS
		EMU 	scFail
T6ASUCCESS:
;-------------------------------------------------------------------------------
;	TZA
;
T6B:
		LDZ_imm	$AA
		LDA		#$0					; Clear N and set Z
		TZA
		BNE		T6B_nonZero			; MAke sure Z is clear
		EMU		scFail
T6B_nonZero:
		BMI		T6B_minus			; Make sure N is now set
		EMU		scFail
T6B_minus:
		CMP		#$AA				; Make sure the value is correct
		BEQ		T6B_success
		EMU		scFail
T6B_success:
;-------------------------------------------------------------------------------
;	JMP	(abs)
;
T6C:
		LDA		#<T6C_jumpTo
		STA		ABSIND
		LDA		#>T6C_jumpTo
		STA		ABSIND+1
		JMP		(ABSIND)
		EMU		scFail
T6C_jumpTo:
;-------------------------------------------------------------------------------
;		ADC abs
;
T6D:
		LDA		#$6A				; Load the absolute location with the target address
		STA		ABSLOC
		LDA		#$50				; The other operand
		CLC
		ADC		ABSLOC
		BCC		T6D_carryClear		; MAke sure carry is set
		EMU		scFail
T6D_carryClear:
		BVS		T6D_vSet			; Overflow should be clear
		EMU		scFail
T6D_vSet:
		CMP		#$BA				; Test against the expected value
		BEQ		T6DSuccess	
		EMU 	scFail
T6DSuccess:
;-------------------------------------------------------------------------------
;	ROR abs
;
T6E:
		CLC							; Make sure carry is known
		LDA		#$A9				; An appropriate value for shifting
		STA		ABSLOC
		ROR		ABSLOC
		BCS		T6ECARRY			; Make sure the carry is set
		EMU 	scFail
T6ECARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T6EV1CORRECT
		EMU 	scFail
T6EV1CORRECT:
		ROR		ABSLOC				; Do it again	
		BCC		T6ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T6ECARRY2:
		LDA		#$AA				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T6ESUCCESS
		EMU 	scFail
T6ESUCCESS:
;-------------------------------------------------------------------------------
;		ADC (bp),Y
;
T71:
		LDA		#<(ABYTE-2)			; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>(ABYTE-2)
		STA		ZPLOC+1
		LDY		#2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		CLC
		ADC		(ZPLOC),Y
		BCS		T71_carrySet		; MAke sure carry is set
		EMU		scFail
T71_carrySet:
		BVC		T71_vClear			; Overflow should be clear
		EMU		scFail
T71_vClear:
		CMP		#$9A				; Test against the expected value
		BEQ		T71Success	
		EMU 	scFail
T71Success:
;-------------------------------------------------------------------------------
;		ADC (bp),Y
;
T72:
		LDA		#<(ABYTE-2)			; Load the zp location with the target address
		STA		ZPLOC
		LDA		#>(ABYTE-2)
		STA		ZPLOC+1
		LDZ_imm	$2					; We'll offset from a different zp location
		LDA		#$F0				; The other operand
		CLC
		ADC_bp_ind_z	ZPLOC
		BCS		T72_carrySet		; MAke sure carry is set
		EMU		scFail
T72_carrySet:
		BVC		T72_vClear			; Overflow should be clear
		EMU		scFail
T72_vClear:
		CMP		#$9A				; Test against the expected value
		BEQ		T72Success	
		EMU 	scFail
T72Success:
;-------------------------------------------------------------------------------
;  BVS word rel
;
T73:
		LDA		#$C0					; Make sure V is set by doing a bit test
		STA		ZPLOC
		BIT		ZPLOC
		BVS_W	T73_vSet
		EMU 	scFail
T73_vSet:
		CLV								; V is cleared
		BVS_W	T73_fail
		BRA		T73SUCCESS
T73_fail:
		EMU 	scFail
T73SUCCESS:
;-------------------------------------------------------------------------------
;		STZ bp,X
;
T74:
		LDZ_imm	$64
		LDX		#$2
		STZ_bpx	ZPLOC-2
		LDY		ZPLOC
		CPY		#$64
		BEQ		T74_success
		EMU		scFail
T74_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;		ADC bp,X
;
T75:
		LDA		#$6A				; Load the zp location with the target address
		STA		ZPLOC
		LDX		#$2
		LDA		#$50				; The other operand
		CLC
		ADC		ZPLOC-2,X
		BCC		T75_carryClear		; MAke sure carry is set
		EMU		scFail
T75_carryClear:
		BVS		T75_vSet			; Overflow should be clear
		EMU		scFail
T75_vSet:
		CMP		#$BA				; Test against the expected value
		BEQ		T75Success	
		EMU 	scFail
T75Success:
;-------------------------------------------------------------------------------
;	ROR bp,X
;
T76:
		CLC							; Make sure carry is known
		LDA		#$A9				; An appropriate value for shifting
		STA		ZPLOC
		LDX		#$5
		ROR		ZPLOC-5,X
		BCS		T76CARRY			; Make sure the carry is set
		EMU 	scFail
T76CARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T76V1CORRECT
		EMU 	scFail
T76V1CORRECT:
		ROR		ZPLOC-5,X			; Do it again	
		BCC		T76CARRY2			; Make sure the carry is clear
		EMU 	scFail
T76CARRY2:
		LDA		#$AA				; Make sure the value is correct
		CMP		ZPLOC
		BEQ		T76SUCCESS
		EMU 	scFail
T76SUCCESS:
;-------------------------------------------------------------------------------
;		ADC abs,Y
;
T79:
		LDA		#$6A				; Load the zp location with the target address
		STA		ABSLOC
		LDY		#$2
		LDA		#$50				; The other operand
		CLC
		ADC		ABSLOC-2,Y
		BCC		T79_carryClear		; MAke sure carry is set
		EMU		scFail
T79_carryClear:
		BVS		T79_vSet			; Overflow should be clear
		EMU		scFail
T79_vSet:
		CMP		#$BA				; Test against the expected value
		BEQ		T79Success	
		EMU 	scFail
T79Success:
;-------------------------------------------------------------------------------
;		PLY
;
T7A:
		TSX							; Preserve the current stack pointer for
		STX		ZPLOC				; checking later.
		LDA		#$AA				; A value for checking
		LDY		#$0
		PHA							; Push a value
		PLY							; and pull it
		TSX							; Check the stack pointer
		CPX		ZPLOC
		BEQ		T7A_stackOK
		EMU		scFail
T7A_stackOK:
		CPY		#$AA				; Check Y is what we expect
		BEQ		T7A_success
		EMU		scFail
T7A_success:
;-------------------------------------------------------------------------------
;		TBA
;
T7B:
		LDA		#$AA
		TAB
		LDA		#$0					; Make sure A is not already correct
		TBA
		BNE		T7B_zClear			; Should clear Z flag
		EMU		scFail
T7B_zClear:
		BMI		T7B_nSet			; N should be set for $AA
		EMU		scFail
T7B_nSet:
		CMP		#$AA				; MAke sure the value is right
		BEQ		T7B_valueCorrect
		EMU		scFail
T7B_valueCorrect:
		LDA		#$0					; Reset the B register
		TAB	
		LDA		#$AA				; We'll check for zero too
		TBA
		BEQ		T7B_zSet			; Should clear Z flag
		EMU		scFail
T7B_zSet:
		BPL		T7B_nClear			; N should be set for $AA
		EMU		scFail
T7B_nClear:
T7B_success:
;-------------------------------------------------------------------------------
;	JMP	(abs,X)
;
T7C:
		LDA		#<T7C_jumpTo
		STA		ABSIND
		LDA		#>T7C_jumpTo
		STA		ABSIND+1
		LDX		#$2
		JMP		(ABSIND-2,X)
		EMU		scFail
T7C_jumpTo:
;-------------------------------------------------------------------------------
;		ADC abs,X
;
T7D:
		LDA		#$6A				; Load the zp location with the target address
		STA		ABSLOC
		LDX		#$2
		LDA		#$50				; The other operand
		CLC
		ADC		ABSLOC-2,X
		BCC		T7D_carryClear		; MAke sure carry is set
		EMU		scFail
T7D_carryClear:
		BVS		T7D_vSet			; Overflow should be clear
		EMU		scFail
T7D_vSet:
		CMP		#$BA				; Test against the expected value
		BEQ		T7DSuccess	
		EMU 	scFail
T7DSuccess:
;-------------------------------------------------------------------------------
;	ROR abs,X
;
T7E:
		CLC							; Make sure carry is known
		LDA		#$A9				; An appropriate value for shifting
		STA		ABSLOC
		LDX		#$5
		ROR		ABSLOC-5,X
		BCS		T7ECARRY			; Make sure the carry is set
		EMU 	scFail
T7ECARRY:
		LDA		#$54				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T7EV1CORRECT
		EMU 	scFail
T7EV1CORRECT:
		ROR		ABSLOC-5,X			; Do it again	
		BCC		T7ECARRY2			; Make sure the carry is clear
		EMU 	scFail
T7ECARRY2:
		LDA		#$AA				; Make sure the value is correct
		CMP		ABSLOC
		BEQ		T7ESUCCESS
		EMU 	scFail
T7ESUCCESS:
;-------------------------------------------------------------------------------
;  STA (bp,X)
T81:
		LDA		#<ABSLOC
		STA		ZPLOC
		LDA		#>ABSLOC
		STA		ZPLOC+1
		LDA		#$81				; something to store
		LDX		#$2
		STA		(ZPLOC-2,X)			; Store it
		CMP		ABSLOC				; Is ABSLOC correct?
		BEQ		T81_success
		EMU		scFail
T81_success:
;-------------------------------------------------------------------------------
;  STA (d,sp),Y
;
T82:
		LDA		#>(ABSLOC-2)		; The location minus an offset that Y will
		PHA							; correct needs to go on the stack
		LDA		#<(ABSLOC-2)
		PHA
		PHA							; Push an extra byte for interest
		LDA		#$82				; A number to store
		LDY		#$2
		STA_svi_y	$2				; 2 is the offset needed for the vector
		PLA							; Unwind the stack
		PLA
		PLA
		LDA		#$82				; To compare against absloc
		CMP		ABSLOC				; Is absloc correct?
		BEQ		T82_success
		EMU		scFail
T82_success:
;-------------------------------------------------------------------------------
;  BRA	word
;
T83:
		BRA		T83_skipBad
T83_beforeBad:
		BRA		T83_success
		EMU		scFail
T83_skipBad:
		BRA_W	T83_beforeBad
		EMU		scFail
T83_success:
;-------------------------------------------------------------------------------
;  STY	bp
;
T84:
		LDA		#$0				; Clear the stuff we will use
		STA		ZPLOC
		LDY		#$84			; A value to store
		STY		ZPLOC
		LDA		ZPLOC
		CMP		#$84			; Check we got the right value
		BEQ		T84_success
		EMU		scFail
T84_success:
;-------------------------------------------------------------------------------
;	SMB bp (do all of them)
;
T87:
		LDA		#$0					; Test value with all bits clear
		STA		ZPLOC
		SMB		0, ZPLOC			; Bit 0
		LDA		#$1					; Make sure only bit 0 is reset
		CMP		ZPLOC	
		BEQ		T8700SUCCESS
		EMU 	scFail
T8700SUCCESS:
		SMB		1, ZPLOC			; Bit 1
		LDA		#$3					; Make sure only bit 1 is reset
		CMP		ZPLOC	
		BEQ		T8701SUCCESS
		EMU 	scFail
T8701SUCCESS:
		SMB		2, ZPLOC			; Bit 2
		LDA		#$7					; Make sure only bit 2 is reset
		CMP		ZPLOC	
		BEQ		T8702SUCCESS
		EMU 	scFail
T8702SUCCESS:
		SMB		3, ZPLOC			; Bit 3
		LDA		#$F					; Make sure only bit 3 is reset
		CMP		ZPLOC	
		BEQ		T8703SUCCESS
		EMU 	scFail
T8703SUCCESS:
		SMB		4, ZPLOC			; Bit 4
		LDA		#$1F				; Make sure only bit 4 is reset
		CMP		ZPLOC	
		BEQ		T8704SUCCESS
		EMU 	scFail
T8704SUCCESS:
		SMB		5, ZPLOC			; Bit 5
		LDA		#$3F				; Make sure only bit 5 is reset
		CMP		ZPLOC	
		BEQ		T8705SUCCESS
		EMU 	scFail
T8705SUCCESS:
		SMB		6, ZPLOC			; Bit 6
		LDA		#$7F				; Make sure only bit 6 is reset
		CMP		ZPLOC	
		BEQ		T8706SUCCESS
		EMU 	scFail
T8706SUCCESS:
		SMB		7, ZPLOC			; Bit 7
		LDA		#$FF				; Make sure only bit 7 is reset
		CMP		ZPLOC	
		BEQ		T87SUCCESS
		EMU 	scFail
T87SUCCESS:
;-------------------------------------------------------------------------------
;		BIT immediate
;
T89:
		LDA		#$7A				; Set up zp loc
		LDX		#$FF				; Set the negative flag
		CLV							; Clear the overflow flag (it will be set after)
		BMI		T89NegSet			; Check the flags re correct
		EMU 	scFail
T89NegSet:
		BVC		T89VClear
		EMU 	scFail
T89VClear:
		BIT		#$FC
		BMI		T89NOK				; Test the negative bit is set
		EMU 	scFail
T89NOK:
		BVS		T89VOK				; Test the overflow is clear
		EMU 	scFail
T89VOK:
		BNE		T89ZeroTest			; Check zero is still not set
		EMU 	scFail
T89ZeroTest:
		LDA		#$85				; Should clear the result
		BIT		#$7A
		BEQ		T89Success			; Test the result was zero
		EMU 	scFail
T89Success:

;-------------------------------------------------------------------------------
;	TXA
;
T8A:
		LDX		#$8A
		LDA		#$0					; Clear N and set Z
		TXA
		BNE		T8A_nonZero			; MAke sure Z is clear
		EMU		scFail
T8A_nonZero:
		BMI		T8A_minus			; Make sure N is now set
		EMU		scFail
T8A_minus:
		CMP		#$8A				; Make sure the value is correct
		BEQ		T8A_success
		EMU		scFail
T8A_success:
;-------------------------------------------------------------------------------
;  STY	abs,X
;
T8B:
		LDA		#$0				; Clear the stuff we will use
		STA		ABSLOC
		LDY		#$8B			; A value to store
		LDX		#$3				; Set an X offset
		STY_absx	ABSLOC-3
		LDA		ABSLOC
		CMP		#$8B			; Check we got the right value
		BEQ		T8B_success
		EMU		scFail
T8B_success:
;-------------------------------------------------------------------------------
;  STY	abs
;
T8C:
		LDA		#$0				; Clear the stuff we will use
		STA		ABSLOC
		LDY		#$8C			; A value to store
		STY		ABSLOC
		LDA		ABSLOC
		CMP		#$8C			; Check we got the right value
		BEQ		T8C_success
		EMU		scFail
T8C_success:
;-------------------------------------------------------------------------------
;  STX	abs
;
T8E:
		LDA		#$0				; Clear the stuff we will use
		STA		ABSLOC
		LDX		#$8E			; A value to store
		STX		ABSLOC
		LDA		ABSLOC
		CMP		#$8E			; Check we got the right value
		BEQ		T8E_success
		EMU		scFail
T8E_success:
;-------------------------------------------------------------------------------
;	BBS bp
;
T8F:
	LDA		#$01					; Bit 0 for a jump, all the others failed
									; jump
	STA		ZPLOC
	BBS		0,ZPLOC,T8F0SUCCESS		; Bit 0 is set so branch should happen
	EMU 	scFail
T8F0SUCCESS:
	BBS		1,ZPLOC,T8FFAIL			; Bit 1 is not set so branch should not happen
	BBS		2,ZPLOC,T8FFAIL			; Bit 1 is not set so branch should not happen
	BBS		3,ZPLOC,T8FFAIL			; Bit 1 is not set so branch should not happen
	BBS		4,ZPLOC,T8FFAIL			; Bit 1 is not set so branch should not happen
	BBS		5,ZPLOC,T8FFAIL			; Bit 1 is not set so branch should not happen
	BBS		6,ZPLOC,T8FFAIL			; Bit 1 is not set so branch should not happen
	BBS		7,ZPLOC,T8FFAIL			; Bit 1 is not set so branch should not happen
	BRA		T8FSUCCESS
T8FFAIL:
	EMU 	scFail
T8FSUCCESS:
;-------------------------------------------------------------------------------
;  STA (bp),Y
T91:
		LDA		#<(ABSLOC-3)
		STA		ZPLOC
		LDA		#>(ABSLOC-3)
		STA		ZPLOC+1
		LDA		#$91				; something to store
		LDY		#$3
		STA		(ZPLOC),Y			; Store it
		CMP		ABSLOC				; Is ABSLOC correct?
		BEQ		T91_success
		EMU		scFail
T91_success:
;-------------------------------------------------------------------------------
;  STA (bp),Z
T92:
		LDA		#<(ABSLOC-3)
		STA		ZPLOC
		LDA		#>(ABSLOC-3)
		STA		ZPLOC+1
		LDA		#$92				; something to store
		LDZ_imm	$3
		STA_bp_ind_z	ZPLOC		; Store it
		CMP		ABSLOC				; Is ABSLOC correct?
		BEQ		T92_success
		EMU		scFail
T92_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;  BCC	word
;
T93:
		BRA		T93_skipBad
T93_beforeBad:
		BRA		T93_testCS
		EMU		scFail
T93_skipBad:
		CLC
		BCC_W	T93_beforeBad
		EMU		scFail
T93_testCS:
		SEC
		BCC_W	T93_setFail
		BRA		T93_success
T93_setFail:
		EMU		scFail
T93_success:
;-------------------------------------------------------------------------------
;		STY bp,X
;
T94:
		LDY		#$94
		LDX		#$2
		STY		ZPLOC-2,X
		LDX		ZPLOC
		CPX		#$94
		BEQ		T94_success
		EMU		scFail
T94_success:
;-------------------------------------------------------------------------------
;		STA bp,X
;
T95:
		LDA		#$95
		LDX		#$2
		STA		ZPLOC-2,X
		LDX		ZPLOC
		CPX		#$95
		BEQ		T95_success
		EMU		scFail
T95_success:
;-------------------------------------------------------------------------------
;		STX bp,Y
;
T96:
		LDX		#$96
		LDY		#$2
		STX		ZPLOC-2,Y
		LDY		ZPLOC
		CPY		#$96
		BEQ		T96_success
		EMU		scFail
T96_success:
;-------------------------------------------------------------------------------
;	TYA
;
T98:
		LDY		#$98
		LDA		#$0					; Clear N and set Z
		TYA
		BNE		T98_nonZero			; MAke sure Z is clear
		EMU		scFail
T98_nonZero:
		BMI		T98_minus			; Make sure N is now set
		EMU		scFail
T98_minus:
		CMP		#$98				; Make sure the value is correct
		BEQ		T98_success
		EMU		scFail
T98_success:
;-------------------------------------------------------------------------------
;		STA abs,Y
;
T99:
		LDA		#$99
		LDY		#$2
		STA_absy	ABSLOC-2
		LDX		ABSLOC
		CPX		#$99
		BEQ		T99_success
		EMU		scFail
T99_success:
;-------------------------------------------------------------------------------
;		STX abs,Y
;
T9B:
		LDX		#$9B
		LDY		#$2
		STX_absy	ABSLOC-2
		LDA		ABSLOC
		CMP		#$9B
		BEQ		T9B_success
		EMU		scFail
T9B_success:
;-------------------------------------------------------------------------------
;		STZ abs
;
T9C:
		LDZ_imm	$9C
		STZ_abs	ABSLOC
		LDX		ABSLOC
		CPX		#$9C
		BEQ		T9C_success
		EMU		scFail
T9C_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;		STA abs,X
;
T9D:
		LDA		#$9D
		LDX		#$2
		STA		ABSLOC-2,X
		LDY		ABSLOC
		CPY		#$9D
		BEQ		T9D_success
		EMU		scFail
T9D_success:
;-------------------------------------------------------------------------------
;		STZ abs,X
;
T9E:
		LDZ_imm	$9E
		LDX		#$2
		STZ_absx	ABSLOC-2
		LDY		ABSLOC
		CPY		#$9E
		BEQ		T9E_success
		EMU		scFail
T9E_success:
		LDZ_imm	0
;-------------------------------------------------------------------------------
;		LDZ abs
;
TABt:
		LDX		#$AB
		STX		ABSLOC
		LDZ_abs	ABSLOC
		CPZ_imm	$AB
		BEQ		TAB_success
		EMU		scFail
TAB_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;		LDA (bp),Y
;
TB1:
		LDA		#<(ABSLOC-2)			; Set up the pointer address
		STA		ZPLOC
		LDA		#>(ABSLOC-2)			; and the high byte
		STA		ZPLOC+1
		LDA		#$B1					; A value to get
		STA		ABSLOC
		LDY		#$2						; The offset
		LDA		(ZPLOC),Y				; The actual test
;
;		Flag tests
;
		BNE		TB1_ne					; Z flag should be clear
		EMU		scFail
TB1_ne:
		BMI		TB1_mi					; N flag should be set
		EMU		scFail
TB1_mi:
;
;		Value test
;
		CMP		#$B1
		BEQ		TB1_success
		EMU		scFail
TB1_success:
;-------------------------------------------------------------------------------
;		LDA (bp),Z
;
TB2:
		LDA		#<(ABSLOC-2)			; Set up the pointer address
		STA		ZPLOC
		LDA		#>(ABSLOC-2)			; and the high byte
		STA		ZPLOC+1
		LDA		#$B2					; A value to get
		STA		ABSLOC
		LDZ_imm	$2						; The offset
		LDA_bp_ind_z	ZPLOC			; The actual test
;
;		Flag tests
;
		BNE		TB2_ne					; Z flag should be clear
		EMU		scFail
TB2_ne:
		BMI		TB2_mi					; N flag should be set
		EMU		scFail
TB2_mi:
;
;		Value test
;
		CMP		#$B2
		BEQ		TB2_success
		EMU		scFail
TB2_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;  BCS	word
;
TB3:
		BRA		TB3_skipBad
TB3_beforeBad:
		BRA		TB3_testCS
		EMU		scFail
TB3_skipBad:
		SEC
		BCS_W	TB3_beforeBad
		EMU		scFail
TB3_testCS:
		CLC
		BCS_W	TB3_setFail
		BRA		TB3_success
TB3_setFail:
		EMU		scFail
TB3_success:
;-------------------------------------------------------------------------------
;  LDY	bp,X
;
TB4:
		LDA		#$B4				; a value to get
		STA		ZPLOC
		LDX		#$2
		LDY		#$0					; Make sure Y starts off cleared
		LDY		ZPLOC-2,X			; get the value
;
;		Flag tests
;
		BNE		TB4_ne					; Z flag should be clear
		EMU		scFail
TB4_ne:
		BMI		TB4_mi					; N flag should be set
		EMU		scFail
TB4_mi:
;
;		Value test
;
		CPY		#$B4
		BEQ		TB4_success
		EMU		scFail
TB4_success:
;-------------------------------------------------------------------------------
;  LDA	bp,X
;
TB5:
		LDA		#$B5				; a value to get
		STA		ZPLOC
		LDX		#$2
		LDA		#$0					; Make sure A starts off cleared
		LDA		ZPLOC-2,X			; get the value
;
;		Flag tests
;
		BNE		TB5_ne					; Z flag should be clear
		EMU		scFail
TB5_ne:
		BMI		TB5_mi					; N flag should be set
		EMU		scFail
TB5_mi:
;
;		Value test
;
		CMP		#$B5
		BEQ		TB5_success
		EMU		scFail
TB5_success:
;-------------------------------------------------------------------------------
;  LDX	bp,Y
;
TB6:
		LDA		#$B6				; a value to get
		STA		ZPLOC
		LDY		#$2
		LDX		#$0					; Make sure X starts off cleared
		LDX		ZPLOC-2,Y			; get the value
;
;		Flag tests
;
		BNE		TB6_ne					; Z flag should be clear
		EMU		scFail
TB6_ne:
		BMI		TB6_mi					; N flag should be set
		EMU		scFail
TB6_mi:
;
;		Value test
;
		CPX		#$B6
		BEQ		TB6_success
		EMU		scFail
TB6_success:
;-------------------------------------------------------------------------------
;  LDA	abs,Y
;
TB9:
		LDA		#$B9				; a value to get
		STA		ABSLOC
		LDY		#$2
		LDA		#$0					; Make sure A starts off cleared
		LDA		ABSLOC-2,Y			; get the value
;
;		Flag tests
;
		BNE		TB9_ne					; Z flag should be clear
		EMU		scFail
TB9_ne:
		BMI		TB9_mi					; N flag should be set
		EMU		scFail
TB9_mi:
;
;		Value test
;
		CMP		#$B9
		BEQ		TB9_success
		EMU		scFail
TB9_success:
;-------------------------------------------------------------------------------
;  LDZ	abs,X
;
TBB:
		LDA		#$BB					; a value to get
		STA		ABSLOC
		LDX		#$2
		LDZ_imm	$0						; Make sure A starts off cleared
		LDZ_absx	ABSLOC-2			; get the value
;
;		Flag tests
;
		BNE		TBB_ne					; Z flag should be clear
		EMU		scFail
TBB_ne:
		BMI		TBB_mi					; N flag should be set
		EMU		scFail
TBB_mi:
;
;		Value test
;
		CPZ_imm	$BB
		BEQ		TBB_success
		EMU		scFail
TBB_success:
		LDZ_imm	0

;-------------------------------------------------------------------------------
;  LDY	abs,X
;
TBC:
		LDA		#$BC					; a value to get
		STA		ABSLOC
		LDX		#$2
		LDY		#$0						; Make sure A starts off cleared
		LDY		ABSLOC-2,X			; get the value
;
;		Flag tests
;
		BNE		TBC_ne					; Z flag should be clear
		EMU		scFail
TBC_ne:
		BMI		TBC_mi					; N flag should be set
		EMU		scFail
TBC_mi:
;
;		Value test
;
		CPY		#$BC
		BEQ		TBC_success
		EMU		scFail
TBC_success:
;-------------------------------------------------------------------------------
;  LDA	abs,X
;
TBD:
		LDA		#$BD					; a value to get
		STA		ABSLOC
		LDX		#$2
		LDA		#$0						; Make sure A starts off cleared
		LDA		ABSLOC-2,X			; get the value
;
;		Flag tests
;
		BNE		TBD_ne					; Z flag should be clear
		EMU		scFail
TBD_ne:
		BMI		TBD_mi					; N flag should be set
		EMU		scFail
TBD_mi:
;
;		Value test
;
		CMP		#$BD
		BEQ		TBD_success
		EMU		scFail
TBD_success:

;-------------------------------------------------------------------------------
;  LDX	abs,Y
;
TBE:
		LDA		#$BE					; a value to get
		STA		ABSLOC
		LDY		#$2
		LDX		#$0						; Make sure A starts off cleared
		LDX		ABSLOC-2,Y			; get the value
;
;		Flag tests
;
		BNE		TBE_ne					; Z flag should be clear
		EMU		scFail
TBE_ne:
		BMI		TBE_mi					; N flag should be set
		EMU		scFail
TBE_mi:
;
;		Value test
;
		CPX		#$BE
		BEQ		TBE_success
		EMU		scFail
TBE_success:
;-------------------------------------------------------------------------------
;  CMP	(bp,X)
;
TC1:
		LDA		#<ABSLOC
		STA		ZPLOC
		LDA		#>ABSLOC
		STA		ZPLOC+1
		LDX		#$2
		LDA		#$C1
		STA		ABSLOC
		LDA		#$C0			; Get a negative difference
		CMP		(ZPLOC-2,X)
		BMI		TC1_mi			; Make sure the result is negative
		EMU		scFail
TC1_mi:	BNE		TC1_nz			; Make sure not zero
		EMU		scFail
TC1_nz:	
		BCC		TC1_cc			; make sure carry is clear
		EMU		scFail
TC1_cc:
		LDA		#$C3			; Get a positive difference
		CMP		(ZPLOC-2,X)
		BPL		TC1_pl			; Make sure the result is negative
		EMU		scFail
TC1_pl:	BNE		TC1_nz2			; Make sure not zero
		EMU		scFail
TC1_nz2:
		BCS		TC1_cs			; make sure carry is set
		EMU		scFail
TC1_cs:	
		LDA		#$C1			; Should be equal
		CMP		(ZPLOC-2,X)
		BEQ		TC1_success		; Make sure the result is negative
		EMU		scFail
TC1_success:
;-------------------------------------------------------------------------------
;  DEW bp - decrement word base page
;
TC3:
		LDA		#$1				; Set up a word in the base page
		STA		ZPLOC	
		STA		ZPLOC+1			; ZPLOC is $0101
		DEW		ZPLOC			; ZPLOC should mow be $0100
		LDX		ZPLOC
		BEQ		TC3_testHigh1
		EMU		scFail			; Low byte not decremented
TC3_testHigh1:
		LDY		ZPLOC+1
		CPY		#$1
		BEQ		TC3_decAgain
		EMU		scFail			; High byte not the same
TC3_decAgain:
		DEW		ZPLOC
		LDX		#$FF
		CPX		ZPLOC
		BEQ		TC3_testHigh2
		EMU		scFail
TC3_testHigh2:
		LDY		ZPLOC+1
		BEQ		TC3_success
		EMU		scFail
TC3_success:
;-------------------------------------------------------------------------------
;  CPY	bp
;
TC4:
		LDA		#$C4
		STA		ZPLOC
		LDY		#$C0			; Get a negative difference
		CPY		ZPLOC
		BMI		TC4_mi			; Make sure the result is negative
		EMU		scFail
TC4_mi:	BNE		TC4_nz			; Make sure not zero
		EMU		scFail
TC4_nz:	
		BCC		TC4_cc			; make sure carry is clear
		EMU		scFail
TC4_cc:
		LDY		#$C6			; Get a positive difference
		CPY		ZPLOC
		BPL		TC4_pl			; Make sure the result is negative
		EMU		scFail
TC4_pl:	BNE		TC4_nz2			; Make sure not zero
		EMU		scFail
TC4_nz2:
		BCS		TC4_cs			; make sure carry is set
		EMU		scFail
TC4_cs:	
		LDY		#$C4			; Should be equal
		CPY		ZPLOC
		BEQ		TC4_success		; Make sure the result is negative
		EMU		scFail
TC4_success:
;-------------------------------------------------------------------------------
;  DEC bp - decrement  base page
;
TC6:
		LDA		#$1				; Set up a word in the base page
		STA		ZPLOC	
		DEC		ZPLOC			; ZPLOC should mow be $00
		BEQ		TC6_z
		EMU		scFail			; Low byte not decremented
TC6_z:
		BPL		TC6_pl
		EMU		scFail			; High byte not the same
TC6_pl:
		DEC		ZPLOC
		BNE		TC6_nz
		EMU		scFail
TC6_nz:
		BMI		TC6_mi
		EMU		scFail
TC6_mi:		
		LDX		#$FF
		CPX		ZPLOC
		BEQ		TC6_success
		EMU		scFail
TC6_success:
;-------------------------------------------------------------------------------
;  INY -
;
TC8:
		LDY		#$FF				; Set up a word in the base page
		INY							; Y should mow be $00
		BEQ		TC8_z
		EMU		scFail			; Low byte not decremented
TC8_z:
		BPL		TC8_pl
		EMU		scFail			; High byte not the same
TC8_pl:
		LDY		#$7F
		INY	
		BNE		TC8_nz
		EMU		scFail
TC8_nz:
		BMI		TC8_mi
		EMU		scFail
TC8_mi:		
		LDX		#$FF
		CPX		ZPLOC
		BEQ		TC8_success
		EMU		scFail
TC8_success:
;-------------------------------------------------------------------------------
;  TCB - ASW (left)
;
TCB:
		LDA		#$3
		STA		ZPLOC
		LDA		#$AA
		STA		ZPLOC+1
		SEC
		ASW		ZPLOC
		BCS		TCB_cs				; Carry should be set
		EMU		scFail
TCB_cs:
		BPL		TCB_pl				; Top bit should be 0
		EMU		scFail
TCB_pl:
		BNE		TCB_nz				; Number should be non zero
		EMU		scFail
TCB_nz:
		LDA		#$54				; Check the shift result is correct
		CMP		ZPLOC+1				; high byte first
		BEQ		TCB_hbOK
		EMU		scFail
TCB_hbOK:
		LDA		#$6					; Check the low byte
		CMP		ZPLOC
		BEQ		TCB_success
		EMU		scFail
TCB_success:
;-------------------------------------------------------------------------------
;  CPY	abs
;
TCC:
		LDA		#$CC
		STA		ABSLOC
		LDY		#$C0			; Get a negative difference
		CPY		ABSLOC
		BMI		TCC_mi			; Make sure the result is negative
		EMU		scFail
TCC_mi:	BNE		TCC_nz			; Make sure not zero
		EMU		scFail
TCC_nz:	
		BCC		TCC_cc			; make sure carry is clear
		EMU		scFail
TCC_cc:
		LDY		#$CD			; Get a positive difference
		CPY		ABSLOC
		BPL		TCC_pl			; Make sure the result is negative
		EMU		scFail
TCC_pl:	BNE		TCC_nz2			; Make sure not zero
		EMU		scFail
TCC_nz2:
		BCS		TCC_cs			; make sure carry is set
		EMU		scFail
TCC_cs:	
		LDY		#$CC			; Should be equal
		CPY		ABSLOC
		BEQ		TCC_success		; Make sure the result is negative
		EMU		scFail
TCC_success:
;-------------------------------------------------------------------------------
;  DEC abs - decrement  absolute
;
TCE:
		LDA		#$1				; Set up a word in the base page
		STA		ABSLOC	
		DEC		ABSLOC			; ZPLOC should mow be $00
		BEQ		TCE_z
		EMU		scFail			; Low byte not decremented
TCE_z:
		BPL		TCE_pl
		EMU		scFail			; High byte not the same
TCE_pl:
		DEC		ABSLOC
		BNE		TCE_nz
		EMU		scFail
TCE_nz:
		BMI		TCE_mi
		EMU		scFail
TCE_mi:		
		LDX		#$FF
		CPX		ABSLOC
		BEQ		TCE_success
		EMU		scFail
TCE_success:
;-------------------------------------------------------------------------------
;  CMP	(bp),Y
;
TD1:
		LDA		#<(ABSLOC-2)
		STA		ZPLOC
		LDA		#>(ABSLOC-2)
		STA		ZPLOC+1
		LDY		#$2
		LDA		#$D1
		STA		ABSLOC
		LDA		#$C0			; Get a negative difference
		CMP		(ZPLOC),Y
		BMI		TD1_mi			; Make sure the result is negative
		EMU		scFail
TD1_mi:	BNE		TD1_nz			; Make sure not zero
		EMU		scFail
TD1_nz:	
		BCC		TD1_cc			; make sure carry is clear
		EMU		scFail
TD1_cc:
		LDA		#$D3			; Get a positive difference
		CMP		(ZPLOC),Y
		BPL		TD1_pl			; Make sure the result is negative
		EMU		scFail
TD1_pl:	BNE		TD1_nz2			; Make sure not zero
		EMU		scFail
TD1_nz2:
		BCS		TD1_cs			; make sure carry is set
		EMU		scFail
TD1_cs:	
		LDA		#$D1			; Should be equal
		CMP		(ZPLOC),Y
		BEQ		TD1_success		; Make sure the result is negative
		EMU		scFail
TD1_success:
;-------------------------------------------------------------------------------
;  CMP	(bp),Z
;
TD2:
		LDA		#<(ABSLOC-2)
		STA		ZPLOC
		LDA		#>(ABSLOC-2)
		STA		ZPLOC+1
		LDZ_imm	$2
		LDA		#$D2
		STA		ABSLOC
		LDA		#$C0			; Get a negative difference
		CMP_bp_ind_z	ZPLOC
		BMI		TD2_mi			; Make sure the result is negative
		EMU		scFail
TD2_mi:	BNE		TD2_nz			; Make sure not zero
		EMU		scFail
TD2_nz:	
		BCC		TD2_cc			; make sure carry is clear
		EMU		scFail
TD2_cc:
		LDA		#$D3			; Get a positive difference
		CMP_bp_ind_z	ZPLOC
		BPL		TD2_pl			; Make sure the result is negative
		EMU		scFail
TD2_pl:	BNE		TD2_nz2			; Make sure not zero
		EMU		scFail
TD2_nz2:
		BCS		TD2_cs			; make sure carry is set
		EMU		scFail
TD2_cs:	
		LDA		#$D2			; Should be equal
		CMP_bp_ind_z	ZPLOC
		BEQ		TD2_success		; Make sure the result is negative
		EMU		scFail
TD2_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;  BNE	word
;
TD3:
		BRA		TD3_skipBad
TD3_beforeBad:
		BRA		TD3_testCS
		EMU		scFail
TD3_skipBad:
		LDX		#$1						; Clear the Z flag
		BNE_W	TD3_beforeBad
		EMU		scFail
TD3_testCS:
		LDX		#$0						; Set the Z flag
		BNE_W	TD3_setFail
		BRA		TD3_success
TD3_setFail:
		EMU		scFail
TD3_success:
;-------------------------------------------------------------------------------
;  CPZ	bp
;
TD4:
		LDA		#$D4
		STA		ZPLOC
		LDZ_imm	$D0				; Get a negative difference
		CPZ_bp	ZPLOC
		BMI		TD4_mi			; Make sure the result is negative
		EMU		scFail
TD4_mi:	BNE		TD4_nz			; Make sure not zero
		EMU		scFail
TD4_nz:	
		BCC		TD4_cc			; make sure carry is clear
		EMU		scFail
TD4_cc:
		LDZ_imm	$D6				; Get a positive difference
		CPZ_bp	ZPLOC
		BPL		TD4_pl			; Make sure the result is negative
		EMU		scFail
TD4_pl:	BNE		TD4_nz2			; Make sure not zero
		EMU		scFail
TD4_nz2:
		BCS		TD4_cs			; make sure carry is set
		EMU		scFail
TD4_cs:	
		LDZ_imm	$D4				; Should be equal
		CPZ_bp	ZPLOC
		BEQ		TD4_success		; Make sure the result is negative
		EMU		scFail
TD4_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;  CMP	bp,X
;
TD5:
		LDA		#$D5
		STA		ZPLOC
		LDA		#$C0			; Get a negative difference
		LDX		#$2
		CMP		ZPLOC-2,X
		BMI		TD5_mi			; Make sure the result is negative
		EMU		scFail
TD5_mi:	BNE		TD5_nz			; Make sure not zero
		EMU		scFail
TD5_nz:	
		BCC		TD5_cc			; make sure carry is clear
		EMU		scFail
TD5_cc:
		LDA		#$D6			; Get a positive difference
		CMP		ZPLOC-2,X
		BPL		TD5_pl			; Make sure the result is negative
		EMU		scFail
TD5_pl:	BNE		TD5_nz2			; Make sure not zero
		EMU		scFail
TD5_nz2:
		BCS		TD5_cs			; make sure carry is set
		EMU		scFail
TD5_cs:	
		LDA		#$D5			; Should be equal
		CMP		ZPLOC-2,X
		BEQ		TD5_success		; Make sure the result is negative
		EMU		scFail
TD5_success:
;-------------------------------------------------------------------------------
;  DEC bp,X - decrement  base page
;
TD6:
		LDA		#$1				; Set up a word in the base page
		LDX		#$1
		STA		ZPLOC	
		DEC		ZPLOC-1,X		; ZPLOC should mow be $00
		BEQ		TD6_z
		EMU		scFail			; Low byte not decremented
TD6_z:
		BPL		TD6_pl
		EMU		scFail			; High byte not the same
TD6_pl:
		DEC		ZPLOC-1,X		; ZPLOC should mow be $00
		BNE		TD6_nz
		EMU		scFail
TD6_nz:
		BMI		TD6_mi
		EMU		scFail
TD6_mi:		
		LDY		#$FF
		CPY		ZPLOC
		BEQ		TD6_success
		EMU		scFail
TD6_success:
;-------------------------------------------------------------------------------
;  CMP	abs,Y
;
TD9:
		LDA		#$D9
		STA		ABSLOC
		LDA		#$C0			; Get a negative difference
		LDY		#$2
		CMP		ABSLOC-2,Y
		BMI		TD9_mi			; Make sure the result is negative
		EMU		scFail
TD9_mi:	BNE		TD9_nz			; Make sure not zero
		EMU		scFail
TD9_nz:	
		BCC		TD9_cc			; make sure carry is clear
		EMU		scFail
TD9_cc:
		LDA		#$DE			; Get a positive difference
		CMP		ABSLOC-2,Y
		BPL		TD9_pl			; Make sure the result is negative
		EMU		scFail
TD9_pl:	BNE		TD9_nz2			; Make sure not zero
		EMU		scFail
TD9_nz2:
		BCS		TD9_cs			; make sure carry is set
		EMU		scFail
TD9_cs:	
		LDA		#$D9			; Should be equal
		CMP		ABSLOC-2,Y
		BEQ		TD9_success		; Make sure the result is negative
		EMU		scFail
TD9_success:
;-------------------------------------------------------------------------------
;  PHZ
;
TDB:
		LDA		#$0
		LDZ_imm	$DB				; A value to push
		PHZ						; Push it
		PLA						; Pull it back to A
		CMP		#$DB			; Check we got the right value
		BEQ		TDB_success
		EMU		scFail
TDB_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;  CPZ	abs
;
TDC:
		LDA		#$DC
		STA		ABSLOC
		LDZ_imm	$D0			; Get a negative difference
		CPZ_abs	ABSLOC
		BMI		TDC_mi			; Make sure the result is negative
		EMU		scFail
TDC_mi:	BNE		TDC_nz			; Make sure not zero
		EMU		scFail
TDC_nz:	
		BCC		TDC_cc			; make sure carry is clear
		EMU		scFail
TDC_cc:
		LDZ_imm	$DD			; Get a positive difference
		CPZ_abs	ABSLOC
		BPL		TDC_pl			; Make sure the result is negative
		EMU		scFail
TDC_pl:	BNE		TDC_nz2			; Make sure not zero
		EMU		scFail
TDC_nz2:
		BCS		TDC_cs			; make sure carry is set
		EMU		scFail
TDC_cs:	
		LDZ_imm	$DC			; Should be equal
		CPZ_abs	ABSLOC
		BEQ		TDC_success		; Make sure the result is negative
		EMU		scFail
TDC_success:
		LDZ_imm	$0
		
;-------------------------------------------------------------------------------
;  CMP	abs,X
;
TDD:
		LDA		#$DD
		STA		ABSLOC
		LDA		#$C0			; Get a negative difference
		LDX		#$2
		CMP		ABSLOC-2,X
		BMI		TDD_mi			; Make sure the result is negative
		EMU		scFail
TDD_mi:	BNE		TDD_nz			; Make sure not zero
		EMU		scFail
TDD_nz:	
		BCC		TDD_cc			; make sure carry is clear
		EMU		scFail
TDD_cc:
		LDA		#$DE			; Get a positive difference
		CMP		ABSLOC-2,X
		BPL		TDD_pl			; Make sure the result is negative
		EMU		scFail
TDD_pl:	BNE		TDD_nz2			; Make sure not zero
		EMU		scFail
TDD_nz2:
		BCS		TDD_cs			; make sure carry is set
		EMU		scFail
TDD_cs:	
		LDA		#$DD			; Should be equal
		CMP		ABSLOC-2,X
		BEQ		TDD_success		; Make sure the result is negative
		EMU		scFail
TDD_success:
;-------------------------------------------------------------------------------
;  DEC abs,X - decrement  absolute
;
TDE:
		LDA		#$1				; Set up a word in the base page
		STA		ABSLOC
		LDX		#$3
		DEC		ABSLOC-3,X		; ABSLOC should mow be $00
		BEQ		TDE_z
		EMU		scFail			; Low byte not decremented
TDE_z:
		BPL		TDE_pl
		EMU		scFail			; High byte not the same
TDE_pl:
		DEC		ABSLOC-3,X
		BNE		TDE_nz
		EMU		scFail
TDE_nz:
		BMI		TDE_mi
		EMU		scFail
TDE_mi:		
		LDX		#$FF
		CPX		ABSLOC
		BEQ		TDE_success
		EMU		scFail
TDE_success:
;-------------------------------------------------------------------------------
;  SBC (bp,X)
;
TE1:
		SEC		
		LDA		#$2
		STA		ABSLOC
		LDA		#<ABSLOC
		STA		ZPLOC
		LDA		#>ABSLOC
		STA		ZPLOC+1
		LDA		#$E1
		LDX		#$4
		SBC		(ZPLOC-4,X)
		BCS		TE1_cOK
		EMU		scFail
TE1_cOK:
		BMI		TE1_nOK
		EMU		scFail
TE1_nOK:
		cmp		#$DF
		BEQ		TE1_success
		EMU		scFail
TE1_success:
;-------------------------------------------------------------------------------
;  LDA (d,sp),Y
;
TE2:
		LDA		#$E2				; Set the target data up
		STA		ABSLOC
		LDA		#>(ABSLOC-2)		; The location minus an offset that Y will
		PHA							; correct needs to go on the stack
		LDA		#<(ABSLOC-2)
		PHA
		PHA							; Push an extra byte for interest
		LDY		#$2
		LDA_svi_y	$2				; 2 is the offset needed for the vector
		PLX							; Unwind the stack
		PLY
		PLZ
		CMP		#$E2				; Is absloc correct?
		BEQ		TE2_success
		EMU		scFail
TE2_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;  INW bp - increment word base page
;
TE3:
		LDA		#$FE			; Set up a word in the base page
		STA		ZPLOC
		LDA		#$01	
		STA		ZPLOC+1			; ZPLOC is $01FE
		INW		ZPLOC			; ZPLOC should mow be $01FF
		LDX		ZPLOC
		CPX		#$FF
		BEQ		TE3_testHigh1
		EMU		scFail			; Low byte not decremented
TE3_testHigh1:
		LDY		ZPLOC+1
		CPY		#$1
		BEQ		TE3_decAgain
		EMU		scFail			; High byte not the same
TE3_decAgain:
		INW		ZPLOC			; ZPLOC is $0200 
		LDX		ZPLOC
		BEQ		TE3_testHigh2
		EMU		scFail
TE3_testHigh2:
		LDY		ZPLOC+1
		CPY		#$2
		BEQ		TE3_success
		EMU		scFail
TE3_success:

;-------------------------------------------------------------------------------
;  SBC bp
;
TE5:
		SEC		
		LDA		#$2
		STA		ZPLOC
		LDA		#$E5
		SBC		ZPLOC
		BCS		TE5_cOK
		EMU		scFail
TE5_cOK:
		BMI		TE5_nOK
		EMU		scFail
TE5_nOK:
		cmp		#$E3
		BEQ		TE5_success
		EMU		scFail
TE5_success:
;-------------------------------------------------------------------------------
;  INC bp - increment base page
;
TE6:
		LDA		#$FE			; Set up a word in the base page
		STA		ZPLOC
		INC		ZPLOC			; ZPLOC should mow be $01FF
		LDX		ZPLOC
		CPX		#$FF
		BEQ		TE6_decAgain
		EMU		scFail			; Low byte not decremented
TE6_decAgain:
		INC		ZPLOC			; ZPLOC is $0200 
		LDX		ZPLOC
		BEQ		TE6_success
		EMU		scFail
TE6_success:
;-------------------------------------------------------------------------------
;  INX -
;
TE8:
		LDX		#$FF				; Set up a word in the base page
		INX							; Y should mow be $00
		BEQ		TE8_z
		EMU		scFail			; Low byte not decremented
TE8_z:
		BPL		TE8_pl
		EMU		scFail			; High byte not the same
TE8_pl:
		LDX		#$7F
		INX	
		BNE		TE8_nz
		EMU		scFail
TE8_nz:
		BMI		TE8_success
		EMU		scFail
TE8_success:
;-------------------------------------------------------------------------------
;  NOP
;
TEA:
		NOP
;-------------------------------------------------------------------------------
;  ROW rotate right word
;
TEB:
		LDA		#$81				; The low byte
		STA		ZPLOC
		LDA		#$71				; The high byte
		STA		ZPLOC+1				; ZPLOC contains $7181
		CLC
		ROW		ZPLOC
		BCS		TEB_cOK
		EMU		scFail				; carry was not set
TEB_cOK:
		BPL		TEB_nOK
		EMU		scFail				; N flag was set
TEB_nOK:
		LDA		ZPLOC				; Check the low byte
		CMP		#$C0	
		BEQ		TEB_lbOK
		EMU		scFail				; Low byte was wrong
TEB_lbOK:
		LDA		ZPLOC+1				; Check the high byte
		CMP		#$38
		BEQ		TEB_hbOK
		EMU		scFail				; high byte was not OK
TEB_hbOK:
		ROW		ZPLOC				; rotate again
		BCC		TEB_cOK2
		EMU		scFail				; carry was not set
TEB_cOK2:
		BMI		TEB_nOK2
		EMU		scFail				; N flag was set
TEB_nOK2:
		LDA		ZPLOC				; Check the low byte
		CMP		#$60	
		BEQ		TEB_lbOK2
		EMU		scFail				; Low byte was wrong
TEB_lbOK2:
		LDA		ZPLOC+1				; Check the high byte
		CMP		#$9C
		BEQ		TEB_success
		EMU		scFail				; high byte was not OK
TEB_success:
;-------------------------------------------------------------------------------
;  SBC abs
;
TED:
		SEC		
		LDA		#$2
		STA		ABSLOC
		LDA		#$ED
		SBC		ABSLOC
		BCS		TED_cOK
		EMU		scFail
TED_cOK:
		BMI		TED_nOK
		EMU		scFail
TED_nOK:
		cmp		#$EB
		BEQ		TED_success
		EMU		scFail
TED_success:
;-------------------------------------------------------------------------------
;  INC abs - increment absolute
;
TEE:
		LDA		#$FE			; Set up a word in the base page
		STA		ABSLOC
		INC		ABSLOC			; ZPLOC should mow be $01FF
		LDX		ABSLOC
		CPX		#$FF
		BEQ		TEE_decAgain
		EMU		scFail			; Low byte not decremented
TEE_decAgain:
		INC		ABSLOC			; ZPLOC is $0200 
		LDX		ABSLOC
		BEQ		TEE_success
		EMU		scFail
TEE_success:
;-------------------------------------------------------------------------------
;  SBC (bp),Y
;
TF1:
		SEC		
		LDA		#$2
		STA		ABSLOC
		LDA		#<(ABSLOC-2)
		STA		ZPLOC
		LDA		#>(ABSLOC-2)
		STA		ZPLOC+1
		LDA		#$F1
		LDY		#$2
		SBC		(ZPLOC),Y
		BCS		TF1_cOK
		EMU		scFail
TF1_cOK:
		BMI		TF1_nOK
		EMU		scFail
TF1_nOK:
		cmp		#$EF
		BEQ		TF1_success
		EMU		scFail
TF1_success:
;-------------------------------------------------------------------------------
;  SBC (bp),Y
;
TF2:
		SEC		
		LDA		#$2
		STA		ABSLOC
		LDA		#<(ABSLOC-2)
		STA		ZPLOC
		LDA		#>(ABSLOC-2)
		STA		ZPLOC+1
		LDA		#$F1
		LDZ_imm	$2
		SBC_bp_ind_z	ZPLOC
		BCS		TF2_cOK
		EMU		scFail
TF2_cOK:
		BMI		TF2_nOK
		EMU		scFail
TF2_nOK:
		cmp		#$EF
		BEQ		TF2_success
		EMU		scFail
TF2_success:
		LDZ_imm	$0
;-------------------------------------------------------------------------------
;  BEQ	word
;
TF3:
		BRA		TF3_skipBad
TF3_beforeBad:
		BRA		TF3_testCS
		EMU		scFail
TF3_skipBad:
		LDX		#$0						; Set the Z flag
		BEQ_W	TF3_beforeBad
		EMU		scFail
TF3_testCS:
		LDX		#$FF					; Clear the Z flag
		BEQ_W	TF3_setFail
		BRA		TF3_success
TF3_setFail:
		EMU		scFail
TF3_success:
;-------------------------------------------------------------------------------
;  PHW	push word immediate
;
TF4:
		TSX
		STX		ZPLOC					; Save the stack pointer to compare
		PHW_imm	$F3F4					; Push the word
		PLX
		PLY
		CPX		#$F4					; Check the low byte
		BEQ		TF4_lbOK
		EMU		scFail					; Low byte wrong
TF4_lbOK:
		CPY		#$F3					; Check the high byte
		BEQ		TF4_hbOK
		EMU		scFail					; High byte is wrong
TF4_hbOK:
		TSX								; Check the stack pointer is correct
		CPX		ZPLOC
		BEQ		TF4_success
		EMU		scFail					; Stack pointer is incorrect
TF4_success:
;-------------------------------------------------------------------------------
;  SBC bp,X
;
TF5:
		SEC		
		LDA		#$2
		STA		ZPLOC
		LDA		#$F5
		LDX		#$3
		SBC		ZPLOC-3,X
		BCS		TF5_cOK
		EMU		scFail
TF5_cOK:
		BMI		TF5_nOK
		EMU		scFail
TF5_nOK:
		cmp		#$F3
		BEQ		TF5_success
		EMU		scFail
TF5_success:
;-------------------------------------------------------------------------------
;  INC bp,X - increment base page
;
TF6:
		LDA		#$FE			; Set up a word in the base page
		STA		ZPLOC
		LDX		#$2
		INC		ZPLOC-2,X		; ZPLOC should mow be $01FF
		LDY		ZPLOC
		CPY		#$FF
		BEQ		TF6_decAgain
		EMU		scFail			; Low byte not decremented
TF6_decAgain:
		INC		ZPLOC-2,X		; ZPLOC is $0200 
		LDY		ZPLOC
		BEQ		TF6_success
		EMU		scFail
TF6_success:
;-------------------------------------------------------------------------------
;  SED - Set decimal flag
;
TF8:
		SED						; Set the decimal flag
		PHP						; Put status on the stack so we can bit test
		CLD						; Clear the decimal flag
		PLA						; Status into A
		AND		#$08			; D is bit 3 of the status register
		BNE		TF8_success
		EMU		scFail			; D was not set
TF8_success:
;-------------------------------------------------------------------------------
;  SBC abs,Y
;
TF9:
		SEC		
		LDA		#$2
		STA		ABSLOC
		LDA		#$F5
		LDY		#$3
		SBC		ABSLOC-3,Y
		BCS		TF9_cOK
		EMU		scFail
TF9_cOK:
		BMI		TF9_nOK
		EMU		scFail
TF9_nOK:
		cmp		#$F3
		BEQ		TF9_success
		EMU		scFail
TF9_success:
;-------------------------------------------------------------------------------
;  SBC abs,X
;
TFD:
		SEC		
		LDA		#$2
		STA		ABSLOC
		LDA		#$F5
		LDX		#$3
		SBC		ABSLOC-3,X
		BCS		TFD_cOK
		EMU		scFail
TFD_cOK:
		BMI		TFD_nOK
		EMU		scFail
TFD_nOK:
		cmp		#$F3
		BEQ		TFD_success
		EMU		scFail
TFD_success:
		
;-------------------------------------------------------------------------------
;  PHW	push word absolute
;
TFC:
		TSX
		STX		ZPLOC					; Save the stack pointer to compare
		LDA		#$FC					; Set up the location
		STA		ABSLOC
		LDA		#$FE
		STA		ABSLOC+1
		PHW_abs	ABSLOC					; Push the word
		PLX
		PLY
		CPX		#$FC					; Check the low byte
		BEQ		TFC_lbOK
		EMU		scFail					; Low byte wrong
TFC_lbOK:
		CPY		#$FE					; Check the high byte
		BEQ		TFC_hbOK
		EMU		scFail					; High byte is wrong
TFC_hbOK:
		TSX								; Check the stack pointer is correct
		CPX		ZPLOC
		BEQ		TFC_success
		EMU		scFail					; Stack pointer is incorrect
TFC_success:
;-------------------------------------------------------------------------------
;  INC abs,X - increment absolute
;
TFE:
		LDA		#$FE			; Set up a word in the base page
		STA		ABSLOC
		LDX		#$5
		INC		ABSLOC-5,X			; ZPLOC should mow be $01FF
		LDY		ABSLOC
		CPY		#$FF
		BEQ		TFE_decAgain
		EMU		scFail			; Low byte not decremented
TFE_decAgain:
		INC		ABSLOC-5,X			; ZPLOC is $0200 
		LDY		ABSLOC
		BEQ		TFE_success
		EMU		scFail
TFE_success:

;-------------------------------------------------------------------------------
; Decimal mode tests - Addition
decimal1:
		SED
		SEC
		LDA		#$58
		ADC		#$46
		CLD
		BCS		decimal1_cOK
		EMU		scFail			; carry should be set
decimal1_cOK:
		CMP		#$05
		BEQ		decimal1Success
		EMU		scFail			; result was not $105
decimal1Success:
;-------------------------------------------------------------------------------
; Decimal mode tests - subtraction
bcdSBC1: 
		SED
		SEC
		LDA 	#$46
		SBC		#$12
		CLD
		BCS		bcdSBC1_cOK
		EMU		scFail			; carry should be set
bcdSBC1_cOK:
		CMP		#$34
		BEQ		bcdSBC1_sumOK
		EMU		scFail			; result was not $134
bcdSBC1_sumOK:				
bcdSBC2: 
		SED
		SEC
		LDA 	#$40
		SBC		#$13
		CLD
		BCS		bcdSBC2_cOK
		EMU		scFail			; carry should be set
bcdSBC2_cOK:
		CMP		#$27
		BEQ		bcdSBC2_sumOK
		EMU		scFail			; result was not $134
bcdSBC2_sumOK:				

;===============================================================================
; If the tests complete, go round and do them again
SUCCESS:
		LDA		#$00				; Clear the flag at the bottom of RAM 
		STA		successFlag			; to indicate success
		STA		successFlag+1				
		JMP		START
		
				
;-------------------------------------------------------------------------------
;	Reset the CPU to a known state
;   A, X, Y set to zero.
;   All flags clear except Z = 1, E unaffected, I unaffected
;
.proc	RESETCPU
		LDA		#0
		TAX				; Clear X and Y, also N and Z = 1
		TAY				
		CLC				; Clear other flags
		CLD
		CLV
		RTS				; All done
		EMU 	scFail				; Make sure we don't run off the end without knowing
.endproc
;-------------------------------------------------------------------------------
;  A test subroutine - just increments the accumulator
;
.proc	IncAcc
		ADC	#1
		RTS
		EMU 	scFail
.endproc
;-------------------------------------------------------------------------------
;  A test interrupt routine - sets a flag to say an interrupt occurred
;
.proc	TestInterrupt
;
;	First thing is to clear the raised interrupt so that when the interrupt
;   flag is cleared, we don't immediately raise another interrupt.
;
		EMU		simulateClearInterrupt
;
;	Now we just set a flag to say the interrupt occurred
;
		PHA					; Save the accumulator
		LDA		#$1			; Set the flag
		STA		interrupted
		PLA					; Restore the accumulator
		RTI					; 
		EMU 	scFail					; make sure we don't overrun
.endproc

;-------------------------------------------------------------------------------
;  A test BRK routine - sets a flag to say a BRK occurred
;
.proc	TestBRK
		PHA					; Save the accumulator and X
		PHX					
;
;		Check the BRK flag is set
;
		TSX					; Offset on the stack page of the current sp
		INX					; sp points to 1 below the stack bottom
		INX					; skip the two saved registers
		INX
		LDA		#$10		; BRK bit is bit 4 of the flags
		BIT		$0100,X		; Test bit 4 of the status on the stack
		BNE		BRKSet
		EMU		scFail		; This means we weren't called via BRK
BRKSet:
;
;	Now we just set a flag to say the interrupt occurred
;
		LDA		#$2			; Set the flag
		STA		interrupted
		PLX					; Restore X
		PLA					; and the accumulator
		RTI					; 
		EMU 	scFail					; make sure we don't overrun
.endproc
;-------------------------------------------------------------------------------
;		Data
;
ABYTE:	.BYTE	$AA
ABSLOC:	.word	$0
ABSIND:	.word	$0
interrupted:
		.byte	$0
TOP:    .END           	; end of listing

