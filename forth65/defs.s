;
; Definitions used to emulate a 65CE02 assembler.  Also provides defs for 
; "BIOS" hooks.
;
; We assemble as a 65SC02 because that gives us more of our supported opcodes.
;
		.psc02
		.setcpu			"65SC02"
		
		interruptVector	= $FFFE		; Interrupt routine address loaded here
		nmiVector		= $FFFA		; NMI Interrupt routine address loaded here	
		resetVector     = $FFFC     ; The reset vector. This contains the address
		                            ; from which the 6502 will start on hard reset.	
;
;   These are symbols for instructions that are not supported by the ca65
;	assembler.
;
		AUG		  = $5C				; 65CE02 AUG instruction
		TSB_bp	  = $04				; 65EC02 TSB base page instruction
		TRB_bp	  = $14				; 65EC02 TRB base page instruction
		TSB_abs	  = $0C				; 65EC02 TSB absolute instruction
		TRB_abs	  = $1C				; 65EC02 TRB absolute instruction
		INZ		  = $1B				; 65CE02 INZ instruction
		DEZ		  = $3B				; 65CE02 DEZ instruction
		TAZ		  = $4B				; 65CE02 TAZ instruction
		NEG_A	  = $42				; 65CE02 NEG instruction
		ASR_A	  = $43				; 65CE02 ASR instruction
		ASR_bp	  = $44				; 65CE02 ASR bp instruction
		ASR_bp_x  = $54				; 65CE02 ASR bp,X instruction
;
;	65EC02 RMB base page instructions
;
		RMB0	= $07
		RMB1	= $17
		RMB2	= $27
		RMB3	= $37
		RMB4	= $47
		RMB5	= $57
		RMB6	= $67
		RMB7	= $77
;
;	65CE02 BBR base page instructions
;
		BBR0	= $0F
		BBR1	= $1F
		BBR2	= $2F
		BBR3	= $3F
		BBR4	= $4F
		BBR5	= $5F
		BBR6	= $6F
		BBR7	= $7F
		
		ORA_bp_ind_z	= $12		; Base page indirect indexed by Z
		AND_bp_ind_z	= $32		; Base page indirect indexed by Z
		EOR_bp_ind_z	= $52		; Base page indirect indexed by Z
		BPL_word		= $13		; 65EC02 BPL with word offset 
		BMI_word		= $33		; 65EC02 BMI with word offset 
		BVC_word		= $53		; 65EC02 BVC with word offset 
		JSR_abs_ind		= $22		; 65CE02 JSR (abs)
		JSR_absx_ind	= $23		; 65CE02 JSR (abs,X)
		BIT_bpx			= $34		; BIT bp,X
		BIT_absx		= $3C		; BIT abs,X
;-------------------------------------------------------------------------------
; RTN macro not really sure what this does.  Assume it adjusts the SP before
; returning
;
.macro	RTN	offset
		.byte	$62
		.byte	offset
.endmacro

.macro	ASW		address
		.byte	$CB
		.word	address
.endmacro

;-------------------------------------------------------------------------------
; BSR word
.macro	BSR	offset
		.byte	$63
		.word	offset-*-2
.endmacro

.macro	BRA_W	offset
		.byte	$83
		.word	(offset-*-2) & $ffff
.endmacro

.macro	BCC_W	offset
		.byte	$93
		.word	(offset-*-2) & $ffff
.endmacro

.macro	BCS_W	offset
		.byte	$B3
		.word	(offset-*-2) & $ffff
.endmacro

.macro	BEQ_W	offset
		.byte	$F3
		.word	(offset-*-2) & $ffff
.endmacro

.macro	BNE_W	offset
		.byte	$D3
		.word	(offset-*-2) & $ffff
.endmacro

.macro	BVS_W	offset
		.byte	$73
		.word	offset-*-2
.endmacro

.macro	DEW		bp
		.byte	$C3,bp
.endmacro

.macro	INW		bp
		.byte	$E3,bp
.endmacro

.macro	CMP_bp_ind_z	bp
		.byte	$D2,bp
.endmacro

.macro	CPZ_bp	bp
		.byte	$D4, bp
.endmacro

.macro	CPZ_imm	number
		.byte	$C2
		.byte	number
.endmacro

.macro	CPZ_abs	number
		.byte	$DC
		.word	number
.endmacro

.macro	LDA_bp_ind_z number
		.byte	$B2
		.byte	number
.endmacro

.macro	LDA_svi_y	d
		.byte	$E2
		.byte 	d
.endmacro

.macro	LDZ_imm	number
		.byte	$A3
		.byte	number
.endmacro

.macro	LDZ_abs	number
		.byte	$AB
		.word	number
.endmacro

.macro	LDZ_absx	number
		.byte	$BB
		.word	number
.endmacro

.macro	ROW	address
		.byte $EB
		.word address
.endmacro

.macro	PHW_imm	number
		.byte	$F4
		.word	number
.endmacro

.macro	PHW_abs	number
		.byte	$FC
		.word	number
.endmacro

.macro	PHZ
		.byte $DB
.endmacro

.macro	PLZ
		.byte $FB
.endmacro

.macro	SBC_bp_ind_z bp
		.byte $F2, bp
.endmacro	
.macro	STZ_abs	address
		.byte	$9C
		.word	address
.endmacro

.macro	STZ_absx	address
		.byte	$9E
		.word	address
.endmacro

.macro	STZ_bp	zpAddress
		.byte	$64
		.byte	zpAddress
.endmacro

.macro	STZ_bpx	zpAddress
		.byte	$74
		.byte	zpAddress
.endmacro

.macro	TYS
		.byte	$2B
.endmacro

.macro	TZA
		.byte	$6B
.endmacro

.macro	TSY
		.byte	$0B
.endmacro

.macro	TAB
		.byte	$5B
.endmacro

.macro	TBA
		.byte	$7B
.endmacro

.macro	CLE
		.byte	$02
.endmacro

.macro	SEE
		.byte	$03
.endmacro

.macro	ADC_bp_ind_z	bp
		.byte	$72, bp
.endmacro

.macro	STA_svi_y	d
		.byte	$82
		.byte 	d
.endmacro

.macro	STA_bp_ind_z	bp
		.byte	$92
		.byte 	bp
.endmacro

.macro	STA_absy	address
		.byte	$99
		.word 	address
.endmacro

.macro	STX_absy	address
		.byte	$9B
		.word 	address
.endmacro

.macro	STY_absx	address
		.byte	$8B
		.word 	address
.endmacro

.macro	SMB	bit, address
		.byte	(bit << 4) | $87
		.byte	address
.endmacro

.macro	BBS	bit, loc, offset
		.byte		(bit << 4) | $8F
		.byte		loc
		.byte		offset-*-1
.endmacro

;-------------------------------------------------------------------------------
;	A macro that emulates an "emulator" call.  The emulator uses AUG #$FF as a
;   hook into the underlying emulator so that complex instructions not part of
;	the instruction set can be executed.
;
.macro	EMU	callNumber
		.byte	AUG
		.byte	$FF
		.word	callNumber
.endmacro

		scFail	= $0000	; sys call that makes the CPU fail.  The emulator will 
						; report a failure and give the address of the system
						; call.

;-------------------------------------------------------------------------------
; "BIOS" routines
; These are faked by putting a EMU 	 at the location and then the simulator 
; examines the program counter and runs Objective-C code accordingly.  
;
		simulateInterrupt		= $0001
		simulateClearInterrupt	= $0002
		pullDownNMI				= $0003
		pullUpNMI				= $0004
		traceOn					= $0005
		traceOff				= $0006
		outputAAsASCII			= $D2C1
		outputAAsASCIIToError	= $D2C2
		inputAChar				= $D1DC
