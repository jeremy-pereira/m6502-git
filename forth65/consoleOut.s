;
		.include "defs.s"
				
		ORIG      		= $0200     ; program start.
									; base page addressing

		.ORG   ORIG
;-------------------------------------------------------------------------------
;		Set up the stack
;
START:	LDX		#$FF
		LDY		#01
		TYS							; This is the high byte of SP
		TXS							; The low byte of SP
		JSR		RESETCPU			; MAke sure the rest of the CPU is tidy
		LDX		#$20				; Used as a counter so we are not infinite
firstChar:
		LDA		#' '				; Loop through the printable characters
nextChar:
		EMU		outputAAsASCII		; Output the character
		INC		A
		CMP		#$7F
		BEQ		firstChar
		DEX
		BNE		nextChar
		LDA		#$0A
		EMU		outputAAsASCII
;-------------------------------------------------------------------------------
;		Try reading a character
;
		LDA		#$0
		EMU		inputAChar
		EMU		inputAChar
		EMU		inputAChar
		EMU		inputAChar
		EMU		inputAChar
		EMU		scFail				; Stops the processor early
				
;-------------------------------------------------------------------------------
;	Reset the CPU to a known state
;   A, X, Y set to zero.
;   All flags clear except Z = 1, E unaffected, I unaffected
;
.proc	RESETCPU
		LDA		#0
		TAX				; Clear X and Y, also N and Z = 1
		TAY				
		CLC				; Clear other flags
		CLD
		CLV
		RTS				; All done
		EMU 	scFail				; Make sure we don't run off the end without knowing
.endproc
TOP:    .END           	; end of listing
