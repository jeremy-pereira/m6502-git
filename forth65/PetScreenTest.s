; Verify decimal mode behavior
; This is based on a program found at 
; http://www.6502.org/tutorials/decimal_mode.html
;
; Returns:
;   ERROR = 0 if the test passed
;   ERROR = 1 if the test failed
;
; This routine requires 17 bytes of RAM -- 1 byte each for:
;   AR, CF, DA, DNVZC, ERROR, HA, HNVZC, N1, N1H, N1L, N2, N2L, NF, VF, and ZF
; and 2 bytes for N2H
;
; Variables:
;   N1 and N2 are the two numbers to be added or subtracted
;   N1H, N1L, N2H, and N2L are the upper 4 bits and lower 4 bits of N1 and N2
;   DA and DNVZC are the actual accumulator and flag results in decimal mode
;   HA and HNVZC are the accumulator and flag results when N1 and N2 are
;     added or subtracted using binary arithmetic
;   AR, NF, VF, ZF, and CF are the predicted decimal mode accumulator and
;     flag results, calculated using binary arithmetic
;
; This program takes approximately 1 minute at 1 MHz (a few seconds more on
; a 65C02 than a 6502 or 65816)
;
		.include "defs.s"
		
		screenBase = $8000		; PET screen RAM is at 32768
		screenCols	= 40		; 40 character PET
		screenRows	= 25		; 25 Rows
		screenSize 	= screenCols * screenRows
		pageBase	= $10	; ZP address of a screen counter
		
		.org	$200
START:
		JSR		TEST
		LDA		ERROR
		BEQ		SUCCESS
		EMU		scFail
SUCCESS:
		JMP		START
		EMU		scFail
		
ERROR:	.byte	0

		
TEST:   
;
;		First try filling the whole screen with a single character
;
		LDA		#$0			; Start at zero
screenLoop:
		JSR		fillScreen
		INC		A			; Next character
		BNE		screenLoop
;
;		Now do a diagonal line.  First clear the screen.
;
;		A will contain the character to use on the line.
;		Y will contain the column on the current line.
;
diagonalStart:
		LDA		#$20		; Space character
		JSR 	fillScreen	
		LDA		#$0			; The CBM has no non printing characters		
		
diagonalLoop:
		LDY		#<screenBase
		STY		pageBase
		LDY		#>screenBase
		STY		pageBase+1
		
		LDY		#$0
nextLine:
		STA		(pageBase),Y
;
;		Next line
;
		JSR		incrementLine
		INY
		CPY		#screenRows
		BNE		nextLine
;
;		Do it again for the next character
;
		INC		A
		BNE		diagonalLoop
		
		JMP		TEST	; Round we go forever
;-------------------------------------------------------------------------------
;
;		Fill the screen with a given character.
;		A contains the character we are going to put on the screen
;
;		This uses 8 bytes of stack space including the return address (including
;		calls to fillPage).
;
fillScreen:
;
;		First save the X register as we use it for the page number.
;
		PHX						; X register will be used for the character
;
;		The fill is done by filling four pages with a character.
;
		LDX		#>screenBase	; First page number	
		JSR		fillPage		; Fill the first page
		INX
		JSR		fillPage		; Fill the second page
		INX
		JSR		fillPage		; Fill the third page
		INX
		JSR		fillPage		; Fill the fourth page
		PLX						; Restore X
		RTS						; Restore the PC
;-------------------------------------------------------------------------------
;
;		Fill a page with a given character.
;		On entry, A contains the character we are going to put in the page
;                 X contains the page number
;
;		This uses 5 bytes of stack space including the return address
;
fillPage:
;
;		Save stuff that we might destroy.  The Y reg and the ZP location we will
;		use.
;
		PHY
		LDY		pageBase
		PHY
		LDY		pageBase+1
		PHY
;
;		Set up the ZP location and the Y index register
;
		STX		pageBase+1
		LDY		#$0
		STY		pageBase
;
;		The fill loop
;
fillPage_loop:
		STA		(pageBase),Y
		INY
		BNE		fillPage_loop
		
fillPage_rtn:
		PLY
		STY		pageBase+1
		PLY
		STY		pageBase
		PLY
		RTS
;-------------------------------------------------------------------------------
; 		Point the page base at the beginning of the next line.  Essentially, we
;		add the number of screen columns to it with a standard 16 bit addition.
;
;		This uses 3 bytes of stack space including the return address
;
incrementLine:
		PHA						; A is needed to do the addition.
;
;		Low byte first
;		
		LDA		pageBase
		CLC
		ADC		#screenCols
		STA		pageBase
;
;		Now the high byte
;
		LDA		pageBase+1
		ADC		#0
		STA		pageBase+1
		
		PLA
		RTS
