//
//  ByteBusTests.swift
//  M6502
//
//  Created by Jeremy Pereira on 29/06/2015.
//
//

import XCTest
import Swift6502

class ByteBusTests: XCTestCase
{
    var theBus: Bus!
    var setCalled: Bool = false
    var value: S65Byte = 0

    override func setUp()
    {
        super.setUp()
        theBus = Bus(value: 0xff)
        setCalled = false
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of 
        // each test method in the class.
        super.tearDown()
    }

    func testConnect()
    {
        theBus.connect(self, mask: 0xf0, notifier: setValueFromBus)
        XCTAssert(setCalled && value == 0xf0, "Connect failed")
    }

    func testValue()
    {
        theBus.connect(self, mask: 0xf0, notifier: setValueFromBus)
        setCalled = false
        theBus.value = 0x3c
        XCTAssert(setCalled && value == 0x30, "Value change failed, should be 0x30, is 0x\(value.hexString)")
    }

    func testMask()
    {
        theBus.value = 0x3c
        theBus.connect(self, mask: 0xf0, notifier: setValueFromBus)
        setCalled = false
        theBus.changeMask(0x0f, forObject: self)
        XCTAssert(setCalled && value == 0x0c, "Value change failed, should be 0x0c, is 0x\(value.hexString)")
    }

    func testWireForBit()
    {
        let wire = theBus.wireForBit(5)
        theBus.value = 0b00100000
        XCTAssert(wire.value, "Wire has wrong value: should be true")
        theBus.value = 0b11011111
        XCTAssert(!wire.value, "Wire has wrong value: should be false")
    }

    func testSetWire()
    {
        theBus.value = 0
        let wire = theBus.wireForBit(7)

		wire.value = true

        XCTAssert(theBus.value == 0x80, "Bus has wrong value: should be 0x80, is 0x\(theBus.value.hexString)")
    }

    func setValueFromBus(_ newValue: S65Byte)
    {
        assert(!setCalled, "Should set setCalled to false")
        setCalled = true
		value = newValue
    }
}
