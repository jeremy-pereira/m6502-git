//
//  WirreTests.swift
//  M6502
//
//  Created by Jeremy Pereira on 28/06/2015.
//
//

import XCTest
import Swift6502

class WireTests: XCTestCase
{
    var theWire: Wire!
    var setCalled: Bool = false
    var value: Bool = false
    var andGate: AndGate!

    override func setUp()
    {
        super.setUp()
        theWire = Wire()
        andGate = AndGate()
        andGate.outputWire = Wire()
        setCalled = false
        value = false
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation 
        // of each test method in the class.
        super.tearDown()
    }

    func testConnect()
    {
        theWire.connectNotifier(setValueFromWire)
        XCTAssert(setCalled && !value, "Wire not initialised on connect")
    }

    func testSetDifferent()
    {
        theWire.connectNotifier(setValueFromWire)
        setCalled = false
        theWire.value = true
        XCTAssert(setCalled && value, "object not set when wire set")
    }

    func testSame()
    {
        theWire.value = true
        theWire.connectNotifier(setValueFromWire)
        setCalled = false
        theWire.value = true
        XCTAssert(!setCalled && value, "object not set when wire set")
    }

    func testPerformance()
    {
        self.measure()
        {
            self.theWire.connectNotifier(self.setValueFromWire)
            for i in 0 ..< 100000
            {
				self.theWire.value = i % 2 == 0
            }
        }
    }

    func setValueFromWire(_ newValue: Bool)
    {
        setCalled = true
        value = newValue
    }

    func testAndGate()
    {
        let inputWire = Wire()
        andGate.addInputWire(inputWire)
		andGate.outputWire.connectNotifier(setValueFromWire)
        setCalled = false
        value = false
        inputWire.value = true
        XCTAssert(setCalled && value, "object not set when wire set")
    }

    func testAndGateTwoInputs()
    {
        let inputWire = Wire()
        andGate.addInputWire(inputWire)
        andGate.outputWire.connectNotifier(setValueFromWire)
        setCalled = false
        value = false
        inputWire.value = true
        let input2 = Wire()
        setCalled = false
        andGate.addInputWire(input2)
        XCTAssert(setCalled && !value, "adding input2 should set and low")
        setCalled = false
        input2.value = true
        XCTAssert(setCalled && value, "input2 = true should set and high")
        setCalled = false
        inputWire.value = false
        XCTAssert(setCalled && !value, "inputWire = false should set and low")
    }
}
