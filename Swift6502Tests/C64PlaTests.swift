//
//  C64PlaTests.swift
//  Swift6502Tests
//
//  Created by Jeremy Pereira on 04/10/2018.
//

import XCTest
@testable import Swift6502

extension  C64RamPla.MemoryType
{
    var testPattern: S65Byte?
    {
        switch self
        {
        case .ram: return 0xaa
        case .basicRom: return 0xbb
        case .kernalRom: return 0xcc
        case .charRom: return 0xdd
        case .cartridge: return 0xee
        case .io: return nil
        }
    }
    static let allTypes: [C64RamPla.MemoryType] = [.ram, .basicRom, .kernalRom, .charRom, .cartridge, .io]
}

class C64PlaTests: XCTestCase
{
	private let pla = C64RamPla()

    private func setTestPattern(memoryType: C64RamPla.MemoryType)
    {
        if let pattern = memoryType.testPattern
        {
            pla.fill(memoryType, pattern: pattern)
        }
    }

    override func setUp()
    {
        C64RamPla.MemoryType.allTypes.forEach
        {
           setTestPattern(memoryType: $0)
        }
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    private func checkValues(pla: C64RamPla, expected: [S65Byte?], function: String = #function)
    {
        assert(expected.count == C64RamPla.bankCount, "Incorrect bank count: \(expected.count), expected: \(C64RamPla.bankCount)")
        var bankChecked: [Bool] = Array<Bool>(repeating: false, count: C64RamPla.bankCount)
        for address in 0 ... S65Address.max
        {
            if let expectedByte = expected[address.c64BankNumber]
            {
                let actualByte = pla.get(address: address)
                if actualByte != expectedByte  && !bankChecked[address.c64BankNumber]
                {
                    XCTFail("\(function): $\(address.hexString) expected $\(expectedByte.hexString), got $\(actualByte.hexString), bank: \(address.c64BankNumber)")
					bankChecked[address.c64BankNumber] = true
                }
            }
        }
    }


    func testRam()
    {
		pla.mode = [.charEn, .game, .exRom ]

        checkValues(pla: pla,
               expected: Array<S65Byte?>(repeating: C64RamPla.MemoryType.ram.testPattern,
                                             count: C64RamPla.bankCount))
    }

    func testInitialConfig()
    {
        checkValues(pla: pla,
               expected: [
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.basicRom,
            C64RamPla.MemoryType.basicRom,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.io,
            C64RamPla.MemoryType.kernalRom,
            C64RamPla.MemoryType.kernalRom
            ].map({ $0.testPattern }))
    }

    func testSwitchNoCartridge()
    {
        let defaultExpected: [S65Byte?] = [
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.basicRom,
            C64RamPla.MemoryType.basicRom,
            C64RamPla.MemoryType.ram,
            C64RamPla.MemoryType.io,
            C64RamPla.MemoryType.kernalRom,
            C64RamPla.MemoryType.kernalRom
            ].map({ $0.testPattern })

        var expected = defaultExpected

        checkValues(pla: pla,
                    expected: expected, function: #function + " (1) initial config")
        pla.mode = [.exRom, .game, .charEn, .hiRam]
        expected[S65Address(0xa000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        expected[S65Address(0xb000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        checkValues(pla: pla,
                    expected: expected, function: #function + " (2) switched out basic ROM")
        pla.mode = [.game, .charEn, .hiRam]
        checkValues(pla: pla,
                    expected: expected, function: #function + " (3) switched out basic ROM")
        pla.mode = [.exRom, .game, .charEn, .loRam]
        expected[S65Address(0xd000).c64BankNumber] = 0 // Make sure IO is still there
        expected[S65Address(0xe000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        expected[S65Address(0xf000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        checkValues(pla: pla,
                    expected: expected, function: #function + " (4) switched out all ROM")
        pla.mode = [.game, .charEn, .loRam]
        checkValues(pla: pla,
                    expected: expected, function: #function + " (5) switched out all ROM")
        pla.mode = [.exRom, .game, .charEn]
        expected = Array<S65Byte?>(repeating: C64RamPla.MemoryType.ram.testPattern, count: C64RamPla.bankCount)
        checkValues(pla: pla,
                    expected: expected, function: #function + " (6) switched out everything, just RAM")
        pla.mode = [.exRom, .game]
        checkValues(pla: pla,
                    expected: expected, function: #function + " (8) switched out everything, just RAM")
        pla.mode = [.exRom, .game, .hiRam, .loRam]
		expected = defaultExpected
        expected[S65Address(0xd000).c64BankNumber] = C64RamPla.MemoryType.charRom.testPattern
        checkValues(pla: pla,
                    expected: expected, function: #function + " (9) switched out IO for char ROM")
        pla.mode = [.exRom, .game, .hiRam]
        expected[S65Address(0xa000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        expected[S65Address(0xb000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        checkValues(pla: pla,
                    expected: expected, function: #function + " (10) switched out basic and IO for char ROM")
        pla.mode = [.game, .hiRam]
        checkValues(pla: pla,
                    expected: expected, function: #function + " (11) switched out basic and IO for char ROM")
        pla.mode = [.exRom, .game, .loRam]
        expected[S65Address(0xe000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        expected[S65Address(0xf000).c64BankNumber] = C64RamPla.MemoryType.ram.testPattern
        checkValues(pla: pla,
                    expected: expected, function: #function + " (12) switched out basic, kernal and IO for char ROM")
        pla.mode = [.game, .loRam]
        checkValues(pla: pla,
                    expected: expected, function: #function + " (13) switched out basic, kernal and IO for char ROM")
        pla.mode = [.game, .charEn]
        expected = Array<S65Byte?>(repeating: C64RamPla.MemoryType.ram.testPattern, count: C64RamPla.bankCount)
        checkValues(pla: pla,
                    expected: expected, function: #function + " (14) switched out everything")
       	pla.mode = [.game]
        checkValues(pla: pla,
                    expected: expected, function: #function + " (15) switched out everything")
        pla.mode = [.charEn]
        checkValues(pla: pla,
                    expected: expected, function: #function + " (16) switched out everything")
        pla.mode = []
        checkValues(pla: pla,
                    expected: expected, function: #function + " (17) switched out everything")
        pla.mode = [.charEn, .loRam]
        expected[S65Address(0xd000).c64BankNumber] = 0 // IO
        checkValues(pla: pla,
                    expected: expected, function: #function + " (18) switched out everything except IO")
        pla.mode = [.loRam]
        expected = Array<S65Byte?>(repeating: C64RamPla.MemoryType.ram.testPattern, count: C64RamPla.bankCount)
        checkValues(pla: pla,
                    expected: expected, function: #function + " (19) switched out everything")
   }
}
