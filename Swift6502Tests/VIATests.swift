//
//  VIATests.swift
//  M6502
//
//  Created by Jeremy Pereira on 06/07/2015.
//
//

import XCTest
import Swift6502

class VIATests: XCTestCase
{
    var via: VIA6522!
    var setCalled = false
    var value = false
    var portValue: S65Byte = 0

    override func setUp()
    {
        super.setUp()
		via = VIA6522()
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testMemoryMapping()
    {
        XCTAssert(via.portCount == 16, "Wrong port count")
    }

    func testCA2ManualOut()
    {
		via.ca2.connectNotifier(notifier)
        var pcr: S65Byte = 14
        setCalled = false
        via.didWrite(12, byte: pcr)
        XCTAssert(setCalled && value, "Failed to set CA2")
        setCalled = false
        pcr = 12
        via.didWrite(12, byte: pcr)
        XCTAssert(setCalled && !value, "Failed to set CA2")
    }

    func testPortBOut()
    {
        setCalled = false
        via.portB.connect(self, mask: 0xff, notifier: portNotifier)
        var byte: S65Byte = 0x1e
        via.didWrite(2, byte: byte)	// Set the output mask
        setCalled = false
        byte = 0xff
        via.didWrite(0, byte: byte)	// Write all 1s
        XCTAssert(setCalled && portValue == 0x1e, "Invalid port output 0x\(portValue.hexString)")
    }

    func testTimers()
    {
        via.drive(cycleDelta: 5)
        var byte: S65Byte = via.willRead(4)
        XCTAssert(byte == 0xfb, "Timer 1 not counting")
        byte = via.willRead(5)
        XCTAssert(byte == 0xff, "Timer 1 over counting")
        byte = via.willRead(8)
        XCTAssert(byte == 0xfb, "Timer 2 not counting")
        byte = via.willRead(9)
        XCTAssert(byte == 0xff, "Timer 2 over counting")
        /*
         *  Put timer 2 in output mode
		 */
        byte = 0x5
        via.didWrite(8, byte: byte)
        byte = 0x0
        via.didWrite(9, byte: byte)
        via.drive(cycleDelta: 6)
        byte = via.willRead(4)
        XCTAssert(byte == 0xf5, "Timer 1 not counting")
        byte = via.willRead(5)
        XCTAssert(byte == 0xff, "Timer 1 over counting")
		byte = via.willRead(13)
        let interrupts = VIAInterrupt(rawValue: byte)
        XCTAssert(interrupts.contains(.Timer2), "Timer 2 interrupt flag not set")
        XCTAssert(!interrupts.contains(.InterruptOccurred), "Timer 2 IER should have prevented interrupt")
    }

    func testT2Latch()
    {
        via.drive(cycleDelta: 5)
        var byte: S65Byte = 0
        byte = via.willRead(8)
        XCTAssert(byte == 0xfb, "Timer 2 not counting")
        byte = via.willRead(9)
        XCTAssert(byte == 0xff, "Timer 2 over counting")
        /*
         *  Put timer 2 in output mode
         */
        byte = 0x5
        via.didWrite(8, byte: byte)
        byte = via.willRead(8)
        XCTAssert(byte == 0xfb, "Timer 2 counter should not change")

        byte = 0x0
        via.didWrite(9, byte: byte)
        byte = via.willRead(8)
        XCTAssert(byte == 0x05, "Timer 2 counter should change")
        via.drive(cycleDelta: 6)
        byte = via.willRead(13)
        let interrupts = VIAInterrupt(rawValue: byte)
        XCTAssert(interrupts.contains(.Timer2), "Timer 2 interrupt flag not set")
        XCTAssert(!interrupts.contains(.InterruptOccurred), "Timer 2 IER should have prevented interrupt")
    }

    func testT1Latch()
    {
        via.drive(cycleDelta: 5)
        var byte: S65Byte = 0
        byte = via.willRead(4)
        XCTAssert(byte == 0xfb, "Timer 1 not counting")
        byte = via.willRead(5)
        XCTAssert(byte == 0xff, "Timer 1 over counting")
        /*
         *  Write to the latch
         */
        byte = 0x5
        via.didWrite(6, byte: byte)
        byte = 0x0
        via.didWrite(7, byte: byte)
        byte = via.willRead(4)
        XCTAssert(byte == 0xfb, "Timer 1 counter should be unaffected")

        via.drive(cycleDelta: 6)
        byte = via.willRead(4)
        XCTAssert(byte == 0xf5, "Timer 1 not counting")
        byte = via.willRead(5)
        XCTAssert(byte == 0xff, "Timer 1 over counting")
        byte = via.willRead(6)
        XCTAssert(byte == 0x5, "Timer 1 latch should be unaffected")
    }


    func notifier(_ newValue: Bool)
    {
    	assert(!setCalled, "setCalled should be cleared before we start")
        setCalled = true
        value = newValue
    }

    func portNotifier(_ newValue: S65Byte)
    {
        assert(!setCalled, "setCalled should be cleared before we start")
		setCalled = true
        portValue = newValue
    }
}
