//
//  CoverageTests.swift
//  M6502
//
//  Created by Jeremy Pereira on 12/06/2015.
//
//

import XCTest
@testable import Swift6502
import Foundation

extension Mos65xx
{
    var pcRegister: S65Address { return self.register16("PC")! }
    var aRegister: S65Byte
    {
        get { return self.register8("A")! }
        set { precondition(self.set(register: "A", byte: newValue), "We don't have the accumulator") }
    }
}

fileprivate let programStart: S65Address = 0x0200

class CoverageTests: XCTestCase
{
    var cpu: Mos65xx = make6502()

    override func setUp()
    {
        super.setUp()
        cpu.set(word: programStart,  at: 0xFFFC)
		let myBundle = Bundle(for: type(of: self))
        let binaryFileUrl = myBundle.url(forResource: "coveragetest", withExtension: "bin")
		if let binaryFileUrl = binaryFileUrl
        {
			let program = try? Data(contentsOf: binaryFileUrl)

			if let program = program
            {
                var programBytes = [S65Byte](repeating: 0, count: program.count)
                program.copyBytes(to: &programBytes, count: programBytes.count)
                let currentAddress = programStart
                cpu.set(bytes: programBytes, at: currentAddress)
            }
            else
            {
                fatalError("Could not read test coverage program")
            }
        }
        else
        {
            fatalError("Could not locate coverage test binary")
        }
        cpu.fatalErrorHandler =
        {
            (cpu, message: @autoclosure () -> String) in
            XCTFail(message())
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation 
        // of each test method in the class.
        super.tearDown()
    }

    func testADCAndLDA()
    {
        let     CLC: S65Byte = 0x18
        let LDA_imm: S65Byte = 0xa9
        let ADC_imm: S65Byte = 0x69
        let counter = programStart;
        cpu.set(bytes: [CLC, LDA_imm, 2, ADC_imm, 3],
                   at: counter)
        cpu.notReset = false
        cpu.notReset = true
        cpu.runForCycles(6)
        XCTAssert(!cpu.fatalErrorHappened, "Failed to run reset cycle")
        // Should have read the CLC instruction
        XCTAssert(cpu.pcRegister == programStart + 1, "Reset failed to initialise the PC")

        cpu.runForCycles(2)
        XCTAssert(!cpu.fatalErrorHappened, "Failed to run CLC cycle")
        cpu.runForCycles(2)
        XCTAssert(!cpu.fatalErrorHappened, "Failed to run LDA cycle")
        XCTAssert(cpu.aRegister == 2, "Load failed")
        cpu.runForCycles(2)
        XCTAssert(!cpu.fatalErrorHappened, "Failed to run ADC cycle")
        XCTAssert(cpu.aRegister == 5, "Add failed")
    }

    func testSTAAbs()
    {
        let STA_abs: S65Byte = 0x8d
        let targetAddress: S65Address = 0x0401
        let data: S65Byte = 0xaa
        var counter = programStart;
        cpu.set(bytes: [ STA_abs, targetAddress.lowByte, targetAddress.highByte], at: counter++)
        cpu.notReset = false
        cpu.notReset = true
        cpu.runForCycles(6)
        XCTAssert(!cpu.fatalErrorHappened, "Failed to run reset cycle")
        cpu.aRegister = data
        cpu.runForCycles(4)
        let result = cpu.byte(at: targetAddress)
        XCTAssert(result == data, "Invalid result $\(result.hexString)")
        let pc = cpu.pcRegister
        XCTAssert(pc == 0x204, "pc is wrong \(pc.hexString)")
    }


	func testCoverage()
    {
        cpu.notReset = false
        cpu.notReset = true
        cpu.trace = true
        cpu.runForCycles(20000)
        let firstWord = cpu.word(at: 0)
        XCTAssert(!cpu.fatalErrorHappened, "Run failed with CPU fatal error")
        XCTAssert(firstWord == 0, "Tests did not complete, 0 loc = $\(firstWord.hexString)")
    }

    func testPerformanceExample()
    {
        self.measure()
        {
			self.cpu.runForCycles(1_000_000)
        }
    }
}
