//
//  PIATests.swift
//  M6502
//
//  Created by Jeremy Pereira on 28/06/2015.
//
//

import XCTest
import Swift6502

let dataA: S65Address    = 0
let controlA: S65Address = 1
let dataB: S65Address    = 2
let controlB: S65Address = 3

class PIATests: XCTestCase
{
    var pia: PIA6520!

    override func setUp()
    {
        super.setUp()
        pia = PIA6520()
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testManualCB2()
    {
        let cb2 = pia.cb2
        var ioByte: S65Byte = PIAControl([.C2Output, .C2Manual, .C2ManualOn]).rawValue	// Turn on manual CB2
		pia.didWrite(controlB, byte: ioByte)
        XCTAssert(cb2.value, "Invalid CB2 value, should be true")
        ioByte = PIAControl([.C2Output, .C2Manual]).rawValue
        pia.didWrite(controlB, byte: ioByte)
        XCTAssert(!cb2.value, "Invalid CB2 value, should be false")
    }

    func testHandshakedCB2()
    {
        let cb2 = pia.cb2
        cb2.value = false
        let cb1 = pia.cb1
        cb1.value = false
        var ioByte: S65Byte = PIAControl([.C2Output, .C1ActiveHigh, .PortIOSelect]).rawValue	// Turn on manual CB2
        pia.didWrite(controlB, byte: ioByte)
        XCTAssert(!cb2.value, "Invalid CB2 value, should be false")
        cb1.value = true
        XCTAssert(cb2.value, "Invalid CB2 value, should be true")
        ioByte = 0
        pia.didWrite(2, byte: ioByte)
        XCTAssert(!cb2.value, "Invalid CB2 value, should be false (2)")
    }

    func testHandshakedCA2()
    {
        let ca2 = pia.ca2
        ca2.value = false
        let ca1 = pia.ca1
        ca1.value = false
        var ioByte: S65Byte = PIAControl([.C2Output, .C1ActiveHigh, .PortIOSelect]).rawValue	// Turn on manual CB2
        pia.didWrite(controlA, byte: ioByte)
        XCTAssert(!ca2.value, "Invalid CA2 value, should be false")
        ca1.value = true
        XCTAssert(ca2.value, "Invalid CA2 value, should be true")
        ioByte = pia.willRead(0)
        XCTAssert(!ca2.value, "Invalid CA2 value, should be false (2)")
    }

    func testInterrupt()
    {
        let ca1 = pia.ca1
        let ioByte = PIAControl([.C1IrqOn, .C1ActiveHigh, .PortIOSelect]).rawValue
        pia.didWrite(controlA, byte: ioByte)
		ca1.value = false
        ca1.value = true
        XCTAssert(!pia.irqA.value, "Interrupt was not raised")
		_ = pia.willRead(dataA)
        XCTAssert(pia.irqA.value, "Interrupt was not cleared")
    }

}
