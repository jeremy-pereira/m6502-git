//
//  CIA6526Tests.swift
//  Swift6502Tests
//
//  Created by Jeremy Pereira on 09/10/2018.
//

import XCTest
@testable import Swift6502

class CIA6526Tests: XCTestCase
{

    var cia: CIA6526!

    override func setUp()
    {
        cia = CIA6526(baseAddress: 0x0)
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTimerA()
    {
		cia.didWrite(.timerAControl, byte: 0x0)	// Stop the timer
        cia.didWrite(.timerALow, byte: 10)	// Load the timer
        cia.didWrite(.timerAHigh, byte: 0)
        cia.didWrite(.timerAControl, byte: 0x10)    // Stops the timer and latches it
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.willRead(.timerALow) == 10, "Accidentally drove timer A")
       	cia.didWrite(.timerAControl, byte: 0x11)	// Starts the timer and latches it
        cia.drive(cycleDelta: 1)
		XCTAssert(cia.willRead(.timerALow) == 9, "Failed to drive timer A")
    }

    func testTimerAPortBControlOneShot()
    {
        let bit6Mask: S65Byte = 0b0100_0000
        let testTimerValue: UInt16 = 1
        var portB: S65Byte? = nil
        cia.connectPortB(mask: bit6Mask, notifier: { portB = $0 })
        portB = nil
        cia.didWrite(.timerAControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerALow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x81)   // Set the interrupt mask for timer A only
        cia.didWrite(.timerAControl, byte: 0b0001_0011)    // Start the timer - one shot mode and pulse port B
        portB = nil // Will have been set when the data direction went to out
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 0)
        XCTAssert(portB == nil, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit6Mask != 0, "Port B should have gone high for one cycle")
        portB = nil
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit6Mask == 0, "Port B should have gone low after one cycle")
    }

    func testTimerAPortBControlToggle()
    {
        let bit6Mask: S65Byte = 0b0100_0000
        let testTimerValue: UInt16 = 1
        var portB: S65Byte? = nil
        cia.connectPortB(mask: bit6Mask, notifier: { portB = $0 })
        portB = nil
        cia.didWrite(.timerAControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerALow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testTimerValue.highByte)
        cia.didWrite(.timerAControl, byte: 0b0001_0111)    // Start the timer - one shot mode and pulse port B
        portB = nil // Will have been set when the data direction went to out
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 0)
        XCTAssert(portB == nil, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit6Mask != 0, "Port B should have gone high")
        portB = nil
        cia.drive(cycleDelta: 1)
        XCTAssert(portB == nil, "Port B should havenot havew changed one cycle")
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit6Mask == 0, "Port B should have changed")
    }


    func testTimerBPortBControlOneShot()
    {
        let bit7Mask: S65Byte = 0b1000_0000
        let testTimerValue: UInt16 = 1
        var portB: S65Byte? = nil
        cia.connectPortB(mask: bit7Mask, notifier: { portB = $0 })
        portB = nil
        cia.didWrite(.timerBControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerBLow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerBHigh, byte: testTimerValue.highByte)
        cia.didWrite(.timerBControl, byte: 0b0001_0011)    // Start the timer - one shot mode and pulse port B
        portB = nil // Will have been set when the data direction went to out
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 0)
        XCTAssert(portB == nil, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit7Mask != 0, "Port B should have gone high for one cycle")
        portB = nil
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit7Mask == 0, "Port B should have gone low after one cycle")
    }

    func testTimerBPortBControlToggle()
    {
        let bit7Mask: S65Byte = 0b1000_0000
        let testTimerValue: UInt16 = 1
        var portB: S65Byte? = nil
        cia.connectPortB(mask: bit7Mask, notifier: { portB = $0 })
        portB = nil
        cia.didWrite(.timerBControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerBLow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerBHigh, byte: testTimerValue.highByte)
        cia.didWrite(.timerBControl, byte: 0b0001_0111)    // Start the timer - one shot mode and pulse port B
        portB = nil // Will have been set when the data direction went to out
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 0)
        XCTAssert(portB == nil, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit7Mask != 0, "Port B should have gone high")
        portB = nil
        cia.drive(cycleDelta: 1)
        XCTAssert(portB == nil, "Port B should havenot havew changed one cycle")
        cia.drive(cycleDelta: 1)
        XCTAssert(portB != nil && portB! & bit7Mask == 0, "Port B should have changed")
    }

    func testOneShot()
    {
        let testTimerValue: UInt16 = 1
        var irqChanged: Bool = false
        cia.notIRQ.connectNotifier { _ in irqChanged = true }
        irqChanged = false // The notifier fires when we connect it
        cia.didWrite(.timerAControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerALow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x81)   // Set the interrupt mask for timer A only
        cia.didWrite(.timerAControl, byte: 0x9)    // Start the timer - one shot mode
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 0)
        XCTAssert(!irqChanged, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 1, "After interrupt timer should be set to latch")
        XCTAssert(irqChanged)
        irqChanged = false
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 1, "In one shot mode timer shoud stop")
    }

    func testTimerInterruptPhi2()
    {
        let testTimerValue: UInt16 = 1
        var irqChanged: Bool = false
        cia.notIRQ.connectNotifier { _ in irqChanged = true }
        irqChanged = false // The notifier fires when we connect it
        cia.didWrite(.timerAControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerALow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x81)   // Set the interrupt mask for timer A only
        cia.didWrite(.timerAControl, byte: 0x1)    // Start the timer - continuous mode
		cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 0)
        XCTAssert(!irqChanged, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 1, "After interrupt timer should be set to latch")
        XCTAssert(irqChanged)
		irqChanged = false
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 0, "In continuous mode timer shoud carry on")
        XCTAssert(!irqChanged)
        irqChanged = false
        let interruptData = cia.willRead(.interruptControl) // Should read the data and clear the interrupt
        XCTAssert(irqChanged)
        XCTAssert(interruptData & 0x80 != 0, "Interrupt flag not cleared")
        XCTAssert(interruptData & 0x1 != 0, "Timer A interrupt flag not cleared")
    }

    func testForceLoad()
    {
        precondition(cia.timerALatch == 0 && cia.timerAValue == 0)
        let testValue: UInt16 = 0xaabb
        cia.didWrite(.timerAControl, byte: 0)
        cia.didWrite(.timerALow, byte: testValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testValue.highByte)
        XCTAssert(cia.timerAValue == 0xaabb, "Expected $aabb, got \(cia.timerAValue.hexString)")
        XCTAssert(cia.timerALatch == 0xaabb)
        cia.didWrite(.timerAControl, byte: 0x01)
        cia.didWrite(.timerALow, byte: testValue.highByte)
        cia.didWrite(.timerAHigh, byte: testValue.lowByte)
        XCTAssert(cia.timerAValue == 0xaabb)
        XCTAssert(cia.timerALatch == 0xbbaa)
        cia.didWrite(.timerAControl, byte: 0x11)
        XCTAssert(cia.timerAValue == 0xbbaa)
        XCTAssert(cia.timerALatch == 0xbbaa)
    }


    func testTimerASourceCnt()
    {
        let testTimerValue: UInt16 = 1
        var irqChanged: Bool = false
        cia.notIRQ.connectNotifier { _ in irqChanged = true }
        irqChanged = false // The notifier fires when we connect it
        cia.didWrite(.timerAControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerALow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x81)   // Set the interrupt mask for timer A only
        cia.didWrite(.timerAControl, byte: 0b0010_0001)    // Start the timer - continuous mode on CNT
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerAValue == 1) // Timer does not count clock ticks
        XCTAssert(!irqChanged, "No interrupt should have occurred yet")
        cia.cntPin = false
        cia.cntPin = true
        XCTAssert(cia.timerAValue == 0, "One positive edge should decrement the timer")
        XCTAssert(!irqChanged, "Interrupt should not be triggered yet")
        cia.cntPin = false
        cia.cntPin = true
        XCTAssert(cia.timerAValue == 1, "Timer should reload from latch")
        XCTAssert(irqChanged)
        irqChanged = false
        let interruptData = cia.willRead(.interruptControl) // Should read the data and clear the interrupt
        XCTAssert(irqChanged)
        XCTAssert(interruptData & 0x80 != 0, "Interrupt flag not cleared")
        XCTAssert(interruptData & 0x1 != 0, "Timer A interrupt flag not cleared")
    }

    func testTimerBSourceCnt()
    {
        let testTimerValue: UInt16 = 1
        var irqChanged: Bool = false
        cia.notIRQ.connectNotifier { _ in irqChanged = true }
        irqChanged = false // The notifier fires when we connect it
        cia.didWrite(.timerBControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerBLow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerBHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x82)   // Set the interrupt mask for timer B only
        cia.didWrite(.timerBControl, byte: 0b0010_0001)    // Start the timer - continuous mode on CNT
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 1) // Timer does not count clock ticks
        XCTAssert(!irqChanged, "No interrupt should have occurred yet")
        cia.cntPin = false
        cia.cntPin = true
        XCTAssert(cia.timerBValue == 0, "One positive edge should decrement the timer")
        XCTAssert(!irqChanged, "Interrupt should not be triggered yet")
        cia.cntPin = false
        cia.cntPin = true
        XCTAssert(cia.timerBValue == 1, "Timer should reload from latch")
        XCTAssert(irqChanged)
        irqChanged = false
        let interruptData = cia.willRead(.interruptControl) // Should read the data and clear the interrupt
        XCTAssert(irqChanged)
        XCTAssert(interruptData & 0x80 != 0, "Interrupt flag not cleared")
        XCTAssert(interruptData & 0x2 != 0, "Timer B interrupt flag not cleared")
    }

    func testTimerBInterruptPhi2()
    {
        let testTimerValue: UInt16 = 1
        var irqChanged: Bool = false
        cia.notIRQ.connectNotifier { _ in irqChanged = true }
        irqChanged = false // The notifier fires when we connect it
        cia.didWrite(.timerBControl, byte: 0x0)    // Stop the timer
        cia.didWrite(.timerBLow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerBHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x82)   // Set the interrupt mask for timer B only
        cia.didWrite(.timerBControl, byte: 0x1)    // Start the timer - continuous mode
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 0)
        XCTAssert(!irqChanged, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 1, "After interrupt timer should be set to latch")
        XCTAssert(irqChanged)
        irqChanged = false
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 0, "In continuous mode timer shoud carry on")
        XCTAssert(!irqChanged)
        irqChanged = false
        let interruptData = cia.willRead(.interruptControl) // Should read the data and clear the interrupt
        XCTAssert(irqChanged)
        XCTAssert(interruptData & 0x80 != 0, "Interrupt flag not cleared")
        XCTAssert(interruptData & 0x2 != 0, "Timer B interrupt flag not cleared")
    }

    func testTimerBSourceTimerA()
    {
        let testTimerValue: UInt16 = 1
        var irqChanged: Bool = false
        cia.notIRQ.connectNotifier { _ in irqChanged = true }
        irqChanged = false // The notifier fires when we connect it
        cia.didWrite(.timerAControl, byte: 0x0)    // Stop the A timer
        cia.didWrite(.timerBControl, byte: 0x0)    // Stop the B timer
        cia.didWrite(.timerALow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testTimerValue.highByte)
        cia.didWrite(.timerBLow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerBHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x82)   // Set the interrupt mask for timer B only
        cia.didWrite(.timerAControl, byte: 0b0010_0001)    // Start the A timer - continuous mode on CNT
        cia.didWrite(.timerBControl, byte: 0b0100_0001)    // Start the B timer - continuous mode on A
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 1) // Timer does not count clock ticks
        XCTAssert(!irqChanged, "No interrupt should have occurred yet")
        cia.cntPin = false ; cia.cntPin = true // A = 0
        XCTAssert(cia.timerBValue == 1, "Timer A has not underflowed yet")
        cia.cntPin = false ; cia.cntPin = true // A = underflow
        XCTAssert(cia.timerBValue == 0, "Timer A has underflowed")
        XCTAssert(!irqChanged, "Interrupt should not be triggered yet")
        cia.cntPin = false ; cia.cntPin = true
        cia.cntPin = false ; cia.cntPin = true
        XCTAssert(cia.timerBValue == 1, "Timer should reload from latch")
        XCTAssert(irqChanged)
        irqChanged = false
        let interruptData = cia.willRead(.interruptControl) // Should read the data and clear the interrupt
        XCTAssert(irqChanged)
        XCTAssert(interruptData & 0x80 != 0, "Interrupt flag not cleared")
        XCTAssert(interruptData & 0x2 != 0, "Timer B interrupt flag not cleared")
    }

    func testTimerBSourceTimerACnt()
    {
        let testTimerValue: UInt16 = 1
        var irqChanged: Bool = false
        cia.notIRQ.connectNotifier { _ in irqChanged = true }
        irqChanged = false // The notifier fires when we connect it
        cia.didWrite(.timerAControl, byte: 0x0)    // Stop the A timer
        cia.didWrite(.timerBControl, byte: 0x0)    // Stop the B timer
        cia.didWrite(.timerALow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerAHigh, byte: testTimerValue.highByte)
        cia.didWrite(.timerBLow, byte: testTimerValue.lowByte)
        cia.didWrite(.timerBHigh, byte: testTimerValue.highByte)
        cia.didWrite(.interruptControl, byte: 0x82)   // Set the interrupt mask for timer B only
        cia.didWrite(.timerAControl, byte: 0b0000_0001)    // Start the A timer - continuous mode on phi2
        cia.didWrite(.timerBControl, byte: 0b0110_0001)    // Start the B timer - continuous mode on A
        cia.cntPin = false
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 1) // Timer does not count clock ticks
        XCTAssert(!irqChanged, "No interrupt should have occurred yet")
        cia.drive(cycleDelta: 1)
        XCTAssert(cia.timerBValue == 1, "Timer A has underflowed but B is disabled by CNT")
        XCTAssert(!irqChanged, "Interrupt should not be triggered yet")
        cia.cntPin = true
        cia.drive(cycleDelta: 2)
        XCTAssert(cia.timerBValue == 0, "Timer B should now decrement")
    }
}
