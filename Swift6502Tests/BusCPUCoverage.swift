//
//  BusCPUCoverage.swift
//  M6502
//
//  Created by Jeremy Pereira on 07/08/2017.
//
//

import XCTest
@testable import Swift6502

fileprivate let programStart: S65Address = 0x0200

class BusCPUCoverage: XCTestCase {

    var cpu: Mos65xx = make6502Bus()

    override func setUp() {
        super.setUp()
        cpu.set(word: programStart,  at: 0xFFFC)
        let myBundle = Bundle(for: type(of: self))
        let binaryFileUrl = myBundle.url(forResource: "coveragetest", withExtension: "bin")
        if let binaryFileUrl = binaryFileUrl
        {
            let program = try? Data(contentsOf: binaryFileUrl)

            if let program = program
            {
                var programBytes = [S65Byte](repeating: 0, count: program.count)
                program.copyBytes(to: &programBytes, count: programBytes.count)
                let currentAddress = programStart
                cpu.set(bytes: programBytes, at: currentAddress)
            }
            else
            {
                fatalError("Could not read test coverage program")
            }
        }
        else
        {
            fatalError("Could not locate coverage test binary")
        }
        cpu.fatalErrorHandler =
            {
                (cpu, message: @autoclosure () -> String) in
                XCTFail(message())
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testCoverage()
    {
        cpu.notReset = false
        cpu.notReset = true
        cpu.trace = true
        cpu.runForCycles(20000)
        let firstWord = cpu.word(at: 0)
        XCTAssert(!cpu.fatalErrorHappened, "Run failed with CPU fatal error")
        XCTAssert(firstWord == 0, "Tests did not complete, 0 loc = $\(firstWord.hexString)")
    }

}
