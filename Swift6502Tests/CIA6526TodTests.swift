//
//  CIA6526TodTests.swift
//  Swift6502Tests
//
//  Created by Jeremy Pereira on 01/11/2018.
//

import XCTest
@testable import Swift6502

class CIA6526TodTests: XCTestCase
{

    var cia: CIA6526!

    override func setUp()
    {
        cia = CIA6526(baseAddress: 0x0)
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample()
    {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func driveClock(tenths: Int)
    {
        for _ in 0 ..< (tenths * cia.realTimeClockFrequency / 10)
        {
            cia.todPin = false
            cia.todPin = true
        }
    }

    let t1Result = "clock:00:00:00.1 am, alarm:00:00:00.0 am, latch: 00:00:00.1 am"
    let s1t1Result = "clock:00:00:01.1 am, alarm:00:00:00.0 am, latch: 00:00:01.1 am"
    let m59s1t1Result = "clock:00:59:01.1 am, alarm:00:00:00.0 am, latch: 00:59:01.1 am"
    let h11m59s1t1Result = "clock:11:59:01.1 am, alarm:00:00:00.0 am, latch: 11:59:01.1 am"
    let h12m59s1t1Result = "clock:00:59:01.1 pm, alarm:00:00:00.0 am, latch: 00:59:01.1 pm"

    func testTodDriver()
    {
        cia.didWrite(.timeOfDayHours, byte: 0)
        cia.didWrite(.timeOfDayMinutes, byte: 0)
        cia.didWrite(.timeOfDaySeconds, byte: 0)
        cia.didWrite(.timeOfDay10ths, byte: 0)
        // Default is TOD runs at 60 Hz, so 6 toggles = 1/10 second
        driveClock(tenths: 1)
		XCTAssert(cia.realTimeClockState == t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 10)
        XCTAssert(cia.realTimeClockState == s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 59 * 60 * 10)
        XCTAssert(cia.realTimeClockState == m59s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 11 * 60 * 60 * 10)
        XCTAssert(cia.realTimeClockState == h11m59s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 1 * 60 * 60 * 10)
        XCTAssert(cia.realTimeClockState == h12m59s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
    }

    func testTodDriver50Hz()
    {
        cia.didWrite(.timerAControl, byte: 0x80)
        cia.didWrite(.timeOfDayHours, byte: 0)
        cia.didWrite(.timeOfDayMinutes, byte: 0)
        cia.didWrite(.timeOfDaySeconds, byte: 0)
        cia.didWrite(.timeOfDay10ths, byte: 0)
        XCTAssert(cia.realTimeClockFrequency == 50)
        driveClock(tenths: 1)
        XCTAssert(cia.realTimeClockState == t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 10)
        XCTAssert(cia.realTimeClockState == s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 59 * 60 * 10)
        XCTAssert(cia.realTimeClockState == m59s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 11 * 60 * 60 * 10)
        XCTAssert(cia.realTimeClockState == h11m59s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
        driveClock(tenths: 1 * 60 * 60 * 10)
        XCTAssert(cia.realTimeClockState == h12m59s1t1Result, "Tod pin failed to drive time of day. '\(cia.realTimeClockState)'")
    }

    func testTodBCD()
    {
        cia.didWrite(.timeOfDayHours, byte: 0)
        cia.didWrite(.timeOfDayMinutes, byte: 0)
        cia.didWrite(.timeOfDaySeconds, byte: 0)
        cia.didWrite(.timeOfDay10ths, byte: 0)
        driveClock(tenths: 11 * 60 * 60 * 10
                              + 58 * 60 * 10
                                   + 57 * 10
                                      +    9)
		let hours = cia.willRead(.timeOfDayHours)
        XCTAssert(hours == 0x11)
        let minutes = cia.willRead(.timeOfDayMinutes)
        XCTAssert(minutes == 0x58)
        let seconds = cia.willRead(.timeOfDaySeconds)
        XCTAssert(seconds == 0x57)
        let tenths = cia.willRead(.timeOfDay10ths)
        XCTAssert(tenths == 9)
    }

    func testAlarmAccess()
    {
        let alarmWriteResult = "clock:09:09:09.9 am, alarm:08:08:08.8 am, latch: 09:09:09.9 am"
        let alarmWriteResult2 = "clock:07:07:07.7 am, alarm:08:08:08.8 am, latch: 07:07:07.7 am"

        cia.didWrite(.timeOfDayHours, byte: 9)
        cia.didWrite(.timeOfDayMinutes, byte: 9)
        cia.didWrite(.timeOfDaySeconds, byte: 9)
        cia.didWrite(.timeOfDay10ths, byte: 9)
        cia.didWrite(.timerBControl, byte: 0x80)
        cia.didWrite(.timeOfDayHours, byte: 8)
        cia.didWrite(.timeOfDayMinutes, byte: 8)
        cia.didWrite(.timeOfDaySeconds, byte: 8)
        cia.didWrite(.timeOfDay10ths, byte: 8)
        XCTAssert(cia.realTimeClockState == alarmWriteResult, "Wrong alarm write result: \(cia.realTimeClockState)")
        cia.didWrite(.timerBControl, byte: 0)
        cia.didWrite(.timeOfDayHours, byte: 7)
        cia.didWrite(.timeOfDayMinutes, byte: 7)
        cia.didWrite(.timeOfDaySeconds, byte: 7)
        cia.didWrite(.timeOfDay10ths, byte: 7)
        XCTAssert(cia.realTimeClockState == alarmWriteResult2, "Wrong alarm write result: \(cia.realTimeClockState)")
    }

    func testStopOnHoursStartOnTenths()
    {
        let initialState = "clock:00:00:00.1 am, alarm:00:00:00.0 am, latch: 00:00:00.1 am"
        driveClock(tenths: 1)
        XCTAssert(cia.realTimeClockState == initialState, "State wrong while clock running: \(cia.realTimeClockState)")
        cia.didWrite(.timeOfDayHours, byte: 0)
        driveClock(tenths: 1)
        XCTAssert(cia.realTimeClockState == initialState, "State wrong while clock stopped: \(cia.realTimeClockState)")
        cia.didWrite(.timeOfDay10ths, byte: 0)
        driveClock(tenths: 1)
        XCTAssert(cia.realTimeClockState == initialState)

    }

    func testReadLatch()
    {
        let initialState = "clock:00:00:00.1 am, alarm:00:00:00.0 am, latch: 00:00:00.1 am"
        let latchedState = "clock:00:00:00.2 am, alarm:00:00:00.0 am, latch: 00:00:00.1 am"
        let unlatchedState = "clock:00:00:00.2 am, alarm:00:00:00.0 am, latch: 00:00:00.2 am"
        let unlatchedState2 = "clock:00:00:00.3 am, alarm:00:00:00.0 am, latch: 00:00:00.3 am"

        cia.didWrite(.timeOfDayHours, byte: 0)
        cia.didWrite(.timeOfDayMinutes, byte: 0)
        cia.didWrite(.timeOfDaySeconds, byte: 0)
        cia.didWrite(.timeOfDay10ths, byte: 0)
        driveClock(tenths: 1)
        XCTAssert(cia.realTimeClockState == initialState, "State wrong while clock running: \(cia.realTimeClockState)")
        _ = cia.willRead(.timeOfDayHours)
        driveClock(tenths: 1)
        XCTAssert(cia.realTimeClockState == latchedState, "State wrong while clock stopped: \(cia.realTimeClockState)")
        _ = cia.willRead(.timeOfDay10ths)
        XCTAssert(cia.realTimeClockState == unlatchedState, "State wrong while clock stopped: \(cia.realTimeClockState)")
        driveClock(tenths: 1)
        XCTAssert(cia.realTimeClockState == unlatchedState2, "State wrong while clock stopped: \(cia.realTimeClockState)")

    }

    func testAlarmInterrupt()
    {
        var irqValue: Bool?
        // First set the alarm to 08:08:08.8
        cia.didWrite(.timerBControl, byte: 0x80)
        cia.didWrite(.timeOfDayHours, byte: 8)
        cia.didWrite(.timeOfDayMinutes, byte: 8)
        cia.didWrite(.timeOfDaySeconds, byte: 8)
        cia.didWrite(.timeOfDay10ths, byte: 8)
        cia.didWrite(.timerBControl, byte: 0x00)
        cia.didWrite(.interruptControl, byte: 0b1000_0100) // Enable alarm interrupt
        cia.notIRQ.connectNotifier { irqValue = $0 }
        irqValue = nil
        driveClock(tenths: 1)
        XCTAssert(irqValue == nil)
        driveClock(tenths: (((8 * 60) + 8) * 60 + 8) * 10 + 7)
        XCTAssert(irqValue != nil && !(irqValue!))
		let interruptData = cia.willRead(.interruptControl)
        XCTAssert(interruptData == 0b1000_0100)
    }
}
