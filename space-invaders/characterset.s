; da65 V2.13.2 - (C) Copyright 2000-2009,  Ullrich von Bassewitz
; Created:    2017-01-11 16:57:47
; Input file: space_invaders.prg
; Page:       1


        .setcpu "6502"

; ----------------------------------------------------------------------------
screen          := $8000                        ; The Screen RAM
lcUcLocation	:= 59468						; Poke location for lower case
lowerCase		:= 14
upperCase		:= 12
; ----------------------------------------------------------------------------
loadAddress:
        .byte   $01,$04                         ; 03FF 01 04                    ..
; SYS(1039)
basicBootstrap:
        .byte   $0D,$04,$0A,$00,$9E,$28,$31,$30 ; 0401 0D 04 0A 00 9E 28 31 30  .....(10
        .byte   $33,$39,$29,$00,$00,$00         ; 0409 33 39 29 00 00 00        39)...
; ----------------------------------------------------------------------------
        nop                                     ; 040F EA                       .
        nop                                     ; 0410 EA                       .
start:
		LDA		#lowerCase
		STA		lcUcLocation
		LDY	#$0
loop:
		TYA
		STA	screen,Y
		INY
		JMP loop	; loop back to beginning
