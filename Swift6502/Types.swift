//
//  Types.swift
//  M6502
//
//  Created by Jeremy Pereira on 12/06/2015.
//
//

import Foundation
/**
A byte
*/
public typealias S65Byte = UInt8

public extension S65Byte
{
}
/**
A 6502 address - 16 bits
*/
public typealias S65Address = UInt16


/// Reimplement the missing increment operator. Note that, we want wrapping 
/// behaviour i.e. 0xffff++ = 0
///
/// - parameter n: address to post increment.
///
/// - returns: n before the increment.
@discardableResult public postfix func ++ (n: inout S65Address) -> S65Address
{
    defer { n = n &+ 1 }
    return n
}

public postfix func ++ (n: inout Int) -> Int
{
    defer { n = n + 1 }
    return n
}

public typealias CycleCount = Int

public extension S65Byte
{
    func isBitSet(_ bit: Int) -> Bool
    {
        return (self & S65Byte(1 << bit)) != 0
    }

    mutating func setBit(_ bitNumber: Int, _ value: Bool)
    {
        if value
        {
            self |= S65Byte(1 << bitNumber)
        }
        else
        {
            self &= ~S65Byte(1 << bitNumber)
        }
    }

    var hexString: String { return String(format: "%02lx", Int(self)) }
    var isTcNegative: Bool { return self >= 0x80 }

    var signedAddress: S65Address
    {
        var address = S65Address(self)
        if self.isTcNegative
        {
            address.highByte = 0xFF
        }
        return address
    }


	/// Return a copy of the byte either set or cleared
	///
	/// - Parameters:
	///   - bit: The bit that should be set or cleared
	///   - isSet: if true, the bit will be set, if false it will be cleared.
	/// - Returns: A copy of the byte with the bit specified set or cleeared.
	func with(bit: Int, set isSet: Bool) -> S65Byte
    {
        if isSet
        {
            return self | S65Byte(1 << bit)
        }
        else
        {
            return self & ~S65Byte(1 << bit)
        }
    }
}

///
/// Special byte constants
///
public extension S65Byte
{
    /// ASCII Linefeed character
	static let lineFeed: S65Byte = "\n".utf8.first!
	static let carriageReturn: S65Byte = "\r".utf8.first!
	static let colon: S65Byte = ":".utf8.first!
    static let comma: S65Byte = ",".utf8.first!
    static let dollar: S65Byte = "$".utf8.first!
    static let ascii0: S65Byte = "0".utf8.first!
	static let ascii9: S65Byte = "9".utf8.first!
    static let A: S65Byte = "A".utf8.first!
    static let F: S65Byte = "F".utf8.first!
    static let a: S65Byte = "a".utf8.first!
    static let f: S65Byte = "f".utf8.first!

}

private let decimal0 = "0".utf16
private let decimal9 = "9".utf16
private let hexA = "A".utf16
private let hexF = "F".utf16
private let codeUnit0 = decimal0[decimal0.startIndex]
private let codeUnit9 = decimal9[decimal9.startIndex]
private let codeUnitA = hexA[hexA.startIndex]
private let codeUnitF = hexF[hexF.startIndex]

public extension S65Address
{
	init(low: S65Byte, high: S65Byte)
    {
        self.init(UInt16(high) << 8 | UInt16(low))
    }
    
    func isBitSet(_ bit: Int) -> Bool
    {
        return (self & S65Address(1 << bit)) != 0
    }

	var lowByte: S65Byte
    {
        get { return S65Byte(self & 0xFF) }
        set { self = (self & 0xFF00) | S65Address(newValue) }
    }
	var highByte: S65Byte
    {
        get { return S65Byte(self >> 8) }
        set { self = (self & 0xFF) | (S65Address(newValue) << 8) }
    }

	var isTcNegative: Bool { return self >= 0x8000 }

    var hexString: String { return String(format: "%04lx", Int(self)) }


	static let spaceSize = 0x10000
}

public extension Int
{
    var isTcNegative: Bool { return (self & (1 << 7)) != 0 }

    func makeHexString(_ digits: Int) -> String
    {
        let formatString = "%0\(digits)lx"
        return String(format: formatString, self)
    }
}

///
/// Useful extension functions to a string
///
public extension String
{
    ///
    /// Get the string as an address if it can be converted
    ///
	var s65Address: S65Address?
    {
        var result: S65Address?

        let workingString = self.uppercased().utf16
        if workingString.count <= 4
        {
            var noErrors = true
            var calculatedValue:S65Address = 0

            for digitChar in workingString
            {
                switch (digitChar)
                {
                case codeUnit0 ... codeUnit9:
                    calculatedValue = (calculatedValue << 4) + (digitChar - codeUnit0)
                case codeUnitA ... codeUnitF:
                    calculatedValue = (calculatedValue << 4) + (digitChar - codeUnitA + 10)
                default:
                    noErrors = false
                }

            }
            if noErrors
            {
                result = calculatedValue
            }
        }
        return result
    }

	var asUTF8Data: Data
    {
		return Data(self.utf8)
    }

	var asASCII: [S65Byte]
    {
		return Array(self.utf8)
    }

}

///
/// Some of the file system path methods disappeared in beta 5.
///
public extension String
{
    ///
    /// The same string with the last path component deleted.
    ///
	var stringByDeletingLastPathComponent: String
    {
		let tempString = NSString(string: self)
        return tempString.deletingLastPathComponent
    }

	func stringByAppendingPathComponent(_ component: String) ->String
    {
        let tempString = NSString(string: self)
        return tempString.appendingPathComponent(component)
    }
}

///
/// Protocol that allows a device to be mapped to a memory address
///
/// A memory mapped device consists of a number of contiguous ports that should
/// be mapped starting from a particular base address.  Once mapped, the device
/// will be notified before each read and after each write.
///
/// Note that the device can no longer rely on the memory also being written or
/// read. The byte passed into `willRead(_ portNumber:byte:)` is not guaranteed
/// not to be garbage and it **must** be overwritten. Devices must maintain
/// their own backing store.
///
public protocol MemoryMappedDevice: AnyObject
{
    /// Number of ports the device has
    var portCount: Int { get }
    /// Base address at which the ports should be mapped.
    var baseAddress: S65Address { get }
    ///
    /// The CPU wrote to the given port.
    ///
    /// - Parameter portNumber: The port which was written to.
    /// - Parameter byte: The byte that was written.
    ///
    func didWrite(_ portNumber: S65Address, byte: S65Byte)
    ///
    /// The CPU is about to read from the given port.
    ///
    /// - Parameter portNumber: The port which will be read.
    /// - Returns: The byte that was read.
    func willRead(_ portNumber: S65Address) -> S65Byte
    ///
    /// Push a device for chaining.
    ///
    /// This allows a device to map to a port that is already mapped.  If the
    /// device doen't support chaining (for example, it has a performance cost)
    /// this function should fatalError.  
    ///
    /// There is no built in support for device chaining.  Devices that do it 
    /// must implement it in some way in their willRead and didWrite methods.
    ///
    /// - Parameter previousDevice: The device to push.
    /// - Parameter port: The port that device to push is on.
    ///
    func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
}

///
/// An object that can give us a clock time in microseconds
///
public protocol MicroSecondClock
{
    ///
    /// The current time shown by the clock in microseconds.
    ///
    var time: CycleCount { get }
}

///
/// Protocol describing devices that need to be driven by the CPU clock
///
public protocol ClockDriver: AnyObject
{
    ///
    /// Function to drive the device.
    ///
    /// - Parameter cycleDelta: Number of cycles since we were last called.
    ///
    func drive(cycleDelta: CycleCount)
}

/// This protocol desribes an objecgt that can be used as memory for the 65xx.
/// it presents a single contiguous space of 64k bytes bno matter how the real
/// memory is organised.
public protocol MemoryMappable: DirectMemoryAccessible
{
    func get(address: S65Address) -> S65Byte
    func set(address: S65Address, byte: S65Byte)
}


///
/// Allows us to split a sequence into an array of subsequences at the selarators.
///
public extension Sequence where Self.Iterator.Element: Equatable
{
    ///
    /// Split the sequence into subsequences.
    ///
    /// Consecutive separators will generate an empty array representing the 
    /// non existence of separators between them.
    ///
    /// - Parameter separator: The separator element for the subsequences.
    /// - Returns: An array of the sub sequences.
    ///
	func splitAt(_ separator: Iterator.Element) -> [[Iterator.Element]]
    {
        var ret: [[Iterator.Element]] = []
        var thisPart: [Iterator.Element] = []

        for element in self
        {
            if element == separator
            {
                ret.append(thisPart)
                thisPart = []
            }
            else
            {
                thisPart.append(element)
            }
        }
        ret.append(thisPart)
        return ret
    }
}

///
/// Convenience function for manipulating arrays of bytes
///
public extension Sequence where Iterator.Element == S65Byte
{
    ///
    /// Assuming the array is a sequence of ASCII (well ISO) characters, get a 
    /// string.
    ///
	var asciiString: String?
    {
        let arraySelf = [S65Byte](self)
        var ret: String?
        if let string = NSString(bytes: arraySelf, length: arraySelf.count, encoding: String.Encoding.isoLatin1.rawValue)
        {
            ret = String(string)
        }
        return ret
    }

    ///
    /// Assuming the array is a sequence of up to two hex digits, get a byte
    ///
	var byteFromHex: S65Byte?
    {
        var ret: S65Byte?

        if let uint32 = self.uint32FromHex , uint32 <= Int(S65Byte.max)
        {
            ret = S65Byte(uint32)
        }
        return ret
    }

    ///
    /// Assuming the array is a sequence of up to eight hex digits, get an unsigned
    /// 32
    ///
	var uint32FromHex: Int?
        {
            var ret: Int?

            var generator = self.makeIterator()
            var isStillValid = true
            var count = 0
            var calculatedByte: Int = 0
            while let byte = generator.next() , isStillValid
            {
                isStillValid = count < 8
                if isStillValid
                {
                    calculatedByte *= 0x10
                    count += 1
                    if byte >= S65Byte.ascii0 && byte <= S65Byte.ascii9
                    {
                        calculatedByte += Int(byte - 0x30)
                    }
                    else if byte >= S65Byte.A && byte <= S65Byte.F
                    {
                        calculatedByte += Int(byte - S65Byte.A) + 0xa
                    }
                    else if byte >= S65Byte.a && byte <= S65Byte.f
                    {
                        calculatedByte += Int(byte - S65Byte.a) + 0xa
                    }
                    else
                    {
                        isStillValid = false
                    }
                }
            }
            if isStillValid && count > 0
            {
                ret = calculatedByte
            }
            return ret
    }
}

///
/// A continuous range structure that allows adding new values to extend the 
/// range.
///
public struct Range<T: SignedNumeric & Comparable>
{
    ///
    /// The start of the range
    ///
    public var start: T
    ///
    /// The end of the range.
    ///
    public var end: T

    ///
    /// Initialise the range with a single value
    /// 
    /// - Parameter firstValue: The one value in the range
    ///
    public init(firstValue: T)
    {
        start = firstValue
        end = firstValue
    }

    ///
    /// Include a new number in the range.
    /// 
    /// - Parameter value: The new value to include in the range.
    ///
    public mutating func include(_ value: T?)
    {
        if let value = value
        {
            if value < start
            {
                start = value
            }
            if value > end
            {
                self.end = value
            }
        }
    }

    ///
    /// The current wifth of the ange.  If there are no values, this will 
    /// invoke a fatal error.
    ///
    public var width: T { return self.end - self.start }

}

open class Stack<T>
{
    fileprivate var store: [T] = []
    fileprivate var top: Int = 0

    public init()
    {

    }

    open var isEmpty: Bool { return top == 0 }

    open func push(_ element: T)
    {
		if top < store.count
        {
            store[top] = element
            top += 1
        }
        else
        {
            store.append(element)
            top = store.count
        }
    }

    open func pop() -> T
    {
        guard top > 0 else { fatalError("Stack underflow") }
        top -= 1
        return store[top]
    }

    open func peek() -> T
    {
    	guard top > 0 else { fatalError("Stack underflow") }
    	return store[top - 1]
    }
}

public extension OptionSet
{
	mutating func set(bits: Self.Element, on value: Bool)
    {
        if value
        {
            self.insert(bits)
        }
        else
        {
			self.remove(bits)
        }
    }
}
