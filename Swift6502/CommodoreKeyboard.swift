//
//  CommodoreKeyboard.swift
//  Swift6502
//
//  Created by Jeremy Pereira on 07/11/2018.
//

import AppKit

private let log = Logger.getLogger("Swift6502.CommodoreKeyboard")

/// Represents Mac OS key codes with some extras to fake PET keys that don't
/// exist on the Mac.
public struct VirtualKeyCode: OptionSet, Hashable
{
    public init(rawValue: UInt32)
    {
		self.rawValue = rawValue
    }

    public let rawValue: UInt32

    public var hashValue: Int { return Int(rawValue) }

    static let ModifierMask = VirtualKeyCode(rawValue: 0xFFF7_0000) // All modifiers except alt
    static let altModifierMask = VirtualKeyCode(rawValue: 0xFFFF_0000) // All modifiers except alt

    static let LineEnd   = VirtualKeyCode([.ModifierMask, VirtualKeyCode(rawValue: 1)])
    static let LeftArrow = VirtualKeyCode([.ModifierMask, VirtualKeyCode(rawValue: 2)])

    static let alt     = VirtualKeyCode(rawValue: 0x0008_0000)
    static let Shift   = VirtualKeyCode(rawValue: 0x0002_0000)
    static let Ctrl    = VirtualKeyCode(rawValue: 0x0004_0000)
    static let Fn      = VirtualKeyCode(rawValue: 0x0008_0000)
    static let Cursor  = VirtualKeyCode(rawValue: 0x000a_0000)
    static let command = VirtualKeyCode(rawValue: 0x0010_0000)
    static let Fake    = VirtualKeyCode(rawValue: 0x8000_0000)    // For virtual key codes that don't really exist

	static let A: VirtualKeyCode = []
    static let S = VirtualKeyCode(rawValue: 0x1)
    static let D = VirtualKeyCode(rawValue: 0x2)
    static let F = VirtualKeyCode(rawValue: 0x3)
    static let H = VirtualKeyCode(rawValue: 0x4)
    static let G = VirtualKeyCode(rawValue: 0x5)
    static let Z = VirtualKeyCode(rawValue: 0x6)
    static let X = VirtualKeyCode(rawValue: 0x7)
    static let C = VirtualKeyCode(rawValue: 0x8)
    static let V = VirtualKeyCode(rawValue: 0x9)
    static let B = VirtualKeyCode(rawValue: 0xb)
    static let Q = VirtualKeyCode(rawValue: 0xc)
    static let W = VirtualKeyCode(rawValue: 0xd)
    static let E = VirtualKeyCode(rawValue: 0xe)
    static let R = VirtualKeyCode(rawValue: 0xf)
    static let Y = VirtualKeyCode(rawValue: 0x10)
    static let T = VirtualKeyCode(rawValue: 0x11)
    static let _1 = VirtualKeyCode(rawValue: 0x12)
    static let _2 = VirtualKeyCode(rawValue: 0x13)
    static let _3 = VirtualKeyCode(rawValue: 0x14)
    static let _4 = VirtualKeyCode(rawValue: 0x15)
    static let _6 = VirtualKeyCode(rawValue: 0x16)
    static let _5 = VirtualKeyCode(rawValue: 0x17)
    static let Equals = VirtualKeyCode(rawValue: 0x18)
    static let _9 = VirtualKeyCode(rawValue: 0x19)
    static let _7 = VirtualKeyCode(rawValue: 0x1a)
    static let Minus = VirtualKeyCode(rawValue: 0x1b)
    static let _8 = VirtualKeyCode(rawValue: 0x1c)
    static let _0 = VirtualKeyCode(rawValue: 0x1d)
    static let RSquare = VirtualKeyCode(rawValue: 0x1e)
    static let O = VirtualKeyCode(rawValue: 0x1f)
    static let U = VirtualKeyCode(rawValue: 0x20)
    static let LSquare = VirtualKeyCode(rawValue: 0x21)
    static let I = VirtualKeyCode(rawValue: 0x22)
    static let P = VirtualKeyCode(rawValue: 0x23)
    static let Return = VirtualKeyCode(rawValue: 0x24)
    static let L = VirtualKeyCode(rawValue: 0x25)
    static let J = VirtualKeyCode(rawValue: 0x26)
    static let Quote = VirtualKeyCode(rawValue: 0x27)
    static let K = VirtualKeyCode(rawValue: 0x28)
    static let SemiColon  = VirtualKeyCode(rawValue: 0x29)
    static let BackSlash  = VirtualKeyCode(rawValue: 0x2a)
    static let Comma      = VirtualKeyCode(rawValue: 0x2b)
    static let Slash      = VirtualKeyCode(rawValue: 0x2c)
    static let N          = VirtualKeyCode(rawValue: 0x2d)
    static let M          = VirtualKeyCode(rawValue: 0x2e)
    static let Dot        = VirtualKeyCode(rawValue: 0x2f)
    static let Space      = VirtualKeyCode(rawValue: 0x31)
    static let BackSpace  = VirtualKeyCode(rawValue: 0x33)
    static let CursorLeft = VirtualKeyCode(rawValue: 0x7b)
    static let CursorDown = VirtualKeyCode(rawValue: 0x7d)

    static let DoubleQuote = VirtualKeyCode([.Shift, .Quote])
    static let Exclamation = VirtualKeyCode([.Shift, ._1])
    static let At          = VirtualKeyCode([.Shift, ._2])
    static let Hash        = VirtualKeyCode([.Shift, ._3])
    static let altHash     = VirtualKeyCode([.alt, ._3]) // For the C64 with a UK Mac keyboard
    static let Dollar      = VirtualKeyCode([.Shift, ._4])
    static let Percent     = VirtualKeyCode([.Shift, ._5])
    static let UpArrow     = VirtualKeyCode([.Shift, ._6])
    static let Ampersand   = VirtualKeyCode([.Shift, ._7])
    static let Asterisk    = VirtualKeyCode([.Shift, ._8])
    static let LPar        = VirtualKeyCode([.Shift, ._9])
    static let RPar        = VirtualKeyCode([.Shift, ._0])

    static let QuestionMark   = VirtualKeyCode([.Shift, .Slash])
    static let Plus           = VirtualKeyCode([.Shift, .Equals])
    static let Colon          = VirtualKeyCode([.Shift, .SemiColon])
    static let LAngled        = VirtualKeyCode([.Shift, .Comma])
    static let RAngled        = VirtualKeyCode([.Shift, .Dot])

    static let pound        = VirtualKeyCode([.Shift, ._3])


    /// Keycode for PET home key
    public static let Home  = VirtualKeyCode([.Fn, VirtualKeyCode(rawValue: 0x73)])
    static let Del          = VirtualKeyCode([.Fn, VirtualKeyCode(rawValue: 0x75)])
    /// Keycode fore reverse
    public static let CtrlR = VirtualKeyCode([.Ctrl, .R])    // Reverse
    /// Keycode for `stop`
    public static let CtrlC = VirtualKeyCode([.Ctrl, .C])    // Stop
    static let CursorRight  = VirtualKeyCode([.Cursor, VirtualKeyCode(rawValue: 0x7c)])

    /// Keycode for left shift key
    public static let LeftShift = VirtualKeyCode([.Fake, VirtualKeyCode(rawValue: 0x1)])
    static let RightShift = VirtualKeyCode([.Fake, VirtualKeyCode(rawValue: 0x2)])


    static func fromEventModifiers(_ modifiers: NSEvent.ModifierFlags) -> VirtualKeyCode
    {
        return VirtualKeyCode(rawValue: UInt32(modifiers.rawValue)).intersection(.ModifierMask)
    }

    static func c64FromEventModifiers(_ modifiers: NSEvent.ModifierFlags) -> VirtualKeyCode
    {
        return VirtualKeyCode(rawValue: UInt32(modifiers.rawValue)).intersection(.altModifierMask)
    }

    var hexString: String
    {
        return Int(rawValue).makeHexString(8)
    }
}

private let leftAltMask  = NSEvent.ModifierFlags(rawValue: 0x00080020)
private let rightAltMask = NSEvent.ModifierFlags(rawValue: 0x00080040)


private struct MatrixRow: OptionSet
{
    let rawValue: S65Byte

    var outputByte: S65Byte { return ~rawValue }

    static func bit(_ n: Int) -> MatrixRow
    {
        return MatrixRow(rawValue: S65Byte(1 << n))
    }
}

private struct KeyMapping: CustomStringConvertible
{
    let row: Int
    let col: Int

    var description: String
    {
        return "key mapping (row: \(row), col: \(col))"
    }
}


/// Reresents a PET keyboard.
///
/// A Commodfore keyboard has a matrix of switches which may be on or off
/// depending on which keys are currently pressed. The computer reads it by
/// selecting a row and a column and then reading the bits for each row on that
/// column.
///
/// This class also converts the Macintosh key codes to a pattern of set bits in
/// the keyboard matrix.
open class PETKeyboard
{
    public init()
    {

    }

    fileprivate var matrix: [MatrixRow] = [MatrixRow](repeating: MatrixRow([]), count: 10)
    fileprivate let keyMap: [ VirtualKeyCode : KeyMapping] =
        [
            .Exclamation : KeyMapping(row: 0, col: 0),
            .Hash : KeyMapping(row: 0, col: 1),
            .Percent : KeyMapping(row: 0, col: 2),
            .Ampersand : KeyMapping(row: 0, col: 3),
            .LPar : KeyMapping(row: 0, col: 4),
            .LeftArrow : KeyMapping(row: 0, col: 5),
            .Home : KeyMapping(row: 0, col: 6),
            .CursorRight : KeyMapping(row: 0, col: 7),

            .DoubleQuote : KeyMapping(row: 1, col: 0),
            .Dollar : KeyMapping(row: 1, col: 1),
            .Quote : KeyMapping(row: 1, col: 2),
            .BackSlash : KeyMapping(row: 1, col: 3),
            .RPar : KeyMapping(row: 1, col: 4),
            .LineEnd : KeyMapping(row: 1, col: 5),
            .CursorDown : KeyMapping(row: 1, col: 6),
            .BackSpace : KeyMapping(row: 1, col: 7),

            .Q : KeyMapping(row: 2, col: 0),
            .E : KeyMapping(row: 2, col: 1),
            .T : KeyMapping(row: 2, col: 2),
            .U : KeyMapping(row: 2, col: 3),
            .O : KeyMapping(row: 2, col: 4),
            .UpArrow : KeyMapping(row: 2, col: 5),
            ._7 : KeyMapping(row: 2, col: 6),
            ._9 : KeyMapping(row: 2, col: 7),

            .W            : KeyMapping(row: 3, col: 0),
            .R            : KeyMapping(row: 3, col: 1),
            .Y            : KeyMapping(row: 3, col: 2),
            .I            : KeyMapping(row: 3, col: 3),
            .P            : KeyMapping(row: 3, col: 4),
            ._8            : KeyMapping(row: 3, col: 6),
            .Slash        : KeyMapping(row: 3, col: 7),

            .A            : KeyMapping(row: 4, col: 0),
            .D            : KeyMapping(row: 4, col: 1),
            .G            : KeyMapping(row: 4, col: 2),
            .J            : KeyMapping(row: 4, col: 3),
            .L            : KeyMapping(row: 4, col: 4),
            ._4            : KeyMapping(row: 4, col: 6),
            ._6            : KeyMapping(row: 4, col: 7),

            .S            : KeyMapping(row: 5, col: 0),
            .F            : KeyMapping(row: 5, col: 1),
            .H            : KeyMapping(row: 5, col: 2),
            .K            : KeyMapping(row: 5, col: 3),
            .Colon        : KeyMapping(row: 5, col: 4),
            ._5            : KeyMapping(row: 5, col: 6),
            .Asterisk        : KeyMapping(row: 5, col: 7),

            .Z            : KeyMapping(row: 6, col: 0),
            .C            : KeyMapping(row: 6, col: 1),
            .B            : KeyMapping(row: 6, col: 2),
            .M            : KeyMapping(row: 6, col: 3),
            .SemiColon   : KeyMapping(row: 6, col: 4),
            .Return        : KeyMapping(row: 6, col: 5),
            ._1            : KeyMapping(row: 6, col: 6),
            ._3               : KeyMapping(row: 6, col: 7),

            .X            : KeyMapping(row: 7, col: 0),
            .V            : KeyMapping(row: 7, col: 1),
            .N            : KeyMapping(row: 7, col: 2),
            .Comma        : KeyMapping(row: 7, col: 3),
            .QuestionMark: KeyMapping(row: 7, col: 4),
            ._2            : KeyMapping(row: 7, col: 6),
            .Plus            : KeyMapping(row: 7, col: 7),

            .LeftShift   : KeyMapping(row: 8, col: 0),
            .At            : KeyMapping(row: 8, col: 1),
            .RSquare        : KeyMapping(row: 8, col: 2),
            .RAngled      : KeyMapping(row: 8, col: 4),
            .RightShift  : KeyMapping(row: 8, col: 5),
            ._0            : KeyMapping(row: 8, col: 6),
            .Minus        : KeyMapping(row: 8, col: 7),

            .CtrlR            : KeyMapping(row: 9, col: 0),
            .LSquare     : KeyMapping(row: 9, col: 1),
            .Space            : KeyMapping(row: 9, col: 2),
            .LAngled    : KeyMapping(row: 9, col: 3),
            .CtrlC         : KeyMapping(row: 9, col: 4),
            .Dot        : KeyMapping(row: 9, col: 6),
            .Equals     : KeyMapping(row: 9, col: 7)
    ]


    /// The Bus instance that selects a keyboard line
    open var lineSelect: Bus?
    {
        didSet
        {
            if let lineSelect = lineSelect
            {
                lineSelect.connect(self, mask: 0xf, notifier: selectLine)
            }
        }
    }

    /// The bus instance that reads the selected row.
    open var selectedRow: Bus?
    {
        didSet
        {
            if let selectedRow = selectedRow
            {
                selectedRow.value = 0xff
            }
        }
    }

    fileprivate func selectLine(_ newByte: S65Byte)
    {
        if let selectedRow = selectedRow
        {
            let selectedLineNumber = Int(newByte & 0xf)
            if selectedLineNumber < matrix.count
            {
                selectedRow.value = matrix[selectedLineNumber].outputByte
            }
            else
            {
                print("Invalid row select byte was $\(selectedLineNumber.makeHexString(2))")
            }
        }
    }

    /// Sets the keyboard matrix for the combination of Mac keycode and
    /// modifiers.
    ///
    /// - Parameters:
    ///   - newKeyCode: The new key code to select
    ///   - modifierFlags: Modifiers currently set on the Mac keyboard.
    open func setKeyCode(_ newKeyCode: UInt16, modifierFlags: NSEvent.ModifierFlags)
    {
        let modifiers = VirtualKeyCode.fromEventModifiers(modifierFlags)
        let compositeKeyCode = VirtualKeyCode([ VirtualKeyCode(rawValue: UInt32(newKeyCode)),
                                                modifiers])
        setMatrixForKeyCode(compositeKeyCode)
        // Fake shift key presses from alts
        if modifierFlags.contains(leftAltMask)
        {
            setMatrixForKeyCode(.LeftShift)
        }
        else if modifierFlags.contains(rightAltMask)
        {
            setMatrixForKeyCode(.RightShift)
        }
    }

    /// Resets the keyboard matrix for the combination of Mac keycode and
    /// modifiers.
    ///
    /// - Parameters:
    ///   - newKeyCode: The  key code to deselect
    ///   - modifierFlags: Modifiers currently set on the Mac keyboard.
    open func resetKeyCode(_ newKeyCode: UInt16, modifierFlags: NSEvent.ModifierFlags)
    {
        let modifiers = VirtualKeyCode.fromEventModifiers(modifierFlags)
        let compositeKeyCode = VirtualKeyCode([ VirtualKeyCode(rawValue: UInt32(newKeyCode)),
                                                modifiers])
        resetMatrixForKeyCode(compositeKeyCode)
        // Fake shift key releases from alts
        if modifierFlags.contains(leftAltMask)
        {
            resetMatrixForKeyCode(.LeftShift)
        }
        else if modifierFlags.contains(rightAltMask)
        {
            resetMatrixForKeyCode(.RightShift)
        }
    }


    /// Emulate the user pressing a key
    ///
    /// - Parameter keyCode: The virtual keycode of the key pressed.
    open func setMatrixForKeyCode(_ keyCode: VirtualKeyCode)
    {
        if let keyMapping = keyMap[keyCode]
        {
            matrix[keyMapping.row].insert(MatrixRow.bit(keyMapping.col))
        }
        else
        {
            print("\(self): set unmapped key code: \(keyCode.hexString)")
        }
    }


    /// Turn off a key code. This is equivalent to the user releasing a key
    ///
    /// - Parameter keyCode: The virtual key code of the key released.
    open func resetMatrixForKeyCode(_ keyCode: VirtualKeyCode)
    {
        if let keyMapping = keyMap[keyCode]
        {
            matrix[keyMapping.row].remove(MatrixRow.bit(keyMapping.col))
        }
        else
        {
            print("\(self): reset unmapped key code: \(keyCode.hexString)")
        }
    }
}


/// A class that models the keyboard of a Commodore 64.
///
/// The Commodore 64 keyboard is an 8 x 8 matrix giving 64 keys. Scanning it is
/// different from the PET in that the CIA selects columns with a set of 8 bits
/// rather than using the number of the column. If a bit is 0, the corresponding
/// column is selected. If a key is pressed in that column, it's row bit is also
/// zero.
///
/// I am assuming that the row is the anded result of all the selected
/// columns, thus if you give zero for the column, and the result is `0xff` then
/// no key has been pressede.
open class C64Keyboard
{
    public init()
    {
        var keyMap: [VirtualKeyCode : KeyMapping] = [:]
        for row in 0 ..< C64Keyboard.rowCount
        {
            for col in 0 ..< C64Keyboard.columnCount
            {
                if let keyCode = matrixKeys[row][col]
                {
                    keyMap[keyCode] = KeyMapping(row: row, col: col)
                }
            }
        }
        self.keyMap = keyMap
    }

    private static let rowCount = 8
    private static let columnCount = 8

    private var matrix: [MatrixRow] = [MatrixRow](repeating: MatrixRow([]), count: columnCount)

    // A keybaord matrix off the internet. Not completely sure it is correct for
    // the ROMs I am loading
    private let matrixKeys: [[VirtualKeyCode?]] =
    [
        [.BackSpace  , ._3       , ._5, ._7, ._9, .Plus , .pound     , ._1       ],
        [.Return     , .W        , .R , .Y , .I , .P    , .Asterisk  , .LeftArrow],
        [.CursorRight, .A        , .D , .G , .J , .L    , .SemiColon , .Ctrl     ],
        [nil         , ._4       , ._6, ._8, ._0, .Minus, .Home      , ._2       ],
        [nil         , .Z        , .C , .B , .M , .Dot  , .RightShift, .Space    ],
        [nil         , .S        , .F , .H , .K , .Colon, .Equals    , .command  ],
        [nil         , .E        , .T , .U , .O , .At   , .UpArrow   , .Q        ],
        [.CursorDown , .LeftShift, .X , .V , .N , .Comma, .Slash     , .CtrlC    ],
    ]

    // Some keys on the Mac don't map to single keys on the C64 or map to
    // different keys. For example, `lpar` on the Mac is a shifted 9. On the C64,
    // it is a shifted 8. So we need to press down shift and 8. If a keycode is
    // not in this table, we will just use itself
    private let translationTable: [VirtualKeyCode : [VirtualKeyCode]] =
    [
        .LPar        : [.LeftShift, ._8],
    	.RPar        : [.LeftShift, ._9],
        .At          : [.At],
    	.pound       : [.pound],
        .altHash     : [.LeftShift, ._3],
        .UpArrow     : [.UpArrow ],
        .Ampersand   : [.LeftShift, ._6],
        .Asterisk    : [.Asterisk],
        .Plus        : [.Plus],
        .LSquare     : [.LeftShift, .Colon],
        .RSquare     : [.LeftShift, .SemiColon],
        .Quote       : [.LeftShift, ._7],
        .DoubleQuote : [.LeftShift, ._2],
        .Colon       : [.Colon],
        .CtrlC       : [.CtrlC],
        .CtrlR       : [.LeftShift, .CtrlC],
    ]

    private let keyMap: [VirtualKeyCode : KeyMapping] //=

    /// This proerty is the currently selected column(s). Note that the sense is
    /// inverted. 0 means all columns selecgted, 0xff means no columns are selected
    open var selectedColumn: S65Byte = 0xff
    {
        didSet { selectRow() }
    }

    /// The bus instance that reads the selected row. The sense id inverted. A 0
    /// means the key on the selected row for one of the selected columns is
    /// down.
    open var selectedRow = Bus()

    private func selectRow()
    {
        var ret: S65Byte = 0xff
        for col in 0 ..< C64Keyboard.columnCount
        {
			if selectedColumn & (1 << col) == 0
            {
                ret &= matrix[col].outputByte
            }
        }
        selectedRow.value = ret
    }

    private func makeKeyCodeSequence(compositeKeyCode: VirtualKeyCode) -> [VirtualKeyCode]
    {
        let ret: [VirtualKeyCode]
        if let translation = translationTable[compositeKeyCode]
        {
            ret = translation
        }
        else
        {
            var theSequence: [VirtualKeyCode] = []
            if compositeKeyCode.contains(.Shift)
            {
				theSequence.append(.LeftShift)
            }
            if compositeKeyCode.contains(.Ctrl)
            {
                theSequence.append(.Ctrl)
            }
            if compositeKeyCode.contains(.command)
            {
                theSequence.append(.command)
            }
            theSequence.append(compositeKeyCode.subtracting(.ModifierMask))
            ret = theSequence
        }
        return ret
    }

    /// Sets the keyboard matrix for the combination of Mac keycode and
    /// modifiers.
    ///
    /// - Parameters:
    ///   - newKeyCode: The new key code to select
    ///   - modifierFlags: Modifiers currently set on the Mac keyboard.
    open func setKeyCode(_ newKeyCode: UInt16, modifierFlags: NSEvent.ModifierFlags)
    {
        log.debug("\(#function) keycode = \(newKeyCode.hexString), modifiers = \(modifierFlags)")
        let modifiers = VirtualKeyCode.c64FromEventModifiers(modifierFlags)
        let compositeKeyCode = VirtualKeyCode([ VirtualKeyCode(rawValue: UInt32(newKeyCode)), modifiers])
        let keyCodeSequence = makeKeyCodeSequence(compositeKeyCode: compositeKeyCode)
        keyCodeSequence.forEach { setMatrixForKeyCode($0) }
    }

    /// Resets the keyboard matrix for the combination of Mac keycode and
    /// modifiers.
    ///
    /// - Parameters:
    ///   - newKeyCode: The  key code to deselect
    ///   - modifierFlags: Modifiers currently set on the Mac keyboard.
    open func resetKeyCode(_ newKeyCode: UInt16, modifierFlags: NSEvent.ModifierFlags)
    {
        let modifiers = VirtualKeyCode.c64FromEventModifiers(modifierFlags)
        let compositeKeyCode = VirtualKeyCode([ VirtualKeyCode(rawValue: UInt32(newKeyCode)),
                                                modifiers])
        let keyCodeSequence = makeKeyCodeSequence(compositeKeyCode: compositeKeyCode).reversed()
        keyCodeSequence.forEach { resetMatrixForKeyCode($0) }
    }


    /// Emulate the user pressing a key
    ///
    /// - Parameter keyCode: The virtual keycode of the key pressed.
    open func setMatrixForKeyCode(_ keyCode: VirtualKeyCode)
    {
        if let keyMapping = keyMap[keyCode]
        {
            log.debug("\(#function): setting key map \(keyMapping)")
            matrix[keyMapping.col].insert(MatrixRow.bit(keyMapping.row))
        }
        else
        {
            log.warn("\(self): set unmapped key code: \(keyCode.hexString)")
        }
    }


    /// Turn off a key code. This is equivalent to the user releasing a key
    ///
    /// - Parameter keyCode: The virtual key code of the key released.
    open func resetMatrixForKeyCode(_ keyCode: VirtualKeyCode)
    {
        if let keyMapping = keyMap[keyCode]
        {
            matrix[keyMapping.col].remove(MatrixRow.bit(keyMapping.row))
        }
        else
        {
            log.warn("\(self): reset unmapped key code: \(keyCode.hexString)")
        }
    }
}
