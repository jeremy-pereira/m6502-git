//
//  VicII.swift
//  Swift6502
//
//  Created by Jeremy Pereira on 04/11/2018.
//
import Darwin

private let log = Logger.getLogger("Swift6502.VicII")

public class VicII: MemoryMappedDevice
{

    /// The size of the character ROM
    public static let characterROMSize = 256 * 8

    /// Size of the Vic address space - 16 kbytes
    public static let memorySize = 1 << 14
    /// Number of  registers, note that the last 17 are unused and reading

    private static let colourRegisterCount = 15

    private static let control1Index: S65Address = 0x11
    private static let control2Index: S65Address = 0x16
    private static let memoryPointerIndex: S65Address = 0x18

    private static let colourRegisterStart = 0x20

    private static let usedRegisterCount = 0x2f

    /// always returns 0xff
    public static let registerCount = 64
    private unowned let pla: C64RamPla

    private var control1 = Control1()
    {
        didSet
        {
            log.debug("Settig control 1 to $\(control1.rawValue.hexString)")
        }
    }
    private var control2 = Control2()
    private var memoryPointer = MemoryPointer()
    private var colours = Array<S65Byte>(repeating: 0, count: colourRegisterCount)

    public init(pla: C64RamPla)
    {
		self.pla = pla
    }


    /// Copy the VIC memory to an arbitrary pointer. The pointer must point
    /// to an area of 16 k bytes (2^16).
    ///
    /// - Parameter to: The pointer to which to copy to
    public func copyMemory(to: UnsafeMutableRawPointer)
    {
        pla.vicCopy(to: to)
    }

    /// Copy the VIC registers to a block of memory
    ///
    /// - Parameter to: Place to copy the registers.
    public func copyRegisters(to: UnsafeMutableRawPointer)
    {
        var bytePointer = to.bindMemory(to: S65Byte.self, capacity: VicII.registerCount)
        bytePointer[VicII.memoryPointerIndex] = memoryPointer.rawValue
        bytePointer[VicII.control1Index] = control1.rawValue
        bytePointer[VicII.control2Index] = control2.rawValue
        memcpy(to + VicII.colourRegisterStart, colours, VicII.colourRegisterCount)
    }

    // MARK: MemoryMappedDevice

    public let portCount = registerCount

    public var baseAddress: S65Address = 0

    public func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        guard portNumber < VicII.usedRegisterCount else { return }
        switch portNumber
        {
        case VicII.control1Index:
            control1.rawValue = byte
        case VicII.control2Index:
			control2.rawValue = byte
        case VicII.memoryPointerIndex:
            memoryPointer.rawValue = byte
            log.debug("Relocated character set to $\(memoryPointer.charRomLoc.hexString)")
        case 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
             0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e:
            colours[Int(portNumber - 0x20)] = byte
        default:
            log.warn("\(#function) does not handle port $\(portNumber.hexString) with byte $\(byte.hexString)")
        }
    }

    public func willRead(_ portNumber: S65Address) -> S65Byte
    {
        guard portNumber < VicII.usedRegisterCount else { return 0xff }

        let ret: S65Byte
        switch portNumber
        {
        case VicII.control1Index:
            ret = control1.rawValue
        case 0x12:
            ret = 0x00 // Supress the log message for raster register
        case VicII.control2Index:
            ret = control2.rawValue
        case 0x18:
			ret = memoryPointer.rawValue
        case 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
             0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e:
            ret = colours[Int(portNumber - 0x20)]
        default:
            ret = 0xff
            log.warn("\(#function) does not handle port $\(portNumber.hexString)")
        }
        return ret
    }

    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("We do not support remapping devices")
    }
}

fileprivate extension VicII
{
	struct MemoryPointer
    {
        fileprivate var rawValue: S65Byte
        {
            get
            {
				let ret = ((videoMatrixLoc >> videoMatrixShift) & videoMatrixMask)
                	    | ((charRomLoc >> charRomShift) & charRomMask)
                		| 1 // low bit is unused and reads as 1
                return S65Byte(ret)
            }
            set
            {
				videoMatrixLoc = (S65Address(newValue) & videoMatrixMask) << videoMatrixShift
                charRomLoc = (S65Address(newValue) & charRomMask) << charRomShift
            }
        }

        private let charRomMask: S65Address = 0b0000_1110
        private let videoMatrixMask: S65Address = 0b1111_0000
        private let charRomShift:S65Address = 10
        private let videoMatrixShift: S65Address = 6
        fileprivate var charRomLoc: S65Address = 0x1000
        fileprivate var videoMatrixLoc: S65Address = 0x0400
    }
}

fileprivate extension VicII
{
    struct Control1: OptionSet
    {
        var rawValue: S65Byte
    }
    struct Control2: OptionSet
    {
        var rawValue: S65Byte
    }
}

/// Models the Commodore 64 colour RAM. The colour RAM is four bits wide. A
/// real VIC II chip retrieves the colour data at the same time as the video
/// matrix data. We don't do that.
public class ColourRAM: MemoryMappedDevice
{
    public init(baseAddress: S65Address)
    {
        self.baseAddress = baseAddress
    }

    /// The colour RAM is 1 kByte in size
    public static let size = 0x400

    private var ram = Array<S65Byte>(repeating: 0, count: ColourRAM.size)

    public func copy(to: UnsafeMutableRawPointer)
    {
        memcpy(to, ram, ColourRAM.size)
    }

    public var portCount: Int = ColourRAM.size

    public private(set) var baseAddress: S65Address

    public func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        ram[Int(portNumber)] = byte & 0xf
    }

    public func willRead(_ portNumber: S65Address) -> S65Byte
    {
        let ret = ram[Int(portNumber)]
		return ret
    }

    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("We do not support pushing a device")
    }
}

extension UnsafeMutablePointer
{
    subscript(_ i: S65Address) -> Pointee
    {
        get { return self[Int(i)] }
        set { self[Int(i)] = newValue }
    }
}
