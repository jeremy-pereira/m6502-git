//
//  Mos65xx.swift
//  M6502
//
//  Created by Jeremy Pereira on 06/08/2017.
//
//

import Foundation


/// Protocol for a device that allows direct access to its memory space.
public protocol DirectMemoryAccessible
{
    /// Function that copies bytes from memory directly to a raw buffer. The raw
    /// buffer must have the capacity to store the requested count of bytes or
    /// bad unpredictable things will happen. The address + the count must not
    /// exceed `S65Address.max` or bad things will happen - maybe a fatal error.
    ///
    /// The interface was designed so implementations can use `memcpy()` and the
    /// `to` argument can be the content of a buffer for a compute pipeline.
    ///
    /// - Parameters:
    ///   - to: Raw buffer into which we put the bytes.
    ///   - from: 16 bit address from which to get the bytes
    ///   - count: Number of bytes to get.
    func copy(to: UnsafeMutableRawPointer, from: S65Address, count: Int)
}

/// Type alias for a function that handles an error
/// - Parameter CPU6502: The CPU that has had a fatal errpr
/// - Parameter message: A descriptive message disclosing the reason for the
/// fatality.
public typealias FatalErrorHandler = (Mos65xx, (@autoclosure () -> String)) -> ()

/// A protocol that represents a 65xx family processor.
public protocol Mos65xx: AnyObject, MicroSecondClock, DeviceWithRegisters, DirectMemoryAccessible
{
	/// Run the CPU for more or less the given number of cycles. The CPU may 
    /// overrun slightly if it doesn't have cycle accuracy. For example, if you
    /// set cycles to 1 and the next instructuion takes 6, it is allowavle to 
    /// overrun by 5 cycles.
	///
	/// - Parameter cycles: Cycles to run for.
	func runForCycles(_ cycles: Int)


    /// Controls CPU tracing. If set, should output trace information somehow
    var trace: Bool { get set }


    /// Models the reset line. Toggle low then high to make the CPU reset 
    /// itself.
    var notReset: Bool { get set }


    /// Remove a clock driver device from the CPU. The device will then no 
    /// longer receive clock notifications.
    ///
    /// - Parameter driver: driver to remove
    func removeClockDriver(_ driver: ClockDriver)


    /// Add a clock driver to the CPU. Use this function that adds a device that
    /// needs to be polled by the CPU based on clock cycles.
    ///
    /// - Parameter driver: The clock driver to add.
    func addClockDriver(_ driver : ClockDriver)


    /// Function to set a byte at a given address. This is for use by external
    /// code and so must not trigger memory mapped device behaviour.
    ///
    /// - Parameters:
    ///   - byte: The byte to set.
    ///   - address: The address where to put it.
    func set(byte: S65Byte, at address: S65Address)


    /// Get the byte at the given address.
    ///
    /// This is for use by external code and so must not trigger memory mapped
    /// device behaviour.
    ///
    /// - Parameter address: Address from where to get the byte
    /// - Returns: The given byte
    func byte(at address: S65Address) -> S65Byte
    /// Get the word at the given address.
    ///
    /// This is for use by external code and so must not trigger memory mapped
    /// device behaviour.
    ///
    /// - Parameter address: Address from where to get the byte
    /// - Returns: The given word
    func word(at address: S65Address) -> S65Address


    /// Map a device into memory.
    ///
    /// - Parameter device: Device to map
    /// - Throws: If something goes wrong.
    func mapDevice(_ device: MemoryMappedDevice) throws


    /// A wire that external devices can attach to to cause an interrupt. Note
    /// that the interrupt triggers when low. i.e. the value of `irqwWire` s
    /// must be set to false.
    var irqWire: Wire { get }


    /// The fatal error hansddler to use if sometghing goes wrong internally in
    /// the CPU.
    var fatalErrorHandler: FatalErrorHandler { get set }

    /// Will be true if the CPU has encountered a fatal error
    var fatalErrorHappened: Bool { get }


    /// Returns the given 16 bit register
    ///
    /// - Parameter name: Register to get
    /// - Returns: the value of the register or nil if it doesn't exist
    func register16(_ name: String) -> S65Address?
    /// Returns the given 8 bit register
    ///
    /// - Parameter name: Register to get
    /// - Returns: the value of the register or nil if it doesn't exist
    func register8(_ name: String) -> S65Byte?


    /// Set the given 8 but register with the given byte
    ///
    /// - Parameters:
    ///   - register: Register to set
    ///   - byte: byte to set it to
    /// - Returns: true if the register exists
    func set(register: String, byte: S65Byte) -> Bool

    /// Load an image from a file on disk into the address space.
    ///
    /// This funmction has a default implementation that uses `set(byte:at:)`
    /// which may not be appropriate if using banked or shadowed memory. For
    /// example, on the C64 6510, it will write to the RAM even when the ROM
    /// banks are enabled.
    ///
    /// - Parameters:
    ///   - fileURL: URL of file to load
    ///   - loadAddress: Address to start at
    ///   - isROM: True if the image is a ROM image
    /// - Throws: If we can't load the file.
    func loadImage(_ fileURL: URL, at loadAddress: S65Address, isROM: Bool) throws

    /// Control trace at an address. If `trace` is a closure, the closure is
    /// executed when the instruction at the address is fetched.
    ///
    /// - Parameters:
    ///   - address: The address to set the trace at.
    ///   - trace: function to execute when the cpu fetches the instruction at
    ///            the given address.
    func set(address: S65Address, trace: (() -> ())?)

    /// Trace function for each JSR that is encountered.
    ///
    /// The function takes two parameters:
    /// - `S65Address` the location of the `JSR`
    /// - `S65Address` the target of the `JSR`

    var jsrTrace: ((S65Address, S65Address) -> ())? { get set }

    /// Trace function for each rts that is encountered
    ///
    /// The function takes one parameter:
    /// - `S65Address` the location of the `RTS`
    var rtsTrace: ((S65Address) -> ())? { get set }
}

public extension Mos65xx
{

    /// Set a word at a particular address. If the address is `0xffff`, the 
    /// high byte will be placed at `0x0000`. Placement is, of course, 
    /// little endian.
    ///
    /// This is for use by external code and so must not trigger memory mapped
    /// device behaviour.
    ///
    /// - Parameters:
    ///   - word: The word to put in memory
    ///   - address: Where to put the word.
	func set(word: S65Address, at address: S65Address)
    {
        set(byte: word.lowByte, at: address)
        set(byte: word.highByte, at: address &+ 1)
    }
    /// Function to set an array of byte starting at a given address.
    /// If the array would extend beyond the top of memory, it wraps round to
    /// the bottom again.
    ///
    /// This is for use by external code and so must not trigger memory mapped
    /// device behaviour.
    ///
    /// - Parameters:
    ///   - bytes: Bytes to put in memory
    ///   - address: Address to start at
	func set(bytes: [S65Byte], at address: S65Address)
    {
        var currentAddress = address
        for byte in bytes
        {
            set(byte: byte, at: currentAddress)
            currentAddress = currentAddress &+ 1
        }
    }
    /// Function to set an array slice of byte starting at a given address.
    /// If the array would extend beyond the top of memory, it wraps round to
    /// the bottom again.
    ///
    /// This is for use by external code and so must not trigger memory mapped
    /// device behaviour.
    ///
    /// - Parameters:
    ///   - bytes: Bytes to put in memory
    ///   - address: Address to start at
	func set(bytes: ArraySlice<S65Byte>, at address: S65Address)
    {
        var currentAddress = address
        for byte in bytes
        {
            set(byte: byte, at: currentAddress)
            currentAddress = currentAddress &+ 1
        }
    }

    /// Load a set of images into the CPU based on the content of an index file.
    ///
    /// The index file has the following format:
    ///
    ///     address \t file \t "R"?
    ///
    /// The address is the address at which to load the image. The file is the
    /// name of the image relative to the index file. If there is an `R` on the
    /// end, the image is assumed to be ROM. The fields are separated by tab 
    /// characters.
    ///
    /// - Parameter fileURL: URL of the index file.
    /// - Throws: If something cannot be loaded.
	func loadImagesFromURL(_ fileURL: URL) throws
    {
        let fileContent = try NSString(contentsOfFile: fileURL.path, encoding: String.Encoding.utf8.rawValue)
        let filePath = fileURL.absoluteURL.path
        let directoryPath = NSString(string: filePath).deletingLastPathComponent
        let directory = URL(fileURLWithPath: directoryPath)
        let lines = fileContent.components(separatedBy: "\n")
        for line in lines
        {
            let components = line.components(separatedBy: "\t")
            switch (components.count)
            {
            case 1:
                if components[0] != ""
                {
                    throw Error6502.fileLoad(message: "Invalid line in \(fileURL)")
                }
            case 2, 3:
                let loadAddressString = components[0]
                let loadFilePath = components[1]
                var isROM = false
                if components.count == 3
                {
                    isROM = components[2] == "R"
                }
                if let loadAddress = loadAddressString.s65Address
                {
                    if let loadFileURL = URL(string: loadFilePath, relativeTo: directory)
                    {
                        try loadImage(loadFileURL, at: loadAddress, isROM: isROM)
                    }
                    else
                    {
                        throw Error6502.fileLoad(message: "Cannot determine file for ROM image \(loadFilePath)")
                    }
                }
                else
                {
                    throw Error6502.fileLoad(message: "Invalid load address: \(loadAddressString)")
                }
            default:
                throw Error6502.fileLoad(message: "Invalid line in \(fileURL)")
            }
        }
    }
}

/// Interface that describes the 6510 incororated into the Commodore 64
public protocol Mos6510: Mos65xx
{
    /// The IO port. This is the only difference as compared to a 6502.
    var ioPort: TriStatePort { get }

    /// Load an image at a particular address.
    ///
    /// The image is loaded into the RAM unless `isRom` is `true` in which case
    /// it will be loaded into ROM.
    ///
    /// - Parameters:
    ///   - image: The image to load.
    ///   - at: The address at which to load it
    ///   - isRom: If true load to ROM, if false load to RAM.
    func load(image: [S65Byte], at: S65Address, isRom: Bool)
}

/// We want to be able to use a raw pointer as memory. So we add an extension to
/// it to conform to `MemoryMappable`
extension UnsafeMutablePointer: DirectMemoryAccessible where Pointee == S65Byte
{
    public var pointerToStart: UnsafeMutablePointer<S65Byte>
    {
        return self
    }

    public func copy(to: UnsafeMutableRawPointer, from: S65Address, count: Int)
    {
        let fromPtr = self + Int(from)
        memcpy(to, fromPtr, count)
    }
}

/// We want to be able to use a raw pointer as memory. So we add an extension to
/// it to conform to `MemoryMappable`
extension UnsafeMutablePointer: MemoryMappable where Pointee == S65Byte
{
    public func get(address: S65Address) -> S65Byte
    {
        return self[Int(address)]
    }

    public func set(address: S65Address, byte: S65Byte)
    {
        self[Int(address)] = byte
    }
}
