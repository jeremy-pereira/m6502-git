//
//  RegisterInfo.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/06/2015.
//
//

import Foundation


/// Describes a register type
///
/// - unsigned8: 8 bit unsigned int
/// - unsigned16: 16 bit unsigned int
/// - flags: Register is a set of one-bit flags
public enum RegisterType
{
    case unsigned8
    case unsigned16
    case flags
}
/// Info for a paticular register
open class RegisterInfo
{

    /// The name of the register
    open var name: String
    /// The type of the register (8 bit, 16 bit, flags)
    open var registerType: RegisterType

    /// Names of the flags for flag register
    open var flagNames: [String]

    init(name: String, registerType: RegisterType, flagNames: [String] = [])
    {
        self.name = name
        self.registerType = registerType
        self.flagNames = flagNames
    }
}


/// Any device that can has registers that can be described for a UI
public protocol DeviceWithRegisters
{
    /// The number of regiters the device has
    var numberOfRegisters: Int { get }

    /// Get the information for a particular register
    ///
    /// - Parameter index: Which register
    /// - Returns: The register information
    func infoForRegister(_ index: Int) -> RegisterInfo

    /// The value of the register cast to an Int
    ///
    /// - Parameter index: The index of the register
    /// - Returns: The value of the register cast to an Int
    func valueOfRegister(_ index: Int) -> Int
}
