//
//  CPU6502Bus.swift
//  M6502
//
//  Created by Jeremy Pereira on 07/08/2017.
//
//
//
//  CPU6502.swift
//  M6502
//
//  Created by Jeremy Pereira on 12/06/2015.
//
//

import Foundation

postfix operator &++

// Defining the operator here hopefully inlines it saving about 4% of execution time
fileprivate extension S65Address
{
	static postfix func &++(x: inout S65Address) -> S65Address
    {
        let ret = x
        x = x &+ 1
        return ret
    }
}

fileprivate extension UInt
{
    var isTCNegative: Bool { return self & 0x80 != 0 }
    func isSet(bit: Int) -> Bool
    {
        return self & UInt(1 << bit) != 0
    }
}


fileprivate prefix func ~(mask: StatusRegister) -> StatusRegister
{
    return mask.symmetricDifference(.AllFlags)
}

private let FIRST_INTERNAL = 0x100		// Internal opc odes > FF so no clashes with external opcodes
private let STOP  = FIRST_INTERNAL + 0	// STOP pseudo opcode stops the CPU
private let RESET = FIRST_INTERNAL + 1	// Internal reset instruction
private let IRQ_SERVICE  = FIRST_INTERNAL + 2	// Run the interrupt service routine
private let NMI_SERVICE  = FIRST_INTERNAL + 3	// Run the NMI service routine
private let Unimplemented = 0xFFFF	// Unimplemented opcode

private let NOP = 0xEA				// No operation instruction

private enum ResetState
{
    case resetLineLow
    case pending
    case none
}

fileprivate enum FunctionGroup: Int, CustomStringConvertible
{
    case branch = 0		// Used for all 8-bit branch instructions
    case ADC		// Add function
    case AND 		// And accumulator with data
    case ASL 		// Arithmetic shift left
    case ASR 		// Arithmetic shift right operand
    case ASW 		// Arithmetic shift word
    case AUG 		// Augmented instructions
    case branchBitTest 		// BBR or BBS instruction branch bit reset??
    case BIT 		// Bit test function
    case BRK 		// Break instruction
    case CMP 		// Compare
    case DEC 		// Decrement operand
    case DEW 		// Decrement word
    case EOR 		// Exclusive or accumulator and operand
    case INC 		// Increment operand
    case INW 		// Increment word
    case JSR 		// Jump saving return
    case LSR 		// Logical shift right operand
    case NEG 		// Negate accumulator
    case NOP 		// No operation
    case or			// bitwise or
    case RMB 		// RMB instruction
    case ROL 		// Rotate left
    case ROR 		// Rotate right operand
    case ROW 		// Rotate word
    case RTN 		// Return and adjust stack pointer
    case RTS 		// Return to Saved address
    case SBC 		// Subtract borrowing carry
    case SMB 		// SMB instruction
    case testAndChange 	// Test and set/reset bits
    case move		// Move from one place to another
    case moveFlags	// Move setting flags
    case setFlag	// Set or clear a flag
    case Custom		// Custom function group, for non opcodes
    case Stop  		// Processor is in a stopped state
    case Reset 		// Processor is going through the reset sequence
    case IrqService	// Interrupt request service.
    case NmiService	// Non maskable interrupt request service.
    case None		// No function group defined for the opcode yet

    var description: String
    {
        switch self
        {
        case .ADC: return "ADC"		// Add function
        case .AND: return "AND"		// And accumulator with data
        case .ASL: return "ASL"		// Arithmetic shift left
        case .ASR: return "ASR"		// Arithmetic shift right operand
        case .ASW: return "ASW"		// Arithmetic shift word
        case .AUG: return "AUG"		// Augmented instructions
        case .branchBitTest: return "BB"		// Branch with bit test
        case .BIT: return "BIT"		// Bit test function
        case .BRK: return "BRK"		// Break instruction
        case .CMP: return "C"		// Compare
        case .DEC: return "DE"		// Decrement operand
        case .DEW: return "DEW"		// Decrement word
        case .EOR: return "EOR"		// Exclusive or accumulator and operand
        case .INC: return "IN"		// Increment operand
        case .INW: return "INW"		// Increment word
        case .JSR: return "JSR"		// Jump saving return
        case .LSR: return "LSR"		// Logical shift right operand
        case .NEG: return "NEG"		// Negate accumulator
        case .NOP: return "NOP"		// No operation
        case .or:  return "ORA"		// Bitwise Or
        case .RMB: return "RMB"		// RMB instruction
        case .ROL: return "ROL"		// Rotate left
        case .ROR: return "ROR"		// Rotate right operand
        case .ROW: return "ROW"		// Rotate word
        case .RTN: return "RTN"		// Return and adjust stack pointer
        case .RTS: return "RTS"		// Return to Saved address
        case .SBC: return "SBC"		// Subtract borrowing carry
        case .SMB: return "SMB"		// SMB instruction
        case .testAndChange: return "T"		// Test and change bits
        case .branch: return "B"	// 8 bit branches
        case .move: return "ST"		// Move from one place to another
        case .moveFlags: return "LD"		// Move from one place to another with flaga
        case .setFlag: return "set flag"	// Will not actually be used: SE or CL
        case .Custom: return "Custom"		// Custom function group, for non opcodes
        case .Stop: return "*** stop"		// Processor is in a stopped state
        case .Reset: return "*** reset"		// Processor is going through the reset sequence
        case .IrqService: return "*** IRQ"		// Interrupt request service.
        case .NmiService: return "*** NMI"		// Non maskable interrupt request service.
        case .None: return "None"		// No function group defined for the opcode yet
        }
    }
}

private enum AddressMode: Int
{
    case a = 0
    case x
    case y
    case z
    case b
    case p
    case spLow
    case spHigh
    case popSP
    case popSPWord
    case pushSP
    case pushSPWord
    case basePage
    case basePageWord
    case relative
    case abs
    case absWord
    case absX
    case absXWord	// Used only for JMP
    case absY
    case absInd
    case absXInd
    case basePageX
    case basePageY
    case basePageXInd
    case basePageIndY
    case basePageIndZ
    case immediate
    case immediateWord
    case relativeWord
    case sviIndY
    case pc
    case useSource		// Use for when you put the op result byte back where the source came from
    case useSourceWord	// Use for when you put the op result word back where the source came from
    case none

    public func formatOperand(_ operandBytes: [S65Byte]) -> String
    {
        switch (self)
        {
        case .abs, .relativeWord, .absWord:
            return "$" + operandBytes[1].hexString + operandBytes[0].hexString + "    "
        case .absInd:
            return "($" + operandBytes[1].hexString + operandBytes[0].hexString + ")  "
        case .absX, .absXWord:
            return "$" + operandBytes[1].hexString + operandBytes[0].hexString + ",X  "
        case .absXInd:
            return "($" + operandBytes[1].hexString + operandBytes[0].hexString + ",X)"
        case .absY:
            return "$" + operandBytes[1].hexString + operandBytes[0].hexString + ",Y  "
        case .basePage, .basePageWord, .relative:
            return "$" + operandBytes[0].hexString + "      "
        case .basePageX:
            return "$" + operandBytes[0].hexString + ",X    "
        case .basePageY:
            return "$" + operandBytes[0].hexString + ",Y    "
        case .basePageXInd:
            return "($" + operandBytes[0].hexString + ",X)  "
        case .basePageIndY:
            return "($" + operandBytes[0].hexString + "),Y  "
        case .basePageIndZ:
            return "($" + operandBytes[0].hexString + "),Z  "
        case .immediate:
            return "#$" + operandBytes[0].hexString + "     "
        case .immediateWord:
            return "#$" + operandBytes[1].hexString + operandBytes[0].hexString + "     "
        case .sviIndY:
            return "(sp+#$\(operandBytes[0].hexString)),Y"
        case .a:
            return "A"
        case .b:
            return "B"
        case .x:
            return "X"
        case .y:
            return "Y"
        case .z:
            return "Z"
        case .p:
            return "P"
        case .spLow, .spHigh:
            return "S"
        case .popSP, .popSPWord, .pushSP, .pushSPWord, .pc:
            return ""
        case .useSource, .useSourceWord: return ""
        case .none:
            return ""
        }
    }
    var size: Int
    {
        switch (self)
        {
        case .basePage, .basePageWord, .basePageIndY, .basePageIndZ, .basePageY, .basePageXInd,
             .basePageX, .immediate, .relative, .sviIndY:
            return 1
        case .none, .a, .b, .x, .y, .z, .spLow, .spHigh, .popSP,
             .popSPWord, .pushSP, .pushSPWord, .pc, .p, .useSource, .useSourceWord:
            return 0
        case .abs, .relativeWord, .immediateWord, .absX, .absXInd, .absY,
             .absInd, .absWord, .absXWord:
            return 2
        }
    }

    var isImplicit: Bool
    {
        switch self
        {
        case .basePage, .basePageWord, .basePageIndY, .basePageIndZ, .basePageY, .basePageXInd,
             .basePageX, .immediate, .relative, .sviIndY, .abs, .relativeWord,
             .immediateWord, .absX, .absXInd, .absY, .absInd,
             .absXWord, .absWord:
            return false
        case .none, .a, .b, .x, .y, .z, .spLow, .spHigh, .popSP, .popSPWord,
             .pushSP, .pushSPWord, .pc, .p, .useSource, .useSourceWord:
            return true
        }
    }
}

private struct OpCodeLookup
{
    let opCode: Int
    let cycleCount: Int
    let function: FunctionGroup
    let source: AddressMode
    let source2: AddressMode
    let dest: AddressMode
    let dest2: AddressMode
    let flag: StatusRegister
    let sense: Bool


    /// Initialise a new opcode
    ///
    /// - Parameters:
    ///   - opCode: The opcode number
    ///   - cycleCount: How many cycvles it takes ro execute
    ///   - function: What function to perform
    ///   - source: Address mode of the source for the operation
    ///   - source2: Addrss mode for second operand if needed
    ///   - dest: Address mode for the destination of the operation
    ///   - addressMode: Deprecated: address mode for the operation
    ///   - flag: Flag for conditional operations
    ///   - sense: Sense for the conditional operation to be executed
    init(opCode: Int,
         cycleCount: Int,
         function: FunctionGroup = .None,
         source: AddressMode = .none,
         source2: AddressMode = .none,
         dest: AddressMode = .none,
         dest2: AddressMode = .none,
         flag: StatusRegister = StatusRegister(rawValue: 0),
         sense: Bool = false)
    {
        self.opCode = opCode
        self.cycleCount = cycleCount
        self.function = function
        self.source = source
        self.source2 = source2
        self.dest = dest
        self.dest2 = dest2
        self.flag = flag
        self.sense = sense
    }

    private static let unimplemented = OpCodeLookup(opCode: Unimplemented, cycleCount: 0)
}

private let opCodeTable: [OpCodeLookup] = [
    // 00
    OpCodeLookup(opCode: 0x00, cycleCount: 7, function: .BRK, source: .pc, source2: .p, dest: .pushSPWord, dest2: .pushSP),
    OpCodeLookup(opCode: 0x01, cycleCount: 6, function: .or, source: .basePageXInd, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x02, cycleCount: 1, function: .setFlag, flag: .E, sense: false),
    OpCodeLookup(opCode: 0x03, cycleCount: 1, function: .setFlag, flag: .E, sense: true),
    // 04
    OpCodeLookup(opCode: 0x04, cycleCount: 4, function: .testAndChange, source: .basePage, source2: .a, dest: .useSource, sense: true),
    OpCodeLookup(opCode: 0x05, cycleCount: 3, function: .or, source: .basePage, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x06, cycleCount: 5, function: .ASL, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0x07, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    // 08
    OpCodeLookup(opCode: 0x08, cycleCount: 3, function: .move, source: .p, dest: .pushSP), // PHP
    OpCodeLookup(opCode: 0x09, cycleCount: 2, function: .or, source: .immediate, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x0a, cycleCount: 2, function: .ASL, source: .a, dest: .a),
    OpCodeLookup(opCode: 0x0b, cycleCount: 1, function: .moveFlags, source: .spHigh, dest: .y),
    // 0C
    OpCodeLookup(opCode: 0x0c, cycleCount: 5, function: .testAndChange, source: .abs, source2: .a, dest: .useSource, sense: true),
    OpCodeLookup(opCode: 0x0d, cycleCount: 4, function: .or, source: .abs, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x0e, cycleCount: 6, function: .ASL, source: .abs, dest: .useSource),
    OpCodeLookup(opCode: 0x0f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 10
    OpCodeLookup(opCode: 0x10, cycleCount: 2, function: .branch, source: .relative, dest: .pc, flag: .N, sense: false),
    OpCodeLookup(opCode: 0x11, cycleCount: 5, function: .or, source: .basePageIndY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x12, cycleCount: 5, function: .or, source: .basePageIndZ, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x13, cycleCount: 3, function: .branch, source: .relativeWord, dest: .pc, flag: .N, sense: false),
    // 14
    OpCodeLookup(opCode: 0x14, cycleCount: 4, function: .testAndChange, source: .basePage, source2: .a, dest: .useSource, sense: false),
    OpCodeLookup(opCode: 0x15, cycleCount: 4, function: .or, source: .basePageX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x16, cycleCount: 6, function: .ASL, source: .basePageX, dest: .useSource),
    OpCodeLookup(opCode: 0x17, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    // 18
    OpCodeLookup(opCode: 0x18, cycleCount: 2, function: .setFlag, flag: .C, sense: false),
    OpCodeLookup(opCode: 0x19, cycleCount: 4, function: .or  , source: .absY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x1a, cycleCount: 1, function: .INC, source: .a, dest: .a),
    OpCodeLookup(opCode: 0x1b, cycleCount: 1, function: .INC, source: .z, dest: .z),
    // 1C
    OpCodeLookup(opCode: 0x1c, cycleCount: 5, function: .testAndChange, source: .abs, source2: .a, dest: .useSource, sense: false),
    OpCodeLookup(opCode: 0x1d, cycleCount: 4, function: .or, source: .absX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x1e, cycleCount: 7, function: .ASL, source: .absX, dest: .useSource),
    OpCodeLookup(opCode: 0x1f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 20
    OpCodeLookup(opCode: 0x20, cycleCount: 6, function: .JSR, source: .immediateWord, source2: .pc, dest: .pc, dest2: .pushSPWord), // JSR abs
    OpCodeLookup(opCode: 0x21, cycleCount: 6, function: .AND, source: .basePageXInd, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x22, cycleCount: 7, function: .JSR, source: .absWord, source2: .pc, dest: .pc, dest2: .pushSPWord),		// JSR (abs)
    OpCodeLookup(opCode: 0x23, cycleCount: 7, function: .JSR, source: .absXWord, source2: .pc, dest: .pc, dest2: .pushSPWord),		// JSR (abs,X)
    // 24
    OpCodeLookup(opCode: 0x24, cycleCount: 3, function: .BIT, source: .basePage, source2: .a),
    OpCodeLookup(opCode: 0x25, cycleCount: 3, function: .AND, source: .basePage, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x26, cycleCount: 5, function: .ROL, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0x27, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    // 28
    OpCodeLookup(opCode: 0x28, cycleCount: 4, function: .move  , source: .popSP, dest: .p),
    OpCodeLookup(opCode: 0x29, cycleCount: 2, function: .AND  , source: .immediate, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x2a, cycleCount: 2, function: .ROL, source: .a, dest: .a),
    OpCodeLookup(opCode: 0x2b, cycleCount: 1, function: .move , source: .y, dest: .spHigh),
    // 2C
    OpCodeLookup(opCode: 0x2c, cycleCount: 4, function: .BIT, source: .abs, source2: .a),
    OpCodeLookup(opCode: 0x2d, cycleCount: 4, function: .AND, source: .abs, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x2e, cycleCount: 6, function: .ROL, source: .abs, dest: .useSource),
    OpCodeLookup(opCode: 0x2f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 30
    OpCodeLookup(opCode: 0x30, cycleCount: 2, function: .branch, source: .relative, dest: .pc, flag: .N, sense: true),
    OpCodeLookup(opCode: 0x31, cycleCount: 5, function: .AND, source: .basePageIndY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x32, cycleCount: 5, function: .AND, source: .basePageIndZ, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x33, cycleCount: 3, function: .branch, source: .relativeWord, dest: .pc, flag: .N, sense: true),
    // 34
    OpCodeLookup(opCode: 0x34, cycleCount: 4, function: .BIT, source: .basePageX, source2: .a),
    OpCodeLookup(opCode: 0x35, cycleCount: 4, function: .AND, source: .basePageX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x36, cycleCount: 6, function: .ROL, source: .basePageX, dest: .useSource),
    OpCodeLookup(opCode: 0x37, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    // 38
    OpCodeLookup(opCode: 0x38, cycleCount: 2, function: .setFlag, flag: .C, sense: true),
    OpCodeLookup(opCode: 0x39, cycleCount: 4, function: .AND  , source: .absY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x3a, cycleCount: 1, function: .DEC, source: .a, dest: .a),
    OpCodeLookup(opCode: 0x3b, cycleCount: 1, function: .DEC, source: .z, dest: .z),
    // 3C
    OpCodeLookup(opCode: 0x3c, cycleCount: 5, function: .BIT, source: .absX, source2: .a),
    OpCodeLookup(opCode: 0x3d, cycleCount: 4, function: .AND, source: .absX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x3e, cycleCount: 7, function: .ROL, source: .absX, dest: .useSource),
    OpCodeLookup(opCode: 0x3f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 40
    OpCodeLookup(opCode: 0x40, cycleCount: 6, function: .move, source: .popSP, source2: .popSPWord, dest: .p, dest2: .pc), // RTI
    OpCodeLookup(opCode: 0x41, cycleCount: 6, function: .EOR, source: .basePageXInd, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x42, cycleCount: 2, function: .NEG, source: .a, dest: .a),
    OpCodeLookup(opCode: 0x43, cycleCount: 2, function: .ASR, source: .a, dest: .a),
    // 44
    OpCodeLookup(opCode: 0x44, cycleCount: 4, function: .ASR, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0x45, cycleCount: 3, function: .EOR, source: .basePage, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x46, cycleCount: 5, function: .LSR, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0x47, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    // 48
    OpCodeLookup(opCode: 0x48, cycleCount: 3, function: .move, source: .a, dest: .pushSP),
    OpCodeLookup(opCode: 0x49, cycleCount: 2, function: .EOR  , source: .immediate, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x4a, cycleCount: 2, function: .LSR, source: .a, dest: .a),
    OpCodeLookup(opCode: 0x4b, cycleCount: 1, function: .moveFlags, source: .a, dest: .z),
    // 4C
    OpCodeLookup(opCode: 0x4c, cycleCount: 3, function: .move, source: .immediateWord, dest: .pc), // JMP abs
    OpCodeLookup(opCode: 0x4d, cycleCount: 4, function: .EOR, source: .abs, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x4e, cycleCount: 6, function: .LSR, source: .abs, dest: .useSource),
    OpCodeLookup(opCode: 0x4f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 50
    OpCodeLookup(opCode: 0x50, cycleCount: 2, function: .branch, source: .relative, dest: .pc, flag: .V, sense: false),
    OpCodeLookup(opCode: 0x51, cycleCount: 5, function: .EOR, source: .basePageIndY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x52, cycleCount: 5, function: .EOR, source: .basePageIndZ, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x53, cycleCount: 3, function: .branch, source: .relativeWord, dest: .pc, flag: .V, sense: false),
    // 54
    OpCodeLookup(opCode: 0x54, cycleCount: 4, function: .ASR, source: .basePageX, dest: .useSource),
    OpCodeLookup(opCode: 0x55, cycleCount: 4, function: .EOR, source: .basePageX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x56, cycleCount: 6, function: .LSR, source: .basePageX, dest: .useSource),
    OpCodeLookup(opCode: 0x57, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    // 58
    OpCodeLookup(opCode: 0x58, cycleCount: 2, function: .setFlag, flag: .I, sense: false),
    OpCodeLookup(opCode: 0x59, cycleCount: 4, function: .EOR, source: .absY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x5a, cycleCount: 3, function: .move, source: .y, dest: .pushSP),
    OpCodeLookup(opCode: 0x5b, cycleCount: 1, function: .moveFlags, source: .a, dest: .b),
    OpCodeLookup(opCode: 0x5c, cycleCount: 4, function: .AUG, source: .immediate, source2: .immediateWord),
    OpCodeLookup(opCode: 0x5d, cycleCount: 4, function: .EOR, source: .absX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x5e, cycleCount: 7, function: .LSR, source: .absX, dest: .useSource),
    OpCodeLookup(opCode: 0x5f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 6
    OpCodeLookup(opCode: 0x60, cycleCount: 4, function: .RTS  , source: .popSPWord, dest: .pc),
    OpCodeLookup(opCode: 0x61, cycleCount: 6, function: .ADC  , source: .basePageXInd, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x62, cycleCount: 7, function: .RTN  , source: .immediate, dest: .pc),
    OpCodeLookup(opCode: 0x63, cycleCount: 5, function: .JSR  , source: .relativeWord, source2: .pc, dest: .pc, dest2: .pushSPWord), // BSR
    OpCodeLookup(opCode: 0x64, cycleCount: 3, function: .move , source: .z, dest: .basePage),
    OpCodeLookup(opCode: 0x65, cycleCount: 3, function: .ADC  , source: .basePage, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x66, cycleCount: 5, function: .ROR  , source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0x67, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0x68, cycleCount: 4, function: .moveFlags, source: .popSP, dest: .a),
    OpCodeLookup(opCode: 0x69, cycleCount: 2, function: .ADC  , source: .immediate, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x6a, cycleCount: 2, function: .ROR, source: .a, dest: .a),
    OpCodeLookup(opCode: 0x6b, cycleCount: 1, function: .moveFlags, source: .z, dest: .a),
    OpCodeLookup(opCode: 0x6c, cycleCount: 5, function: .move  , source: .absWord, dest: .pc),	// JMP (abs)
    OpCodeLookup(opCode: 0x6d, cycleCount: 4, function: .ADC  , source: .abs, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x6e, cycleCount: 6, function: .ROR  , source: .abs, dest: .useSource),
    OpCodeLookup(opCode: 0x6f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 7
    OpCodeLookup(opCode: 0x70, cycleCount: 2, function: .branch, source: .relative, dest: .pc, flag: .V, sense: true),
    OpCodeLookup(opCode: 0x71, cycleCount: 5, function: .ADC, source: .basePageIndY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x72, cycleCount: 5, function: .ADC, source: .basePageIndZ, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x73, cycleCount: 3, function: .branch, source: .relativeWord, dest: .pc, flag: .V, sense: true),
    // 74
    OpCodeLookup(opCode: 0x74, cycleCount: 3, function: .move, source: .z, dest: .basePageX),
    OpCodeLookup(opCode: 0x75, cycleCount: 4, function: .ADC , source: .basePageX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x76, cycleCount: 6, function: .ROR , source: .basePageX, dest: .useSource),
    OpCodeLookup(opCode: 0x77, cycleCount: 4, function: .RMB, source: .basePage, dest: .useSource),
    // 78
    OpCodeLookup(opCode: 0x78, cycleCount: 2, function: .setFlag, flag: .I, sense: true),
    OpCodeLookup(opCode: 0x79, cycleCount: 4, function: .ADC, source: .absY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x7a, cycleCount: 3, function: .moveFlags, source: .popSP, dest: .y),
    OpCodeLookup(opCode: 0x7b, cycleCount: 1, function: .moveFlags, source: .b, dest: .a),
    OpCodeLookup(opCode: 0x7c, cycleCount: 5, function: .move, source: .absXWord, dest: .pc),	// JMP (abs,x)
    OpCodeLookup(opCode: 0x7d, cycleCount: 4, function: .ADC, source: .absX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0x7e, cycleCount: 7, function: .ROR, source: .absX, dest: .useSource),
    OpCodeLookup(opCode: 0x7f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: false),
    // 80
    OpCodeLookup(opCode: 0x80, cycleCount: 2, function: .move, source: .relative, dest: .pc), // BRA
    OpCodeLookup(opCode: 0x81, cycleCount: 6, function: .move, source: .a, dest: .basePageXInd),
    OpCodeLookup(opCode: 0x82, cycleCount: 6, function: .move, source: .a, dest: .sviIndY),
    OpCodeLookup(opCode: 0x83, cycleCount: 3, function: .move, source: .relativeWord, dest: .pc),
    // 84
    OpCodeLookup(opCode: 0x84, cycleCount: 3, function: .move, source: .y, dest: .basePage),
    OpCodeLookup(opCode: 0x85, cycleCount: 3, function: .move, source: .a, dest: .basePage),
    OpCodeLookup(opCode: 0x86, cycleCount: 3, function: .move, source: .x, dest: .basePage),
    OpCodeLookup(opCode: 0x87, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    // 88
    OpCodeLookup(opCode: 0x88, cycleCount: 2, function: .DEC, source: .y, dest: .y),
    OpCodeLookup(opCode: 0x89, cycleCount: 2, function: .BIT, source: .immediate, source2: .a),
    OpCodeLookup(opCode: 0x8a, cycleCount: 2, function: .moveFlags, source: .x, dest: .a),
    OpCodeLookup(opCode: 0x8b, cycleCount: 4, function: .move, source: .y, dest: .absX),
    OpCodeLookup(opCode: 0x8c, cycleCount: 4, function: .move, source: .y, dest: .abs),
    OpCodeLookup(opCode: 0x8d, cycleCount: 4, function: .move, source: .a, dest: .abs),
    OpCodeLookup(opCode: 0x8e, cycleCount: 4, function: .move, source: .x, dest: .abs),
    OpCodeLookup(opCode: 0x8f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),
    // 9
    OpCodeLookup(opCode: 0x90, cycleCount: 2, function: .branch , source: .relative, dest: .pc, flag: .C, sense: false),
    OpCodeLookup(opCode: 0x91, cycleCount: 6, function: .move, source: .a, dest: .basePageIndY),
    OpCodeLookup(opCode: 0x92, cycleCount: 5, function: .move, source: .a, dest: .basePageIndZ),
    OpCodeLookup(opCode: 0x93, cycleCount: 3, function: .branch , source: .relativeWord, dest: .pc, flag: .C, sense: false),
    OpCodeLookup(opCode: 0x94, cycleCount: 4, function: .move, source: .y, dest: .basePageX),
    OpCodeLookup(opCode: 0x95, cycleCount: 4, function: .move, source: .a, dest: .basePageX),
    OpCodeLookup(opCode: 0x96, cycleCount: 4, function: .move, source: .x, dest: .basePageY),
    OpCodeLookup(opCode: 0x97, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0x98, cycleCount: 2, function: .moveFlags, source: .y, dest: .a),
    OpCodeLookup(opCode: 0x99, cycleCount: 5, function: .move, source: .a, dest: .absY),
    OpCodeLookup(opCode: 0x9a, cycleCount: 2, function: .move , source: .x, dest: .spLow),
    OpCodeLookup(opCode: 0x9b, cycleCount: 4, function: .move, source: .x, dest: .absY),
    OpCodeLookup(opCode: 0x9c, cycleCount: 4, function: .move, source: .z, dest: .abs),
    OpCodeLookup(opCode: 0x9d, cycleCount: 5, function: .move, source: .a, dest: .absX),
    OpCodeLookup(opCode: 0x9e, cycleCount: 4, function: .move, source: .z, dest: .absX),
    OpCodeLookup(opCode: 0x9f, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),
    // A
    OpCodeLookup(opCode: 0xa0, cycleCount: 2, function: .moveFlags, source: .immediate   , dest: .y),
    OpCodeLookup(opCode: 0xa1, cycleCount: 6, function: .moveFlags, source: .basePageXInd, dest: .a),
    OpCodeLookup(opCode: 0xa2, cycleCount: 2, function: .moveFlags, source: .immediate   , dest: .x),
    OpCodeLookup(opCode: 0xa3, cycleCount: 2, function: .moveFlags, source: .immediate   , dest: .z),
    OpCodeLookup(opCode: 0xa4, cycleCount: 3, function: .moveFlags, source: .basePage, dest: .y),
    OpCodeLookup(opCode: 0xa5, cycleCount: 3, function: .moveFlags, source: .basePage, dest: .a),
    OpCodeLookup(opCode: 0xa6, cycleCount: 3, function: .moveFlags, source: .basePage, dest: .x),
    OpCodeLookup(opCode: 0xa7, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0xa8, cycleCount: 2, function: .moveFlags, source: .a, dest: .y),
    OpCodeLookup(opCode: 0xa9, cycleCount: 2, function: .moveFlags, source: .immediate, dest: .a),
    OpCodeLookup(opCode: 0xaa, cycleCount: 2, function: .moveFlags, source: .a, dest: .x),
    OpCodeLookup(opCode: 0xab, cycleCount: 4, function: .moveFlags, source: .abs, dest: .z),
    OpCodeLookup(opCode: 0xac, cycleCount: 4, function: .moveFlags, source: .abs, dest: .y),
    OpCodeLookup(opCode: 0xad, cycleCount: 4, function: .moveFlags, source: .abs, dest: .a),
    OpCodeLookup(opCode: 0xae, cycleCount: 4, function: .moveFlags, source: .abs, dest: .x),
    OpCodeLookup(opCode: 0xaf, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),
    // B
    OpCodeLookup(opCode: 0xb0, cycleCount: 2, function: .branch, source: .relative, dest: .pc, flag: .C, sense: true),
    OpCodeLookup(opCode: 0xb1, cycleCount: 5, function: .moveFlags, source: .basePageIndY, dest: .a),
    OpCodeLookup(opCode: 0xb2, cycleCount: 5, function: .moveFlags, source: .basePageIndZ, dest: .a),
    OpCodeLookup(opCode: 0xb3, cycleCount: 3, function: .branch, source: .relativeWord, dest: .pc, flag: .C, sense: true),
    OpCodeLookup(opCode: 0xb4, cycleCount: 4, function: .moveFlags, source: .basePageX, dest: .y),
    OpCodeLookup(opCode: 0xb5, cycleCount: 4, function: .moveFlags, source: .basePageX, dest: .a),
    OpCodeLookup(opCode: 0xb6, cycleCount: 4, function: .moveFlags, source: .basePageY, dest: .x),
    OpCodeLookup(opCode: 0xb7, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0xb8, cycleCount: 2, function: .setFlag, flag: .V, sense: false),
    OpCodeLookup(opCode: 0xb9, cycleCount: 4, function: .moveFlags, source: .absY, dest: .a),
    OpCodeLookup(opCode: 0xba, cycleCount: 2, function: .moveFlags, source: .spLow, dest: .x),
    OpCodeLookup(opCode: 0xbb, cycleCount: 4, function: .moveFlags, source: .absX, dest: .z),
    OpCodeLookup(opCode: 0xbc, cycleCount: 4, function: .moveFlags, source: .absX, dest: .y),
    OpCodeLookup(opCode: 0xbd, cycleCount: 4, function: .moveFlags, source: .absX, dest: .a),
    OpCodeLookup(opCode: 0xbe, cycleCount: 4, function: .moveFlags, source: .absY, dest: .x),
    OpCodeLookup(opCode: 0xbf, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),
    // C0
    OpCodeLookup(opCode: 0xC0, cycleCount: 2, function: .CMP, source: .immediate, source2: .y),
    OpCodeLookup(opCode: 0xC1, cycleCount: 6, function: .CMP, source: .basePageXInd, source2: .a),
    OpCodeLookup(opCode: 0xC2, cycleCount: 2, function: .CMP, source: .immediate, source2: .z),
    OpCodeLookup(opCode: 0xC3, cycleCount: 6, function: .DEW, source: .basePageWord, dest: .useSourceWord),
    // C4
    OpCodeLookup(opCode: 0xC4, cycleCount: 3, function: .CMP, source: .basePage, source2: .y),
    OpCodeLookup(opCode: 0xC5, cycleCount: 3, function: .CMP, source: .basePage, source2: .a),
    OpCodeLookup(opCode: 0xC6, cycleCount: 5, function: .DEC, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0xc7, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    // C8
    OpCodeLookup(opCode: 0xC8, cycleCount: 2, function: .INC, source: .y, dest: .y),
    OpCodeLookup(opCode: 0xC9, cycleCount: 2, function: .CMP, source: .immediate, source2: .a),
    OpCodeLookup(opCode: 0xCA, cycleCount: 2, function: .DEC, source: .x, dest: .x),
    OpCodeLookup(opCode: 0xCB, cycleCount: 7, function: .ASW, source: .absWord, dest: .useSourceWord),
    // CC
    OpCodeLookup(opCode: 0xCC, cycleCount: 4, function: .CMP, source: .abs, source2: .y),
    OpCodeLookup(opCode: 0xCD, cycleCount: 4, function: .CMP, source: .abs, source2: .a),
    OpCodeLookup(opCode: 0xCE, cycleCount: 6, function: .DEC, source: .abs, dest: .useSource),
    OpCodeLookup(opCode: 0xcf, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),
    // D
    OpCodeLookup(opCode: 0xD0, cycleCount: 2, function: .branch, source: .relative, dest: .pc, flag: .Z, sense: false),
    OpCodeLookup(opCode: 0xD1, cycleCount: 5, function: .CMP, source: .basePageIndY, source2: .a),
    OpCodeLookup(opCode: 0xD2, cycleCount: 5, function: .CMP, source: .basePageIndZ, source2: .a),
    OpCodeLookup(opCode: 0xD3, cycleCount: 3, function: .branch, source: .relativeWord, dest: .pc, flag: .Z, sense: false),
    OpCodeLookup(opCode: 0xD4, cycleCount: 3, function: .CMP, source: .basePage, source2: .z),
    OpCodeLookup(opCode: 0xD5, cycleCount: 4, function: .CMP, source: .basePageX, source2: .a),
    OpCodeLookup(opCode: 0xD6, cycleCount: 6, function: .DEC, source: .basePageX, dest: .useSource),
    OpCodeLookup(opCode: 0xd7, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0xD8, cycleCount: 2, function: .setFlag, flag: .D, sense: false),
    OpCodeLookup(opCode: 0xD9, cycleCount: 4, function: .CMP, source: .absY, source2: .a),
    OpCodeLookup(opCode: 0xDA, cycleCount: 3, function: .move, source: .x, dest: .pushSP),
    OpCodeLookup(opCode: 0xDB, cycleCount: 3, function: .move, source: .z, dest: .pushSP),
    OpCodeLookup(opCode: 0xDC, cycleCount: 4, function: .CMP, source: .abs, source2: .z),
    OpCodeLookup(opCode: 0xDD, cycleCount: 4, function: .CMP, source: .absX, source2: .a),
    OpCodeLookup(opCode: 0xDE, cycleCount: 7, function: .DEC, source: .absX, dest: .useSource),
    OpCodeLookup(opCode: 0xdf, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),
    // E0
    OpCodeLookup(opCode: 0xE0, cycleCount: 2, function: .CMP, source: .immediate, source2: .x),
    OpCodeLookup(opCode: 0xE1, cycleCount: 6, function: .SBC, source: .basePageXInd, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xE2, cycleCount: 6, function: .moveFlags, source: .sviIndY, dest: .a),
    OpCodeLookup(opCode: 0xE3, cycleCount: 5, function: .INW, source: .basePageWord, dest: .useSourceWord),
    // E4
    OpCodeLookup(opCode: 0xE4, cycleCount: 3, function: .CMP, source: .basePage, source2: .x),
    OpCodeLookup(opCode: 0xE5, cycleCount: 3, function: .SBC, source: .basePage, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xE6, cycleCount: 5, function: .INC, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0xe7, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    // E8
    OpCodeLookup(opCode: 0xE8, cycleCount: 2, function: .INC, source: .x, dest: .x),
    OpCodeLookup(opCode: 0xE9, cycleCount: 2, function: .SBC, source: .immediate, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xEA, cycleCount: 2, function: .NOP),
    OpCodeLookup(opCode: 0xEB, cycleCount: 6, function: .ROW, source: .absWord, dest: .useSourceWord),
    // EC
    OpCodeLookup(opCode: 0xEC, cycleCount: 4, function: .CMP, source: .abs, source2: .x),
    OpCodeLookup(opCode: 0xED, cycleCount: 4, function: .SBC, source: .abs, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xEE, cycleCount: 6, function: .INC, source: .abs, dest: .useSource),
    OpCodeLookup(opCode: 0xef, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),
    // F
    OpCodeLookup(opCode: 0xF0, cycleCount: 2, function: .branch, source: .relative, dest: .pc, flag: .Z, sense: true),
    OpCodeLookup(opCode: 0xF1, cycleCount: 5, function: .SBC, source: .basePageIndY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xF2, cycleCount: 5, function: .SBC, source: .basePageIndZ, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xF3, cycleCount: 3, function: .branch, source: .relativeWord, dest: .pc, flag: .Z, sense: true),
    OpCodeLookup(opCode: 0xF4, cycleCount: 5, function: .move, source: .immediateWord, dest: .pushSPWord), // PHW #n
    OpCodeLookup(opCode: 0xF5, cycleCount: 4, function: .SBC, source: .basePageX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xF6, cycleCount: 6, function: .INC, source: .basePageX, dest: .useSource),
    OpCodeLookup(opCode: 0xf7, cycleCount: 4, function: .SMB, source: .basePage, dest: .useSource),
    OpCodeLookup(opCode: 0xF8, cycleCount: 2, function: .setFlag, flag: .D, sense: true),
    OpCodeLookup(opCode: 0xF9, cycleCount: 4, function: .SBC, source: .absY, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xFA, cycleCount: 3, function: .moveFlags, source: .popSP, dest: .x),
    OpCodeLookup(opCode: 0xFB, cycleCount: 3, function: .moveFlags, source: .popSP, dest: .z),
    OpCodeLookup(opCode: 0xFC, cycleCount: 7, function: .move, source: .absWord, dest: .pushSPWord),
    OpCodeLookup(opCode: 0xFD, cycleCount: 4, function: .SBC, source: .absX, source2: .a, dest: .a),
    OpCodeLookup(opCode: 0xFE, cycleCount: 7, function: .INC, source: .absX, dest: .useSource),
    OpCodeLookup(opCode: 0xff, cycleCount: 4, function: .branchBitTest, source: .basePage, source2: .relative, dest: .pc, sense: true),

    OpCodeLookup(opCode: STOP , cycleCount: 1, function: FunctionGroup.Stop ),
    OpCodeLookup(opCode: RESET, cycleCount: 6, function: FunctionGroup.Reset),
    OpCodeLookup(opCode: IRQ_SERVICE, cycleCount: 7, function: .IrqService),
    OpCodeLookup(opCode: NMI_SERVICE, cycleCount: 7, function: .NmiService),
]

private extension Array
{
    /*
     *  Array doesn't allow subscripting with types other than Int, fix that.
     */
    subscript(i: S65Address) -> Element
    {
        get { return self[Int(i)] }
        set { self[Int(i)] = newValue }
    }
}

private let cpuRegisterInfo: [RegisterInfo] =
    [
        RegisterInfo(name: "A" , registerType: RegisterType.unsigned8),
        RegisterInfo(name: "X" , registerType: RegisterType.unsigned8),
        RegisterInfo(name: "Y" , registerType: RegisterType.unsigned8),
        RegisterInfo(name: "P" , registerType: RegisterType.flags, flagNames: ["C", "Z", "I", "D", "B", "E", "V", "N"]),
        RegisterInfo(name: "SP", registerType: RegisterType.unsigned16),
        RegisterInfo(name: "PC", registerType: RegisterType.unsigned16),
        RegisterInfo(name: "Z" , registerType: RegisterType.unsigned8),
        RegisterInfo(name: "B" , registerType: RegisterType.unsigned8),
]

// The dummy fast clock driver, used if no other clock driver is set.
private class DummyClockDriver: ClockDriver
{
    func drive(cycleDelta: CycleCount) {}
}

// MARK:-


public func make6502Bus() -> Mos65xx
{
    return CPU6502Bus()
}

/// This 6502 simulation has an explicit address and databus. So reads and writes
/// to memory use the bus variables
fileprivate class CPU65xxBus: Mos65xx, DeviceWithRegisters, MicroSecondClock
{
    func mapDevice(_ device: MemoryMappedDevice) throws
    {
        fatalError("\(#function) needs to be implemented")
    }

    fileprivate var memoryMap: MemoryMappable { fatalError("\(#function) needs to be implemented") }

    public func copy(to: UnsafeMutableRawPointer, from: S65Address, count: Int)
    {
        memoryMap.copy(to: to, from: from, count: count)
    }

    // public var functionGroupProfile: [Int]
    // public var addressModeProfile: [Int]

    private var resetState : ResetState = .none
    private var nmiPending : Bool = false
    public var clock: CycleCount = 0
    public var trace: Bool = false
    private var instruction: Int = STOP

    /*
     * CPU registers
     */
    private var pc: S65Address = 0
    private var sp: S65Address = 0x100
    private var b : S65Byte = 0
    private var x : S65Byte = 0
    private var y : S65Byte = 0
    private var z : S65Byte = 0
    private var a : S65Byte = 0
    private var p : StatusRegister = StatusRegister([ .I, .E])

    private var cFlag: Bool
    {
        get { return p.contains(.C) }
        set { setFlag(.C, value: newValue) }
    }
    private var zFlag: Bool
    {
        get { return p.contains(.Z) }
        set { setFlag(.Z, value: newValue) }
    }
    private var iFlag: Bool
    {
        get { return p.contains(.I) }
        set { setFlag(.I, value: newValue) }
    }
    private var dFlag: Bool
    {
        get { return p.contains(.D) }
        set { setFlag(.D, value: newValue) }
    }
    private var bFlag: Bool
    {
        get { return p.contains(.B) }
        set { setFlag(.B, value: newValue) }
    }
    private var eFlag: Bool
    {
        get { return p.contains(.E) }
        set { setFlag(.E, value: newValue) }
    }
    private var vFlag: Bool
    {
        get { return p.contains(.V) }
        set { setFlag(.V, value: newValue) }
    }
    private var nFlag: Bool
    {
        get { return p.contains(.N) }
        set { setFlag(.N, value: newValue) }
    }

    private func setFlag(_ flag: StatusRegister, value: Bool)
    {
        if value
        {
            p.insert(flag)
        }
        else
        {
            p.remove(flag)
        }
    }

    /*
     *  External view of the CPU registers
     */
    public var pcRegister: S65Address { return self.pc }
    public var aRegister: S65Byte
    {
        get { return self.a }
        set { self.a = newValue }
    }
    public var pRegister: S65Byte { return p.rawValue }

    private static let registerNames = ["A", "X", "Y", "P", "SP", "PC", "Z", "B"]

    public func register16(_ name: String) -> S65Address?
    {
        let ret: S65Address?
        switch name.uppercased()
        {
        case "PC":
            ret = pc
        case "SP":
            ret = sp
        default:
            ret = nil
        }
        return ret
    }

    public func register8(_ name: String) -> S65Byte?
    {
        let ret: S65Byte?
        switch name.uppercased()
        {
        case "A":
            ret = a
        case "X":
            ret = x
        case "Y":
            ret = y
        case "Z":
            ret = z
        case "P":
            ret = p.rawValue
        case "B":
            ret = b
        default:
            ret = nil
        }
        return ret
    }

    public func set(register: String, byte: S65Byte) -> Bool
    {
        var ret: Bool = true
        switch register.uppercased()
        {
        case "A":
            a = byte
        default:
            ret = false
        }
        return ret
    }

    public var numberOfRegisters: Int
    {
        return type(of: self).registerNames.count
    }

    public func infoForRegister(_ index: Int) -> RegisterInfo
    {
        return cpuRegisterInfo[index]
    }

    public func valueOfRegister(_ index: Int) -> Int
    {
        switch (index)
        {
        case 0:
            return Int(a)
        case 1:
            return Int(x)
        case 2:
            return Int(y)
        case 3:
            return Int(pRegister)
        case 4:
            return Int(sp)
        case 5:
            return Int(pc)
        case 6:
            return Int(z)
        case 7:
            return Int(b)
        default:
            fatal("This 65CE02 has 8 registers, not \(index) registers")
            return -1
        }
    }

    public init()
    {
        checkOpCodeIntegrity()
        irqWire.value = true     // IRQ high means no interrupt
        irqWire.connectNotifier(irqNotifier)
    }

    private func checkOpCodeIntegrity()
    {
        var unimplementedOpCodes = 0
        for (i, lookup) in opCodeTable.enumerated()
        {
            let opCode = lookup.opCode
            if opCode == Unimplemented
            {
                unimplementedOpCodes += 1
            }
            else if opCode != i
            {
                let iStr = String(format: "$%02x", i)
                let ocStr = String(format: "$%02x", opCode)
                print("opCodeTable entry \(iStr) has wrong opcode \(ocStr)")
            }
        }
        print("There are \(unimplementedOpCodes) op codes unimplemented")
    }

    /**

     The reset line, true for normal running.

     */
    public final var notReset: Bool = false
    {
        didSet
        {
            if !notReset
            {
                resetState = .resetLineLow
            }
            else if resetState == .resetLineLow
            {
                resetState = .pending
            }
        }
    }

    public final var nmi: Bool = true
    {
        didSet
        {
            if !nmi && oldValue
            {
                nmiPending = true
            }
        }
    }

    public final var notIrq: Bool = true // High means no interrupt

    private var traceControl = Array<(()->())?>(repeating: nil,
                                            count: Int(S65Address.max) + 1)

    /// Trace fuunction to be called on every `jsr.
    ///
    /// The function takes two parameters:
    /// - `S65Address` the location of the `JSR`
    /// - `S65Address` the target of the `JSR`
    public var jsrTrace: ((S65Address, S65Address) -> ())?
    /// Trace function to be called on every `rts`
    ///
    /// The function takes one parameter:
    /// - `S65Address` the location of the `RTS`
    public var rtsTrace: ((S65Address) -> ())?

    /// Control trace at an address. If `trace` is a closure, the closure is
    /// executed when the instruction at the address is fetched.
    ///
    /// - Parameters:
    ///   - address: The address to set the trace at.
    ///   - trace: function to execute when the cpu fetches the instruction at
    ///            the given address.
    public func set(address: S65Address, trace: (() -> ())?)
    {
        traceControl[Int(address)] = trace
    }
    /// The error handler for this CPU. If anything goes wrong, the handler is
    /// called and the clock is set to Int.max to ensure that the CPU stops
    /// running. By default, the fatal error handler is `fatalError`
    public var fatalErrorHandler: FatalErrorHandler =
    { (cpu: Mos65xx, message: @autoclosure () -> String) -> () in fatalError(message()) }

    private func fatal(_ message: @autoclosure () -> String)
    {
        fatalErrorHandler(self, message())
        clock = Int.max - 8	// Will exit the run loop if the error handler returns
        fatalErrorHappened = true
    }

    public var fatalErrorHappened = false

    fileprivate var dataBus: UInt = 0
    fileprivate var addressBus: S65Address = 0

    public final func runForCycles(_ cycles: Int)
    {
        precondition(!fatalErrorHappened, "Cannot run a CPU if a fatal error happened")
        let maximumCycles = cycles + clock;
        let fastClockDriver: ClockDriver
        let fastClockDriverIsDummy: Bool
        if clockDrivers.count == 0
        {
            fastClockDriver = DummyClockDriver()
            fastClockDriverIsDummy = true
        }
        else
        {
            fastClockDriver = clockDrivers[0]
            fastClockDriverIsDummy = false
            clockDrivers.remove(at: 0)
        }
        while clock < maximumCycles
        {
            let decodedOpCode = opCodeTable[instruction]
            if trace
            {
                outputTraceInfo()
            }
            var source: UInt = 0
            var source2: UInt = 0
            var dest: UInt = 0
            var dest2: UInt = 0
            var destMode = decodedOpCode.dest
            if decodedOpCode.source != .none
            {
                fetch(source: decodedOpCode.source)
                source = dataBus
                if decodedOpCode.source2 != .none
                {
                    fetch(source: decodedOpCode.source2)
                    source2 = dataBus
                }
            }

            /*
             *  Perform the operation based on the opcode's function
             */
            // functionGroupProfile[decodedOpCode.function.rawValue] += 1
            switch (decodedOpCode.function)
            {
            case .branch:
                if p[decodedOpCode.flag] == decodedOpCode.sense
                {
                    dest = source
                }
                else
                {
                    destMode = .none
                }
            // MARK: ADC
            case .ADC:
                if !dFlag
                {
                    dest = source2 + source + (cFlag ? 1 : 0)
                    p[.C] = dest & 0x100 != 0
                }
                else
                {
                    var lowNibble = (source2 & 0xf) + (source & 0xf) + (cFlag ? 1 : 0)
                    if lowNibble > 9
                    {
                        lowNibble += 6
                    }
                    var highNibble = (source2 >> 4) + (source >> 4) + (lowNibble > 9 ? 1 : 0)
                    p[.C] = highNibble > 9
                    if p[.C]
                    {
                        highNibble = highNibble &+ 6
                    }
                    dest = (highNibble << 4) | (lowNibble & 0xf)
                }
                p[.V] = (source2.isTCNegative == source.isTCNegative && source.isTCNegative != dest.isTCNegative)
                p[.Z] = (dest & 0xff) == 0
                p[.N] = dest.isTCNegative
            case .AND:
                dest = source & source2
                p[.Z] = dest == 0
                p[.N] = dest.isTCNegative
            case .ASL:
                dest = execASL(source)
            case .ASR:
                dest = execASR(source)
            case .ASW:
                dest = source << 1
                p[.C] = source & (1 << 15) != 0
                p[.N] = dest & (1 << 15) != 0
                p[.Z] = dest & 0xffff == 0
            case .AUG:
                if (source == augHook)
                {
                    execAugCall(source2)
                }
            case .branchBitTest:
                let bitNumber = (instruction >> 4) & 7
                if source.isSet(bit: bitNumber) == decodedOpCode.sense
                {
                    dest = source2
                }
                else
                {
                    destMode = .none
                }
            case .BIT:
                p[.N] = source.isSet(bit: 7)
                p[.V] = source.isSet(bit: 6)
                p[.Z] = source & source2 & 0xff == 0
            case .BRK:
                dest = source &+ 1	// This is the PC - skip the byte after the BRK
                dest2 = source2 | UInt(StatusRegister.B.rawValue)
                addressBus = irqVector
                read()
                pc.lowByte = S65Byte(dataBus & 0xff)
                addressBus = irqVector + 1
                read()
                pc.highByte = S65Byte(dataBus & 0xff)
            case .CMP:
                execCMP(register: source2, data: source)
            case .DEC:
                dest = execSubtract(source, offset: 1)
            case .DEW:
                dest = source &- 1
                p[.N] = dest.isTCNegative
                p[.Z] = dest & 0xff == 0
            case .EOR:
                dest = source2 ^ source
                p[.N] = dest.isTCNegative
                p[.Z] = dest & 0xff == 0
            case .INC:
                dest = execAdd(source, offset: 1)
            case .INW:
                dest = source &+ 1
                p[.N] = dest.isTCNegative
                p[.Z] = dest & 0xffff == 0
            case .JSR:
                dest = source
                dest2 = source2 &- 1	// the PC adjusted to point to last byte of current instruction
            case .LSR:
                dest = execLSR(source)
            case .NEG:
                dest = 0 &- source
                p[.N] = dest.isTCNegative
                p[.Z] = dest & 0xff == 0
            case .NOP:
                break
            case .or:
                dest = source | source2
                p[.Z] = dest & 0xff == 0
                p[.N] = dest.isTCNegative
            case .RMB:
                dest = source & ~(1 << ((UInt(instruction) >> 4) & 0x7))
            case .ROL:
                dest = execROL(source)
            case .ROR:
                dest = execROR(source)
            case .ROW:
                dest = (source >> 1) | (p[.C] ? 0x8000 : 0)
                p[.C] = source & 1 == 1
                p[.N] = dest & (1 << 15) != 0
                p[.Z] = dest & 0xffff == 0
            case .RTN:
                adjustSP(source)
                dest = (pop() | pop() << 8) &+ 1
            case .RTS:
                dest = source &+ 1
                pc += 1
            case .SBC:
                if !dFlag
                {
                    dest = source2 &- source
                    if !p[.C]
                    {
                        dest = dest &- 1
                    }
                    p[.C] = dest & 0x100 == 0
                }
                else
                {
                    var lowNibble = (source2 & 0xf) &- (source & 0xf) &- (cFlag ? 0 : 1)
                    if lowNibble > 9	// Must have wrapped around
                    {
                        lowNibble -= 6
                    }
                    var highNibble = (source2 >> 4) &- (source >> 4) &- (lowNibble > 9 ? 1 : 0)
                    p[.C] = highNibble <= 9
                    if !cFlag
                    {
                        highNibble = highNibble &- 6
                    }
                    dest = (highNibble << 4) | (lowNibble & 0xf)
                }
                p[.V] = source2.isTCNegative != source.isTCNegative
                    && dest.isTCNegative == source.isTCNegative
                p[.Z] = dest & 0xff == 0
                p[.N] = dest.isTCNegative
            case .SMB:
                dest = source | (1 << ((UInt(instruction) >> 4) & 0x7))
            case .testAndChange:
                dest = decodedOpCode.sense ? source | source2 : source & source2
                p[.Z] = dest & 0xff == 0
            case .Reset:
                eFlag = true		// Turn off extended features
                iFlag = true		// Turn off maskable interrupts
                z = 0				// z register is zero
                sp &= 0xFF
                sp |= 0x100			// sp set to page 1 for 6502 mode
                b = 0				// base page set to page 0 for 6502 mode
                addressBus = ResetVector
                read()
                pc.lowByte  = S65Byte(dataBus)
                addressBus =  ResetVector + 1
                read()
                pc.highByte = S65Byte(dataBus)
                instruction = 0
            case .Stop:
                if resetState == .pending
                {
                    instruction = RESET
                    resetState = .none
                }
            case .IrqService:
                execIrq(irqVector)
            case .NmiService:
                nmiPending = false
                execIrq(nmiVector)
            case .move:
                dest = source
                dest2 = source2
            case .moveFlags:
                dest = source
                nFlag = dest & 0x80 != 0
                zFlag = dest & 0xff == 0
            case .setFlag:
                p[decodedOpCode.flag] = decodedOpCode.sense
            case .Custom:
                break
            case .None:
                fatal("\(decodedOpCode.function) unimplemented")
            }

            // MARK: save dest
            if destMode != .none
            {
                set(data: dest, dest: decodedOpCode.dest)
                if decodedOpCode.dest2 != .none
                {
                    set(data: dest2, dest: decodedOpCode.dest2)
                }
            }

            clock += decodedOpCode.cycleCount
            fastClockDriver.drive(cycleDelta: decodedOpCode.cycleCount)
            for driver in clockDrivers
            {
                driver.drive(cycleDelta: decodedOpCode.cycleCount)
            }
            // MARK: Fetch next instruction
            /*
             *  Check for reset
             */
            if resetState != .none
            {
                instruction = STOP
            }
            if instruction < FIRST_INTERNAL
            {
                if nmiPending
                {
                    instruction = NMI_SERVICE
                }
                else if notIrq || iFlag
                {
					if let trace = traceControl[Int(pc)]
                    {
                        trace()
                    }
                    addressBus = pc&++
                    read()
                    instruction  = Int(dataBus)
                    if let jsrTrace = jsrTrace, instruction == 0x20
                    {
                        jsrTrace(pc &- 1, S65Address(memoryMap.get(address: pc))
                                			| S65Address(memoryMap.get(address: pc &+ 1)) << 8)
                    }
                    if let rtsTrace = rtsTrace, instruction == 0x60
                    {
                        rtsTrace(pc &- 1)
                    }
                }
                else
                {
                    instruction = IRQ_SERVICE
                }
            }
        }
        if !fastClockDriverIsDummy
        {
            clockDrivers.insert(fastClockDriver, at: 0)
        }
    }

    fileprivate func read()
    {
        fatalError("\(#function) needs to be implemented")
    }

    fileprivate func write()
    {
        fatalError("\(#function) needs to be implemented")
    }

    private func fetch(source: AddressMode)
    {
        switch source
        {
        case .a:
            dataBus = UInt(a)
        case .b:
            dataBus = UInt(b)
        case .x:
            dataBus = UInt(x)
        case .y:
            dataBus = UInt(y)
        case .z:
            dataBus = UInt(z)
        case .p:
            dataBus = UInt(p.rawValue)
        case .spLow:
            dataBus = UInt(sp.lowByte)
        case .pc:
            dataBus = UInt(pc)
        case .spHigh:
            dataBus = UInt(sp.highByte)
        case .popSP:
            dataBus = pop()
        case .popSPWord:
            dataBus = pop() | (pop() << 8)
        case .abs:
            addressBus = fetchAbs()
            read()
        case .absWord:
            let address = fetchAbs()
            addressBus = address
            read()
            let readSave = dataBus
            addressBus = address &+ 1
            read()
            dataBus = (dataBus << 8) | readSave
            addressBus = address
        case .absX:
            addressBus = fetchAbs() &+ S65Address(x)
            read()
        case .absXWord:
            let address = fetchAbs() &+ S65Address(x)
            addressBus = address
            read()
            let readSave = dataBus
            addressBus = address &+ 1
            read()
            dataBus = (dataBus << 8) | readSave
        case .absY:
            addressBus = fetchAbs() &+ S65Address(y)
            read()
        case .basePage:
            addressBus = pc&++
            read()
            addressBus = S65Address(dataBus) | (S65Address(b) << 8)
            read()
        case .basePageWord:
            addressBus = pc&++
            read()
            let zpAddress = S65Address(dataBus)
            addressBus = zpAddress | (S65Address(b) << 8)
            read()
            let dataLow = dataBus
            addressBus = ((zpAddress &+ 1) & 0xff) | (S65Address(b) << 8)
			read()
            dataBus = dataBus << 8 | dataLow
            addressBus = zpAddress | (S65Address(b) << 8)
        case .basePageXInd:
            addressBus = fetchBasePage(index: x)
            addressBus = fetchAddressFrom(address: addressBus)
            read()
        case .basePageIndY:
            addressBus = pc&++
            read()
            addressBus = fetchBasePageInd(S65Byte(dataBus & 0xff), index: y)
            read()
        case .basePageIndZ:
            addressBus = pc&++
            read()
            addressBus = fetchBasePageInd(S65Byte(dataBus & 0xff), index: z)
            read()
        case .basePageX:
            addressBus = S65Address(fetchBasePage(index: x))
            read()
        case .basePageY:
            addressBus = fetchBasePage(index: y)
            read()
        case .immediate:
            addressBus = pc&++
            read()
        case .sviIndY:
            addressBus = pc&++
            read()
            addressBus = sp &+ S65Address(dataBus) // stack address
            read()
            let lowByte = S65Address(dataBus)
            _ = addressBus&++
            read()
            addressBus = lowByte &+ (S65Address(dataBus) << 8) &+ S65Address(y)
            read()
        case .relative:
            addressBus = pc&++
            read()
            let offset = S65Byte(dataBus & 0xff)
            dataBus = UInt(pc &+ offset.signedAddress)
        case .immediateWord:
			addressBus = pc&++
            read()
            let readSave = dataBus
            addressBus = pc&++
            read()
			dataBus = dataBus << 8 | readSave
        case .relativeWord:
            addressBus = pc&++
            read()
            let readSave = dataBus
            addressBus = pc&++
            read()
            dataBus = dataBus << 8 | readSave
            dataBus = UInt(pc &+ S65Address(dataBus))
        default:
            fatal("Unimplemented source address mode \(source)")
            dataBus = 0
        }
    }

    private func set(data: UInt, dest: AddressMode)
    {
        switch dest
        {
        case .a:
            a = UInt8(data & 0xff)
        case .b:
            b = UInt8(data & 0xff)
        case .x:
            x = UInt8(data & 0xff)
        case .y:
            y = UInt8(data & 0xff)
        case .z:
            z = UInt8(data & 0xff)
        case .p:
            p = StatusRegister(rawValue: S65Byte(data & 0xff)).subtracting(.B)
        case .pc:
            pc = S65Address(data & 0xffff)
        case .spLow:
            sp.lowByte = UInt8(data & 0xff)
        case .spHigh:
            sp.highByte = UInt8(data & 0xff)
        case .pushSP:
            push(UInt8(data & 0xff))
        case .pushSPWord:
            push(UInt16(data & 0xffff))
        case .abs:
            addressBus = fetchAbs()
            dataBus = data
            write()
        case .absX:
            addressBus = fetchAbs() &+ S65Address(x)
            dataBus = data
            write()
        case .absY:
            addressBus = fetchAbs() &+ S65Address(y)
            dataBus = data
            write()
        case .basePage:
            addressBus = effectiveAddress(source: .basePage)
            dataBus = data
            write()
        case .basePageX, .basePageY, .basePageIndY, .basePageIndZ, .basePageXInd:
            addressBus = effectiveAddress(source: dest)
            dataBus = data
            write()
        case .sviIndY:
            addressBus = pc&++
            read()
            let stackAddress = sp &+ S65Address(dataBus)
            addressBus = fetchAddressFrom(address: stackAddress) &+ S65Address(y)
            dataBus = data
            write()
        case .useSource:
            dataBus = data
            write()
        case .useSourceWord:
            dataBus = data & 0xff
            write()
            addressBus = addressBus &+ 1
            dataBus = data >> 8
            write()
        case .none:
            break
        default:
            fatal("Unimplemented destination address mode \(dest)")
        }

    }

    private func effectiveAddress(source: AddressMode) -> S65Address
    {
        let ret: S65Address
        switch source
        {
        case .abs, .absWord:
            ret = fetchAbs()
        case .absX:
            ret = fetchAbs() &+ S65Address(x)
        case .absY:
            ret = fetchAbs() &+ S65Address(y)
        case .basePage, .basePageWord:
            addressBus = pc&++
            read()
            ret = S65Address(dataBus) | (S65Address(b) << 8)
        case .basePageXInd:
            let address = fetchBasePage(index: x)
            ret = fetchAddressFrom(address: address)
        case .basePageIndY:
            addressBus = pc&++
            read()
            ret = fetchBasePageInd(S65Byte(dataBus), index: y)
        case .basePageIndZ:
            addressBus = pc&++
            read()
            ret = fetchBasePageInd(S65Byte(dataBus), index: z)
        case .basePageX:
            ret = fetchBasePage(index: x)
        case .basePageY:
            ret = fetchBasePage(index: y)
        default:
            fatal("Unimplemented source address mode \(source)")
            ret = 0
        }
        return ret
    }

    /// Set a byte at an address. This function will not trigger the memory
    /// device layer and so should not be used by the running CPU.
    ///
    /// - Parameters:
    ///   - byte: The byte to set
    ///   - address: The address where to put it.
    public func set(byte: S65Byte, at address: S65Address)
    {
        memoryMap.set(address: address, byte: byte)
    }

    private func push(_ byte: S65Byte)
    {
        addressBus = sp
        dataBus = UInt(byte)
        write()
        if eFlag
        {
            sp.lowByte = sp.lowByte &- 1
        }
        else
        {
            sp = sp &- 1
        }
    }

    /// Function to read bytes for use by external entities. This is used to
    /// peek memory without affecting the data or address bus. An it should
    /// avoid triggering devices
    ///
    /// - Parameter address: Address to rerad from
    /// - Returns: Byte at the address.
    public func byte(at address: S65Address) -> S65Byte
    {
        return memoryMap.get(address: address)
    }


    /// Reads a 16 bit word from two consecutive addresses.
    ///
    /// - Parameter address: The address of the low byte.
    /// - Returns: The word at the address and address + 1.
    public func word(at address: S65Address) -> S65Address
    {
        return S65Address(memoryMap.get(address: address)) | S65Address(memoryMap.get(address: address &+ 1)) << 8
    }

    private func push(_ word: S65Address)
    {
        push(word.highByte)
        push(word.lowByte)
    }

    private func pop() -> UInt
    {
        if eFlag
        {
            sp.lowByte = sp.lowByte &+ 1
        }
        else
        {
            sp = sp &+ 1
        }
        addressBus = sp
        read()
        return dataBus
    }

    private func adjustSP(_ offset: UInt)
    {
        if eFlag
        {
            sp.lowByte = sp.lowByte &+ S65Byte(offset & 0xff)
        }
        else
        {
            sp = sp &+ S65Address(offset & 0xffff)
        }
    }

    private func fetchAbs() -> S65Address
    {
        let ret: S65Address = fetchAddressFrom(address: pc)
        pc = pc &+ 2
        return ret
    }

    private func fetchBasePage() -> S65Address
    {
        addressBus = pc&++
        read()
        return S65Address(dataBus)
    }

    private func fetchBasePage(index: S65Byte) -> S65Address
    {
        var ret: S65Address = fetchBasePage()
        ret.lowByte = ret.lowByte &+ index
        return ret
    }

    private func fetchAddressFrom(address: S65Address) -> S65Address
    {

        addressBus = address
        read()
        let lowByte = dataBus
        addressBus = address &+ 1
        read()
        return S65Address((dataBus << 8) | lowByte)
    }

    private func execBranch(offset: S65Address)
    {
        pc = pc &+ offset
    }

    private func execCMP(register: UInt, data: UInt)
    {
        p[.C] = register >= data
        let difference = register &- data
        p[.N] = difference.isTCNegative
        p[.Z] = difference & 0xff == 0
    }

    private func execASL(_ data: UInt) -> UInt
    {
        let result = data << 1
        p[.C] = data.isTCNegative
        p[.N] = result.isTCNegative
        p[.Z] = result & 0xff == 0
        return result
    }

    private func execASR(_ data: UInt) -> UInt
    {
        let result = (data >> 1) | (data.isTCNegative ? 0x80 : 0)
        p[.C] = data & 1 != 0
        p[.N] = result.isTCNegative
        p[.Z] = result & 0xff == 0
        return result
    }

    private func execLSR(_ data: UInt) -> UInt
    {
        let result = (data >> 1)

        p[.C] = data & 1 == 1
        p[.N] = result.isTCNegative
        p[.Z] = result & 0xff == 0
        return result
    }

    private func execROL(_ data: UInt) -> UInt
    {
        let result = (data << 1) | (cFlag ? 1 : 0)
        p[.N] = result.isTCNegative
        p[.Z] = result & 0xff == 0
        p[.C] = data.isTCNegative
        return result
    }

    private func execROR(_ data: UInt) -> UInt
    {
        let result = (data >> 1) | (cFlag ? 0x80 : 0)
        p[.N] = result.isTCNegative
        p[.Z] = result & 0xff == 0
        p[.C] = data & 1 == 1
        return result
    }

    private func execAdd(_ data: UInt, offset: UInt) -> UInt
    {
        let result = data &+ 1
        p[.N] = result.isTCNegative
        p[.Z] = result & 0xff == 0
        return result
    }

    private func execSubtract(_ data: UInt, offset: UInt) -> UInt
    {
        let result = data &- 1
        p[.N] = result.isTCNegative
        p[.Z] = result & 0xff == 0
        return result
    }

    private func execAugCall(_ hook: UInt)
    {
        switch S65Address(hook)
        {
        case cpuFail:
            fatal("CPU fail at address: \((pc &- 4).hexString)") // Need to fix this
        case simulateInterrupt:
            notIrq = false
        case simulateClearInterrupt:
            notIrq = true
        case simulatePullDownNMI:
            nmi = false
        case simulatePullUpNMI:
            nmi = true
        default:
            fatal("Unimplemented AUG $\((pc &- 4).hexString): AUG $\(S65Address(hook).hexString)")
        }
    }

    private func execIrq(_ vectorAddress: S65Address)
    {
        push(pc.highByte)
        push(pc.lowByte)
        push(p.intersection(~StatusRegister.B).rawValue) 	// Cleared so we know this is an interrupt
        dFlag = false				// A 65CE02 feature
        iFlag = true				// Might be a 65CE02 feature
        pc = fetchAddressFrom(address: vectorAddress)
        instruction = NOP			// Reenables the fetch/execute cycle
    }

    private func fetchBasePageInd(_ basePageByte: S65Byte, index: S65Byte) -> S65Address
    {
        var basePageAddress = S65Address(basePageByte)
        basePageAddress.highByte = b
        return fetchAddressFrom(address: basePageAddress) &+ S65Address(index)
    }

    private func outputTraceInfo()
    {
        let instructionStart = pc &- 1
        var traceString = instructionStart.hexString + ": "
        let decodedOp = opCodeTable[instruction]
        var addressMode: AddressMode
        if !decodedOp.source.isImplicit
        {
            addressMode = decodedOp.source
        }
        else if !decodedOp.dest.isImplicit
        {
            addressMode = decodedOp.dest
        }
        else
        {
            addressMode = .none
        }
        var operandBytes: [S65Byte] = []
        /*
         *  First print the bytes of the instruction in hex.  Also, save the
         *  bytes in an array to print the operand disassembly
         */
        for i in S65Address(0) ..< S65Address(4)
        {
            if (i < addressMode.size + 1) // size of operand + opcode
            {
                let thisByte = memoryMap.get(address: instructionStart &+ i)
                traceString += thisByte.hexString + " "
                if i > 0
                {
                    operandBytes.append(thisByte)
                }
            }
            else
            {
                traceString += "   "
            }
        }
        if decodedOp.function == .move || decodedOp.function == .moveFlags
        {
            switch (decodedOp.source.isImplicit, decodedOp.dest.isImplicit)
            {
            case (false, true):
                if decodedOp.dest == .pc
                {
                    traceString += "JMP"
                    // With JMP, address mode should be shown one more indirected
                    // than it really is.
                    switch addressMode
                    {
                    case .immediateWord:
                        addressMode = .abs
                    case .absWord:
                        addressMode = .absInd
                    case .absXWord:
                        addressMode = .absXInd
                    default:
                        break
                    }
                }
                else
                {
                    traceString += "LD" + decodedOp.dest.formatOperand([])
                }
            case (true, false):
                // Probably a STx
                traceString += "ST" + decodedOp.source.formatOperand([])
            default:
                switch (decodedOp.source, decodedOp.dest)
                {
                case (_, .pushSP):
                    traceString += "PH" + decodedOp.source.formatOperand([])
                case (.popSP, .p):
                    switch (decodedOp.source2, decodedOp.dest2)
                    {
                    case (.popSPWord, .pc):
                        traceString += "RTI"
                    default:
                        traceString += "PLP"
                    }
                case (.popSP, _):
                    traceString += "PL" + decodedOp.dest.formatOperand([])
                default:
                    traceString += "T" + decodedOp.source.formatOperand([])
                        + decodedOp.dest.formatOperand([])

                }
            }
        }
        else if decodedOp.function == .CMP
        {
            switch decodedOp.source2
            {
            case .a:
                traceString += "CMP"
            case .x:
                traceString += "CPX"
            case .y:
                traceString += "CPY"
            case .z:
                traceString += "CPZ"
            default:
                traceString += "C??"
            }
        }
        else if decodedOp.function == .DEC || decodedOp.function == .INC
        {
            traceString += decodedOp.function.description
            switch decodedOp.source
            {
            case .x, .y, .z:
                traceString += decodedOp.source.formatOperand([])
            case .a:
                traceString += "C A"
            default:
                traceString += "C"
            }
        }
        else if decodedOp.function == .setFlag
        {
            traceString += decodedOp.sense ? "SE" : "CL"
            switch decodedOp.flag
            {
            case StatusRegister.C: traceString += "C"
            case StatusRegister.D: traceString += "D"
            case StatusRegister.E: traceString += "E"
            case StatusRegister.I: traceString += "I"
            case StatusRegister.V: traceString += "V"
            default: traceString += "?"
            }

        }
        else if decodedOp.function == .branchBitTest
        {
            traceString += "BB" + (decodedOp.sense ? "S" : "R")
        }
        else if decodedOp.function == .testAndChange
        {
            traceString += "T" + (decodedOp.sense ? "SB" : "RB")
        }
        else if decodedOp.function == .JSR
        {
            traceString += decodedOp.source == .relativeWord ? "BSR" : "JSR"
        }
        else
        {
            traceString += decodedOp.function.description
            if decodedOp.function == .branch
            {
                switch (decodedOp.flag, decodedOp.sense)
                {
                case (StatusRegister.C, true):
                    traceString += "CS"
                case (StatusRegister.C, false):
                    traceString += "CC"
               	case (StatusRegister.Z, true):
                    traceString += "EQ"
                case (StatusRegister.Z, false):
                    traceString += "NE"
                case (StatusRegister.V, true):
                    traceString += "VC"
                case (StatusRegister.V, false):
                    traceString += "VC"
                case (StatusRegister.N, true):
                    traceString += "MI"
                case (StatusRegister.N, false):
                    traceString += "PL"

                default:
                    traceString += "??"
                }
            }
        }
        traceString += " "
        traceString += addressMode.formatOperand(operandBytes)
        while traceString.count < 34
        {
            traceString += " "
        }
        traceString += "\t\(self.description)"
        traceString += "\t" + stackDescription(bytes: 8)
        print(traceString)
    }

    private func stackDescription(bytes: Int) -> String
    {
        var ret = ""
		for index in sp ..< (sp + S65Address(bytes))
        {
            let address: S65Address
            if p[.E]
            {
                address = index & 0xff | 0x100
            }
            else
            {
                address = index & 0xffff
            }
			ret += " " + memoryMap.get(address: address).hexString
        }
        return "[" + ret + " ]"
    }

    public var description: String
    {
        return "pc:\(pc.hexString) a:\(a.hexString) x:\(x.hexString) y:\(y.hexString) z:\(z.hexString) p:\(pRegister.hexString) sp:\(sp.hexString) b:\(b.hexString)"
    }

    // MARK: MicroSecondClock
    
    public var time: Int { return clock }
    
    // MARK: IRQ mechanism
    
    public private(set) var irqWire: Wire = Wire()
    
    func irqNotifier(_ newValue: Bool)
    {
        notIrq = newValue
    }
    
    // MARK: Clock  drivers
    
    private var clockDrivers: [ClockDriver] = []
    
    ///
    /// Add a clock driven device. 
    ///
    /// Note that the clock driver function will be
    /// called very frequently and so will have a significant impact on CPU speed.
    ///
    /// - Parameter driver: The clock driver
    public func addClockDriver(_ driver : ClockDriver)
    {
        clockDrivers.append(driver)
    }
    
    public func removeClockDriver(_ driver: ClockDriver)
    {
        clockDrivers = clockDrivers.filter({ element in return driver !== element })
    }

    public func loadImage(_ fileURL: URL, at loadAddress: S65Address, isROM: Bool) throws
    {
        fatalError("\(#function) must be overridden")
    }

}


/// A standard 6502
private class CPU6502Bus: CPU65xxBus
{
    private var memory = UnsafeMutablePointer<S65Byte>.allocate(capacity: S65Address.spaceSize)
    private var pageInfo = UnsafeMutablePointer<MemoryMappedDevice?>.allocate(capacity: Int(UInt16.max) + 1)
    fileprivate override var memoryMap: MemoryMappable { return memory }

    /// Initialise the processor.
    ///
    /// - Parameter memory: The memory for the processor.
    public override init()
    {
        for i in 0 ... Int(UInt16.max)
        {
            pageInfo[i] = nil
        }
        super.init()
    }

    fileprivate override func read()
    {
        if let device = pageInfo[Int(addressBus)]
        {
            dataBus = UInt(device.willRead(addressBus &- device.baseAddress))
        }
        else
        {
            dataBus = UInt(memory.get(address: addressBus))
        }
    }

    fileprivate override func write()
    {
        let byte = S65Byte(dataBus & 0xff)

        if let device = pageInfo[Int(addressBus)]
        {
            device.didWrite(addressBus &- device.baseAddress, byte: byte)
        }
        else
        {
            memory.set(address: addressBus, byte: byte)
        }
    }

    // MARK: Memory mapped devices

    /// Map a device into the address space of the CPU. Once mapped, the device
    /// will be notified of all read and writes to the ports it maps.
    ///
    /// - Parameter device: The device to map.
    /// - Throws: If we are trying to map a device outside our memory range.
    public override func mapDevice(_ device: MemoryMappedDevice) throws
    {
        let portCount = device.portCount
        guard portCount < S65Address.max
            else { throw Error6502.addressOutOfRange(portCount) }

        for i in 0 ..< portCount
        {
            try map(portNumber: S65Address(i), device: device)
        }
        print("Mapped \(device) at $\(device.baseAddress.hexString) with \(portCount) ports")
    }

    private func map(portNumber: S65Address, device: MemoryMappedDevice) throws
    {
        guard device.baseAddress <= S65Address.max - portNumber
            else { throw Error6502.addressOutOfRange(Int(device.baseAddress) + Int(portNumber)) }

        let addressOfPort = device.baseAddress + portNumber
        /*
         *  Devices can support multiple devices at the same address.
         */
        if let previousDevice = pageInfo[Int(addressOfPort)]
        {
            device.pushDevice(previousDevice, port: portNumber)
        }
        pageInfo[Int(addressOfPort)] = device
    }
    /// Load an image from a file on disk into the address space.
    ///
    /// - Parameters:
    ///   - fileURL: URL of file to load
    ///   - loadAddress: Address to start at
    ///   - isROM: True if the image is a ROM image
    /// - Throws: If we can't load the file.
    public override func loadImage(_ fileURL: URL, at loadAddress: S65Address, isROM: Bool) throws
    {
        let image = try Data(contentsOf: fileURL, options: NSData.ReadingOptions(rawValue: 0))
        let byteCount = image.count
        var programBytes = [S65Byte](repeating: 0, count: byteCount)
        (image as NSData).getBytes(&programBytes, length: programBytes.count)

        set(bytes: programBytes, at: loadAddress)
    }

}


public func make6502() -> Mos65xx
{
    return CPU6502Bus()
}

public func make6510Bus(memory: C64RamPla) -> Mos6510
{
    return CPU6510Bus(memory: memory)
}

/// A 6510
private class CPU6510Bus: CPU65xxBus, Mos6510
{
    private let memory: C64RamPla

    fileprivate override var memoryMap: MemoryMappable { return memory }

    public init(memory: C64RamPla)
    {
        self.memory = memory
        super.init()
        // Map self to addresses 0 and 1
    }

    private let _ioPort = IOPort()

    public var ioPort: TriStatePort { return _ioPort }

    fileprivate override func read()
    {
        switch addressBus
        {
        case 0:
            dataBus = UInt(_ioPort.willRead(0))
        case 1:
            dataBus = UInt(_ioPort.willRead(1))
       default:
            dataBus = UInt(memory.get(address: addressBus))
        }
    }

    fileprivate override func write()
    {
        switch addressBus
        {
        case 0:
            _ioPort.didWrite(0, byte: S65Byte(dataBus & 0xff))
        case 1:
            _ioPort.didWrite(1, byte: S65Byte(dataBus & 0xff))
        default:
            memory.set(address: addressBus, byte: S65Byte(dataBus & 0xff))
        }
    }

    public override func loadImage(_ fileURL: URL, at loadAddress: S65Address, isROM: Bool) throws
    {
        let image = try Data(contentsOf: fileURL, options: NSData.ReadingOptions(rawValue: 0))
        let byteCount = image.count
        var programBytes = [S65Byte](repeating: 0, count: byteCount)
        (image as NSData).getBytes(&programBytes, length: programBytes.count)
        load(image: programBytes, at: loadAddress, isRom: isROM)
    }

    public func load(image: [S65Byte], at: S65Address, isRom: Bool)
    {
        memory.set(bytes: image, at: at, useRom: isRom)
    }

    /// Function to read bytes for use by external entities. This is used to
    /// peek memory without affecting the data or address bus. An it should
    /// avoid triggering devices
    ///
    /// - Parameter address: Address to rerad from
    /// - Returns: Byte at the address.
    public override func byte(at address: S65Address) -> S65Byte
    {
        switch address
        {
        case 0:
            return _ioPort.ddr
        case 1:
            return _ioPort.data
        default:
            return memoryMap.get(address: address)
        }
    }

    /// Reads a 16 bit word from two consecutive addresses.
    ///
    /// - Parameter address: The address of the low byte.
    /// - Returns: The word at the address and address + 1.
    public override func word(at address: S65Address) -> S65Address
    {
        return S65Address(byte(at: address)) | S65Address(byte(at: address &+ 1)) << 8
    }
    /// Set a byte at an address. This function will not trigger the memory
    /// device layer and so should not be used by the running CPU. Note that
    /// this excludes the IOPort ar addresses 0 and 1. So you can use this to
    /// externally bank the ROM in and out.
    ///
    /// - Parameters:
    ///   - byte: The byte to set
    ///   - address: The address where to put it.
    public override func set(byte: S65Byte, at address: S65Address)
    {
        switch address
        {
        case 0:
            _ioPort.ddr = byte
        case 1:
            _ioPort.data = byte
        default:
            memoryMap.set(address: address, byte: byte)
        }
    }


    /// A set of tri-state wires that represent the port pins.
    private class IOPort: TriStatePort, MemoryMappedDevice
    {
        /// There will always be two ports.
        public let portCount: Int = 2

        /// It'll never move....
        public let baseAddress: S65Address = 0

        public func didWrite(_ portNumber: S65Address, byte: S65Byte)
        {
            switch portNumber
            {
            case 0:
                ddr = byte
            case 1:
                data = byte
            default:
                fatalError("Writing undefined port \(portNumber) on 6510 IO device")
            }
        }

        public func willRead(_ portNumber: S65Address) -> S65Byte
        {
            switch portNumber
            {
            case 0:
                return ddr
            case 1:
                return data
            default:
                fatalError("Reading undefined port \(portNumber) on 6510 IO device")
            }
        }

        public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
        {
            fatalError("Do not support remapped devices")
        }
    }

}

