//
//  C64RamPla.swift
//  Swift6502
//
//  Created by Jeremy Pereira on 30/09/2018.
//

import Darwin

private let log = Logger.getLogger("Swift6502.C64RamPla")

extension S65Address
{
    /// The bank number of the address
    var c64BankNumber: Int { return Int(self >> C64RamPla.addressInBankBits) }
	/// The address relative to the bank that contains it.
    var c64BankRelativeAddress: S65Address { return self & C64RamPla.addressInBankMask }
}
/// Models the C64 memory bank PLA including the memory and all the ROMs and
/// the device mappings.
///
/// From the point of view of the Commodore 64, the address space is divided up
/// into 16 x 4kb "banks" of memory. Most of the banks are just RAM, but some
/// banks have alternative mappings to RAM e.g. ROM or IO devices or both.
///
/// which alternate types of memory are seen is controlled by five input lines
/// modelled by the `Mode` type.
///
/// The C64 memory map looks like this.
/// ```
/// 0x0000 - 0x0fff : Zero page, stack, RAM (2 pages), Screen (4 pages), RAM
/// 0x1000 - 0x1fff : RAM
/// 0x2000 - 0x2fff : RAM
/// 0x3000 - 0x3fff : RAM
/// 0x4000 - 0x4fff : RAM
/// 0x5000 - 0x5fff : RAM
/// 0x6000 - 0x6fff : RAM
/// 0x7000 - 0x7fff : RAM
/// 0x8000 - 0x8fff : RAM
/// 0x9000 - 0x9fff : RAM
/// 0xa000 - 0xafff : BASIC / RAM
/// 0xb000 - 0xbfff : BASIC / RAM
/// 0xc000 - 0xcfff : RAM
/// 0xd000 - 0xdfff : IO / Char ROM / RAM
/// 0xe000 - 0xefff : Kernal / RAM
/// 0xf000 - 0xffff : Kernal / RAM
/// ```
///
/// The VIC II will use any one of the four aligned 16 kbyte banks and will
/// always see RAM except for banks 0 and 2 where addresses `$1000 - $2000` and
/// `$9000 - $A000` respecively will be mapped to the character ROM.
public class C64RamPla: MemoryMappable
{

    /// The number of bits in an address that address a byte within a bank
	public static let addressInBankBits = 12

    /// A mask to give us the address in a bank
    public static let addressInBankMask = ~(~S65Address(0) << addressInBankBits)

    /// Each bank is 4Kbytes. Some of them always switch in pairs though.
    public static let bankSize = 1 << addressInBankBits
    /// The number of banks in the address space
    public static let bankCount = S65Address.spaceSize / bankSize

    public static let basicRomSize = bankSize * 2
    public static let kernalRomSize = bankSize * 2
    public static let charRomSize = bankSize
    public static let ioSize = bankSize
    public static let ioBase: S65Address = 0xD000

    private var banksForReading: [MemoryMappable]
    private var banksForWriting: [MemoryMappable]

    private let ram: UnsafeMutablePointer<S65Byte>
    private let basicRom = UnsafeMutablePointer<S65Byte>.allocate(capacity: basicRomSize)
    private let kernalRom = UnsafeMutablePointer<S65Byte>.allocate(capacity: kernalRomSize)
    private let charRom = UnsafeMutablePointer<S65Byte>.allocate(capacity: charRomSize)
    private var cartridgeRom: UnsafeMutablePointer<S65Byte>?
    private var io = Array<MemoryMappedDevice?>(repeating: nil, count: ioSize)

    /// The bank number seleccted for VIC memory accesses.
    public var vicBankNumber = 0 // TODO: Will be selected by CIA in the range 0 ..< 4

    public init()
    {
        let ram = UnsafeMutablePointer<S65Byte>.allocate(capacity: S65Address.spaceSize)
        banksForReading = (0 ..< C64RamPla.bankCount).map
        {
            return ram + C64RamPla.bankSize * $0
        }
        banksForWriting = banksForReading
        self.ram = ram
        selectBanks()
    }

    deinit
    {
        self.ram.deallocate()
        self.basicRom.deallocate()
        self.kernalRom.deallocate()
        self.charRom.deallocate()

    }

    private final func decode(address: S65Address) -> (Int, S65Address)
    {
		return (address.c64BankNumber, address.c64BankRelativeAddress)
    }

    public func get(address: S65Address) -> S65Byte
    {
        let (bankNumber, addressInBank) = decode(address: address)

        return banksForReading[bankNumber].get(address: addressInBank)
    }

    public func set(address: S65Address, byte: S65Byte)
    {
        let (bankNumber, addressInBank) = decode(address: address)

        banksForWriting[bankNumber].set(address: addressInBank, byte: byte)
    }

    public func copy(to: UnsafeMutableRawPointer, from: S65Address, count: Int)
    {
        var currentToPtr = to
        var bytesToCopy = count
        var currentAddress = from
		while bytesToCopy > 0
        {
            let (bankNumber, addressInBank) = decode(address: currentAddress)
            let thisCount = min(C64RamPla.bankSize - Int(addressInBank), bytesToCopy)
            banksForReading[bankNumber].copy(to: currentToPtr, from: addressInBank, count: thisCount)
            currentToPtr += thisCount
            bytesToCopy -= thisCount
            currentAddress += S65Address(thisCount)
        }
    }


    /// Models the five input lines that control bank select in the PLA.
    ///
    /// The low three lines `loRam`, `hiRam` and `charEn` are sourced from the
    /// 6510's IO port. The other two lines are wired to the cartridge port.
    ///
    /// In theory, inserting a cartridge pulls one or both lines low, which will
    /// cause the PLA to select RAM or ROM from the cartridge for certain banks.
    /// As of 27/11/2018, this mechanism is not yet implemented.

    public struct Mode: OptionSet
    {
        public private(set) var rawValue: UInt8
        public init(rawValue: UInt8)
        {
            self.rawValue = rawValue
        }
        public static let  loRam = Mode(rawValue: 0b0000_0001)
        public static let  hiRam = Mode(rawValue: 0b0000_0010)
        public static let charEn = Mode(rawValue: 0b0000_0100)
        public static let   game = Mode(rawValue: 0b0000_1000)
        public static let  exRom = Mode(rawValue: 0b0001_0000)
    }

    public var mode: Mode = [.loRam, .hiRam, .charEn, .game, .exRom ]
    {
        didSet
        {
            selectBanks()
        }
    }

    private static let readSelectTable: [[MemoryType]] =
    [
    	[.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io  , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram    , .ram      , .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .charRom, .ram      , .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .charRom, .kernalRom, .kernalRom],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram    , .ram      , .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io , .kernalRom, .kernalRom],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram    , .ram, .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram     , .ram, .charRom, .ram      , .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram     , .ram, .charRom, .kernalRom, .kernalRom],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .basicRom, .basicRom, .ram, .charRom, .kernalRom, .kernalRom],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram     , .ram, .io , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram     , .ram, .io , .kernalRom, .kernalRom],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .basicRom, .basicRom, .ram, .io , .kernalRom, .kernalRom],
    ]
    private static let writeSelectTable: [[MemoryType]] =
    [
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram, .ram, .ram, .ram      , .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram, .ram, .ram, .ram      , .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram, .ram, .ram, .kernalRom, .kernalRom],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram, .ram, .ram, .ram      , .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io  , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram     , .ram, .ram, .ram      , .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram     , .ram, .ram, .ram      , .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram     , .ram     , .ram, .ram, .kernalRom, .kernalRom],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .basicRom, .basicRom, .ram, .ram, .kernalRom, .kernalRom],

        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io  , .ram, .ram],
        [.ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .ram, .io  , .ram, .ram],
    ]

    private func selectBanks()
    {
        log.debug("selecting banks for mode: \(mode.rawValue.hexString)")
		banksForReading = bankSet(types: C64RamPla.readSelectTable[Int(mode.rawValue)])
        banksForWriting = bankSet(types: C64RamPla.writeSelectTable[Int(mode.rawValue)])
    }

    func bankSet(types: [MemoryType]) -> [MemoryMappable]
    {
        return (0 ..< C64RamPla.bankCount).map
        {
            bankNumber in
            let memoryType = types[bankNumber]
            let mappable: MemoryMappable
            switch memoryType
            {
            case .ram:
                mappable = ram + bankNumber * C64RamPla.bankSize
            case .basicRom:
                mappable = basicRom + (bankNumber % 2) * C64RamPla.bankSize
            case .kernalRom:
                mappable = kernalRom + (bankNumber % 2) * C64RamPla.bankSize
            case .charRom:
                mappable = charRom
            case .cartridge:
                if let cartridgeRom = cartridgeRom
                {
                    mappable = cartridgeRom
                }
                else
                {
                    mappable = C64RamPla.unmapped
                }
            case .io:
                mappable = io
            }
            return mappable
        }
    }

    internal enum MemoryType: Hashable
	{
		case ram
		case basicRom
        case kernalRom
        case charRom
        case cartridge
        case io
    }

    internal func fill(_ memoryType: MemoryType, pattern: S65Byte)
    {
        switch memoryType
        {
        case .ram:
            memset(ram, Int32(pattern), S65Address.spaceSize)
        case .basicRom:
            memset(basicRom, Int32(pattern), C64RamPla.basicRomSize)
        case .kernalRom:
            memset(kernalRom, Int32(pattern), C64RamPla.kernalRomSize)
        case .charRom:
            memset(charRom, Int32(pattern), C64RamPla.charRomSize)
        case .cartridge:
            if let cartridgeRom = cartridgeRom
            {
                memset(cartridgeRom, Int32(pattern), C64RamPla.charRomSize)
            }
        case .io:
			break
        }
    }

    /// Memory that is unmapped. Reads return 0 and writes are thrown away.
    public struct Unmapped: MemoryMappable
    {
        public func get(address: S65Address) -> S65Byte
        {
            return 0
        }

        public func set(address: S65Address, byte: S65Byte) {}

        public func copy(to: UnsafeMutableRawPointer, from: S65Address, count: Int)
        {
            memset(to, 0, count)
        }
    }
    private static let unmapped = Unmapped()


    func set(bytes: [UInt8], at: S65Address, useRom: Bool)
    {
        // We are going to change the mode, so save it and restore at the end
        // of this scope.
        let savedMode = self.mode
        defer { self.mode = savedMode }

        // Switch in all the ROM or all the RAM depending on whether we are
        // storing to ROM or RAM. Note that `charEn` needs to be low to switch
        // it in.
        self.mode = useRom ? [.exRom, .game, .hiRam, .loRam] : []
        // Force writing to the banks we read. This prevents the normal
        // behaviour of writing through to RAM
        self.banksForWriting = self.banksForReading

        for i in 0 ..< bytes.count
        {
            self.set(address: at &+ S65Address(i), byte: bytes[i])
        }
    }

    /// Map a device into the IO space. The base address must be relative to the
    /// start of IO space which means subtract `$D000` from the device's
    /// location in the C64 memory map.
    ///
    /// - Parameter device: Device to map. The PLA will take ownership of the
    ///                     device.
    public func map(device: MemoryMappedDevice)
    {
        guard Int(device.baseAddress) + device.portCount < C64RamPla.ioSize
        else { fatalError("Device out of range for IO") }

        for i in 0 ..< device.portCount
        {
            io[i + Int(device.baseAddress)] = device
        }
    }
}

extension C64RamPla
{
    fileprivate class RepeatMapping: MemoryMappedDevice
    {
        fileprivate init(baseAddress: S65Address,
                           portCount: Int,
                              target: MemoryMappedDevice)
        {
            self.baseAddress = baseAddress
            self.portCount = portCount
            self.target = target
        }

        let portCount: Int
        let baseAddress: S65Address
        unowned let target: MemoryMappedDevice

        func didWrite(_ portNumber: S65Address, byte: S65Byte)
        {
            let targetPortNumber = portNumber % S65Address(target.portCount)
            target.didWrite(targetPortNumber, byte: byte)
        }

        func willRead(_ portNumber: S65Address) -> S65Byte
        {
            let targetPortNumber = portNumber % S65Address(target.portCount)
            return target.willRead(targetPortNumber)
        }

        func pushDevice(_ device: MemoryMappedDevice, port: S65Address) {
            fatalError("RepeatMapping does not support remapped devices")
        }
    }

    public func repeatMapping(target: MemoryMappedDevice, range: CountableRange<S65Address>)
    {
        if let baseAddress = range.first
        {
            let repeatMapping = RepeatMapping(baseAddress: baseAddress,
                                                portCount: range.count,
                                                   target: target)
            self.map(device: repeatMapping)
        }
    }
}

extension Array: DirectMemoryAccessible where Element == MemoryMappedDevice?
{
    public func copy(to: UnsafeMutableRawPointer, from: S65Address, count: Int)
    {
        let baseIndex = Int(from)
        var byteArray = Array<S65Byte>(repeating: 0, count: count)
        for i in 0 ..< count
        {
            if let device = self[i + baseIndex]
            {
                let port = S65Address(i + baseIndex) - device.baseAddress
                byteArray[i] = device.willRead(port)
            }
        }
        memcpy(to, byteArray, count)
    }
}

extension Array: MemoryMappable where Element == MemoryMappedDevice?
{
    public func get(address: S65Address) -> S65Byte
    {
        if let device = self[Int(address)]
        {
			return device.willRead(address - device.baseAddress)
        }
        else
        {
            return 0
        }
    }

    public func set(address: S65Address, byte: S65Byte)
    {
        if let device = self[Int(address)]
        {
            device.didWrite(address - device.baseAddress, byte: byte)
        }
    }
}

/// Describes the interface a cartridge must implement to be usable with the
/// PLA
public protocol Cartridge
{
    /// The low 8k of the cartridge
    var romLo: MemoryMappable { get }
    /// The high 8k of the cartridge
    var romHi: MemoryMappable { get }

    /// The ultramax memory. If exRom is high and game is low, the address
    /// space from `$1000` to `$ffff` is turned over to the cartridge to do
    /// what it likes with, with the exceptions:
    /// - `$d000 ..< $e000` is mapped to the IO space
    /// - `$8000 ..< $A000` is `romLo`
    /// - `$E000 ... $FFFF` is `romHi`
    var ultramax: [MemoryMappable] { get }

    /// Whether the game line should be driven high or low when the cartridge
    /// is inserted
    var game: Bool { get }
    /// Whether the expansion Rom line should be driven high or low when the cartridge
    /// is inserted
    var exRom: Bool { get }

}

// MARK: VIC access to the RAM
public extension C64RamPla
{

    /// Copy the entire VIC II address space into a buffer.
    ///
    /// Copy command for the VIC II chip. The VIC II can only access 16kb so
    /// the actual banks used are determined from the CIA lines that control the
    /// upper two bits of the VIC address.
    ///
    /// Also, the VIC always accesses RAM except when the CIA lines select for
    /// `$0000 - $3FFF` or `$8000 - $BFFF` in which case the character ROM is
    /// mapped into the address space.
    ///
    /// - Parameters:
    ///   - to: A buffer to copy to. It must point to a ,memory area of at
    ///         least 16k bytes.
	func vicCopy(to: UnsafeMutableRawPointer)
    {
        var currentToPtr = to
        var ramPointer = ram + (vicBankNumber * VicII.memorySize)

        for bankNumber in (vicBankNumber << 2) ..< ((vicBankNumber + 1) << 2)
        {
            if bankNumber == 1 || bankNumber == 9
            {
                charRom.copy(to: currentToPtr, from: 0, count: C64RamPla.bankSize)
            }
            else
            {
				ramPointer.copy(to: currentToPtr, from: 0, count: C64RamPla.bankSize)
            }
            currentToPtr += C64RamPla.bankSize
            ramPointer += C64RamPla.bankSize
        }
    }
}
