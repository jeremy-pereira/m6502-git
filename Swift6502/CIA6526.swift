//
//  CIA6526.swift
//  Swift6502
//
//  Created by Jeremy Pereira on 09/10/2018.
//

private let log = Logger.getLogger("Swift6502.CIA6526")

/// Provides a programming level implementation of the CIA
public class CIA6526: MemoryMappedDevice
{
    /// Initialise
    ///
    /// - Parameter baseAddress: The base address of this CIA
    public init(baseAddress: S65Address)
    {
        self.baseAddress = baseAddress
        // Wire up the timers to their respective interrupt data bits
        timerA.interruptRequester = { [unowned self] in self.timerAInterrupted() }
        timerB.interruptRequester = { [unowned self] in self.timerBInterrupted() }
        // Tell the time of day clock how it determines if the clock or the alarm
        // is being set.
        todClock.alarmControl =
            {
                [unowned self] in self.timerB.control.contains(.writeAlarm)
        	}
        todClock.interruptRequester = { [unowned self] in self.alarmInterrupted() }
    }

    fileprivate var tickCount: CycleCount = 0
    fileprivate var futureActions = ActionList()

	private let portA = TriStatePort(pull: 0xff)
    private let portB = TriStatePort(pull: 0xff)
    private let timerA = ATimer()
    private let timerB = BTimer()
    private var todClock = RealTimeClock()
    public var notIRQ = Wire(value: true)
    private var interruptMask: InterruptSource = []
    private var interruptData: InterruptSource = []
    {
        didSet
        {
            if !interruptData.intersection(interruptMask).isEmpty
            {
                notIRQ.value = false
            }
            else
            {
                notIRQ.value = true
            }
        }
    }

    private func timerAInterrupted()
    {
        interruptData.insert(.underflowTimerA)
        timerB.decrement(from: .timerA)
        if timerA.control.contains(.showOnPortB)
        {
            if timerA.control.contains(.portBInverted)
            {
                portB.data ^= 0b0100_0000 // Inverts bit 6
            }
            else
            {
                portB.data |= 0b0100_0000
                futureActions.add(tick: tickCount + 1)
                {
                    [unowned self] in
                    self.portB.data &= ~0b0100_0000
                }
            }
        }
   }

    private func timerBInterrupted()
    {
        interruptData.insert(.underflowTimerB)
        if timerB.control.contains(.showOnPortB)
        {
            if timerB.control.contains(.portBInverted)
            {
                portB.data ^= 0b1000_0000 // Inverts bit 7
            }
            else
            {
                portB.data |= 0b1000_0000
                futureActions.add(tick: tickCount + 1)
                {
                    [unowned self] in
                    self.portB.data &= ~0b1000_0000
                }
            }
        }
    }

    private func alarmInterrupted()
    {
        interruptData.insert(.alarm)
    }

    /// The interrupt mask is controlled in the following way. If bit 7 is set,
    /// the interrupt sources corresponding to the set bits in the mask are
    /// enabled. Other sources are not affected. If bit 7 is not set, the sources
    /// corresponding to the **set** bits in the mask are disabled. Other sources
    /// are not affected.
    ///
    /// - Parameter mask: Mask controlling interrupts.
    private func setInterruptMask(_ mask: S65Byte)
    {
        let sources = InterruptSource(rawValue: mask & 0x1f)
        if mask.isBitSet(7)
        {
            interruptMask.insert(sources)
        }
        else
        {
            interruptMask.remove(sources)
        }
    }


    /// The cnt pin which privides an external source to drive the timers.
    public var cntPin: Bool = false
    {
        didSet
        {
            timerA.cnt = cntPin
            timerB.cnt = cntPin
        }
    }


    /// The pin that drives the real time clock
    public var todPin: Bool
    {
        get { return todClock.todPin }
        set { todClock.todPin = newValue }
    }

    /// The external pins for port A. Reading them returns the values of all
    /// pins set to output. Pins set to input are pulled up to 1. Writing them
    /// sets the data for all pins set to input.
    ///
    public var portAPins: S65Byte
    {
        get { return portA.pins }
        set { portA.pins = newValue }
    }
    /// The external pins for port B. Reading them returns the values of all
    /// pins set to output. Pins set to input are pulled up to 1. Writing them
    /// sets the data for all pins set to input.
    public var portBPins: S65Byte
    {
        get { return portB.pins }
        set { portB.pins = newValue }
    }

    /// Connect a notifier to port A to process changes on the output pins of
    /// port A.
    ///
    /// - Parameters:
    ///   - notifier: The notifier to connect.
    ///   - mask: The bits of the port which the notifier is interested in.
    /// - Returns: A notification target that can be used to remove the notifier
    @discardableResult
    public func connectPortA(mask: S65Byte, notifier: @escaping (S65Byte) -> Void) -> TriStatePort.NotificationTarget
    {
        return portA.connect(mask: mask, notifier: notifier)
    }
    /// Connect a notifier to port A to process changes on the output pins of
    /// port B.
    ///
    /// - Parameters:
    ///   - notifier: The notifier to connect.
    ///   - mask: The bits of the port which the notifier is interested in.
    /// - Returns: A notification target that can be used to remove the notifier
    @discardableResult
    public func connectPortB( mask: S65Byte, notifier: @escaping (S65Byte) -> Void) -> TriStatePort.NotificationTarget
    {
        return portB.connect(mask: mask, notifier: notifier)
    }

    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("We do not support multiple device mappings")
    }

    // MARK: Memory mapped device

    public let portCount = 16

    public let baseAddress: S65Address

    public func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        if baseAddress == 0xc00 && portNumber < 4
        {
            log.debug("CIA at \(baseAddress.hexString) writing port \(portNumber) with byte $\(byte.hexString)")
        }

        switch portNumber
        {
        case 0:
            portA.data = byte
        case 1:
            portB.data = byte
        case 2:
            portA.ddr = byte
        case 3:
            // Set the data direction. The timers override bits 6 and 7 if they
            // are outputting to a port.
            portB.ddr = byte | (timerA.control.contains(.showOnPortB) ? 0b0100_0000 : 0)
                             | (timerB.control.contains(.showOnPortB) ? 0b1000_0000 : 0)
        case 4:
            timerA.registerLow = byte
        case 5:
            timerA.registerHigh = byte
        case 6:
            timerB.registerLow = byte
        case 7:
            timerB.registerHigh = byte
        case 8:
            todClock.tenths = byte & 0xf
        case 9:
            todClock.seconds = byte & 0x7f
        case 10:
            todClock.minutes = byte & 0x7f
        case 11:
            todClock.hours = byte
        case 13:
            setInterruptMask(byte)
        case 14:
            timerA.control = Timer.Control(rawValue: byte)
            if timerA.control.contains(.showOnPortB)
            {
                portB.ddr |= 0b0100_0000
            }
            if timerA.control.contains(.rtcIs50Hz)
            {
                todClock.frequency = 50
            }
            else
            {
                todClock.frequency = 60
            }
        case 15:
            timerB.control = Timer.Control(rawValue: byte)
            if timerB.control.contains(.showOnPortB)
            {
                portB.ddr |= 0b1000_0000
            }
      	default:
            fatalError("We have not implemented writes to port \(portNumber)")
        }
    }

    public func willRead(_ portNumber: S65Address) -> S65Byte
    {
        let ret: S65Byte
        switch portNumber
        {
        case 0:
            ret = portA.data
        case 1:
            ret = portB.data
        case 2:
            ret = portA.ddr
        case 3:
            ret = portB.ddr
        case 4:
            ret = timerA.registerLow
        case 5:
            ret = timerA.registerHigh
        case 6:
            ret = timerB.registerLow
        case 7:
            ret = timerB.registerHigh
        case 8:
            ret = todClock.tenths
        case 9:
            ret = todClock.seconds
        case 10:	
            ret = todClock.minutes
        case 11:
            ret = todClock.hours
        case 13:
            let interruptOccurred = !interruptData.intersection(interruptMask).isEmpty
            ret = interruptData.rawValue | (interruptOccurred ? 0x80 : 0)
            interruptData = []
        case 14:
            ret = timerA.control.rawValue
        default:
            fatalError("We have not implemented writes to port \(portNumber)")
        }
        if baseAddress == 0xc00 && portNumber == 1
        {
            log.debug("CIA at \(baseAddress.hexString) reading port \(portNumber), byte $\(ret.hexString)")
        }
        return ret
   }
}

fileprivate extension CIA6526
{
	struct InterruptSource: OptionSet
    {
        var rawValue: S65Byte

        fileprivate static let underflowTimerA = InterruptSource(rawValue: 0b0000_0001)
        fileprivate static let underflowTimerB = InterruptSource(rawValue: 0b0000_0010)
        fileprivate static let alarm           = InterruptSource(rawValue: 0b0000_0100)
        fileprivate static let serialData      = InterruptSource(rawValue: 0b0000_1000)
        fileprivate static let flagPin         = InterruptSource(rawValue: 0b0001_0000)
    }
}

// MARK:- CIA6526 timer
fileprivate extension CIA6526
{
	class Timer
    {
        fileprivate var interruptRequester: (() -> ())?

        fileprivate init()
        {
        }

        fileprivate var latch: UInt16 = 0
        fileprivate var value: UInt16 = 0

        fileprivate var control: Control = []
        {
            didSet
            {
                if control.contains(.loadFromLatch)
                {
                    value = latch
                }
                setTickSource()
            }
        }

        fileprivate var cnt: Bool = false
        {
            didSet
            {
                if !oldValue && cnt // positive edge
                {
					self.decrement(from: .cnt)
                }
            }
        }

        private var isRunning: Bool { return control.contains(.startTimer) }

        fileprivate var registerLow: S65Byte
        {
            get { return value.lowByte }
            set { latch.lowByte = newValue }
        }
        fileprivate var registerHigh: S65Byte
        {
            get { return value.highByte }
            set
            {
                latch.highByte = newValue
				if !isRunning
                {
                    value = latch
                }
            }
        }


        /// Sets the tick source. This must be overridden by subclasses.
        fileprivate func setTickSource()
        {
            fatalError("Must override \(#function)")
        }

        fileprivate var tickSource = TickSource.phi2

        /// Called to decrement the timer. Also fires the interrupt request if
        /// the timer was on zero and reloads from the latch.
        /// Note that, if the tick source is not the same ad the provided source
        /// nothing will happen
        /// - Parameter source: Source of the attempted decrement
        fileprivate func decrement(from source: TickSource)
        {
            if isRunning && shouldDecrement(source: source)
            {
                if value == 0
                {
					value = latch
                    if control.contains(.oneShot)
                    {
                        control.remove(.startTimer)
                    }
                    if let interruptRequester = interruptRequester
                    {
                        interruptRequester()
                    }
                }
                else
                {
                    value &-= 1
                }
            }
        }


        /// Test if the timer should decrement based on its current state. This
        /// should be overridden by subclasses of the base `Timer` class.
        ///
        /// - Parameter source: The source of the decrement request
        /// - Returns: True if the timer should decrement based on its current state.
        fileprivate func shouldDecrement(source: TickSource) -> Bool
        {
            fatalError("Subclasses should override \(#function)")
        }
    }
	class ATimer: Timer
    {
        override func setTickSource()
        {
            self.tickSource = control.contains(.cntCount) ? TickSource.cnt : TickSource.phi2
        }

        override func shouldDecrement(source: CIA6526.Timer.TickSource) -> Bool
        {
            return self.tickSource == source
        }
    }

	class BTimer: Timer
    {
        override func setTickSource()
        {
            switch control.intersection([.cntCount, .useTimerA])
            {
            case []:
                self.tickSource = .phi2
            case [.cntCount]:
                self.tickSource = .cnt
            case [.useTimerA]:
                self.tickSource = .timerA
            case [.cntCount, .useTimerA]:
                self.tickSource = .timerACnt
            default:
                fatalError("Invalid tick source bits: \(control.intersection([.cntCount, .useTimerA]))")
            }
        }

        override func shouldDecrement(source: CIA6526.Timer.TickSource) -> Bool
        {
            return self.tickSource == source ||
                    (self.tickSource == .timerACnt && self.cnt && source == .timerA)
        }

    }
}

fileprivate extension CIA6526.Timer
{
	enum TickSource
    {
        case phi2
        case cnt
        case timerA
        case timerACnt
    }
}

fileprivate extension CIA6526.Timer
{
    struct Control: OptionSet
    {
        var rawValue: S65Byte
        static let startTimer    = Control(rawValue: 0b0000_0001)
        static let showOnPortB   = Control(rawValue: 0b0000_0010)
        static let portBInverted = Control(rawValue: 0b0000_0100)
        static let oneShot       = Control(rawValue: 0b0000_1000)
        static let loadFromLatch = Control(rawValue: 0b0001_0000)
        static let cntCount      = Control(rawValue: 0b0010_0000)
        static let shiftIsOut    = Control(rawValue: 0b0100_0000)
        static let useTimerA     = Control(rawValue: 0b0100_0000)
        static let rtcIs50Hz     = Control(rawValue: 0b1000_0000)
        static let writeAlarm    = Control(rawValue: 0b1000_0000)
    }
}

// MARK:- Real time clock
fileprivate extension CIA6526
{
	struct RealTimeClock
    {
        fileprivate init()
        {
        }


        /// Will be called to request an interrupt
        fileprivate var interruptRequester: (() -> ())?

        private enum State
        {
            case stopped // Clock is not running
			case running // Clock is running
            case reading // Clock is running but hours have been read
        }
        private var state: State = .running
        {
            didSet
            {
                if oldValue == .reading && state == .running
                {
					readLatch = bcdClock // Sync the latch with the clock
                }
            }
        }

        /// Toggle this to make the clock tick
        fileprivate var todPin: Bool = false
        {
            didSet
            {
                if !oldValue && todPin // Drive on positive edge
                {
                    todCount += 1
                    if frequency <= 10 * todCount
                    {
                        todCount = 0
                        if state != .stopped
                        {
                            bcdClock.increment()
                            if let interruptRequester = interruptRequester, bcdClock == bcdAlarm
                            {
								interruptRequester()
                            }
                        }
                    }
                }
            }
        }

        /// The clock frequency. This is the frequency of the `todPin`.
        /// Defaults to 60Hz. Since the clock counts in 10ths of a second, this
        /// needs to be a multiple of 10. If not, the clock will run slow.
        fileprivate var frequency: Int = 60

        /// Counts the number of times `todPin` has gone from `false` to `true`
        /// since the last time the clock was incremented.
        private var todCount = 0

        fileprivate var bcdClock = BcdCounter()
        {
			didSet
            {
                if state != .reading
                {
                    readLatch = bcdClock
                }
            }
        }

        fileprivate var readLatch = BcdCounter()
        fileprivate var bcdAlarm = BcdCounter()

        fileprivate var tenths: S65Byte
        {
            mutating get
            {
                if state == .reading
                {
                    state = .running
                }
                return readLatch.tenths
            }
            set
            {
                if alarmControl()
                {
                    bcdAlarm.tenths = newValue & 0xf
                }
                else
                {
                    if state == .stopped
                    {
                        state = .running
                    }
                    bcdClock.tenths = newValue & 0xf
                }
            }
        }

        fileprivate var seconds: S65Byte
        {
            get { return readLatch.seconds }
            set
            {
                if alarmControl()
                {
                    bcdAlarm.seconds = newValue & 0x7f
                }
                else
                {
                    bcdClock.seconds = newValue & 0x7f
                }
            }
        }
        fileprivate var minutes: S65Byte
        {
            get { return readLatch.minutes }
            set
            {
                if alarmControl()
                {
                    bcdAlarm.minutes = newValue & 0x7f
                }
                else
                {
                    bcdClock.minutes = newValue & 0x7f
                }
            }
        }

        fileprivate var hours: S65Byte
        {
            mutating get
            {
                if state == .running
                {
                    state = .reading
                }
                return readLatch.hours | (readLatch.pm ? 0x80 : 0)
            }
            set
            {
                if alarmControl()
                {
                    bcdAlarm.hours = newValue & 0x1f
                    bcdAlarm.pm = newValue.isBitSet(7)
                }
                else
                {
                    state = .stopped
                    bcdClock.hours = newValue & 0x1f
                    bcdClock.pm = newValue.isBitSet(7)
                }
            }

        }

        /// Function that the clock can use to get the bit that controls
        /// whether we are setting the alarm or the clock.
        fileprivate var alarmControl: () -> Bool = { false }
    }
}

fileprivate extension CIA6526.RealTimeClock
{
	struct BcdCounter: CustomStringConvertible, Comparable
    {
        static func == (lhs: CIA6526.RealTimeClock.BcdCounter, rhs: CIA6526.RealTimeClock.BcdCounter) -> Bool
        {
			return lhs.tenths == rhs.tenths
                && lhs.seconds == rhs.seconds
                && lhs.minutes == rhs.minutes
            	&& lhs.hours == rhs.hours
            	&& lhs.pm == rhs.pm
        }
        
        static func < (lhs: CIA6526.RealTimeClock.BcdCounter, rhs: CIA6526.RealTimeClock.BcdCounter) -> Bool
        {
            if lhs.pm && !rhs.pm
            {
                return true
            }
            else if !lhs.pm && rhs.pm
            {
                return false
            }
            else if lhs.hours < rhs.hours
            {
                return true
            }
            else if lhs.hours > rhs.hours
            {
                return false
            }
            else if lhs.minutes < rhs.minutes
            {
                return true
            }
            else if lhs.minutes > rhs.minutes
            {
                return false
            }
            else if lhs.seconds < rhs.seconds
            {
                return true
            }
            else if lhs.seconds > rhs.seconds
            {
                return false
            }
            else if lhs.tenths < rhs.tenths
            {
                return true
            }
            else
            {
                return false
            }
        }

        var description: String
        {
            let amPmString = pm ? "pm" : "am"
            return "\(hours.hexString):\(minutes.hexString):\(seconds.hexString).\(tenths) \(amPmString)"
        }

        var tenths: UInt8 = 0
        var seconds: UInt8 = 0
        var minutes: UInt8 = 0
        var hours: UInt8 = 0
        var pm: Bool = false

        mutating func increment()
        {
            tenths.bcdIncrement()
            if tenths == 0x10
            {
                tenths = 0
                seconds.bcdIncrement()
                if seconds == 0x60
                {
                    seconds = 0
                    minutes.bcdIncrement()
                    if minutes == 0x60
                    {
                        minutes = 0
                        hours.bcdIncrement()
                        if hours == 0x12
                        {
							pm = !pm
                            hours = 0
                        }
                    }
                }
            }
        }
    }
}


// MARK:- CIA6526 clock driver
extension CIA6526: ClockDriver
{
    public func drive(cycleDelta: CycleCount)
    {
        for _ in 0 ..< cycleDelta
        {
            tick()
        }
    }

    fileprivate func tick()
    {
        tickCount += 1
        while let action = futureActions.remove(maxTick: tickCount)
        {
            action.action()
        }
		timerA.decrement(from: .phi2)
        timerB.decrement(from: .phi2)
    }

    fileprivate struct ActionList
    {
        private var first: Action?
        private var last: Action?

        fileprivate class Action
        {
            init(tick: CycleCount, action: @escaping () -> ())
            {
                self.tick = tick
                self.action = action
            }

            fileprivate var prev: Action? = nil
            fileprivate var next: Action? = nil
            fileprivate let tick: CycleCount
            fileprivate let action: () -> ()
        }

        mutating func add(tick: CycleCount, action: @escaping () -> ())
        {
            var current = first
            var insertBefore: Action?
            while let anAction = current, insertBefore == nil
            {
                if anAction.tick >= tick
                {
                    insertBefore = anAction
                }
                else
                {
                    current = anAction.next
                }
            }
            let newAction = Action(tick: tick, action: action)
            if let insertBefore = insertBefore
            {
				newAction.next = insertBefore.next
                newAction.prev = insertBefore.prev
                insertBefore.prev = newAction
                if let prev = newAction.prev
                {
                    prev.next = newAction
                }
                else
                {
                    assert(first! === insertBefore)
                    first = newAction
                }
            }
            else // We ran off the end
            {
				if let last = self.last
                {
                    assert(first != nil)
					last.next = newAction
                    newAction.prev = last
                    self.last = newAction
                }
                else
                {
                    assert(first == nil)
                    self.last = newAction
                    self.first = newAction
                }
            }
        }

        mutating func remove(maxTick: CycleCount) -> Action?
        {
            var ret: Action?
			if let first = self.first
            {
                if first.tick <= maxTick
                {
					self.first = first.next
                    if let newFirst = first.next
                    {
                        newFirst.prev = nil
                    }
                    else
                    {
                        last = nil
                    }
                    ret = first
                }
            }
            return ret
        }
    }
}


// MARK: - Diagmostic API for unit testing.
internal extension CIA6526
{
    /// Latch of timer A. Usable with `@teastable`
    var timerALatch: UInt16 { return self.timerA.latch }
    /// Latch of timer B. Usable with `@teastable`
    var timerBLatch: UInt16 { return self.timerB.latch }
    /// Value of timer A. Usable with `@teastable`
    var timerAValue: UInt16 { return self.timerA.value }
    /// Value of timer B. Usable with `@teastable`
    var timerBValue: UInt16 { return self.timerB.value }

    enum Register: S65Address
    {
        case dataA = 0
        case dataB = 1
        case ddrA = 2
        case ddrB = 3
        case timerALow = 4
        case timerAHigh = 5
        case timerBLow = 6
        case timerBHigh = 7
        case timeOfDay10ths = 8
        case timeOfDaySeconds = 9
        case timeOfDayMinutes = 10
        case timeOfDayHours = 11
        case shiftData = 12
        case interruptControl = 13
        case timerAControl = 14
        case timerBControl = 15
    }

    func didWrite(_ register: Register, byte: S65Byte)
    {
        self.didWrite(register.rawValue, byte: byte)
    }

    func willRead(_ register: Register) -> S65Byte
    {
        return self.willRead(register.rawValue)
    }

    var realTimeClockState: String
    {
        return "clock:\(todClock.bcdClock), alarm:\(todClock.bcdAlarm), latch: \(todClock.readLatch)"
    }

    var realTimeClockFrequency: Int { return todClock.frequency }
}

extension S65Byte
{
    /// Do a BCD increment
    ///
    /// - Returns: The BCD carry
    @discardableResult
    mutating func bcdIncrement() -> Bool
    {
        var carry = false
        var lowBits = self & 0xf
        var highBits = self >> 4
        // Normalise the number we have got
		if lowBits > 9
        {
            lowBits -= 10
            highBits += 1
        }
        if highBits > 9
        {
            highBits -= 10
			carry = true
        }
        assert(lowBits < 10)
        assert(highBits < 10)
        // Increment
        lowBits += 1
		if lowBits == 10
        {
            lowBits = 0
            highBits += 1
            if highBits == 10
            {
                carry = true
                highBits = 0
            }
        }
        assert(lowBits < 10)
        assert(highBits < 10)
       	self = highBits << 4 | lowBits
        return carry
    }
}
