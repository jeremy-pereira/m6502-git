//
//  Error6502.swift
//  M6502
//
//  Created by Jeremy Pereira on 13/06/2015.
//
//

import Foundation

public enum Error6502 : Error, CustomStringConvertible
{
    case notImplemented(message: String)
    case unimplementedAddressMode(message: String)
    case unimplementedFunction(message: String)
    case cpuFail(address: S65Address)
    case fileLoad(message: String)
    case addressOutOfRange(Int)

    public var description : String
    {
        get
        {
            switch (self)
            {
            case .cpuFail(let address):
                return "CPU fail at $\(address.hexString)"
            case .notImplemented(let message):
                return message
            case .unimplementedAddressMode(let message):
                return message
            case .unimplementedFunction(let message):
                return message
            case .fileLoad(let message):
                return "File load error: \(message)"
            case .addressOutOfRange(let address):
                return String(format: "Address out of range: $%0lx", address)
            }
        }
    }
}
