//
//  Logger.swift
//  Swift6502
//
//  Created by Jeremy Pereira on 10/10/2018.
//

private var myLoggers = [ "" : Logger() ]

/// A logger class
///
/// Obtain a logger with the getLogger factory method.
///
open class Logger
{
    /// Log levels
    public enum Level: Int
    {
        case none = -1
        case emergency = 0
        case alert
        case critical
        case error
        case warning
        case notice
        case info
        case debug
        case trace
    }

    fileprivate var parent: Logger?

    fileprivate init()
    {
    }

    fileprivate init(parent: Logger)
    {
        self.parent = parent
    }

    fileprivate var _level: Level?

    /// The log level of this logger
    open var level: Level
        {
        get
        {
            let ret: Level

            if let _level = _level
            {
                ret = _level
            }
            else if let parent = parent
            {
                ret = parent.level
            }
            else
            {
                ret = .none
            }
            return ret
        }

        set
        {
            _level = newValue
        }
    }

    fileprivate var levelStack = Stack<Level>()

    /// Set the log level put save the old level on a stack so we can retrieve
    /// it later.
    /// - Parameter newLevel: new level for the logger
    open func pushLevel(_ newLevel: Level)
    {
        levelStack.push(level)
        level = newLevel
    }

    /// Retrieve a previously saved log level. If no level was saved on the stack,
    /// the level becomes whatever it would be if it had not been set explicitly.
    /// i.e. the level is inherited from its parent.
    open func popLevel()
    {
        if !levelStack.isEmpty
        {
            level = levelStack.pop()
        }
        else
        {
            _level = nil
        }
    }

    /// True if logging is at debug level or more detailed.
    open var isDebug: Bool { return level.rawValue >= Level.debug.rawValue }

    /// Log a message at the requested level.
    ///
    /// Nothing will be logged if the requested level is of lower priority than
    /// the logger's level.
    ///
    /// - Parameter message: The message to log
    /// - Parameter requestedLevel: The level to log at
    ///
    /// - Todo: Something more clever than just printing the message.
    open func log(_ message: @autoclosure () -> String, atLevel requestedLevel: Level)
    {
        if requestedLevel.rawValue <= level.rawValue
        {
            print(message())
        }
    }

    open func error(_ message: @autoclosure () -> String)
    {
        log(message(), atLevel: Level.error)
    }


    open func warn(_ message: @autoclosure () -> String)
    {
        log(message(), atLevel: Level.warning)
    }


    open func info(_ message: @autoclosure () -> String)
    {
        log(message(), atLevel: Level.info)
    }

    open func debug(_ message: @autoclosure () -> String)
    {
        log(message(), atLevel: Level.debug)
    }

    open func trace(_ message: @autoclosure () -> String)
    {
        log(message(), atLevel: Level.trace)
    }


    ///
    /// Get the logger with the given name.
    ///
    /// A hierarchhical naming convention applies.  The separator for name parts
    /// is a "."  If we can't find the name, we chop the last bit off and
    /// repeat until we get to the root logger.
    ///
    /// - Parameter loggerName: . separated hierarchical log name.
    /// - Returns: The best logger match  for the name.
    public static func getLogger(_ loggerName: String) -> Logger
    {
        var ret: Logger

        if let candidate = myLoggers[loggerName]
        {
            ret = candidate
        }
        else
        {
            ret = Logger(parent: getLogger(loggerName.withoutLastComponent))
            myLoggers[loggerName] = ret
        }
        return ret
    }

    ///
    /// Set the log level for the given name.
    ///
    /// Sets the logging level for the logger with the given name and, by extension,
    /// all sub loggers that do not already have explicit levels.
    ///
    /// - Parameter level: The log level
    /// - Parameter forName: Name of the logger to set the level for.
    public static func setLevel(_ level: Logger.Level, forName: String)
    {
        if let theLogger = myLoggers[forName]
        {
            theLogger.level = level
        }
        else
        {
            let newLogger = Logger()
            newLogger.level = level
            myLoggers[forName] = newLogger
        }
    }
    ///
    /// Push the log level for the given name.
    ///
    /// Sets the logging level for the logger with the given name and, by extension,
    /// all sub loggers that do not already have explicit levels. The previous
    /// level is saved on a stack, si it can be restored later.
    ///
    /// - Parameter level: The log level
    /// - Parameter forName: Name of the logger to set the level for.
    public static func pushLevel(_ level: Logger.Level, forName: String)
    {
        if let theLogger = myLoggers[forName]
        {
            theLogger.pushLevel(level)
        }
        else
        {
            let newLogger = Logger()
            newLogger.pushLevel(level)
            myLoggers[forName] = newLogger
        }
    }

    /// Pop the level for the given logger. Discards the current log level and
    /// restores the previous log level. If you pop when the level has never
    /// been saved for this logger, the level defaults to the parent logger's
    /// level. If you pop when the level has never been explicitly set for this
    /// logger, nothing happens.
    /// - Parameter loggerName: The logger to pop the level for.
    public static func popLevel(forName loggerName: String)
    {
        if let theLogger = myLoggers[loggerName]
        {
            theLogger.popLevel()
        }
    }

}

extension String
{

    /// This string but without the last "dot ectension".
    var withoutLastComponent: String
    {
        var index = self.endIndex
        var foundIndex: String.Index?
        let start = self.startIndex
        while foundIndex == nil && index != start
        {
            index = self.index(before: index)
            if self[index] == "."
            {
                foundIndex = index
            }
        }
        let ret: String
        if let foundIndex = foundIndex
        {
            let remainingChars = self[start ..< foundIndex]
            ret = String(remainingChars)
        }
        else
        {
            ret = ""
        }
        return ret
    }
}
