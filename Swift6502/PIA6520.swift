//
//  PIA6520.swift
//  M6502
//
//  Created by Jeremy Pereira on 28/06/2015.
//
//

import Foundation

private class PIA6520Channel: MemoryMappedDevice
{
    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }

    fileprivate init(isAChannel: Bool)
    {
        self.isAChannel = isAChannel
		c1.connectNotifier(changeC1)
        c2.connectNotifier(changeC2)
        port.connect(self, mask: 0xff, notifier: changePort)
    }

    fileprivate var isAChannel: Bool

    fileprivate var name: String { return "PIA channel " + (isAChannel ? "A" : "B") }

    fileprivate var trace: Bool = false

    fileprivate var port: Bus = Bus()
    fileprivate var c1: Wire = Wire()
    fileprivate var c2: Wire = Wire()
    fileprivate var irq: Wire = Wire()
    fileprivate var control: PIAControl = PIAControl()
    {
		didSet
        {
            var irqOccured = false
            if control.contains(PIAControl.C2Output)
            {
                if control.contains(PIAControl.C2Manual)
                {
					c2.value = control.contains(PIAControl.C2ManualOn)
                }
                else if control.contains(PIAControl.C2HandShakePulse)
                {
                    assert(false, "Not implmented")
                }
            }
            else
            {
                if control.contains([PIAControl.C2IrqOn, PIAControl.C2ActiveTransition])
                {
                    irq.value = false
                    irqOccured = true
                }
            }
            if control.contains([PIAControl.C1IrqOn, PIAControl.C1ActiveTransition])
            {
                if trace && !irq.value
                {
                    print("\(name) interrupt will not change")
                }
                irq.value = false
                if trace
                {
                    print("\(name) interrupt low")
                }
               irqOccured = true
            }
            if !irqOccured
            {
                irq.value = true
            }
        }
    }
    /*
	 *  The DDR specifies the direction of the data port.  0 means input and
	 *  1 means output for a specific bit.
	 */
    fileprivate var ddr: S65Byte = 0
    {
		didSet
        {
            if trace
            {
                print("\(name) changed ddr to $\(ddr.hexString)")
            }
            port.changeMask(~ddr, forObject: self)
        }
    }
    fileprivate var data: S65Byte = 0
    {
		didSet
        {
            let oldPort = port.value & ~ddr	// Preserve the input bits
            let newOutputBits = data & ddr
            port.value = newOutputBits | oldPort
        }
    }

    fileprivate var portCount: Int { return 2 }
    fileprivate var baseAddress: S65Address = 0

    fileprivate func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        switch (portNumber)
        {
        case 0:
			if control.contains(PIAControl.PortIOSelect)
            {
                // Input lines will not change
                let maskedByte = byte & ddr 	// Zero the input bits in the new byte
                let maskedData = data & ~ddr 	// Zero the output bits in the data reg
                data = maskedByte | maskedData
                if control.isInHandshakeMode && !self.isAChannel
                {
                    c2.value = false
                }
            }
            else
            {
				ddr = byte
            }
        case 1:
            control = PIAControl(rawValue: byte)
        default:
            fatalError("PIA channel failure: trying to talk to port \(portNumber)")
        }
    }

    fileprivate func willRead(_ portNumber: S65Address) -> S65Byte
    {
        switch (portNumber)
        {
        case 0:
            if control.contains(PIAControl.PortIOSelect)
            {
                let byte = data
                control.remove([.C1ActiveTransition, .C2ActiveTransition])
                if trace
                {
                    print("\(name) read data $\(data.hexString)")
                }
                if control.isInHandshakeMode && self.isAChannel
                {
                    c2.value = false
                }
                return byte
            }
            else
            {
                return ddr
            }
        case 1:
            return control.rawValue
        default:
            fatalError("PIA channel failure: trying to talk to port \(portNumber)")
        }
    }

    fileprivate func changeC1(_ newValue: Bool)
    {
        if newValue == control.contains(PIAControl.C1ActiveHigh)
        {
            control.insert(PIAControl.C1ActiveTransition)
            if control.isInHandshakeMode
            {
                c2.value = true
            }
        }
    }

    fileprivate func changeC2(_ newValue: Bool)
    {
        if !control.contains(PIAControl.C2Output)
            && newValue == control.contains(PIAControl.C2ActiveHigh)
        {
            control.insert(PIAControl.C2ActiveTransition)
        }
    }

    fileprivate func changePort(_ newByte: S65Byte)
    {
        if trace
        {
            print("\(self.name) port changed to \(newByte.hexString)")
        }
        let maskedData = data & ddr 	// Zero the input bits of the existing data
        let maskedByte = newByte & ~ddr	// Zero the output bits of the new byte
        data = maskedByte | maskedData
    }
}
public struct PIAControl: OptionSet
{
    public let rawValue: S65Byte

    public init(rawValue: S65Byte)
    {
        self.rawValue = rawValue
    }

    public static let C1IrqOn      			= PIAControl(rawValue: 0b00000001)
    public static let C1ActiveHigh 			= PIAControl(rawValue: 0b00000010)
    public static let PortIOSelect	 		= PIAControl(rawValue: 0b00000100)
    public static let C2IrqOn      			= PIAControl(rawValue: 0b00001000)
    public static let C2HandShakePulse		= PIAControl(rawValue: 0b00001000)
    public static let C2ManualOn			= PIAControl(rawValue: 0b00001000)
    public static let C2ActiveHigh 			= PIAControl(rawValue: 0b00010000)
    public static let C2Manual 				= PIAControl(rawValue: 0b00010000)
    public static let C2Output     			= PIAControl(rawValue: 0b00100000)
    public static let C2ActiveTransition	= PIAControl(rawValue: 0b01000000)
    public static let C1ActiveTransition	= PIAControl(rawValue: 0b10000000)

    var isInHandshakeMode: Bool
    {
		return contains(.C2Output) && !contains(.C2ManualOn) && !contains(.C2HandShakePulse)
    }
}

public final class PIA6520: MemoryMappedDevice
{
    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }

    public var name: String = "PIA"

    fileprivate let a = PIA6520Channel(isAChannel: true)
    fileprivate let b = PIA6520Channel(isAChannel: false)

    public var ca1: Wire { return a.c1 }
    public var ca2: Wire { return a.c2 }
    public var cb1: Wire { return b.c1 }
    public var cb2: Wire { return b.c2 }

    public var irqA: Wire { return a.irq }
    public var irqB: Wire { return b.irq }

    public var portA: Bus { return a.port }
    public var portB: Bus { return b.port }

    public var traceA: Bool
    {
        set { a.trace = newValue }
        get { return a.trace }
    }

    public var traceB: Bool
    {
        set { b.trace = newValue }
        get { return b.trace }
    }

    public init()
    {
		baseAddress = 0
    }

    public var portCount: Int
    {
		return a.portCount + b.portCount
    }
    public var baseAddress: S65Address
    {
        get { return a.baseAddress }
        set
        {
            a.baseAddress = newValue
            b.baseAddress = newValue + S65Address(a.portCount)
        }
    }

    public func willRead(_ portNumber: S65Address) -> S65Byte
    {
        if portNumber < S65Address(a.portCount)
        {
            return a.willRead(portNumber)
        }
        else
        {
			return b.willRead(portNumber - S65Address(a.portCount))
        }
    }

    public func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        if portNumber < S65Address(a.portCount)
        {
            a.didWrite(portNumber, byte: byte)
        }
        else
        {
            b.didWrite(portNumber - S65Address(a.portCount), byte: byte)
        }
    }
}
