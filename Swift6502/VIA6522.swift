//
//  VIA6522.swift
//  M6502
//
//  Created by Jeremy Pereira on 06/07/2015.
//
//

import Foundation

public struct VIAPCR: OptionSet
{
    public let rawValue: S65Byte

    public init(rawValue: S65Byte)
    {
        self.rawValue = rawValue
    }
	public static let CA1ActiveHigh     = VIAPCR(rawValue: 0b00000001)

    public static let CA2InClearIFROnly = VIAPCR(rawValue: 0b00000110)
    public static let CA2InActiveHigh   = VIAPCR(rawValue: 0b00000100)
    public static let CA2OutManualHigh  = VIAPCR(rawValue: 0b00001110)
    public static let CA2OutManual      = VIAPCR(rawValue: 0b00001100)
    public static let CA2DirectionOut   = VIAPCR(rawValue: 0b00001000)

    public static let CB1ActiveHigh     = VIAPCR(rawValue: 0b00010000)

    public static let CB2InClearIFROnly = VIAPCR(rawValue: 0b01100000)
    public static let CB2InActiveHigh   = VIAPCR(rawValue: 0b01000000)
    public static let CB2OutManualHigh  = VIAPCR(rawValue: 0b11100000)
    public static let CB2OutManual      = VIAPCR(rawValue: 0b11000000)
    public static let CB2DirectionOut   = VIAPCR(rawValue: 0b10000000)
}

public struct VIAInterrupt : OptionSet
{
    public let rawValue: S65Byte

    public init(rawValue: S65Byte)
    {
        self.rawValue = rawValue
    }

    public static let CA2          		= VIAInterrupt(rawValue: 0b00000001)
    public static let CA1           	= VIAInterrupt(rawValue: 0b00000010)
    public static let ShiftRegister 	= VIAInterrupt(rawValue: 0b00000100)
    public static let CB2          		= VIAInterrupt(rawValue: 0b00001000)
    public static let CB1          		= VIAInterrupt(rawValue: 0b00010000)
    public static let Timer2       		= VIAInterrupt(rawValue: 0b00100000)
    public static let Timer1        	= VIAInterrupt(rawValue: 0b01000000)
    public static let EnableIs1     	= VIAInterrupt(rawValue: 0b10000000)
    public static let InterruptOccurred	= VIAInterrupt(rawValue: 0b10000000)

    public var normalised: VIAInterrupt
    {
        let newRawValue = self.contains(.EnableIs1) ? self.rawValue : ~self.rawValue
        return VIAInterrupt(rawValue: newRawValue)
    }
}

public struct VIAACR: OptionSet
{
    public let rawValue: S65Byte

    public init(rawValue: S65Byte)
    {
        self.rawValue = rawValue
    }
	public static let portALatch       = VIAACR(rawValue: 0b00000001)
    public static let portBLatch       = VIAACR(rawValue: 0b00000010)
    public static let shiftControlMask = VIAACR(rawValue: 0b00011100)
    public static let shiftDisabled    = VIAACR(rawValue: 0 << 2)
    public static let shiftInT2        = VIAACR(rawValue: 1 << 2)
    public static let shiftInPhi2      = VIAACR(rawValue: 2 << 2)
    public static let shiftInExt       = VIAACR(rawValue: 3 << 2)
    public static let shiftFreeRunT2   = VIAACR(rawValue: 4 << 2)
    public static let shiftOutT2       = VIAACR(rawValue: 5 << 2)
    public static let shiftOutPhi2     = VIAACR(rawValue: 6 << 2)
    public static let shiftOutExt      = VIAACR(rawValue: 7 << 2)
}

private class Timer
{
    private enum State
    {
        case countingCanInterrupt(Bool)
        case stopped
    }

    private var state: State = .countingCanInterrupt(false)
    private var counter: S65Address = 0
    fileprivate var latch: S65Byte = 0

    private unowned let via: VIA6522
    private let interruptFlag: VIAInterrupt

    fileprivate init(via: VIA6522, interruptFlag: VIAInterrupt)
    {
        self.via = via
        self.interruptFlag = interruptFlag
		state = .countingCanInterrupt(false)
        counter = 0
    }

    fileprivate private(set) var counterLow: S65Byte
    {
		set
        {
            counter.lowByte = newValue
        }
        get
        {
			via.setInterruptFlag(interruptFlag, false)
            return counter.lowByte
        }
    }

    fileprivate var counterHigh: S65Byte
    {
        set
        {
            counter.highByte = newValue
            counter.lowByte = latch
            via.setInterruptFlag(interruptFlag, false)
            state = .countingCanInterrupt(true)
        }
        get
        {
            return counter.highByte
        }
    }

    fileprivate func clock(_ cycles: CycleCount)
    {
        switch (state)
        {
        case .countingCanInterrupt(let canInterrupt):
            if canInterrupt && counter <= S65Address(cycles)
            {
                counter = 0
                state = .stopped
                via.setInterruptFlag(interruptFlag, true)
            }
            else
            {
                counter = counter &- S65Address(cycles)
            }
        default:
            break
        }

    }
}


/// A timer with a two byte latch
private class LatchedTimer: Timer
{
    fileprivate var latchHigh: S65Byte = 0
}


/// A timer that can drive a shift register.
private class ShiftDrivingTimer: Timer
{

}

private class ShiftRegister
{
    var register: S65Byte = 0
}

public final class VIA6522: MemoryMappedDevice, ClockDriver
{
    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }


	public init()
    {
        portB.connect(self, mask: ~ddrB, notifier: portBNotifier)
        irq.value = true
        timer1 = LatchedTimer(via: self, interruptFlag: VIAInterrupt.Timer1)
        timer2 = ShiftDrivingTimer(via: self, interruptFlag: VIAInterrupt.Timer2)
    }

    fileprivate func setInterruptFlag(_ flag: VIAInterrupt, _ shouldSet: Bool)
    {
        if shouldSet
        {
            self.ifr.insert(flag)
        }
        else
        {
            self.ifr.remove(flag)
        }
    }

    /// If true, the processor will output trace information to stdout
    public var trace = false

    // MARK: IO pins

    public var ca1: Wire = Wire()
    public var ca2: Wire = Wire()
    public var cb1: Wire = Wire()
    public var cb2: Wire = Wire()
    public var portB: Bus = Bus()
    public var irq: Wire = Wire()

    // MARK: Registers

    private var b : S65Byte = 0
    {
		didSet
        {
            let changedBits = b ^ oldValue
            if (changedBits & ddrB) != 0
            {
                if trace
                {
                    print("VIA b set to $\(b.hexString), changed bits $\(changedBits.hexString)")
                }
                portB.value = b
            }
        }
    }

    private var ddrB: S65Byte = 0
    {
		didSet
        {
            portB.changeMask(~ddrB, forObject: self)
        }
    }

    private var timer1: LatchedTimer!
    private var timer2: ShiftDrivingTimer!
    private var shift: S65Byte = 0

    private var pcr: VIAPCR = VIAPCR(rawValue: 0b10000000)
    {
		didSet
        {
            if (pcr.contains(.CB2OutManual))
            {
                cb2.value = pcr.contains(.CB2OutManualHigh)
            }
            if (pcr.contains(.CA2OutManual))
            {
				ca2.value = pcr.contains(.CA2OutManualHigh)
            }
        }
    }

    private var acr: VIAACR = VIAACR(rawValue: 0)

    private var ifr: VIAInterrupt = VIAInterrupt(rawValue: 0x00)
    {
        willSet
        {
            assert(!newValue.contains(.InterruptOccurred), "Interrupt occurred comes from the IRQ wire")
        }
		didSet
        {
            let interruptMask = ier.normalised.symmetricDifference(.EnableIs1)
            irq.value = ifr.intersection(interruptMask).isEmpty
        }
    }
    private var ier: VIAInterrupt = VIAInterrupt(rawValue: 0x80)

    // MARK: Memory mapped device

    public var portCount: Int { return 16 }
    public var baseAddress: S65Address = 0
    public func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        switch (portNumber)
        {
        case 0:						// Port B
            let maskedB = b & ~ddrB
            let maskedByte = byte & ddrB
            b = maskedB | maskedByte
            if trace
            {
				print("VIA b is $\(b.hexString)")
            }
        case 2: 					// Data direction for port B
            if trace
            {
                print("VIA ddrb is $\(byte.hexString)")
            }
            ddrB = byte
    	case 4:						// Timer 1 low
            timer1.latch = byte
        case 5:						// Timer 1 high
            timer1.counterHigh = byte
        case 6:						// Timer 1 low
            timer1.latch = byte
        case 7:						// Timer 1 high latch
            timer1.latchHigh = byte
        case 8:						// Timer 2 low
            print("Setting timer 2.lo: $\(byte.hexString)")
            timer2.latch = byte
        case 9:						// Timer 2 high
            print("Setting timer 2.hi: $\(byte.hexString)")
            timer2.counterHigh = byte
        case 10:
            print("Setting shift $\(byte.hexString)")
            shift = byte
        case 11:
            print("Setting acr: $\(byte.hexString)")
            acr = VIAACR(rawValue: byte)

         case 12:
            pcr = VIAPCR(rawValue: byte)
        case 13:					// IFR (can't write it)
            break
        case 14:
            if trace
            {
                print("VIA IER set to $\(byte.hexString)")
            }
            ier = VIAInterrupt(rawValue: byte)
        default:
            fatalError("VIA Port \(portNumber) byte $\(byte.hexString) is not implemented")
        }
    }
    public func willRead(_ portNumber: S65Address) -> S65Byte
    {
        switch (portNumber)
        {
        case 0:		// Port B
            return b
        case 4:		// Timer 1 low
            return timer1.counterLow
        case 5:
            return timer1.counterHigh
        case 6:		// Timer 1 low
            return timer1.latch
        case 7:
            return timer1.latchHigh
        case 8:		// Timer 2 low
            return timer2.counterLow
        case 9:
            return timer2.counterHigh
        case 10:
            return shift
        case 13:	// IFR
            let externalIfr = irq.value ? ifr : ifr.union(.InterruptOccurred)
            return externalIfr.rawValue
        default:
            fatalError("VIA willRead \(portNumber) Not implemented")
        }
    }

    fileprivate func portBNotifier(_ newValue: S65Byte)
    {
        let bMasked = b & ddrB					// Make holes for the input bits
        let newValueMasked = newValue & ~ddrB	// Make holes in the input bits
        b = bMasked | newValueMasked
    }

    // MARK: CPU clock sync

    public func drive(cycleDelta: CycleCount)
    {
        timer1.clock(cycleDelta)
        timer2.clock(cycleDelta)
    }
}
