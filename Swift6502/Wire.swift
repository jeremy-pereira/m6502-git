//
//  Wire.swift
//  M6502
//
//  Created by Jeremy Pereira on 28/06/2015.
//
//

import Foundation

///
/// A single bit wire
///
public final class Wire
{
    private var callbacks: [(Bool) -> Void] = []
    /// The current value of the wire
    public var value: Bool
    {
		didSet
        {
            if oldValue != value
            {
                for callback in callbacks
                {
                    callback(value)
                }
            }
        }
    }

    /// Initiialse the wire
    ///
    /// - Parameter value: The starting value of the wire. Defaults to `false`
    public init(value: Bool = false)
    {
		self.value = value
    }

    /// Connect a notifier that is called when the value changes
    ///
    /// - Parameter notifier: The notifier function/closure
    public func connectNotifier(_ notifier: @escaping (Bool) -> Void)
    {
		callbacks.append(notifier)
		notifier(value)
    }

    /// Pipes the value of this wire to another one.
    ///
    /// - Parameter nextWire: The wire to pipe the value to.
    public func pipeToWire(_ nextWire: Wire)
    {
        self.connectNotifier({ value in nextWire.value = value })
    }
}


/// This is the same as a `Wire` but the the value can be floating. This is
/// represented by using `Optoinal<Bool>` not straightr `Bool`.
public final class TriStateWire
{
    private var callbacks: [(Bool?) -> Void] = []
    /// The current value of the wire
    public var value: Bool? = nil
    {
        didSet
        {
            if oldValue != value
            {
                for callback in callbacks
                {
                    callback(value)
                }
            }
        }
    }

    public init()
    {

    }

    /// Connect a notifier that is called when the value changes
    ///
    /// - Parameter notifier: The notifier function/closure
    public func connectNotifier(_ notifier: @escaping (Bool?) -> Void)
    {
        callbacks.append(notifier)
        notifier(value)
    }

    /// Connect this wire to another non tri-state wire.
    ///
    /// - Parameters:
    ///   - nextWire: nextWire: The wire to pipe the value to.
    ///   - pullUp: If the value is floating, and `pullUp` is true, the target
    ///             wire will be set to `true`, otherwise, it will be set to
    ///             `false`
    public func pipeToWire(_ nextWire: Wire, pullUp: Bool)
    {
        self.connectNotifier
        {
            value in
            if let value = value
            {
                nextWire.value = value
            }
            else
            {
				nextWire.value = pullUp
            }
        }
    }
}

///
/// Models an and gate. 
///
/// The output wire's value should be the anded aggregate of the input wires.
/// Changes are protected with synchronised blocks so that and gates can
/// operate with inputs from multiple threds.
///
public final class AndGate: CustomStringConvertible
{
    /// Outputs trace info if true
    public var trace = false
    var semaphore: DispatchSemaphore

    public init()
    {
		semaphore = DispatchSemaphore(value: 1)
    }

    private var inputWires: [Wire] = []
    private var names: [String] = []

    ///
    /// Add an input wire to the and gate.
    ///
    /// - Parameter newInput: the new input wire to add.
    /// - Parameter name: A name that can be used for debugging
    ///
    public func addInputWire(_ newInput: Wire, withName name: String = "")
    {
        semaphore.wait()
		inputWires.append(newInput)
        names.append(name)
        semaphore.signal()
        newInput.connectNotifier({ [unowned self] flag in self.inputChanged(name, flag) })
    }

    ///
    /// The output wire for this and gate
    ///
    public var outputWire = Wire()

    ///
    /// Convenience property - the value of the gate is the same as the value
    /// of the output wire.
    ///
    public var value: Bool { return outputWire.value }

    private func inputChanged(_ name: String, _ newValue: Bool)
    {
        if trace
        {
            print("\(name) changing to \(newValue)")
        }
        semaphore.wait()
        if newValue
        {
            outputWire.value = andedInputs
        }
        else
        {
            outputWire.value = false
        }
        semaphore.signal()
    }

    private var andedInputs: Bool
    {
        if trace
        {
            print("\(self)")
        }
        for wire in inputWires
        {
            if !wire.value
            {
                return false
            }
        }
        return true
    }

    public var description: String
    {
        var ret = "Swift6502.AndGate:\n"

        for i in 0 ..< inputWires.count
        {
			ret += ("\(names[i]) = \(inputWires[i].value)\n")
        }
        return ret
    }
}

/// This is effectively a set of eight wires. The bus has a value which can be
/// read for output by either conecting a notifier to one of its eight output
/// wires or by connecting an object using `connect(_:, mask:, notifier:)` to
/// read some combination of bits from the entire value.
public final class Bus
{

    /// Initialise mthe Bus
    ///
    /// - Parameter value: The starting value to initialise with, defaults to 0
    public init(value: S65Byte = 0)
    {
		self.value = value
    }

    private class ObjectRef
    {
        var mask: S65Byte
        let object: AnyObject				// TODO: might leak
        let notifier: (S65Byte) -> ()

        init(mask: S65Byte, object: AnyObject, notifier: @escaping (S65Byte) -> ())
        {
            self.mask = mask
            self.object = object
            self.notifier = notifier
        }

        func notify(_ value: S65Byte)
        {
            let newValue = value & self.mask
            self.notifier(newValue)
        }
    }

    private var objects: [ObjectRef] = []


    /// The value of the byte thisd bus represents. If you set this, it will
    /// notify any listeners of the change.
    public var value: S65Byte
    {
		didSet
        {
            if value != oldValue
            {
                let changedBits = value ^ oldValue
                forceChange(mask: changedBits)
            }
        }
    }


    /// Force a change notification to go to any listeners even when the value
    /// has not changed.
    ///
    /// - Parameter mask: Which bits must be in a listeners mask to force a
    ///                   notificaation e.g. `0b00000110` will force
    ///                   notifications to listeners that include bit 1 or bit 2.
    public func forceChange(mask: S65Byte)
    {
        let objectsToChange = objects.filter{ $0.mask & mask != 0 }
        for ref in objectsToChange
        {
            ref.notify(value)
        }
    }

    public func connect(_ anObject: AnyObject, mask: S65Byte, notifier: @escaping (S65Byte) -> ())
    {
        let ref: ObjectRef = ObjectRef(mask: mask, object: anObject, notifier: notifier)
		objects.append(ref)
		ref.notify(value)
    }

    public func changeMask(_ newMask: S65Byte, forObject: AnyObject)
    {
        for ref in objects
        {
            if ref.object === forObject
            {
                ref.mask = newMask
                ref.notify(value)
            }
        }
    }

    private var wires: [Wire?] = [Wire?](repeating: nil, count: 8)

    public func wireForBit(_ bitNumber: Int) -> Wire
    {
        var ret: Wire

        if let wire = wires[bitNumber]
        {
            ret = wire
        }
        else
        {
            ret = Wire()
            let bitMask = S65Byte(1 << bitNumber)
            self.connect(ret, mask: bitMask, notifier:
            {
                value in
				ret.value = value & bitMask != 0
            })
            wires[bitNumber] = ret
            ret.connectNotifier({ [unowned self] flag in self.setBit(bitNumber, flag) })
        }

        return ret
    }

    public func setBit(_ bitNumber: Int, _ bitValue: Bool)
    {
        if bitValue
        {
			value |= S65Byte(1 << bitNumber)
        }
        else
        {
            value &= ~S65Byte(1 << bitNumber)
        }
    }
}
/// An 8 bit tri-state port. Conceptually this has a data direction register
/// and a data register. These are used by the CPU to set values and control the
/// register. The `pins` represent the external interface controlled by the
/// registers.
///
/// Pins set for input will take the value of the `pull` property when read.
///
public class TriStatePort
{

    /// Create a tristate port.
	///
    /// - Parameter pull: The pull property. If a pin is configured for input
    ///                   the equivalent bit from this value will be used when
    ///                   read. By default the port uses pull ups on all bits.
    public init(pull: S65Byte = 0xff)
    {
		self.pull = pull
    }


    /// The data direction register. If a bit is `1` the corresponding bit in
    /// the pins is output.
    public var ddr: S65Byte = 0
    {
        didSet { self.notify(changedBits: ddr ^ oldValue) }
    }
    public var data: S65Byte = 0
    {
        didSet { self.notify(changedBits: data ^ oldValue) }
    }

    /// The direction that the pins are pulled in when set for input
    public let pull: S65Byte

    /// The pins driven by the IO port in output mode and which are driven by
    /// an external entity on input.
    public var pins: S65Byte
    {
        get
        {
            // Combinne data and ouput with pull and input.
			return (data & ddr) | (pull & ~ddr)
        }
        set
        {
			// Set the data but only the input pins
            data = (data & ddr) | (newValue & ~ddr)
        }
    }

    private func notify(changedBits: S65Byte)
    {
        for target in targets.filter({ $0.mask & changedBits != 0 })
        {
            target.notifier(target.mask & pins)
        }
    }

    private var targets: [NotificationTarget] = []


    /// Connect a target for notifications
    ///
    /// - Parameter target: The new target.
    public func connect(target: NotificationTarget)
    {
		targets.append(target)
        target.notifier(target.mask & pins)
    }


    /// Adds a notifier function to the port.
    ///
    /// - Parameters:
    ///   - notifier: The function to call when the notifier needs to fire
    ///   - mask: The bits the nitifier is interested in.
    /// - Returns: A notification target created so thart the notifier can be
    ///            removed later. The port owns the target, so you can ignore
    ///            the return vale.
    @discardableResult
    public func connect(mask: S65Byte, notifier: @escaping (S65Byte) -> Void) -> NotificationTarget
	{
		let target = NotificationTarget(mask: mask, notifier: notifier)
        connect(target: target)
        return target
    }

    /// A class describing something that needs to be notified when the output
    /// of the pins changes.
    public class NotificationTarget
    {
        /// The bits that thos target is interested in.
        public let mask: S65Byte
        fileprivate let notifier: (S65Byte) -> Void

        /// Creater a new notification target
        ///
        /// - Parameters:
        ///   - mask: The pins the target is interested in.
        ///   - notifier: The function to execute when the interesting pins
        ///               change.
        public init(mask: S65Byte, notifier: @escaping (S65Byte) -> Void)
        {
            self.mask = mask
            self.notifier = notifier
        }
    }
}



