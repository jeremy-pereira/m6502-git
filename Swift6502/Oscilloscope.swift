//
//  Oscilloscope.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/08/2015.
//
//

import Foundation


///
/// Class that emulates an oscilloscope.  
///
open class Oscilloscope: MicroSecondClock
{
    ///
    /// An oscilloscope data source.  Each source has a name, a set of transitions
    /// and values bracketing the time period and the values.
    ///
    open class Source
    {
        ///
        /// A transition from one value to another
        ///
        public struct Transition
        {
            /// The time of the data point in microseconds
            public let time: Int
            /// The value of the data point
            public let value: Int
        }

        /// The name of the source for display purposes
        public let name: String

        /// Data points recorded for this source
        open var data: [Transition] = []

        init(name: String)
        {
            self.name = name
        }

        fileprivate func appendData(_ value: Int, time: CycleCount)
        {
			let transition = Transition(time: time, value: value)
            data.append(transition)
            if range == nil
            {
                range = Range(firstValue: value)
            }
            else
            {
                range!.include(value)
            }
        }

        ///
        /// The range of values covered by this source.  If there are no values
        /// this is nil
        ///
        open var range: Range<Int>?
    }


    fileprivate var clock: MicroSecondClock

    ///
    /// What time the oscilloscope thinks it is in microseconds.
    ///
    open var time: CycleCount { return clock.time }

    fileprivate var _minTime: CycleCount

    ///
    /// When the oscilloscope was created.
    ///
    open var minTime: CycleCount { return _minTime }

    ///
    /// Array of the oscilloscope sources
    ///
    open var source: [Source] = []

    public init(clock: MicroSecondClock)
    {
        self.clock = clock
        _minTime = clock.time
    }

    open func registerSource(_ name: String, wire: Wire)
    {
        let source = Source(name: name)
        wire.connectNotifier
        {
            [weak self] value in
            source.appendData(value ? 1 : 0, time: self!.time)
        }
        self.source.append(source)
    }

    open func registerSource(_ name: String, bus: Bus)
    {
        let source = Source(name: name)
        bus.connect(source, mask: 0xff)
        {
                [weak self] value in
                source.appendData(Int(value), time: self!.time)
        }
        self.source.append(source)
    }

}
