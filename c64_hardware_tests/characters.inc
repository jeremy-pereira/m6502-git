;
; Contains routines for manipulating C64 character sets.
;
; The routines defined here may destroy A and Y, but X is guaranteed to be 
; preserved.

.global copyCharactersSub, createASCIISub, displayAllCharactersSub

charRom			:= $D000					; Location of the character ROM
charRomLowerCase:= $D800					; Location of the lower case char set

;
; Copy a character set from one place to another.
;
; Parameters are passed on the data stack.
;
; @param destinationLoc location of the destination, must be a 2k aligned 2k 
;                       block.
;
; @param sourceLoc location of the source block. must be a 2k aligned 2k block
;                  with a character set in it.
.macro copyCharacters 
		jsr copyCharactersSub
.endmacro

;
; Create an ASCII character set at the specified location.
;
; @param destinationLoc	Location tostore characters at.
;
.macro createASCII
		jsr createASCIISub 
.endmacro

;
; Put all characters onto the screen in a 32 x 8 grid
;
; @param screenLoc Where the screen is
;
.macro displayAllCharacters
		jsr displayAllCharactersSub
.endmacro
