; Commodore 64 screen tests - (C) Copyright 2018, Jeremy Pereira

        .setcpu "6502"

.include	"stackmachine.inc"
.include	"characters.inc"

; memory map
;
; The program starts at $7ff with a location address used by the C64 to load
; the remaining bytes. The location address is going to be $0801 to be 
; compatible with a C64 BASIC program.
;
; We use $8000 for a data stack (that grows downwards) and $3000 for an alternate
; character set that maps to ASCII correctly so we can display strings without
; having to correct them.
; 

vicBank0Address := 0						; Location of bank 0
screen          := $0400                    ; The Screen RAM
altCharacters	:= $3000					; The alternate character sets
vicBank1Address := $4000					; Location of bank 1
vicBank2Address := $8000					; Location of bank 2
stackBase		:= $9000					; Location of our data stack
colourRAM       := $D800                    ; The colour RAM

; Kernal routines

kernalchrOut	:= $FFD2					; Output a character to the current device
kernalGetIn		:= $FFE4					; Get a character from the keyboard
kernalPlot		:= $FFF0					; Get or set the screen cursor position

; VIC registers

vicControl1			:= $D011				; VIC control reg 1
vicControl1ECMMask	 = $40					; ECM mode bit in control 1
vicControl1BitmapModeMask = $20				; Bitmap mode mask
vicControl2			:= $D016				; VIC control reg 2
vicControl2CSelMask	 = $08					; CSel mode select
vicControl2MCMMask	 = $10					; multicolour mode select
vicMemoryPtr		:= $D018				; Screen address and char rom address
vicCharRomMask  	 = $0e					; The bits in the memory pointer that
											; control the character ROM loc.
vicBorderColour			:= $D020			; Border colour rgeister
vicBackgroundColour0	:= $D021			; Background colour 0
vicBackgroundColour1	:= $D022			; Background colour 1
vicBackgroundColour2	:= $D023			; Background colour 2
vicBackgroundColour3	:= $D024			; Background colour 3 (for ECM)

; CIA2

cia2PortA		:= $DD00					; Data for CIA2 port A

.macro switchCharacterSet newCharSet
		lda vicMemoryPtr
		and	#~vicCharRomMask & $ff
		ora	#newCharSet
		sta	vicMemoryPtr
.endmacro

	.org	$07ff							; This makes the addresses in the 
											; list file right
											

; ----------------------------------------------------------------------------
loadAddress:
        .byte   $01,$08						; Always load at $0801
; The bytes below are for:
; 10 SYS(2063)
basicBootstrap:
        .byte   $0D,$08,$0A,$00,$9E,$28,$32,$30
        .byte   $36,$33,$29,$00,$00,$00
; ----------------------------------------------------------------------------
        nop
        nop 
; Save the registers
        pha       
        tya
        pha   
        txa
        pha
;
; Initialise the data stack
;
		initStack	stackBase
		
; ----------------------------------------------------------------------------
; Set up an alternative character set
; 
; Set up the destination and source addresses. These will be the top two words
; on the data stack
;

		pushi16 altCharacters
		createASCII
		
;-----------------------------------------------------------------------------
;
; Menu screen
;
menu:

; Switch to our ASCII character set

		switchCharacterSet $0c ; Switch to our ASCII character set

	menuSize = menuEnd-menuStart
		pushi16	menuSize
		pushi16	menuStart 
		pushi16 screen
		memCpy16
		
		jsr pressAnyKey
		jsr testSelect
		jmp menu

.proc testSelect

; Select a test, for each  select we jump to the relevant test and the rts
; will unravel to the caller of this. If no test matches the selection we need
; to exit the program which we do by popping the return address and then 
; falling through to the tidy up code.

		cmp	#$31					; Is it the colour test
		bne notColourTest
		jmp standardColourTest
		jmp menu
notColourTest:
		cmp	#$32					; Is it the border Test
		bne notBorderTest
		jmp borderColourTest
notBorderTest:
		cmp	#$33					; Is it the video matrix test
		bne notVideoMatrix
		jmp videoMatrixSelect
notVideoMatrix:
		cmp	#$34					; Is it the extended colour mode test
		bne notExtendedColourMode
		jmp extendedColourModeTest
notExtendedColourMode:
		cmp	#$35					; Is it the standard bitmap test
		bne notStandardBitmap
		jmp standardBitmap
notStandardBitmap:

; No test matches, exit by popping the return address and falling through to
; the tidy up

		pla
		pla 	
					
; Tidy up

		switchCharacterSet $06
		
; Restore the registers and return
		pla
		tax
		pla
		tay
		pla
		rts
.endproc

		
; ----------------------------------------------------------------------------
; Test the video matrix relocation
;

.proc videoMatrixSelect

; Switch the character set to one of the standard ones so we can see the text
; on the other bank.

		switchCharacterSet $06

; Label the default screen

		ldx	#0				; Bank 0
		ldy #1				; screen 1
		jsr labelScreen		

; Label the last screen in bank 0

		ldx	#0				; Bank 0
		ldy #$f				; screen 15
		jsr labelScreen		

; Label the fist screen in bank 2

		ldx	#2				; Bank 2
		ldy #0				; screen 0
		jsr labelScreen		

		jsr pressAnyKey		; Gives me a chance to see the old screen
		
; Switch to the new screen

		lda	#$0f			; Screen 15
		jsr switchScreen	; Change it

		jsr	pressAnyKey		; Give me a chance to see the switch
		
; Switch to bank 2 and screen 0

		lda	#$2
		jsr	switchVicBank
		lda	#$0				; Screen 0
		jsr switchScreen	; Change it

		jsr	pressAnyKey		; Give me a chance to see the switch
		
;
; Switch back to the default bank and screen 
;
		lda	#$0
		jsr	switchVicBank
		lda	#$01			; Screen 1
		jsr switchScreen	; Change it
		
		rts
.endproc
		
;----------------------------------------------------------------------------
; The colour tests
;	
.proc standardColourTest
;
		pushi16	screen
		displayAllCharacters
; Set some character colours

		ldx #0
colourLoopStart:
		txa
		sta	colourRAM,x
colourLoopTest: 
		inx
		bne colourLoopStart
		
		jsr	pressAnyKey		; Give me a chance to see the switch

;-----------------------------------------------------------------------------		
; Test multicolour character mode.


multiColourCharacterTest:
; 
;
; Set the VIC to multicolour mode. The colours already cycle from 0 to 16
; Colours 0 - 8 should be normal, 8 - 16 are multicolour.
;
		ldx #15				; Grey
		stx vicBackgroundColour1	; A background colour
		dex
		stx vicBackgroundColour2	; A different background colour
		
		lda vicControl2
		ora	#vicControl2MCMMask	; Set multicolour mode
		sta vicControl2			

		jsr	pressAnyKey		; Give me a chance to see the switch

; Set the character colours back to standard.

		pushi8	$00			; This is asking for 256 bytes to fill
		pushi8	$0e			; The colour
		pushi16	colourRAM	; The address 
		memSet 
		
; Restore normal character mode

		lda vicControl2
		and	#~vicControl2MCMMask & $ff	; Unset multicolour mode
		sta vicControl2
		rts
.endproc

;-----------------------------------------------------------------------------
; Test extended background colour mode

.proc extendedColourModeTest

; Switch to extended colour mode

		ldx #15				; Grey
		stx vicBackgroundColour1	; A background colour
		dex
		stx vicBackgroundColour2	; A different background colour
		dex
		stx vicBackgroundColour3	; A different background colour again
		lda vicControl1 
		ora	#vicControl1ECMMask
		sta vicControl1

; Show all characters

		pushi16	screen
		displayAllCharacters
		
		jsr	pressAnyKey		; Give me a chance to see the switch

; Back to standard colour mode

		lda vicControl1 
		and	#~vicControl1ECMMask & $ff
		sta vicControl1
		rts
.endproc

;-----------------------------------------------------------------------------
; Test standard bitmap mode

.proc standardBitmap

		lda	#$1				; Bank 1 starts at $4000
		jsr	switchVicBank
		lda vicMemoryPtr	; Save the memory pointer
		pha
		lda	#$0				; Screen 0
		jsr switchScreen	; Change it
		switchCharacterSet $8 ; Put the bitmap in the upper half of the bank
		
; Change to bitmap mode
		
		lda vicControl1 
		ora #vicControl1BitmapModeMask
		sta vicControl1
		
; Set the colours to be white on black

		pushi16		1000	; The size of the video/colour matrix
		pushi8		$10		; White foreground, black background
		pushi16		vicBank1Address	; This is where the video memory is
		memSet16

		jsr drawBitMapTestPattern
				
		jsr	pressAnyKey		; Give me a chance to see the switch
		
; Change some colours

		pushi16	vicBank1Address	; The screen is at the bottom of the VIC bank
		displayAllCharacters
		jsr	pressAnyKey		; Give me a chance to see the switch
		
		pla					; Restore the old memory pointer
		sta vicMemoryPtr
		
; Restore character mode

		lda vicControl1 
		and #~vicControl1BitmapModeMask & $ff
		sta vicControl1

		lda	#$0				; Back to bank 0
		jsr	switchVicBank

		rts
.endproc

;-----------------------------------------------------------------------------
;
; Draw a pattern on the bit map
;
.proc drawBitMapTestPattern
		txa 				; Save the X register
		pha
		pushi16	0			; Initial y coordinate
		pushi16	0			; Initial x coordinate
		saveBaseRegister
	initialX = 2	
	initialY = initialX + 2	
		
;
;   First clear the screen
;
		pushi16	8192		; Screen butmap is 8k
		pushi8	0			; All pixels are zero
		push16	bitmapLoc	; The base address of the screen
		memSet16	
;
; Draw some lines
;
		ldx #16				; Loop counter
lineLoop:
		pushBase16	initialY
		pushBase16	initialX
		jsr drawDiagonalLine
		pushBase16	initialY 
		pushi16		8
		add16
		popBase16	initialY
		dex
		bne lineLoop

		restoreBaseRegister
		discardi8 4
		pla
		tax
		rts
.endproc

;----------------------------------------------------------------------------
;
; Draw diagonal line from the start coordinates to the edge of the screen
;
; y.w x.w --> () ; Line on the screen
;

.proc drawDiagonalLine
		saveBaseRegister
	xCoord = 2
	yCoord = xCoord + 2
	
drawLoopStart:
		pushi8	1			; Colour = foreground
		pushBase16	yCoord
		pushBase16	xCoord
		jsr drawPoint
		pushBase16	xCoord
		add1_16
		dup16
		pushi16		320		; If it's 320 stop
		subtract16			; 
		pushi16		endLine
		branchZero16
		popBase16	xCoord
		
		pushBase16	yCoord
		add1_16
		dup16
		pushi16		200		; If it's 200 stop
		subtract16			; 
		pushi16		endLine
		branchZero16
		popBase16	yCoord	
		jmp drawLoopStart	
		
endLine:
		restoreBaseRegister	
		discardi8	4		; Remove the parameters
		rts
.endproc


;----------------------------------------------------------------------------
;
; Draw a point on the screen
; @param x (16 bit) x cooordinate 0 - 319
; @param y (16 bit) y cooordinate 0 - 199
; @param colour (8 bit) 0 = background, 1 = foreground
;
.proc drawPoint
		push16	bitmapLoc		; For the byte address
		pushi8	0				; For the byte at the address
	pixelByte		= 2
	bitmapAddress 	= pixelByte+1
	xCoord 			= bitmapAddress+2
	yCoord 			= xCoord+2
	colour 			= yCoord+2
		saveBaseRegister
		
;
; Get the bitmap address by multiplying the Y coord by 40 (bytes on a line)
; and then adding xCoord / 8. And then adding to contents of bitmapLoc
;
		pushBase16	yCoord
		jsr times40			; get the address of the row
		pushBase16 		bitmapAddress
		add16
		
		pushBase16 	xCoord  ; Add the xCoord
		pushi8		3
		lShiftRight16
		add16
;
; This gives us the address of the byte containing the pixel
;
		dup16
		popBase16 bitmapAddress 	; Save it for later
		pushIndirect8				; Get the byte
		popBase8	pixelByte		; Save the byte
;
; Calculate the bit we need.
;
		pushBase8	xCoord
		popA 
		and	#7					; xCoord mod 8 gives us the bit
		pushA
		pushi8	$80				; Create a mask by shifting 1 the required bits
		swap8
		lShiftRight8
		pushBase8	colour 		; Get the colour 
		popA 
		bne	foreground 			; Test if this is foreground
		
; Background so force the bit to 0. The mask is on the top of the stack

		invert8
		popA					; Put the mask in the stack
		ldy	#pixelByte
		and	(baseRegister),y 	; And with the pixel
		jmp storeByte
foreground:
; Force the bit to 1
		popA					; Put the mask in the stack
		ldy	#pixelByte
		ora	(baseRegister),y 	; Or with the pixel
storeByte:
		pushA
		pushBase16	bitmapAddress
		popIndirect8
;
; That's it, clean up
;
		restoreBaseRegister
		discardi8	8
		rts
.endproc

bitmapLoc:	.word $6000			; By default is top part of bank 1

;-----------------------------------------------------------------------------
;
; Multiply the number on the top of the stack by 40. If the result is more than
; 16 bits, the excess bits are thrown away.
;
; n.w --> (n * 40).w
.proc times40
;
; 40 is 32 x 8. So we can shift the operand by 3 and store the result then 
; shift it by another 2 and add to the stored result.
;
	pushi8	3
	lShiftLeft16
	dup16
	pushi8	2
	lShiftLeft16 
	add16
	rts
.endproc
;-----------------------------------------------------------------------------		
; Cycle the border and background colours 

.proc borderColourTest
		ldx	#$0
		ldy	#$0f
nextBorder: 

; x is the border colour and y is the background colour. x counts up and y 
; counts down.

		stx	vicBorderColour
		sty	vicBackgroundColour0
		inx
		dey
		txa 
		pha
		tya
		pha
		jsr pressAnyKey
		pla
		tay
		pla
		tax
		cpx	#$10
		bne	nextBorder
		
		rts
.endProc

; ----------------------------------------------------------------------------
;
; Goes into a tight loop waiting for a key to be pressed.
;
; This relies on the standard kernal routine and so, the normal interrupt
; service routine must be working.
;
; @return the character pressed in `A`
.proc	pressAnyKey
pauseLoop:
		lda	#0
		jsr	kernalGetIn
		cmp #0
		beq pauseLoop
		cmp #$ff 
		beq pauseLoop
		rts
.endproc

; ----------------------------------------------------------------------------
;
; Label the screen by putting its bank and video matrix numbers in the 
; top left corner.
; @parameter X contains the bank number
; @parameter Y contains the screen number
;
.proc labelScreen
		pha						; Save the accumulator
		tya						; and y
		pha
;
; First calculate the location of the screen. We will put it in scratch0
; 
; The bank needs to be multiplied by 16k, which we do by zeroing the low byte 
; and multiplying the top byte by 64 
;
		lda	#$0
		sta	scratch0
		txa
		asl
		asl	
		asl
		asl	
		asl
		asl
		sta	scratch0+1
;
; Now we need to add on the screen number. The number in Y needs to be 
; multiplied by 1k which we do by using zero for the low byte and shifting
; Y twice
;
		tya 
		asl
		asl
;
; Now add the screen location to the bank location
;
		clc
		adc	scratch0+1
		sta	scratch0+1
;
; Bank number in the first byte. 
;
		ldy	#$0				; y is already saved on the top of the stack
		txa
		jsr printHex
		lda	#$20			; A space
		sta	(scratch0),y
		iny
;
;  Now the screen number. 		
;	
		pla					; Retrieve the screen number from the stack
		pha					; save it again.
		jsr printHex
		lda	#$20			; Another space
		sta	(scratch0),y
;		
; Tidy up and return
;
		pla
		tay
		pla
		rts   
.endproc

; ----------------------------------------------------------------------------
;
; Convert A to hex and print it at the location specified by scratch0 and y
;
; @param scratch0 base location 
; @param y index from base where to put the hex digits. y will be incremented
;          to just after the last hex digit.
;
.proc   printHex
		pha 				; Save A
		
;
;  First digit
;
		lsr					; move the high nibble into the low nibble 
		lsr 
		lsr
		lsr
		jsr makeAHexDigit
		sta (scratch0),y
		iny
		pla					; Get A back
		pha					; and save it again
		jsr makeAHexDigit
		sta (scratch0),y
		iny
; Clean up and exit					
		pla
		rts
.endProc

; ----------------------------------------------------------------------------
;
; Take the low nibble of A and make it into a hex digit
;
; Note that this gives us a screen code, not PETSCII.
;
; @param A the byte to convert to hex. A is replaced by the hex representation
;          of its low nibble.
;
.proc makeAHexDigit
		and	#$0f			; zap the high bits
		cmp	#$0a			; Is it 10 or more?
		bpl	useLetter		; If so, we use a letter, not a decimal digit
		ora #$30			; Make an ASCII digit
		rts
useLetter:
;
;		Letter A is 1 in screen codes. So we subtract 10 and add 1 i.e. subtract 9
		sec 
		sbc #9
		rts
.endproc
		
; ----------------------------------------------------------------------------
;
; Switch screen within a bank
;
; @param A the new screen. The top four bits of A are ignored. A is destroyed
;          by this routine.
;
.proc switchScreen

		asl						; Shift the screen number into the right bits
		asl
		asl
		asl
		sta	scratch0 			; Save the value
		lda vicMemoryPtr		; Get the existing memory pointer
		and	#vicCharRomMask		; zap the existing screen Location
		ora	scratch0			; Set the screen number
		sta vicMemoryPtr		; Save back to the VIC register.
		rts
.endproc	

; ----------------------------------------------------------------------------
;
; Switch VIC bank
;
; @param A the new bank number. The top six bits of A are ignored. A is destroyed
;          by this routine.
;
; Bank | Location
; -----+---------
;    0 | $0000    (includes char rom)
;    1 | $4000
;    2 | $8000    (includes char rom, overlaps BASIC ROM)
;    3 | $C000
;
.proc switchVicBank

		eor	#$3					; Negate the bottom two bits
		and #$3					; Zap all the other bits
		sta	scratch0 			; Save the value
		lda cia2PortA			; Get the CIA data
		and #$FC				; Zap the bottom two bits
		ora	scratch0			; Replace with the bank bits
		sta cia2PortA			; Save back to the CIA.
		rts
.endproc

menuStart:
	.byte "                                        "
	.byte " Options:                               "
	.byte "                                        "
	.byte " 1 Colour test                          "
	.byte " 2 Border colour test                   " ; 5 lines
	.byte " 3 Relocation of video matrix           "
	.byte " 4 Extended colour mode test            "
	.byte " 5 Standard bitmap test                 "
	.byte "                                        "
	.byte " Press the number of the test you want  "
	.byte "                                        "

menuEnd:
