;
; C64 stack machine
; 
; This implements a simple data stack based machine
;
; The data stack pointer is located at $6c which is actually in the floating 
; point accumulator, which the C64 does not use when not in BASIC.
;
; The stack pointe points to the item on the top of the stack.
;

.include "stackmachine.inc"

;
; Push the accumulator
;
; @param A the byte to push
;
; Also destroys Y
;
.proc pushAStack
		pha					; Save the accumulator
		sec					; Subtract 1 from the stack pointer
		lda	dataStackPtr	
		sbc	#1
		sta dataStackPtr	
		bcs	noBorrow		; If a borrow is needed, subtract 1 from high byte
		dec	dataStackPtr+1
noBorrow:
		ldy	#0
		pla					; Retrieve the value to push
		sta	(dataStackPtr),y	
		rts	
.endproc

;
; Pop into the accumulator
;
; @return A contains the popped byte
;
; Destroys Y
;
.proc	popAStack
		ldy	#0
		lda	(dataStackPtr),y	; Retrieve the byte on the stack top
		tay						; Save it for later
		clc						; Adjust the stack pointer
		lda	dataStackPtr
		adc	#1
		sta	dataStackPtr
		bcc	noCarry
		inc dataStackPtr+1
noCarry:
		tya						; Restore the return value
		rts	
.endproc

.proc add16Stack
; add the low bytes of the first two words on the stack and store in the 
; second word.
		ldy	#0
		lda (dataStackPtr),y
		clc
		iny
		iny
		adc	(dataStackPtr),y
		sta (dataStackPtr),y
; Add the top bytes of the first to words on the stack and store in the second
; word.
		dey
		lda (dataStackPtr),y
		iny
		iny
		adc	(dataStackPtr),y
		sta (dataStackPtr),y
;
; The second word contains the result. The first word is garbage.
;
		discardi8	2
		rts
.endproc

;
;  Fill a block of memory with a constant byte. Parameters are passed on the 
; data stack 
;
; @param address (16 bit) where to to start fill at
; @param byte (8 bit) byte to fill with
; @param count (8 bit) byte count $00 = 256 bytes
;

.proc memSetStack
		pop16	scratch0 	; Get the address into zero page
		pop8A				; Get the byte to fill in A
		pop8Y				; Get the byte count in Y
mssLoopStart:
		dey
		sta	(scratch0),y	
		cpy #$00
		bne mssLoopStart
		rts
.endproc

;
; Memory copy
;
; Copy one block of memory to another. This is not safe if the blocks overlap.
;
; @param destination (16 bit) address of destination block
; @param source (16 bit) source address to copy from
; @param count (8 bit) number of bytes to copy 0 = the entire memory
;
.proc memCpyStack 
		pop16 scratch0
		pop16 scratch2
		pop8Y
mcLoop:
		dey
		lda (scratch2),y
		sta (scratch0),y
		cpy #$00
		bne mcLoop
		rts
.endproc

.proc memCpy16Stack 
	dest 	:= scratch0
	source 	:= scratch2
	count 	:= scratch4		
		pop16 dest
		pop16 source
		pop16 count
		
; while count > 0
;    *dest = *source 
;    dest++
;    source++
;    count--

		jmp copyLoopTest
copyLoopStart:
		ldy	#0
		lda	(source),y 
		sta (dest),y
		inc16 dest 
		inc16 source
		dec16 count
		
copyLoopTest: 
		lda	count 
		bne	copyLoopStart
		lda	count+1
		bne copyLoopStart
			
		rts
.endproc

.proc memSet16Stack 
	dest 	:= scratch0
	copyByte := scratch2
	count 	:= scratch4		
		pop16 dest
		pop8 copyByte
		pop16 count
		
; while count > 0
;    *dest = copyByte 
;    dest++
;    count--

		jmp copyLoopTest
copyLoopStart:
		ldy	#0
		lda	copyByte 
		sta (dest),y
		inc16 dest 
		dec16 count
	
copyLoopTest: 
		lda	count 
		bne	copyLoopStart
		lda	count+1
		bne copyLoopStart
			
		rts
.endproc


;
; Logical shift left the second argument ton the stack by the top argument
;
; @param shift (8 bit) amount to shift
; @param operand (16 bit) thing to shift
; @return shifted operand
;
.proc lShiftLeft16Stack 
		txa
		pha
		
		pop8X
		pop16	scratch0
		cpx	#0		
		jmp shiftLoopTest
shiftLoopStart:
		asl	scratch0
		rol	scratch0+1				
		dex
shiftLoopTest:
		bne	shiftLoopStart 		; Exit if no shift
		
		push16	scratch0
		
cleanUp:
		pla
		tax
		rts
.endproc

.proc lShiftRight16Stack 
		txa
		pha
		
		pop8X
		pop16	scratch0
		cpx	#0		
		jmp shiftLoopTest
shiftLoopStart:
		lsr	scratch0+1
		ror	scratch0				
		dex
shiftLoopTest:
		bne	shiftLoopStart 		; Exit if no shift
		
		push16	scratch0	
cleanUp:
		pla
		tax
		rts
.endproc

.proc lShiftRight8Stack 
		txa
		pha
		
		pop8X
		popA
		cpx	#0		
		jmp shiftLoopTest
shiftLoopStart:
		lsr	
		dex
shiftLoopTest:
		bne	shiftLoopStart 		; Exit if no shift
		pushA	
cleanUp:
		pla
		tax
		rts
.endproc


.proc swap8Stack
		txa
		pha
		ldy	#0
		lda	(dataStackPtr),y
		tax
		iny
		lda	(dataStackPtr),y
		dey
		sta (dataStackPtr),y
		txa
		iny
		sta (dataStackPtr),y	
		pla
		tax
		rts
.endproc

.proc	dup16Stack
		ldy	#1
		lda (dataStackPtr),y 	; The high byte of the stack
		pushA
		ldy	#1
		lda (dataStackPtr),y 	; The low byte of the stack (stack pointer moved down)
		pushA
		rts
.endproc

.proc add1_16Stack
		ldy	#0
		lda #1
		clc
		adc	(dataStackPtr),y
		sta	(dataStackPtr),y
		bcc addEnd
		iny
		lda	#0
		adc	(dataStackPtr),y
		sta	(dataStackPtr),y
addEnd:
		rts
.endproc

.proc subtract16Stack
; subtract the low bytes of the first two words on the stack and store in the 
; second word. The top is subtracted from the next word
		ldy	#2
		lda (dataStackPtr),y
		sec
		ldy #0
		sbc	(dataStackPtr),y
		ldy	#2
		sta (dataStackPtr),y
; Subtract the top bytes of the first to words on the stack and store in the second
; word.
		iny						; cheaper than ldy #3
		lda (dataStackPtr),y
		ldy	#1
		sbc	(dataStackPtr),y
		ldy	#3
		sta (dataStackPtr),y
;
; The second word contains the result. The first word is garbage.
;
		discardi8	2
		rts
.endproc 

.proc branchZero16Stack
		pop16 scratch0
		
; Find out if the value on the stack is 0

		popA
		bne noJump1
		popA 
		bne noJump2
		
; Remove the return address because we are going to jump

		pla
		pla
		jmp (scratch0)

noJump1:
		popA				; Remove the other byte of the test value		
noJump2:
		rts
.endproc

.proc saveBaseRegisterStack
		push16	baseRegister		; 
		lda		dataStackPtr
		sta		baseRegister
		lda		dataStackPtr+1
		sta		baseRegister+1
		rts
.endproc

.proc restoreBaseRegisterStack
		lda	baseRegister
		sta dataStackPtr
		lda	baseRegister+1
		sta dataStackPtr+1
		pop16 baseRegister
		rts
.endproc

.proc invert8Stack
		popA
		eor	#$ff
		pushA
		rts
.endproc

.proc pushIndirect8Stack
		pop16 scratch0
		ldy	#0
		lda (scratch0),y
		pushA
		rts
.endproc

.proc popIndirect8Stack
		pop16 scratch0
		popA
		ldy	#0
		sta (scratch0),y
		rts
.endproc

.proc removeFromStack8Stack
		pop8 scratch0
		clc
		lda	dataStackPtr
		adc	scratch0
		sta dataStackPtr
		bcc noInc
		inc dataStackPtr+1
noInc:
		rts
.endproc

.proc pushBaseOffset16Stack
		pop8Y					; Get the offset
		tya 					; Preserve the offset
		pha
		iny						; The high byte is fetched first
		lda (baseRegister),y	; Got the high byte
		jsr pushAStack			; Put it on the stack
		pla						; Get the saved offset
		tay
		lda (baseRegister),y 	; Get the low byte
		jsr pushAStack	
		rts
.endproc

.proc popBaseOffset16Stack
		pop8 scratch0			; Get the offset
		jsr popAStack			; Get the low byte of the operand
		ldy scratch0			; get the offset into y
		sta (baseRegister),y	; store the low byte
		jsr popAStack			; Get the high byte off the stack
		ldy	scratch0			; Get the offset
		iny						; Add 1 for the high byte
		sta (baseRegister),y	; Store the high byte
		rts
.endproc
