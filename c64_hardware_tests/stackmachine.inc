;
; C64 stack machine
; 
; This implements a simple data stack based machine
;
; The data stack pointer is located at $6c which is actually in the floating 
; point accumulator, which the C64 does not use when not in BASIC.
;
; The stack pointer points to the item on the top of the stack.
;
; The accumulator and Y registers are not guaranteed to survive any of the 
; macros and routines defined here. The x register is.
;

.global pushAStack, popAStack, memSetStack, memCpyStack, add16Stack
.global memCpy16Stack, lShiftRight16Stack, dup8Stack, dup16Stack, swap8Stack
.global lShiftRight8Stack, lShiftLeft16Stack, memSet16Stack, add1_16Stack
.global subtract16Stack, branchZero16Stack

; Registers - these use the C64 floating point accumulator area

scratch0			:= $62				; Zero page scratch location
scratch2			:= $64				; Zero page scratch location
scratch4			:= $66				; Zero page scratch location
scratch6			:= $68				; Zero page scratch location
baseRegister		:= $6A				; Used as a base to reference stack variables
dataStackPtr		:= $6c				; ZP location of the data stack pointer

;
; Initialise the stack
;
; @param base A literal address
;
.macro	initStack	base 
		lda	#<base 
		sta dataStackPtr
		lda	#>base
		sta	dataStackPtr+1
.endmacro

;
; Load a 16 bit literal at an address
;
; @param address The address to load at
; @param literal (16 bit) the literal to load at the address
;
.macro loadi16	address,number
	lda #<number
	sta	address
	lda	#>number
	sta	address+1
.endmacro

;
; Push the accummulator onto the data stack
;
.macro pushA 
		jsr pushAStack
.endmacro

;
; Pop the stack into the accumulator.
;
.macro popA 
		jsr popAStack
.endmacro
;
; Push an 8 bit operand onto the data stack
; 
; @param operand any address mode supported by lda
.macro	push8	operand
		lda operand
		jsr pushAStack
.endmacro

;
; Push a 16 bit literal onto the data stack
;
; @param number The literal to push
;
.macro	pushi16	number
		lda #>number
		jsr pushAStack
		lda	#<number
		jsr	pushAStack
.endmacro

;
; Push the 16 bit number at the given address onto the stack
;
; @param address thew 16 bit address of the 16 bit number to push
;
.macro push16 address
		lda	address+1
		jsr pushAStack
		lda	address
		jsr pushAStack
.endmacro
;
; Push an 8 bit literal onto the data stack
;
; @param number The literal to push
;
.macro	pushi8	number
		lda #number
		jsr pushAStack
.endmacro
;
; Pop a 16 bit value to an address
;
; @param address the address to pop to
;
.macro pop16 address
		jsr popAStack
		sta	address
		jsr popAStack
		sta	address+1
.endmacro

;
; Pop an 8 bit value to an address
;
; v.b --> ()   ; *address = v
.macro pop8 address
		jsr popAStack
		sta	address
.endmacro

;
; Push the x register onto the data stack
;
.macro pushX 
		pha
		txa
		jsr pushAStack
		pla
.endmacro
;
; Pop an 8 bit value to the accumulator
;
.macro pop8A
		jsr popAStack
.endmacro
;
; Pop an 8 bit value to the Y reg
;
.macro pop8Y
		pha
		jsr popAStack
		tay
		pla
.endmacro

;
; Pop an 8 bit value to the X reg
;
.macro pop8X
		pha
		jsr popAStack
		tax
		pla
.endmacro

;
; Push the 16 bit value at the offset from the base register
;
;	@param offset offset from the base register. Note, max offset is 254
;
.macro pushBase16	offset
		pushi8	offset
		pushBaseOffset16
.endmacro

.global pushBaseOffset16Stack

;
; Push the 16 bit value referenced by the base register + the 8 bit offset on 
; the data stack
;
; offset.b --> (*baseRegister + offset).w
;
.macro pushBaseOffset16
		jsr pushBaseOffset16Stack
.endmacro
;
; Push the 8 bit value at the offset from the base register
;
;	@param offset offset from the base register. Note, max offset is 254
;
.macro pushBase8	offset
	ldy	#offset
	lda (baseRegister),y
	jsr pushAStack	
.endmacro

;
; Pop the 16 bit value on the stack to the offset from the base register
;
;	@param offset offset from the base register. Note, max offset is 254
;
.macro popBase16	offset
		pushi8	offset
		popBaseOffset16
.endmacro

.global popBaseOffset16Stack

;
; The top of the stack contains an offset used to calculate the place the next
; 16 bit element will be popped to.
;
; n.w offset.b --> () ; *(baseRegister + offset) = n
;
.macro popBaseOffset16
		jsr popBaseOffset16Stack
.endmacro

;
; Pop the 8 bit value on the stack to the offset from the base register
;
;	@param offset offset from the base register. Note, max offset is 254
;
.macro popBase8	offset
	jsr popAStack
	ldy	#offset
	sta (baseRegister),y
.endmacro

.global saveBaseRegisterStack

;
; Set up the base register. This gives something to index off for variables
; on the stack. Because (zp),y addressing is unsigned. It's best to do this after
; creating local variables.
;
; The old base register is saved on the stack and then the stack pointer is
; copied to the base register.
;
.macro saveBaseRegister
		jsr saveBaseRegisterStack
.endmacro

.global restoreBaseRegisterStack
;
; Restore the base register. This reverses the effects of a previous saveBaseRegister.
;
; Note that the data stack pointer will also be restored to where it was before
; the base register was saved.
;
.macro	restoreBaseRegister
		jsr restoreBaseRegisterStack
.endmacro

;
; Logical shift left the second argument ton the stack by the top argument
;
; @param shift (8 bit) amount to shift
; @param operand (16 bit) thing to shift
; @return shifted operand
;
.macro lShiftLeft16
		jsr lShiftLeft16Stack
.endmacro
;
; Logical shift right the second argument ton the stack by the top argument
;
; @param shift (8 bit) amount to shift
; @param operand (16 bit) thing to shift
; @return shifted operand
;
.macro lShiftRight16
		jsr lShiftRight16Stack
.endmacro

;
; Logical shift right an 8 bit quantity
;
; @param shift (8 bit) amount to shift
; @param operand (8 bit) thing to shift
;
; o.b s.b --> (o << s).b
;
.macro lShiftRight8
		jsr lShiftRight8Stack
.endmacro

.global invert8Stack
;
; Invert the top of the stack
;
; n.b --> ~n.b
;
.macro invert8
		jsr invert8Stack
.endmacro
;
; Discard the top n bytes off the stack
;
; @param count (8 bit) the number of bytes to discard
;
.macro discardi8 count
		pushi8	count
		removeFromStack8
.endmacro

.global removeFromStack8Stack
;
; The top 8 bit value on the stack is a number that tells us to rmove the next
; n values. from the stack
;
; x.b*n n --> ()
;
.macro removeFromStack8
		jsr removeFromStack8Stack
.endmacro

;
; Add the top two 16 bit items on the stack and replace them with their sum
.macro	add16
	jsr add16Stack
.endmacro

;
; Set a block of memory to a value
;
; @param block (16 bit) address of block to set
; @param value (8 bit) value to set in the block
; @param count (8 bit)  byte count $00 = 256 bytes
;
.macro memSet 
		jsr memSetStack
.endmacro
;
; Set a block of memory to a value using a 16 bit count
;
; @param block (16 bit) address of block to set
; @param value (8 bit) value to set in the block
; @param count (16 bit)  byte count $00 = 256 bytes
;
.macro memSet16 
		jsr memSet16Stack
.endmacro

;
; Memory copy
;
; Copy one block of memory to another. This is not safe if the blocks overlap.
;
; @param destination (16 bit) address of destination block
; @param source (16 bit) source address to copy from
; @param count (8 bit) number of bytes to copy 0 = 256 bytes
;
.macro memCpy
		jsr memCpyStack
.endmacro

;
; Memory copy 16 bit
;
; Copy one block of memory to another. This is not safe if the blocks overlap.
;
; In this version, the count is 16 bits allowing us to copy blocks larger than
; a page.
;
; @param destination (16 bit) address of destination block
; @param source (16 bit) source address to copy from
; @param count (16 bit) number of bytes to copy 
;
.macro memCpy16
		jsr memCpy16Stack
.endmacro

;
; Do a 16 bit increment at the address
;
; @param address address of the 16 bit operand to increnent.
;
.macro inc16 address
.local incEnd
		inc address 
		bne incEnd
		inc address+1
incEnd:
.endmacro

;
; Do a 16 bit decrement at the address
;
; @param address address of the 16 bit operand to decrenent.
;
.macro dec16 address
.local decEnd
		dec address
		lda	address
		cmp	#$ff 
		bne decEnd
		dec address+1
decEnd:
.endmacro

;
; Duplicate the byte at the top of the stack
; n.b --> n.b n.b
;
.macro dup8
		jsr dup8Stack
.endmacro
;
; Duplicate the word at the top of the stack
; n.w --> n.w n.w
;
.macro dup16
		jsr dup16Stack
.endmacro

.global pushIndirect8Stack
;
; Take the 16 bit value at the top of the stack and use it as the address of a
; value to push on the stack
;
; address.w --> *address.b
;
.macro pushIndirect8
	jsr pushIndirect8Stack
.endmacro

.global popIndirect8Stack
;
; The 16 bit value at the top of the stack is used as the address to pop the
; next 8 bit value to
;
; v.b address.w --> ()  ; *address == v

.macro popIndirect8
		jsr popIndirect8Stack
.endmacro
;
; Swap the two bytes on the top of the stack
;
; a.b b.b --> b.b a.b
;
.macro swap8
	jsr swap8Stack
.endmacro

;
; Add 1 to the top item on the stack
;
; n.w --> (n + 1).w
.macro	add1_16
		jsr add1_16Stack		
.endmacro

;
;
; Subtract the top item on the stack from the next item down
;
; a.w b.w --> (b - a).w
.macro subtract16
		jsr subtract16Stack 
.endmacro

;
; If the second item on the stack is zero, go to  the top item on the stack
;
; n.w address.w --> () ; if n == 0, jump to address
;  
.macro branchZero16
		jsr branchZero16Stack
.endmacro
