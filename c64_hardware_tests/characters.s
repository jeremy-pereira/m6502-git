;
; characters.s 
;
; Routines for manipulating the character map and characters of the C64
;
; Copyright (c) 2018, Jeremy Pereira
;

.include "stackmachine.inc"
.include "characters.inc"

.macro switchInCharRom
		sei							; We are switching out IO so no interrupts
		lda $1						; Get the current content of the data port
		pha							; Save the current banks state
		and #~4 & $ff				; Zero the char ROM select
		sta $1						; Switch the IO out 
.endmacro

.macro switchOutCharRom
		pla							; Get the old bank state back
		sta $1
		cli							; Re-enable interrupts
.endmacro

;
; Put all characters onto the screen in a 16 x 16 grid
;
; @param screenLoc Where the screen is
;
.proc displayAllCharactersSub

	screenLoc = 2
	saveBaseRegister

; First blank the screen which we do in four chunks

		pushi8	$0					; The count will be 256
		pushi8	$20					; The character is a space
		pushBase16	screenLoc		; Get the screen loc 
		memSet						; The first block memSet
		
; The second chunk

		pushi8	$0					; The count will be 256
		pushi8	$20					; The character is a space
		pushBase16	screenLoc		; The screen location 
		pushi16		$0100			; Needs to be incremented by 256
		add16		
		memSet
		
; The third chunk

		pushi8	$0					; The count will be 256
		pushi8	$20					; The character is a space
		pushBase16	screenLoc		; The screen location 
		pushi16		$0200			; Needs to be incremented by 512
		add16		
		memSet
		
; The last chunk can be 24 bytes smaller
		
		pushi8	$e8					; The count will be 232 (1000 - 3 x 256)
		pushi8	$20					; The character is a space
		pushBase16	screenLoc		; The screen location 
		pushi16		$0300			; Needs to be incremented by 512
		add16		
		memSet
		
; Now we put the chars into 8 lines of 32. x will be a line counter, y will
; be character within a line 
; A will be the character number

		txa 						; Preserve x which will be a loop variable
		pha

	screenLine := scratch0			; Address of line on screen		
	charNumber :=  scratch2			; Use scratch2 as our line counter
	
; initialise everything

		pushBase16	screenLoc		; Get the screen loc 
		pushi16		600				; Adjust it to the last line - we start from the end 
		add16
		pop16	screenLine
		lda	#$ff					; The character number counts down	
		ldx	#15						; Starting with the last line
lineLoopStart:
		ldy	#15						; and the last character on the line
		
; Now loop filling each line in turn

charLoopStart:
		sta	(screenLine),y	
		sec 						; decrement the character
		sbc	#1
		dey							; decrement y
		cpy	#$ff					; Have we finished the line?
		bne charLoopStart			; if not next character 

; Adjust the screenLine down 40 chars
 
		pha
		lda	screenLine
		sec
		sbc	#40
		sta screenLine 
		bcs noBorrow
		dec screenLine+1
noBorrow:
		pla
		
		dex							; Have we finished the screen
		cpx	#$ff
		bne	lineLoopStart			; If not, next line
		
		pla							; Restore x
		tax
		restoreBaseRegister
		discardi8	2				; Throw away the parameter
		rts
.endproc

;
; Copy a character set from one place to another.
;
; Parameters are passed on the data stack.
;
; @param destinationLoc location of the destination, must be a 2k aligned 2k 
;                       block.
;
; @param sourceLoc location of the source block. must be a 2k aligned 2k block
;                  with a character set in it.

.proc copyCharactersSub
	sourceOffset 	= 4
	destOffset 		= 2
		saveBaseRegister 
		txa							; We will use x so preserve it.
		pha
		switchInCharRom				; Make the char ROM accessible to the 6510
		
; A character set is  2k block i.e. 256 x 8. We copy it in 256 byte chunks.
		
		ldx	#8

copyLoop:
		dex
		
; Copy a block of characters

		pushi8		0				; For memCpy 0 is 256
		pushBase16	sourceOffset
		pushX 						; x is the block number, add it x 256 to both offsets
		pushi8		0
		add16
		pushBase16	destOffset
		pushX 						; x is the block number, add it x 256 to both offsets
		pushi8		0
		add16
		memCpy
		cpx	#0
		bne	copyLoop

		switchOutCharRom			; Get the IO space back and reenable interrupts.
		restoreBaseRegister			; Get the old base register back
		discardi8	4				; Throw away the dest and source
		
		pla							; restore x
		tax
		rts
.endproc

;
; Create an ASCII character set at the specified location.
;
; @param destinationLoc	Location tostore characters at.
;
.proc createASCIISub
	destOffset 		= 2
		saveBaseRegister 

; First put the standard lower case character set in place.

		pushi16	charRomLowerCase	; The lower case characters in char ROM
		pushBase16	destOffset
		copyCharacters
		
; Now we need to patch the character set to get the ASCII values right

		txa							; Save x
		pha
		
		switchInCharRom
		
		ldx	#0
		jmp	mapLoopTest
mapLoopStart:
		pushi8	0					; We need a 16 bit count
		push8	{asciiMap,x}		; The count of characters
		pushi8	3					; Multiply by 8
		lShiftLeft16
		inx							; Move to the source offset
		pushi8	0					; We need a 16 bit offset
		push8	{asciiMap,x}		; The character offset in PET
		pushi8	3					; Multiply by 8
		lShiftLeft16
		inx							; Move to the character map
		push8	{asciiMap,x}		; The character map
		pushi8	0					; Lower byte of the character map
		add16
		inx							; Now for the dest offset
		pushi8	0					; We need a 16 bit offset
		push8	{asciiMap,x}		; The character offset in PET
		pushi8	3					; Multiply by 8
		lShiftLeft16
		pushBase16	destOffset
		add16
		memCpy16					; Copy the group in to the destination
		inx							; Point at the next set of bytes in the table	
		
mapLoopTest:
		cpx	#(asciiMapEnd-asciiMap)
		bne	mapLoopStart
		
		switchOutCharRom
		
		pla							; restore x
		tax		

		restoreBaseRegister			; Get the old base register back
		discardi8	2				; Throw away the dest
		rts	

.endproc

.data
;
; This maps PET display characters to ASCII. Each entry is a triplet of
; count, PETchar, source charset, ASCIIchar
;
; Count is the number of characters to copy (not bytes which would be count * 8)
;
; pet char is the number of the PET character or the xharacter index into the 
; source character set.
;
; source character set is the char set from which to take the char. This is the
; upper byte of the address of the character map which is assumed to be 256 byte
; aligned. Some possible values are $D0 for C64 standard, $D8 for C64 lower/upper 
; and >myCharacterMap for self defined characters

asciiMap:
	.byte	1, 0, $D8, $40					; @
	.byte   26, 1, $D8, $61					; a - z
	.byte   4, 27, $D8, $5B                  ; [ \ ] ^ (not all map correctly)
;
; Fill the whole first 16 characters with the multicolpour mode test pattern
;
	.byte	1, 0, >myCharacterMap, 0		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 1		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 2		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 3		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 4		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 5		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 6		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 7		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 8		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 9		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 10		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 11		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 12		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 13		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 14		; Multicolour mode bitmap
	.byte	1, 0, >myCharacterMap, 15		; Multicolour mode bitmap
asciiMapEnd:

.align 256
myCharacterMap:
	.byte $05, $05, $05, $05, $AF, $AF, $AF, $AF	; A pattern for testing MCM
