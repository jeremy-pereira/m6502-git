//
//  OscilloscopeSourceController.swift
//  M6502
//
//  Created by Jeremy Pereira on 03/09/2015.
//
//

import Cocoa
import Swift6502

class OscilloscopeSourceController: NSViewController
{
    @IBOutlet var oscilloscopeView:  OscilloscopeTraceView!
    @IBOutlet var name: NSTextField!

    @IBOutlet var leadingToTrace: NSLayoutConstraint!
    @IBOutlet var trailingFromTrace: NSLayoutConstraint!


    fileprivate var source: Oscilloscope.Source
    fileprivate weak var oscilloscopeController: OscilloscopeController!

    init?(source: Oscilloscope.Source, oscilloscopeController: OscilloscopeController)
    {
        self.source = source
        self.oscilloscopeController = oscilloscopeController
        super.init(nibName: "OscilloscopeSourceController",
            		bundle: Bundle(for: type(of: self)))
    }

    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }


    override func viewDidLoad()
    {
        super.viewDidLoad()
        name.stringValue = source.name
    }

    let bottomMargin = CGFloat(5)

    ///
    /// Get a point array for plotting the data in one of the sources of the
    /// oscilloscope
    ///
    func pointArray(_ bounds: NSRect) -> [NSPoint]
    {
        var ret: [NSPoint] = []
        if source.data.count > 0
        {
            let valueRange = source.range!
            let verticalRange = bounds.height - (bottomMargin * 2)
            let horizontalRange = bounds.width
            let timeCovered = CGFloat(1 + oscilloscopeController.timeRange.width)

            let horizontalScale = horizontalRange / timeCovered
            let verticalScale = verticalRange / CGFloat(max(valueRange.width, 1))
            var lastPoint: NSPoint?
            for transition in source.data
            {
                let x = CGFloat(transition.time - oscilloscopeController.timeRange.start) * horizontalScale
                let y = CGFloat(transition.value - valueRange.start) * verticalScale + bottomMargin
                if let lastPoint = lastPoint
                {
                    ret.append(NSPoint(x: x, y: lastPoint.y))
                }
                let point = NSPoint(x: x, y: y)
                ret.append(point)
                lastPoint = point
            }
            guard let finalPoint = lastPoint else { fatalError("There should be a lastPoint") }
            let nowX = CGFloat(oscilloscopeController.timeRange.width) * horizontalScale
            ret.append(NSPoint(x: nowX, y: finalPoint.y))
        }
        return ret
    }

    func zoomToRange(_ zoomRange: Swift6502.Range<CGFloat>, baseLine: CGFloat)
    {
        oscilloscopeController.zoomToRange(zoomRange, baseLine: baseLine)
    }

    func updateUI()
    {
        oscilloscopeView.needsDisplay = true
    }

}
