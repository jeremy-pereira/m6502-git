//
//  TapeDriveController.swift
//  M6502
//
//  Created by Jeremy Pereira on 04/07/2015.
//
//

import Cocoa
import Swift6502

class TapeDriveController: NSWindowController
{

    weak var tapeCounter: NSTextField!
    var tapeDeck: PETDatasetteDrive!

    override func windowDidLoad()
    {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

    class func tapeDriveController() -> TapeDriveController
    {
        return TapeDriveController(windowNibName: "TapeDriveController")
    }

    @IBAction func pressRecord(_ sender: AnyObject)
    {
		tapeDeck.record()
    }
    @IBAction func pressRewind(_ sender: AnyObject)
    {
		tapeDeck.rewind()
        updateUI()
    }
    @IBAction func pressPlay(_ sender: AnyObject)
    {
		tapeDeck.play()
        startUpdateTimer()
    }
    @IBAction func pressStop(_ sender: AnyObject)
    {
		tapeDeck.stop()
        if let updateTimer = updateTimer
        {
            updateTimer.invalidate()
            self.updateTimer = nil
        }
    }

    @IBAction func pressFastForward(_ sender: AnyObject)
    {
		tapeDeck.fastForward()
        updateUI()
    }


    var updateTimer: Timer?

    func startUpdateTimer()
    {
		updateTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self,
            													selector: #selector(TapeDriveController.updateUI),
            													userInfo: nil,
            													 repeats: true)
    }

    var datasetteName: URL?

    @IBAction func loadTape(_ sender: AnyObject)
    {
		let openPanel = NSOpenPanel()
        openPanel.canChooseFiles = true
        openPanel.canChooseDirectories = false
        openPanel.allowsMultipleSelection = false
        openPanel.title = "Load Tape"
        openPanel.prompt = "Load"
        openPanel.nameFieldLabel = "Tape name"
        openPanel.message = "Load an existing tape"
        openPanel.allowedFileTypes = ["datasette"]
        openPanel.allowsOtherFileTypes = false
        openPanel.beginSheetModal(for: self.window!, completionHandler:
        {
            result in
            if result == .OK
            {
                self.datasetteName = openPanel.url
                guard let path = self.datasetteName?.path
                    else { fatalError("Expected a valid path if OK button pressed") }
                do
                {
                    let cassetteData = try Data(contentsOf: URL(fileURLWithPath: path),
                                                         options: NSData.ReadingOptions(rawValue: 0))
                    let byteCount = cassetteData.count
                    var cassette = [S65Byte](repeating: 0, count: byteCount)
                    (cassetteData as NSData).getBytes(&cassette, length: cassette.count)
					self.tapeDeck.datasette = cassette
                    self.updateUI()
                }
                catch let error as NSError
                {
                    self.presentError(error)
                }
                catch
                {
                    fatalError("Should not get here")
                }
            }
        })
    }

    @IBAction func newTape(_ sender: AnyObject)
    {
        let savePanel = NSSavePanel()
        savePanel.title = "Create New Tape"
        savePanel.prompt = "Create"
        savePanel.nameFieldLabel = "Tape name"
        savePanel.message = "Create a new blank tape"
        savePanel.allowedFileTypes = ["datasette"]
        savePanel.allowsOtherFileTypes = false
        savePanel.beginSheetModal(for: self.window!, completionHandler: {
            result in
            if result == .OK
            {
                self.datasetteName = savePanel.url
                self.createBlankTape()
            }
        })
    }

    func createBlankTape()
    {
        guard let path = datasetteName?.path
        else
        {
            fatalError("Failed to get path of \(String(describing: datasetteName))")
        }
        let fm = FileManager.default
        let blankCassetteData = Data()
        if fm.createFile(atPath: path, contents: blankCassetteData, attributes: nil)
        {
			self.tapeDeck.datasette = []
            self.updateUI()
        }
		else
        {
            // TODO: report an error
        }
    }

    @objc fileprivate func updateUI()
    {
        tapeCounter.integerValue = tapeDeck.tapePosition
    }

    func validateUserInterfaceItem(_ item: NSValidatedUserInterfaceItem) -> Bool
    {
        var ret: Bool = true
		let theAction = item.action
        if 	   theAction == #selector(TapeDriveController.pressRecord(_:)) || theAction == #selector(TapeDriveController.pressPlay(_:))
        	|| theAction == #selector(TapeDriveController.pressRewind(_:)) || theAction == #selector(TapeDriveController.pressFastForward(_:))
        {
			ret = !tapeDeck.isRecording && !tapeDeck.isPlaying && tapeDeck.datasette != nil
        }
		else if theAction == #selector(TapeDriveController.pressStop(_:))
        {
            ret = tapeDeck.isRecording || tapeDeck.isPlaying
        }

        return ret
    }
}
