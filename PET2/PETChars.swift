//
//  PETChars.swift
//  M6502
//
//  Created by Jeremy Pereira on 08/01/2017.
//
//

import Cocoa
import Swift6502

private let pixelSize = CGFloat(2)
private let widthPixels = 8
private let heightPixels = 8

class PETCharDelegate: VDUViewDelegate
{

    var backgroundColour: NSColor = NSColor.black
    var foregroundColour = NSColor.green

    private var drawnImages: [NSImage?] = [NSImage?](repeating: nil,
                                                         count: Int(S65Byte.max) + 1)

    let characterSize: NSSize = NSSize(width: CGFloat(widthPixels) * pixelSize,
                                      height: CGFloat(heightPixels) * pixelSize)

    @available(*, deprecated, message: "MetalVDUView does not call this function")
    func draw(view: VDUView, row: Int, bytes: [S65Byte])
    {
    	foregroundColour.set()

        for (column, byte) in bytes.enumerated()
        {
            let topLeftCorner = NSPoint(x: CGFloat(column) * characterSize.width,
                                        y: CGFloat(row) * characterSize.height)
            let image = characterImageFor(byte: byte)
            image.draw(at: topLeftCorner, from: NSZeroRect, operation: .copy, fraction: 1.0)
        }
    }

    func characterImageFor(byte: S65Byte) -> NSImage
    {
        let ret: NSImage
        if let cachedImage = drawnImages[Int(byte)]
        {
            ret = cachedImage
        }
        else
        {
            let pixelMap = pixelMapFor(byte: byte)
            ret = NSImage(size: characterSize)
            do
            {
                ret.lockFocus()
                defer { ret.unlockFocus() }
                foregroundColour.set()
                for yPixel in 0 ..< heightPixels
                {
                    let line = pixelMap[yPixel]
					var lineIndex = line.startIndex
                    for xPixel in 0 ..< widthPixels
                    {
                        if line[lineIndex] == "X"
                        {
                            let pixelRect = NSRect(x: pixelSize * CGFloat(xPixel),
                                                   y: pixelSize * CGFloat(yPixel),
                                                   width: pixelSize,
                                                   height: pixelSize)
                            NSBezierPath.fill(pixelRect)
                        }
                        lineIndex = line.index(after: lineIndex)
                    }
                }
            }
			drawnImages[Int(byte)] = ret
        }
        return ret
    }

    func setCharacterSet(lowerCase: Bool)
    {
        currentPixelMaps = lowerCase ? businessPixelMaps : graphicsPixelMaps
        _characterSetBitmap = nil
        for i in 0 ..< drawnImages.count
        {
            drawnImages[i] = nil
        }

    }

    private var currentPixelMaps: [[String]] = graphicsPixelMaps

    private func pixelMapFor(byte: S65Byte) -> [String]
    {
        let ret: [String]
        let pixelMapUnreversed = currentPixelMaps[Int(byte.with(bit: reverseBit, set: false))]
		if byte.isBitSet(reverseBit)
        {
			ret = pixelMapUnreversed.map
            {
                (pixelLine) -> String in
                return pixelLine.reduce("") { $0.appending($1 == "X" ? " " : "X") }
            }
        }
        else
        {
            ret = pixelMapUnreversed
        }
        return ret

    }

    var _characterSetBitmap: [UInt8]?

    var characterSetBitmap: [UInt8]
    {
		if let ret = _characterSetBitmap
        {
            return ret
        }

        var ret: [UInt8] = []
        for byte in S65Byte(0) ... S65Byte(255)
        {
            let stringMap = pixelMapFor(byte: byte)
            let thisLineBits = stringMap.map
            {
                return $0.reduce(0)
                {
                    (currentByte, char) -> UInt8 in
                    return (currentByte << 1) | (char == "X" ? 1 : 0)
                }
            }
            ret += thisLineBits
        }
        _characterSetBitmap = ret
        return ret
    }
}

// MARK: graphicsPixelMaps
private let graphicsPixelMaps: [[String]] =
[
    [//@
        "   XXX  ",
        "  X   X ",
        " X  X X ",
        " X X XX ",
        " X  XX  ",
        "  X     ",
        "   XXXX ",
        "        "
    ],
    [	//A
        "   XX   ",
        "  X  X  ",
        " X    X ",
        " XXXXXX ",
        " X    X ",
        " X    X ",
        " X    X ",
        "        "
    ],
    [//B
        " XXXXX  ",
        "  X   X ",
        "  X   X ",
        "  XXXX  ",
        "  X   X ",
        "  X   X ",
        " XXXXX  ",
        "        "
    ],
    [//C
        "   XXX  ",
        "  X   X ",
        " X      ",
        " X      ",
        " X      ",
        "  X   X ",
        "   XXX  ",
        "        "
    ],
    [//D
        " XXXX   ",
        "  X  X  ",
        "  X   X ",
        "  X   X ",
        "  X   X ",
        "  X  X  ",
        " XXXX   ",
        "        "
    ],
    [//E
        " XXXXXX ",
        " X      ",
        " X      ",
        " XXXX   ",
        " X      ",
        " X      ",
        " XXXXXX ",
        "        "
    ],
    [//F
        " XXXXXX ",
        " X      ",
        " X      ",
        " XXXX   ",
        " X      ",
        " X      ",
        " X      ",
        "        "
    ],
    [//G
        "   XXX  ",
        "  X   X ",
        " X      ",
        " X  XXX ",
        " X    X ",
        "  X   X ",
        "   XXX  ",
        "        "
    ],
    [//H
        " X    X ",
        " X    X ",
        " X    X ",
        " XXXXXX ",
        " X    X ",
        " X    X ",
        " X    X ",
        "        "
    ],
    [//I
        "   XXX  ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "   XXX  ",
        "        "
    ],
    [//J
        "    XXX ",
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  ",
        " X   X  ",
        "  XXX   ",
        "        "
    ],
    [//K
        " X    X ",
        " X   X  ",
        " X  X   ",
        " XXX    ",
        " X  X   ",
        " X   X  ",
        " X    X ",
        "        "
    ],
    [//L
        " X      ",
        " X      ",
        " X      ",
        " X      ",
        " X      ",
        " X      ",
        " XXXXXX ",
        "        "
    ],
    [//M
        " X    X ",
        " XX  XX ",
        " X XX X ",
        " X XX X ",
        " X    X ",
        " X    X ",
        " X    X ",
        "        "
    ],
    [//N
        " X    X ",
        " XX   X ",
        " X X  X ",
        " X  X X ",
        " X   XX ",
        " X    X ",
        " X    X ",
        "        "
    ],
    [//O
        "   XX   ",
        "  X  X  ",
        " X    X ",
        " X    X ",
        " X    X ",
        "  X  X  ",
        "   XX   ",
        "        "
    ],
    [//P
        " XXXXX  ",
        " X    X ",
        " X    X ",
        " XXXXX  ",
        " X      ",
        " X      ",
        " X      ",
        "        "
    ],
    [//Q
        "   XX   ",
        "  X  X  ",
        " X    X ",
        " X    X ",
        " X  X X ",
        "  X  X  ",
        "   XX X ",
        "        "
    ],
    [//R
        " XXXXX  ",
        " X    X ",
        " X    X ",
        " XXXXX  ",
        " X  X   ",
        " X   X  ",
        " X    X ",
        "        "
    ],
    [//S
        "  XXXX  ",
        " X    X ",
        " X      ",
        "  XXXX  ",
        "      X ",
        " X    X ",
        "  XXXX  ",
        "        "
    ],
    [//T
        "  XXXXX ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "        "
    ],
    [//U
        " X    X ",
        " X    X ",
        " X    X ",
        " X    X ",
        " X    X ",
        " X    X ",
        "  XXXX  ",
        "        "
    ],
    [//V
        " X    X ",
        " X    X ",
        " X    X ",
        "  X  X  ",
        "  X  X  ",
        "   XX   ",
        "   XX   ",
        "        "
    ],
    [//W
        " X    X ",
        " X    X ",
        " X    X ",
        " X XX X ",
        " X XX X ",
        " XX  XX ",
        " X    X ",
        "        ",
        ],
    [	//X
        " X    X ",
        " X    X ",
        "  X  X  ",
        "   XX   ",
        "   XX   ",
        "  X  X  ",
        " X    X ",
        " X    X ",
        ],
    [//Y
        "  X   X ",
        "  X   X ",
        "  X   X ",
        "   XXX  ",
        "    X   ",
        "    X   ",
        "    X   ",
        "        "
    ],
    [//Z
        " XXXXXX ",
        "      X ",
        "     X  ",
        "   XX   ",
        "  X     ",
        " X      ",
        " XXXXXX ",
        "        "
    ],
    //Somepunctuation
    [//[
        "  XXXX  ",
        "  X     ",
        "  X     ",
        "  X     ",
        "  X     ",
        "  X     ",
        "  XXXX  ",
        "        "
    ],
    [//\
        "        ",
        " X      ",
        "  X     ",
        "   X    ",
        "    X   ",
        "     X  ",
        "      X ",
        "        "
    ],
    [//]
        "  XXXX  ",
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  ",
        "  XXXX  ",
        "        "
    ],
    [//↑
        "    X   ",
        "   XXX  ",
        "  X X X ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [//←
        "        ",
        "        ",
        "   X    ",
        "  X     ",
        " XXXXXXX",
        "  X     ",
        "   X    ",
        "        "
    ],
    defaultPixelMap,	//space
    [//!
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "        ",
        "        ",
        "    X   ",
        "        "
    ],
    [//"
        "  X  X  ",
        "  X  X  ",
        "  X  X  ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "  X  X  ",
        "  X  X  ",
        " XXXXXX ",
        "  X  X  ",
        " XXXXXX ",
        "  X  X  ",
        "  X  X  ",
        "        "
    ],
    [ // $
        "    X   ",
        "   XXXX ",
        "  X X   ",
        "   XXX  ",
        "    X X ",
        "  XXXX  ",
        "    X   ",
        "        "
    ],
    [
        "        ",
        " XX   X ",
        " XX  X  ",
        "    X   ",
        "   X    ",
        "  X  XX ",
        " X   XX ",
        "        "
    ],
    [
        "  XX    ",
        " X  X   ",
        " X  X   ",
        "  XX    ",
        " X  X X ",
        " X   X  ",
        "  XX  X ",
        "        "
    ],
    [
        "     X  ",
        "    X   ",
        "   X    ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "     X  ",
        "    X   ",
        "   X    ",
        "   X    ",
        "   X    ",
        "    X   ",
        "     X  ",
        "        "
    ],
    [
        "  X     ",
        "   X    ",
        "    X   ",
        "    X   ",
        "    X   ",
        "   X    ",
        "  X     ",
        "        "
    ],
    [
        "    X   ",
        "  X X X ",
        "   XXX  ",
        "  XXXXX ",
        "   XXX  ",
        "  X X X ",
        "    X   ",
        "        "
    ],
    [
        "        ",
        "    X   ",
        "    X   ",
        "  XXXXX ",
        "    X   ",
        "    X   ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "    X   ",
        "    X   ",
        "   X    "
    ],
    [
        "        ",
        "        ",
        "        ",
        " XXXXXX ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "   XX   ",
        "   XX   ",
        "        "
    ],
    [
        "        ",
        "      X ",
        "     X  ",
        "    X   ",
        "   X    ",
        "  X     ",
        " X      ",
        "        "
    ],
    //Digits
    [//O
        "  XXXX  ",
        " X    X ",
        " X   XX ",
        " X XX X ",
        " XX   X ",
        " X    X ",
        "  XXXX  ",
        "        "
    ],
    [//1
        "    X   ",
        "   XX   ",
        "  X X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "  XXXXX ",
        "        "
    ],
    [//2
        "  XXXX  ",
        " X    X ",
        "      X ",
        "    XX  ",
        "  XX    ",
        " X      ",
        " XXXXXX ",
        "        "
    ],
    [//3
        "  XXXX  ",
        " X    X ",
        "      X ",
        "   XXX  ",
        "      X ",
        " X    X ",
        "  XXXX  ",
        "        "
    ],
    [//4
        "     X  ",
        "    XX  ",
        "   X X  ",
        "  X  X  ",
        " XXXXXX ",
        "     X  ",
        "     X  ",
        "        "
        
    ],
    [//5
        " XXXXXX ",
        " X      ",
        " XXXX   ",
        "     X  ",
        "      X ",
        " X   X  ",
        "  XXX   ",
        "        "
    ],
    [//6
        "   XXX  ",
        "  X   X ",
        " X      ",
        " XXXXX  ",
        " X    X ",
        " X    X ",
        "  XXXX  ",
        "        "
    ],
    [//7
        " XXXXXX ",
        " X    X ",
        "     X  ",
        "    X   ",
        "   X    ",
        "   X    ",
        "   X    ",
        "        "
    ],
    [//8
        "  XXXX  ",
        " X    X ",
        " X    X ",
        "  XXXX  ",
        " X    X ",
        " X    X ",
        "  XXXX  ",
        "        "
    ],
    [//9
        "  XXXX  ",
        " X    X ",
        " X    X ",
        "  XXXXX ",
        "      X ",
        "     X  ",
        "  XXX   ",
        "        "
    ],
    // More punctuation
    [
        "        ",
        "        ",
        "    X   ",
        "        ",
        "        ",
        "    X   ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "    X   ",
        "        ",
        "        ",
        "    X   ",
        "    X   ",
        "   X    "
    ],
    [
        "    XXX ",
        "   XX   ",
        "  XX    ",
        " XX     ",
        "  XX    ",
        "   XX   ",
        "    XXX ",
        "        "
    ],
    [
        "        ",
        "        ",
        " XXXXXX ",
        "        ",
        " XXXXXX ",
        "        ",
        "        ",
        "        "
    ],
    [
        " XXX    ",
        "   XX   ",
        "    XX  ",
        "     XX ",
        "    XX  ",
        "   XX   ",
        " XXX    ",
        "        "
    ],
    [
        "  XXXX  ",
        " X    X ",
        "      X ",
        "    XX  ",
        "   X    ",
        "        ",
        "   X    ",
        "        "
    ],
    // Graphics characters
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "        ",
        "        ",
        "        "
    ],
    [
        "    X   ",
        "   XXX  ",
        "  XXXXX ",
        " XXXXXXX",
        " XXXXXXX",
        "   XXX  ",
        "  XXXXX ",
        "        "
    ],
    [
        "   X    ",
        "   X    ",
        "   X    ",
        "   X    ",
        "   X    ",
        "   X    ",
        "   X    ",
        "   X    "
    ],
    [
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "XXXXXXXX",
        "        ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "XXXXXXXX",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "        ",
        "        "
    ],
    [
        "  X     ",
        "  X     ",
        "  X     ",
        "  X     ",
        "  X     ",
        "  X     ",
        "  X     ",
        "  X     "
    ],
    [
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  ",
        "     X  "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "XXX     ",
        "   X    ",
        "    X   ",
        "    X   "
    ],
    [
        "    X   ",
        "    X   ",
        "     X  ",
        "      XX",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "    X   ",
        "    X   ",
        "   X    ",
        "XXX     ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "XXXXXXXX"
    ],
    [
        "X       ",
        " X      ",
        "  X     ",
        "   X    ",
        "    X   ",
        "     X  ",
        "      X ",
        "       X"
    ],
    [
        "       X",
        "      X ",
        "     X  ",
        "    X   ",
        "   X    ",
        "  X     ",
        " X      ",
        "X       "
    ],
    [
        "XXXXXXXX",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       "
    ],
    [
        "XXXXXXXX",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X"
    ],
    [
        "        ",
        "  XXXX  ",
        " XXXXXX ",
        " XXXXXX ",
        " XXXXXX ",
        " XXXXXX ",
        "  XXXX  ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "        "
    ],
    [
        "  XX XX ",
        " XXXXXXX",
        " XXXXXXX",
        " XXXXXXX",
        "  XXXXX ",
        "   XXX  ",
        "    X   ",
        "        "
    ],
    [
        " X      ",
        " X      ",
        " X      ",
        " X      ",
        " X      ",
        " X      ",
        " X      ",
        " X      "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "      XX",
        "     X  ",
        "    X   ",
        "    X   "
    ],
    [
        "X      X",
        " X    X ",
        "  X  X  ",
        "   XX   ",
        "   XX   ",
        "  X  X  ",
        " X    X ",
        "X      X"
    ],
    [
        "        ",
        "  XXXX  ",
        " X    X ",
        " X    X ",
        " X    X ",
        " X    X ",
        "  XXXX  ",
        "        "
    ],
    [
        "    X   ",
        "   XXX  ",
        "  X X X ",
        " XXX XXX",
        "  X X X ",
        "    X   ",
        "    X   ",
        "        "
    ],
    [
        "      X ",
        "      X ",
        "      X ",
        "      X ",
        "      X ",
        "      X ",
        "      X ",
        "      X "
    ],
    [
        "    X   ",
        "   XXX  ",
        "  XXXXX ",
        " XXXXXXX",
        "  XXXXX ",
        "   XXX  ",
        "    X   ",
        "        "
    ],
    [
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "XXXXXXXX",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [
        "X X     ",
        " X X    ",
        "X X     ",
        " X X    ",
        "X X     ",
        " X X    ",
        "X X     ",
        " X X    "
    ],
    [
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [
        "        ",
        "        ",
        "       X",
        "  XXXXX ",
        " X X X  ",
        "   X X  ",
        "   X X  ",
        "        "
    ],
    [
        "XXXXXXXX",
        " XXXXXXX",
        "  XXXXXX",
        "   XXXXX",
        "    XXXX",
        "     XXX",
        "      XX",
        "       X"
    ],
    defaultPixelMap, // Shifted space
    [
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
	],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "XXXXXXXX",
        "XXXXXXXX",
        "XXXXXXXX"
    ],
    [
        "XXXXXXXX",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX"
   ],
    [
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       ",
        "X       "
    ],
    [
        "X X X X ",
        " X X X X",
        "X X X X ",
        " X X X X",
        "X X X X ",
        " X X X X",
        "X X X X ",
        " X X X X"
    ],
    [
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X"
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "X X X X ",
        " X X X X",
        "X X X X ",
        " X X X X"
    ],
    [
        "XXXXXXXX",
        "XXXXXXX ",
        "XXXXXX  ",
        "XXXXX   ",
        "XXXX    ",
        "XXX     ",
        "XX      ",
        "X       "
    ],
    [
        "      XX",
        "      XX",
        "      XX",
        "      XX",
        "      XX",
        "      XX",
        "      XX",
        "      XX",
    ],
    [
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    XXXX",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "    XXXX",
        "    XXXX",
        "    XXXX",
        "    XXXX"
    ],
    [
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "    XXXX",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXX   ",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "XXXXXXXX"
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "    XXXX",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "XXXXXXXX",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "XXXXX   ",
        "    X   ",
        "    X   ",
        "    X   "
    ],
    [
        "XX      ",
        "XX      ",
        "XX      ",
        "XX      ",
        "XX      ",
        "XX      ",
        "XX      ",
        "XX      "
    ],
    [
        "XXX     ",
        "XXX     ",
        "XXX     ",
        "XXX     ",
        "XXX     ",
        "XXX     ",
        "XXX     ",
        "XXX     "
    ],
    [
        "     XXX",
        "     XXX",
        "     XXX",
        "     XXX",
        "     XXX",
        "     XXX",
        "     XXX",
        "     XXX"
    ],
    [
        "XXXXXXXX",
        "XXXXXXXX",
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "XXXXXXXX",
        "XXXXXXXX",
        "XXXXXXXX",
        "        ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXXXXXX",
        "XXXXXXXX",
        "XXXXXXXX"
    ],
    [
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "       X",
        "XXXXXXXX"
    ],
    [
        "        ",
        "        ",
        "        ",
        "        ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    "
    ],
    [
        "    XXXX",
        "    XXXX",
        "    XXXX",
        "    XXXX",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "    X   ",
        "    X   ",
        "    X   ",
        "    X   ",
        "XXXXX   ",
        "        ",
        "        ",
        "        "
    ],
    [
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "        ",
        "        ",
        "        ",
        "        "
    ],
    [
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "XXXX    ",
        "    XXXX",
        "    XXXX",
        "    XXXX",
        "    XXXX"
    ]
]

// MARK: businessPixelMaps
private let businessPixelMaps: [[String]] =
    [
        [//@
            "   XXX  ",
            "  X   X ",
            " X  X X ",
            " X X XX ",
            " X  XX  ",
            "  X     ",
            "   XXXX ",
            "        "
        ],
        [	//A
            "        ",
            "        ",
            "  XXX   ",
            "     X  ",
            "  XXXX  ",
            " X   X  ",
            "  XXX X ",
            "        "
        ],
        [//B
            " X      ",
            " X      ",
            " X XXX  ",
            " XX   X ",
            " X    X ",
            " XX   X ",
            " X XXX  ",
            "        "
        ],
        [//C
            "        ",
            "        ",
            "  XXXX  ",
            " X    X ",
            " X      ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//C
            "      X ",
            "      X ",
            "  XXX X ",
            " X   XX ",
            " X    X ",
            " X   XX ",
            "  XXX X ",
            "        "
        ],
        [//C
            "        ",
            "        ",
            "  XXXX  ",
            " X    X ",
            " XXXXXX ",
            " X      ",
            "  XXXX  ",
            "        "
        ],
        [//F
            "    XX  ",
            "   X  X ",
            "   X    ",
            " XXXXX  ",
            "   X     ",
            "   X    ",
            "   X    ",
            "        "
        ],
        [//C
            "        ",
            "        ",
            "  XXX X ",
            " X   XX ",
            " X   XX ",
            "  XXX X ",
            "      X ",
            "  XXXX  "
        ],
        [//H
            " X      ",
            " X      ",
            " X XXX  ",
            " XX   X ",
            " X    X ",
            " X    X ",
            " X    X ",
            "        "
        ],
        [//I
            "    X   ",
            "        ",
            "   XX   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "   XXX  ",
            "        "
        ],
        [//J
            "     X  ",
            "        ",
            "    XX  ",
            "     X  ",
            "     X  ",
            " X   X  ",
            "  XXX   ",
            "        "
        ],
        [//K
            " X      ",
            " X      ",
            " X   X  ",
            " X  X   ",
            " X X    ",
            " XX X   ",
            " X   X  ",
            "        "
        ],
        [//I
            "   XX   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "   XXX  ",
            "        "
        ],
        [//M
            "        ",
            "        ",
            " XXX XX ",
            " X  X  X",
            " X  X  X",
            " X  X  X",
            " X  X  X",
            "        "
        ],
        [//N
            "        ",
            "        ",
            " X XXX  ",
            " XX   X ",
            " X    X ",
            " X    X ",
            " X    X ",
            "        "
        ],
        [//O
            "        ",
            "        ",
            "  XXXX  ",
            " X    X ",
            " X    X ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//P
            "        ",
            "        ",
            " X XXX  ",
            " XX   X ",
            " XX   X ",
            " X XXX  ",
            " X      ",
            " X      "
        ],
        [//P
            "        ",
            "        ",
            "  XXX X ",
            " X   XX ",
            " X   XX ",
            "  XXX X ",
            "      X ",
            "      X "
        ],
        [//P
            "        ",
            "        ",
            " X XXX  ",
            " XX   X ",
            " X      ",
            " X      ",
            " X      ",
            "        "
        ],
        [//S
            "        ",
            "        ",
            "  XXXXX ",
            " X      ",
            "  XXXX  ",
            "      X ",
            " XXXXX  ",
            "        "
        ],
        [//T
            "   X    ",
            "   X    ",
            " XXXXX  ",
            "   X    ",
            "   X    ",
            "   X  X ",
            "    XX  ",
            "        "
        ],
        [//U
            "        ",
            "        ",
            " X    X ",
            " X    X ",
            " X    X ",
            " X   XX ",
            "  XXX X ",
            "        "
        ],
        [//V
            "        ",
            "        ",
            " X    X ",
            " X    X ",
            " X    X ",
            "  X  X  ",
            "   XX   ",
            "        "
        ],
        [//W
            "        ",
            "        ",
            " X     X",
            " X  X  X",
            " X  X  X",
            " X  X  X ",
            "  XX XX ",
            "        ",
            ],
        [	//X
            "        ",
            "        ",
            " X    X ",
            "  X  X  ",
            "   XX   ",
            "  X  X  ",
            " X    X ",
            "        "
            ],
        [//Y
            "        ",
            "        ",
            " X    X ",
            " X    X ",
            " X   XX ",
            "  XXX X ",
            "      X ",
            "  XXXX  "
        ],
        [//Z
            "        ",
            "        ",
            " XXXXXX ",
            "     X  ",
            "   XX   ",
            "  X     ",
            " XXXXXX ",
            "        "
        ],
        //Somepunctuation
        [//[
            "  XXXX  ",
            "  X     ",
            "  X     ",
            "  X     ",
            "  X     ",
            "  X     ",
            "  XXXX  ",
            "        "
        ],
        [//\
            "        ",
            " X      ",
            "  X     ",
            "   X    ",
            "    X   ",
            "     X  ",
            "      X ",
            "        "
        ],
        [//]
            "  XXXX  ",
            "     X  ",
            "     X  ",
            "     X  ",
            "     X  ",
            "     X  ",
            "  XXXX  ",
            "        "
        ],
        [//↑
            "    X   ",
            "   XXX  ",
            "  X X X ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [//←
            "        ",
            "        ",
            "   X    ",
            "  X     ",
            " XXXXXXX",
            "  X     ",
            "   X    ",
            "        "
        ],
        defaultPixelMap,	//space
        [//!
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "        ",
            "        ",
            "    X   ",
            "        "
        ],
        [//"
            "  X  X  ",
            "  X  X  ",
            "  X  X  ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "  X  X  ",
            "  X  X  ",
            " XXXXXX ",
            "  X  X  ",
            " XXXXXX ",
            "  X  X  ",
            "  X  X  ",
            "        "
        ],
        [ // $
            "    X   ",
            "   XXXX ",
            "  X X   ",
            "   XXX  ",
            "    X X ",
            "  XXXX  ",
            "    X   ",
            "        "
        ],
        [
            "        ",
            " XX   X ",
            " XX  X  ",
            "    X   ",
            "   X    ",
            "  X  XX ",
            " X   XX ",
            "        "
        ],
        [
            "  XX    ",
            " X  X   ",
            " X  X   ",
            "  XX    ",
            " X  X X ",
            " X   X  ",
            "  XX  X ",
            "        "
        ],
        [
            "     X  ",
            "    X   ",
            "   X    ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "     X  ",
            "    X   ",
            "   X    ",
            "   X    ",
            "   X    ",
            "    X   ",
            "     X  ",
            "        "
        ],
        [
            "  X     ",
            "   X    ",
            "    X   ",
            "    X   ",
            "    X   ",
            "   X    ",
            "  X     ",
            "        "
        ],
        [
            "    X   ",
            "  X X X ",
            "   XXX  ",
            "  XXXXX ",
            "   XXX  ",
            "  X X X ",
            "    X   ",
            "        "
        ],
        [
            "        ",
            "    X   ",
            "    X   ",
            "  XXXXX ",
            "    X   ",
            "    X   ",
            "        ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "    X   ",
            "    X   ",
            "   X    "
        ],
        [
            "        ",
            "        ",
            "        ",
            " XXXXXX ",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "   XX   ",
            "   XX   ",
            "        "
        ],
        [
            "        ",
            "      X ",
            "     X  ",
            "    X   ",
            "   X    ",
            "  X     ",
            " X      ",
            "        "
        ],
        //Digits
        [//O
            "  XXXX  ",
            " X    X ",
            " X   XX ",
            " X XX X ",
            " XX   X ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//1
            "    X   ",
            "   XX   ",
            "  X X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "  XXXXX ",
            "        "
        ],
        [//2
            "  XXXX  ",
            " X    X ",
            "      X ",
            "    XX  ",
            "  XX  X ",
            " X    X ",
            " XXXXX  ",
            "        "
        ],
        [//3
            "  XXXX  ",
            " X    X ",
            "      X ",
            "   XXX  ",
            "      X ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//4
            "     X  ",
            "    XX  ",
            "   X X  ",
            "  X  X  ",
            " XXXXXX ",
            "     X  ",
            "     X  ",
            "        "

        ],
        [//5
            " XXXXXX ",
            " X      ",
            " XXXX   ",
            "     X  ",
            "      X ",
            " X   X  ",
            "  XXX   ",
            "        "
        ],
        [//6
            "   XXX  ",
            "  X   X ",
            " X      ",
            " XXXXX  ",
            " X    X ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//7
            " XXXXXX ",
            " X    X ",
            "     X  ",
            "    X   ",
            "   X    ",
            "   X    ",
            "   X    ",
            "        "
        ],
        [//8
            "  XXXX  ",
            " X    X ",
            " X    X ",
            "  XXXX  ",
            " X    X ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//9
            "  XXXX  ",
            " X    X ",
            " X    X ",
            "  XXXXX ",
            "      X ",
            "     X  ",
            "  XXX   ",
            "        "
        ],
        // More punctuation
        [
            "        ",
            "        ",
            "    X   ",
            "        ",
            "        ",
            "    X   ",
            "        ",
            "        "
        ],
        [
            "        ",
            "        ",
            "    X   ",
            "        ",
            "        ",
            "    X   ",
            "    X   ",
            "   X    "
        ],
        [
            "    XXX ",
            "   XX   ",
            "  XX    ",
            " XX     ",
            "  XX    ",
            "   XX   ",
            "    XXX ",
            "        "
        ],
        [
            "        ",
            "        ",
            " XXXXXX ",
            "        ",
            " XXXXXX ",
            "        ",
            "        ",
            "        "
        ],
        [
            " XXX    ",
            "   XX   ",
            "    XX  ",
            "     XX ",
            "    XX  ",
            "   XX   ",
            " XXX    ",
            "        "
        ],
        [
            "  XXXX  ",
            " X    X ",
            "      X ",
            "    XX  ",
            "   X    ",
            "        ",
            "   X    ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXXXXXX",
            "        ",
            "        ",
            "        "
        ],
       // Upper case characters
        [	//A
            "   XX   ",
            "  X  X  ",
            " X    X ",
            " XXXXXX ",
            " X    X ",
            " X    X ",
            " X    X ",
            "        "
        ],
        [//B
            " XXXXX  ",
            "  X   X ",
            "  X   X ",
            "  XXXX  ",
            "  X   X ",
            "  X   X ",
            " XXXXX  ",
            "        "
        ],
        [//C
            "   XXX  ",
            "  X   X ",
            " X      ",
            " X      ",
            " X      ",
            "  X   X ",
            "   XXX  ",
            "        "
        ],
        [//D
            " XXXX   ",
            "  X  X  ",
            "  X   X ",
            "  X   X ",
            "  X   X ",
            "  X  X  ",
            " XXXX   ",
            "        "
        ],
        [//E
            " XXXXXX ",
            " X      ",
            " X      ",
            " XXXX   ",
            " X      ",
            " X      ",
            " XXXXXX ",
            "        "
        ],
        [//F
            " XXXXXX ",
            " X      ",
            " X      ",
            " XXXX   ",
            " X      ",
            " X      ",
            " X      ",
            "        "
        ],
        [//G
            "   XXX  ",
            "  X   X ",
            " X      ",
            " X  XXX ",
            " X    X ",
            "  X   X ",
            "   XXX  ",
            "        "
        ],
        [//H
            " X    X ",
            " X    X ",
            " X    X ",
            " XXXXXX ",
            " X    X ",
            " X    X ",
            " X    X ",
            "        "
        ],
        [//I
            "   XXX  ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "   XXX  ",
            "        "
        ],
        [//J
            "    XXX ",
            "     X  ",
            "     X  ",
            "     X  ",
            "     X  ",
            "  X  X  ",
            "   XX   ",
            "        "
        ],
        [//K
            " X    X ",
            " X   X  ",
            " X  X   ",
            " XXX    ",
            " X  X   ",
            " X   X  ",
            " X    X ",
            "        "
        ],
        [//L
            " X      ",
            " X      ",
            " X      ",
            " X      ",
            " X      ",
            " X      ",
            " XXXXXX ",
            "        "
        ],
        [//M
            " X    X ",
            " XX  XX ",
            " X XX X ",
            " X XX X ",
            " X    X ",
            " X    X ",
            " X    X ",
            "        "
        ],
        [//N
            " X    X ",
            " XX   X ",
            " X X  X ",
            " X  X X ",
            " X   XX ",
            " X    X ",
            " X    X ",
            "        "
        ],
        [//O
            "   XX   ",
            "  X  X  ",
            " X    X ",
            " X    X ",
            " X    X ",
            "  X  X  ",
            "   XX   ",
            "        "
        ],
        [//P
            " XXXXX  ",
            " X    X ",
            " X    X ",
            " XXXXX  ",
            " X      ",
            " X      ",
            " X      ",
            "        "
        ],
        [//Q
            "   XX   ",
            "  X  X  ",
            " X    X ",
            " X    X ",
            " X  X X ",
            "  X  X  ",
            "   XX X ",
            "        "
        ],
        [//R
            " XXXXX  ",
            " X    X ",
            " X    X ",
            " XXXXX  ",
            " X  X   ",
            " X   X  ",
            " X    X ",
            "        "
        ],
        [//S
            "  XXXX  ",
            " X    X ",
            " X      ",
            "  XXXX  ",
            "      X ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//T
            "  XXXXX ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "        "
        ],
        [//U
            " X    X ",
            " X    X ",
            " X    X ",
            " X    X ",
            " X    X ",
            " X    X ",
            "  XXXX  ",
            "        "
        ],
        [//V
            " X    X ",
            " X    X ",
            " X    X ",
            "  X  X  ",
            "  X  X  ",
            "   XX   ",
            "   XX   ",
            "        "
        ],
        [//W
            " X    X ",
            " X    X ",
            " X    X ",
            " X XX X ",
            " X XX X ",
            " XX  XX ",
            " X    X ",
            "        ",
            ],
        [	//X
            " X    X ",
            " X    X ",
            "  X  X  ",
            "   XX   ",
            "   XX   ",
            "  X  X  ",
            " X    X ",
            " X    X ",
            ],
        [//Y
            "  X   X ",
            "  X   X ",
            "  X   X ",
            "   XXX  ",
            "    X   ",
            "    X   ",
            "    X   ",
            "        "
        ],
        [//Z
            " XXXXXX ",
            "      X ",
            "     X  ",
            "   XX   ",
            "  X     ",
            " X      ",
            " XXXXXX ",
            "        "
        ],
        [
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "XXXXXXXX",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [
            "X X     ",
            " X X    ",
            "X X     ",
            " X X    ",
            "X X     ",
            " X X    ",
            "X X     ",
            " X X    "
        ],
        [
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [
            "XX  XX  ",
            "XX  XX  ",
            "  XX  XX",
            "  XX  XX",
            "XX  XX  ",
            "XX  XX  ",
            "  XX  XX",
            "  XX  XX"
        ],
        [
            "XX  XX  ",
            " XX  XX ",
            "  XX  XX",
            "X  XX  X",
            "XX  XX  ",
            " XX  XX ",
            "  XX  XX",
            "X  XX  X"
        ],
        defaultPixelMap, // Shifted space
        [
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXXXXXX",
            "XXXXXXXX",
            "XXXXXXXX",
            "XXXXXXXX"
        ],
        [
            "XXXXXXXX",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXXXXXX"
        ],
        [
            "X       ",
            "X       ",
            "X       ",
            "X       ",
            "X       ",
            "X       ",
            "X       ",
            "X       "
        ],
        [
            "X X X X ",
            " X X X X",
            "X X X X ",
            " X X X X",
            "X X X X ",
            " X X X X",
            "X X X X ",
            " X X X X"
        ],
        [
            "       X",
            "       X",
            "       X",
            "       X",
            "       X",
            "       X",
            "       X",
            "       X"
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "X X X X ",
            " X X X X",
            "X X X X ",
            " X X X X"
        ],
        [
            "X  XX  X",
            "  XX  XX",
            " XX  XX ",
            "XX  XX  ",
            "X  XX  X",
            "  XX  XX",
            " XX  XX ",
            "XX  XX  "
        ],
        [
            "      XX",
            "      XX",
            "      XX",
            "      XX",
            "      XX",
            "      XX",
            "      XX",
            "      XX",
            ],
        [
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    XXXX",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "    XXXX",
            "    XXXX",
            "    XXXX",
            "    XXXX"
        ],
        [
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "    XXXX",
            "        ",
            "        ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXXX   ",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXXXXXX",
            "XXXXXXXX"
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "    XXXX",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "XXXXXXXX",
            "        ",
            "        ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXXXXXX",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "XXXXX   ",
            "    X   ",
            "    X   ",
            "    X   "
        ],
        [
            "XX      ",
            "XX      ",
            "XX      ",
            "XX      ",
            "XX      ",
            "XX      ",
            "XX      ",
            "XX      "
        ],
        [
            "XXX     ",
            "XXX     ",
            "XXX     ",
            "XXX     ",
            "XXX     ",
            "XXX     ",
            "XXX     ",
            "XXX     "
        ],
        [
            "     XXX",
            "     XXX",
            "     XXX",
            "     XXX",
            "     XXX",
            "     XXX",
            "     XXX",
            "     XXX"
        ],
        [
            "XXXXXXXX",
            "XXXXXXXX",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "XXXXXXXX",
            "XXXXXXXX",
            "XXXXXXXX",
            "        ",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXXXXXX",
            "XXXXXXXX",
            "XXXXXXXX"
        ],
        [
            "       X", // This is wrong
            "      X ",
            " X   X  ",
            " X  X   ",
            " X X    ",
            " XX     ",
            " X      ",
            "        "
        ],
        [
            "        ",
            "        ",
            "        ",
            "        ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    "
        ],
        [
            "    XXXX",
            "    XXXX",
            "    XXXX",
            "    XXXX",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "    X   ",
            "    X   ",
            "    X   ",
            "    X   ",
            "XXXXX   ",
            "        ",
            "        ",
            "        "
        ],
        [
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "        ",
            "        ",
            "        ",
            "        "
        ],
        [
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "XXXX    ",
            "    XXXX",
            "    XXXX",
            "    XXXX",
            "    XXXX"
        ]
]

private let defaultPixelMap: [String] =
[
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        ",
    "        "
]
