//
//  IEEEBusViewController.swift
//  M6502
//
//  Created by Jeremy Pereira on 19/08/2015.
//
//

import Cocoa
import Swift6502
import M65xxUI

class IEEEBusViewController: NSViewController, OperationUpdatable
{
    @IBOutlet var dav: NSButton!
    @IBOutlet var ndac: NSButton!
    @IBOutlet var nrfd: NSButton!
    @IBOutlet var atn: NSButton!
    @IBOutlet var eoi: NSButton!
    @IBOutlet var ifc: NSButton!
    @IBOutlet var ren: NSButton!
    @IBOutlet var seq: NSButton!
    @IBOutlet var data: NSTextField!

    var ieeeBus: IEEEBus

    init(ieeeBus: IEEEBus)
    {
        self.ieeeBus = ieeeBus
        super.init(nibName: "IEEEBusViewController", bundle: Bundle(for: type(of: self)))
    }

    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }


    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do view setup here.
    }

    func update(operation: ExecutionOperation)
    {
        dav.state  = ieeeBus.dav.outputWire.value ? .on : .off
        ndac.state = ieeeBus.ndac.outputWire.value ? .on : .off
        nrfd.state = ieeeBus.nrfd.outputWire.value ? .on : .off
        atn.state = ieeeBus.atn.value ? .on : .off
        eoi.state = ieeeBus.eoi.value ? .on : .off
		data.stringValue = ieeeBus.dio.value.hexString
    }
}
