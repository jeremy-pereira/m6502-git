//
//  PETError.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/12/2016.
//
//

import Foundation

enum PETError: Error
{
    case invalidProgram
}
