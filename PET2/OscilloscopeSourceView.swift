//
//  OscilloscopeSourceView.swift
//  M6502
//
//  Created by Jeremy Pereira on 03/09/2015.
//
//

import Cocoa

class OscilloscopeSourceView: NSView
{

    override var isFlipped: Bool { return true }

    override var intrinsicContentSize: NSSize { return NSSize(width: 500, height: 200) }
}
