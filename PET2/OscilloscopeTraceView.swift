//
//  OscilloscopeTraceView.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/08/2015.
//
//

import Cocoa
import Swift6502

class OscilloscopeTraceView: NSView
{

    @IBOutlet weak var controller: OscilloscopeSourceController!

    let backgroundColour = NSColor.black
    let traceColour = NSColor.green

    override func draw(_ dirtyRect: NSRect)
    {
        self.backgroundColour.set()
        NSBezierPath.fill(dirtyRect)
        if let startX = startX, let draggingX = draggingX
        {
            var xRange = Range(firstValue: startX)
            xRange.include(draggingX)
            let selectionRect = NSRect(x: xRange.start, y: 0, width: xRange.width, height: self.bounds.height)
            NSColor.gray.set()
            NSBezierPath.fill(selectionRect)
        }

        guard let controller = self.controller else { return }
        let pointArray = controller.pointArray(self.bounds)

        self.traceColour.setStroke()
        let path: NSBezierPath = NSBezierPath()
        var firstTime = true
        for point in pointArray
        {
			if firstTime
            {
                path.move(to: point)
                firstTime = false
            }
            else
            {
                path.line(to: point)
            }
        }
		path.stroke()
    }
    /*
     *  When we click and drag we want to change the portion of the view that
	 *  gets seen.
	 */
    var startX: CGFloat?
    var endX: CGFloat?
    var draggingX: CGFloat?

    override func mouseDown(with theEvent: NSEvent)
    {
        let clickLocation = self.convert(theEvent.locationInWindow, from: nil)
        startX = clickLocation.x
        endX = nil
    }

    override func mouseUp(with theEvent: NSEvent)
    {
        if let startX = startX
        {
            let clickLocation = self.convert(theEvent.locationInWindow, from: nil)
            endX = clickLocation.x
            var zoomRange = Range(firstValue: startX)
            zoomRange.include(endX)
            self.startX = nil
            endX = nil
            controller.zoomToRange(zoomRange, baseLine: self.bounds.width)
        }
    }

    override func mouseDragged(with theEvent: NSEvent)
    {
        let clickLocation = self.convert(theEvent.locationInWindow, from: nil)
		if let startX = startX , endX == nil
        {
            var redrawRange = Range(firstValue: startX)
            redrawRange.include(draggingX)
            draggingX = clickLocation.x
            redrawRange.include(draggingX)
            let invalidRect = NSRect(x: redrawRange.start, y: 0, width: redrawRange.width, height: self.bounds.height)
            self.setNeedsDisplay(invalidRect)
        }
    }
}
