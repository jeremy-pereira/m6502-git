//
//  NativeChars.swift
//  M6502
//
//  Created by Jeremy Pereira on 08/01/2017.
//
//

import Cocoa
import Swift6502

private let defaultFont: NSFont = NSFont(name: "Monaco", size: 12)!

private class CharacterMap
{
    fileprivate var mappings = [String](repeating: "?", count: Int(S65Byte.max) + 1)

    init(ranges: [S65Byte : [String]])
    {
        for key in ranges.keys
        {
            var index = key
            for string in ranges[key]!
            {
                mappings[Int(index)] = string
                index = index &+ 1
            }
        }
    }

    subscript(index: S65Byte) -> String
    {
        return mappings[Int(index)]
    }
}

/// A class that causes the VDU view to use nativ Mac fonts to draw characters
/// on the screen.
class NativeCharDelegate: VDUViewDelegate
{
    var characterSetBitmap: [UInt8]
    {
        fatalError("Native characterset does not support bitmaps")
    }

    var backgroundColour: NSColor = NSColor.black

    var font: NSFont = defaultFont
    {
        didSet
        {
            screenFontAttributes = [ NSAttributedString.Key.font : font,
                                     NSAttributedString.Key.backgroundColor : backgroundColour,
                                     NSAttributedString.Key.foregroundColor : NSColor.green]
            reverseFontAttributes = [ NSAttributedString.Key.font : font,
                                      NSAttributedString.Key.backgroundColor : NSColor.green,
                                      NSAttributedString.Key.foregroundColor : NSColor.black]
            
        }
    }
    private var screenFontAttributes: [NSAttributedString.Key : AnyObject] =
    [
        NSAttributedString.Key.font : defaultFont,
        NSAttributedString.Key.backgroundColor : NSColor.black,
        NSAttributedString.Key.foregroundColor : NSColor.green
    ]
    private var reverseFontAttributes: [NSAttributedString.Key : AnyObject] =
    [
        NSAttributedString.Key.font : defaultFont,
        NSAttributedString.Key.backgroundColor : NSColor.green,
        NSAttributedString.Key.foregroundColor : NSColor.black
    ]

    var characterSize: NSSize
    {
        var fullSize = "M".size(withAttributes: screenFontAttributes)
        fullSize.height -= 2 // Squash it a bit
        return fullSize
    }

    @available(*, deprecated, message: "MetalVDUView does not call this function")
    func draw(view: VDUView, row: Int, bytes: [S65Byte])
    {
        let drawPoint = NSPoint(x: 0, y: CGFloat(row) * characterSize.height)
        var attributes: [VDUAttribute] = [VDUAttribute](repeating: [], count: bytes.count)
        let theLine = stringFor(bytes: bytes, attributes: &attributes)
        let attributedLine = NSMutableAttributedString(string: theLine, attributes: screenFontAttributes)
        for index in 0 ..< attributedLine.length
        {
            if attributes[index].contains(.Reverse)
            {
                let range = NSRange(location: index, length: 1)
                attributedLine.setAttributes(reverseFontAttributes, range: range)
            }
        }
        attributedLine.draw(at: drawPoint)
    }

    private func stringFor(bytes: [S65Byte], attributes: inout [VDUAttribute]) -> String
    {
        let rowLength = bytes.count
        var ret = ""
        for col in 0 ..< rowLength
        {
            let (charString, attribute) = stringAndAttributesFor(byte: bytes[col])
            attributes[col] = attribute
            ret += charString
        }
        return ret
    }


    func stringAndAttributesFor(byte: S65Byte) -> (String, VDUAttribute)
    {
        var ret = "?"
        var attribute: VDUAttribute = []

        if byte.isBitSet(reverseBit)
        {
            attribute = attribute.union(VDUAttribute.Reverse)
        }
        ret = characterMap[byte.with(bit: reverseBit, set: false)]
        return (ret, attribute)
    }

    func setCharacterSet(lowerCase: Bool)
    {
        characterMap = lowerCase ? NativeCharDelegate.lowerCaseCharacterMap
            					 : NativeCharDelegate.graphicsCharacterMap
    }

    private var characterMap = graphicsCharacterMap

    private static let graphicsCharacterMap = CharacterMap(ranges: [
        0 : [ "@" ],
        1 : [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
              "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
        27 : [ "[", "\\", "]", "\u{2191}", "\u{2190}", " ", "!", "\"", "#", "$", "%",
               "&", "'",  "(", ")", "*", "+", ",", "-", ".", "/"],
        48 : [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        58 : [ ":", ";", "<", "=", ">", "?", "_"],
        65 : [ "♠︎", "|", "\u{2500}", "\u{2501}", "\u{2594}", "\u{2581}"],
        73 : [ "\u{256e}", "\u{2570}", "\u{256f}"]
        ])

    private static let lowerCaseCharacterMap = CharacterMap(ranges: [
        0 : [ "@" ],
        1 : [ "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
              "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"],
        27 : [ "[", "\\", "]", "\u{2191}", "\u{2190}", " ", "!", "\"", "#", "$", "%",
               "&", "'",  "(", ")", "*", "+", ",", "-", ".", "/"],
        48 : [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        58 : [ ":", ";", "<", "=", ">", "?", "_"],
        65 : [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
               "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        ])

}
