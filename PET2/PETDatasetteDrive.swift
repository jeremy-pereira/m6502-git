//
//  PETDatasetteDrive.swift
//  M6502
//
//  Created by Jeremy Pereira on 09/07/2015.
//
//

import Foundation
import Swift6502


let pulseTypeA: S65Byte = 0x41
let pulseTypeB: S65Byte = 0x42
let pulseTypeC: S65Byte = 0x43
let pulseTypeD: S65Byte = 0x44

private enum DriveState
{
	case stopped
	case recording
    case playing
}

private enum Pulse
{
    case low
    case high
}

class PETDatasetteDrive: ClockDriver
{
    var trace = false
    
    var datasette: [S65Byte]?
    {
		didSet
        {
            tapePosition = 0
        }
    }

    // MARK: Wiring

    var sense: Wire = Wire()
    {
        willSet
        {
            sense.value = true
        }
		didSet
        {
			sense.value = true
        }
    }

    var dataOut = Wire()
    var dataIn = Wire()
    {
		didSet
        {
            dataIn.connectNotifier(dataInNotifier)
        }
    }

    var motorControl: Wire = Wire()
    {
		didSet
        {
            motorControl.connectNotifier(motorNotifier)
        }
    }

    fileprivate var motorIsRunning: Bool
    {
		return !motorControl.value
    }

    fileprivate func motorNotifier(_ newValue: Bool)
    {
        if trace
        {
            print("Motor changed to \(newValue)")
        }
		switch driveState
        {
        case .stopped:
            break
        case .playing:
            playPulses(!newValue)
        case .recording:
            recordPulses(!newValue)
        }
    }

    // MARK: Drive state

    fileprivate var driveState = DriveState.stopped
    {
		didSet
        {
            switch driveState
            {
            case .stopped:
                sense.value = true
                cpu.removeClockDriver(self)
            case .playing:
                cpu.addClockDriver(self)
                sense.value = false
                playPulses(motorIsRunning)
            case .recording:
                cpu.addClockDriver(self)
                sense.value = false
                recordPulses(motorIsRunning)
            }
        }
    }

    fileprivate var playState = Pulse.low

    var isRecording: Bool { return driveState == .recording }

    var isPlaying: Bool { return driveState == .playing }

	var tapePosition = 0

    // MARK: Buttons on the drive

    func rewind()
    {
        switch driveState
        {
        case .stopped:
            tapePosition = 0
        default:
            break
        }
    }

    func play()
    {
        switch driveState
        {
        case .stopped:
            cycleCount = 0
            driveState = DriveState.playing
        default:
            break
        }
    }

    func record()
    {
        switch driveState
        {
        case .stopped:
            cycleCount = 0
            driveState = DriveState.recording
        default:
            break
        }
    }

    func stop()
    {
        sense.value = true
        driveState = DriveState.stopped
    }

    func fastForward()
    {
        switch driveState
        {
        case .stopped:
            if let datasette = datasette
            {
                tapePosition = datasette.count
            }
        default:
            break
        }
    }

    // MARK: Playing and recording

    static let pulseLength: [ S65Byte : CycleCount] =
    [
        pulseTypeA : 210000,
        pulseTypeB : 220,
        pulseTypeC : 290,
        pulseTypeD : 354
    ]

    func playPulses(_ start: Bool)
    {
        if start
        {
            playState = Pulse.high
            self.nextPlayState()
        }
    }

    var currentPulseType: S65Byte
    {
        var ret: S65Byte = 0
        if let datasette = datasette , tapePosition < datasette.count
        {
            ret = datasette[tapePosition]
        }
        return ret
    }

    func nextPlayState()
    {
        guard let datasette = datasette
        else
        {
            fatalError("Playing without a datasette in the drive")
        }
		cycleCount = 0
        if motorIsRunning && datasette.count > tapePosition
        {
			let pulseType = datasette[tapePosition]
            if let pulseCycleCount = type(of: self).pulseLength[pulseType]
            {
                targetCycleCount = pulseCycleCount
                if pulseType == pulseTypeA
                {
					playState = Pulse.low
                    tapePosition += 1
                }
                else if playState == Pulse.low
                {
                    playState = Pulse.high
                    tapePosition += 1
                }
                else
                {
                    if trace
                    {
                        print("pulse \(pulseName(pulseType)) negative edge")
                    }
                    playState = Pulse.low
                }
				dataOut.value = playState == Pulse.high
            }
            else
            {
                print("Invalid pulse type: \(pulseType)")
            }
        }
        else
        {
            targetCycleCount = CycleCount.max
        }
    }

    func pulseName(_ pulse: S65Byte) -> String
    {
        switch pulse
        {
        case 0x41: return "A"
        case 0x42: return "B"
        case 0x43: return "C"
        case 0x44: return "D"
          default: return "-"
        }
    }
    func recordPulses(_ start: Bool)
    {
        if start
        {
            cycleCount = 0
        }
    }

    fileprivate func dataInNotifier(_ newValue: Bool)
    {
        guard datasette != nil else { return }

        if newValue
        {
            let period = cycleCount
            let pulseLengths = type(of: self).pulseLength
            cycleCount = 0
            var pulse = pulseTypeA
            if period < pulseLengths[pulseTypeB]! * 2
            {
				pulse = pulseTypeB
            }
            else if period < pulseLengths[pulseTypeC]! * 2
            {
                pulse = pulseTypeB
            }
            else if period < pulseLengths[pulseTypeD]! * 2
            {
                pulse = pulseTypeD
            }
			if isRecording && motorIsRunning
            {
                if tapePosition == datasette!.count
                {
					datasette!.append(pulse)
                }
                else
                {
                    datasette![tapePosition] = pulse
                }
                tapePosition += 1
            }
        }
    }

    // MARK: microsecond timer

    /*
	 *  The microsecond timer is driven from the CPU clock.  It's used to measure
	 *  the length of input pulses (for recording) and time the length of output
     *  pulses for playback.
	 */

    fileprivate var cycleCount: CycleCount = 0
    fileprivate var targetCycleCount: CycleCount = CycleCount.max

    func drive(cycleDelta: CycleCount)
    {
        cycleCount += cycleDelta
        if cycleCount >= self.targetCycleCount && self.isPlaying
        {
			self.nextPlayState()
        }
    }

    weak var cpu: Mos65xx!	// Needed for the clock

}
