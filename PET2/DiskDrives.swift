//
//  DiskDrives.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/07/2015.
//
//

import Foundation

import Swift6502

enum FileError: Error, CustomStringConvertible
{
    case fileNotFound(String)
    case zeroLength(String)
    case cannotCreate(String)
    case fileNameNotSpecified
    case fileNotOpen(String)
    case syntaxError
    case invalidDirectoryEntry(String)
    case directoryWrite(Error)
    case noDiskInDrive(Int)
    case fileTypeMismatch(String)
    case endOfFile
    case unrecognisedCommand(S65Byte)
    case scratched(Int)

    var description: String
    {
        switch self
        {
        case .fileNotFound(let message):
			return "File not found: \(message)"
        case .zeroLength(let message):
            return "File \(message) is of zero length"
        case .cannotCreate(let message):
            return "Cannot create \(message)"
        case .fileNameNotSpecified:
            return "No file name specified"
        case .fileNotOpen(let message):
            return "File is not open: \(message)"
        case .syntaxError:
			return "Syntax error"
        case .invalidDirectoryEntry(let message):
            return "Invalid directory entry because: \(message)"
        case .directoryWrite(let error):
            return "Directory write failed: \(error)"
        case .noDiskInDrive(let driveNumber):
            return "No disk in drive \(driveNumber)"
        case .fileTypeMismatch(let fileName):
            return "File type mismatch \(fileName)"
        case .endOfFile:
            return "End of file"
        case .unrecognisedCommand(let byte):
            return "Unrecongnised command \(byte)"
        case .scratched(let fileCount):
            return "Scratched \(fileCount) files"
        }
    }

    var dosErrorString: String
    {
		switch self
        {
        case .fileNotFound:
            return "62,\"FILE DOES NOT EXIST\",0,0"
        case .cannotCreate:
            return "26,\"WRITE PROTECT TAB ON\",0,0"
        case .fileNameNotSpecified:
            return "34,\"FILE NAME OMITTED\",0,0"
        case .fileNotOpen:
            return "61,\"FILE NOT OPEN\",0,0"
        case .syntaxError:
            return "30,\"SYNTAX ERROR\",0,0"
        case .invalidDirectoryEntry:
            return "64, \"FILE TYPE MISMATCHh\",0,0"
        case .directoryWrite:
            return "67 \"SYSTEM OR TRACK ERROR\",0,0"
        case .noDiskInDrive:
            return "70,\"CHANNEL TO DISK UNAVAILABLE\",0,0"
        case .fileTypeMismatch:
            return "64,\"FILE TYPE MISMATCH\",0,0"
        case .endOfFile:
            return "50,\"READ PAST END OF FILE\",0,0"
        case .unrecognisedCommand:
            return "31,\"UNRECOGNIZED COMMAND\",0,0"
        case .scratched(let fileCount):
            return "01,\"FILES SCRATCHED\",\(fileCount),0"

        default:
            fatalError("Cannot handle error of type \(self)")
        }
    }
}

///
/// File types as used on a diskette
///
enum FileType: S65Byte
{
    case scratched = 0
    case directory = 0x91	// Pseudo entry for directory listing
    case deleted = 0x80
    case sequential = 0x81
    case program = 0x82
    case user = 0x83
    case relative = 0x84
}

///
/// Represents a diskette
///
class Diskette
{

    /// 
    /// Directory entry on this diskette
    ///
    class DirectoryEntry
    {
        var fileType: FileType = .scratched
        var fileName: String = ""
        var recordSize: S65Byte = 0
        var node: Int = 0

        convenience init(data: [S65Byte]) throws
        {
			let fields = data.splitAt(S65Byte.colon)
            if fields.count != 4
            {
                throw FileError.invalidDirectoryEntry("Wrong number of fields")
            }
            let fileTypeBytes = fields[0]
            guard let fileTypeRaw = fileTypeBytes.byteFromHex
            else
            {
                throw FileError.invalidDirectoryEntry("Invalid file type")
            }
            guard let fileType = FileType(rawValue: fileTypeRaw)
            else
            {
                throw FileError.invalidDirectoryEntry("Invalid file type")
            }

            let fileNameBytes = fields[1]
            guard let fileName = fileNameBytes.asciiString
            else
            {
                throw FileError.invalidDirectoryEntry("Invalid file name")
            }
            let recordSizeBytes = fields[2]
            guard let recordSize = recordSizeBytes.byteFromHex
            else
            {
                throw FileError.invalidDirectoryEntry("Invalid relative record length")
            }
            let nodeBytes = fields[3]
            guard let node = nodeBytes.uint32FromHex
            else
            {
                throw FileError.invalidDirectoryEntry("Invalid node number")
            }
            self.init(fileName: fileName, fileType: fileType, node: node, recordSize: recordSize)
        }

        init(fileName: String, fileType: FileType, node: Int, recordSize: S65Byte)
        {
            self.fileName = fileName
            self.fileType = fileType
            self.node = node
            self.recordSize = recordSize
        }

        var toString: String
        {
			return "\(fileType.rawValue.hexString):\(fileName):\(recordSize.hexString):\(node.makeHexString(8))"
        }
    }

    var nextNode = 1

    /// Where on the host file system is the diskette located
    var folderPath: String
    var directoryFileName: String  { return folderPath.stringByAppendingPathComponent("00000000") }

    init(folderPath: String)
    {
		self.folderPath = folderPath
        do
        {
            try readDirectoryFile()
        }
        catch
        {
            print("Fatal error \(error) reading directory in \(folderPath)")
        }
    }

    var directory: [String : DirectoryEntry] = [:]

    fileprivate func readDirectoryFile() throws
    {
        let fm = FileManager.default
        if fm.fileExists(atPath: directoryFileName)
        {
			let directoryData = try Data(contentsOf: URL(fileURLWithPath: directoryFileName), options: NSData.ReadingOptions())
            var directoryBytes = [S65Byte](repeating: 0, count: directoryData.count)
            (directoryData as NSData).getBytes(&directoryBytes, length: directoryBytes.count)
			let rawEntries = directoryBytes.splitAt(S65Byte.lineFeed)
            var entryCount: Int = 0
            for entry in rawEntries
            {
                if entry.count > 0
                {
                    do
                    {
                        let dirEntry = try DirectoryEntry(data: entry)
                        if dirEntry.node >= nextNode
                        {
                            nextNode = dirEntry.node + 1
                        }
                        directory[dirEntry.fileName] = dirEntry
                    }
                    catch
                    {
                        print("Disk entry \(entryCount) has error \(error)")
                    }
                    entryCount += 1

                }
            }
        }
        print(directoryString)
    }

    fileprivate var directoryString: String
    {
        var ret: String = ""
		for dirEntry in directory.values
        {
			ret += dirEntry.toString + "\n"
        }
        return ret
    }

    fileprivate func writeDirectoryFile() throws
    {
        let data = self.directoryString.asUTF8Data
		do
        {
            try data.write(to: URL(fileURLWithPath: directoryFileName), options: NSData.WritingOptions())
        }
        catch
        {
            print("Error writing directory")
            throw FileError.directoryWrite(error)
        }
    }

    ///
    /// Get the actual path in the host file system of the specified file.
    /// - Parameter fileName: The file name to find the physical path of
    /// - Returns: The OS X path of the file or nil if it isn't on disk
    ///
    func physicalPath(_ fileName: String) -> String?
    {
        var ret: String?

        if let dirEntry = directory[fileName]
        {
            ret = physicalPath(dirEntry.node)
        }
        return ret
    }

    func physicalPath(_ node: Int) -> String
    {
    	return folderPath.stringByAppendingPathComponent("\(node.makeHexString(8))")
    }

    ///
    /// Get the file tye of the given file.
    ///
    /// - Parameter fileName: The file to get the type of
    /// - Returns: The file type of the file or nil if it doesn't exist in the directory.
    ///
    func fileTypeOf(_ fileName: String) -> FileType?
    {
        let ret: FileType?
        if fileName == "$0"
        {
			ret = FileType.directory
        }
        else
        {
            ret = directory[fileName]?.fileType
        }
		return ret
    }

    ///
    /// Create the physical file for a given file name and return its 
    /// physical path.
    ///
    /// - Parameter fileName: Name of file to create
    /// - Parameter ofType: Type of file to create
    /// - Returns: The physical path of the newly created file.
    ///
    func createFile(_ fileName: String, ofType: FileType) -> String
    {
		let myNode = nextNode++
        let filePath = physicalPath(myNode)

        let fm = FileManager.default
        let ret: String


        if !fm.fileExists(atPath: filePath)
        {
            do
            {
                try fm.createDirectory(atPath: folderPath, withIntermediateDirectories: true, attributes: nil)
            }
            catch
            {
                // TODO: Throw here instead of crashing
                fatalError("Creating directory for disk at \(folderPath) failed")
            }
            if !fm.createFile(atPath: filePath, contents: nil, attributes: nil)
            {
                // TODO: Throw here instead of crashing
                fatalError("Creating directory for disk at \(folderPath) failed")
            }
            else
            {
                ret = filePath
            }
        }
        else
        {
            ret = filePath
        }
        let newEntry = DirectoryEntry(fileName: fileName, fileType: ofType, node: myNode, recordSize: 0)
        directory[fileName] = newEntry
        writeDirectoryToDisk()
		return ret
    }

    func writeDirectoryToDisk()
    {
        let directoryPath = physicalPath(0)
        let data = directoryString.asUTF8Data
        try? data.write(to: URL(fileURLWithPath: directoryPath), options: [])
    }

    ///
    /// List the files on this diskette that match the given pattern
    ///
    /// Patterns can contain `*` and `?` for globbing.  `*` means any sequence
    /// and `?` means any single character, I think.
    ///
    /// - Parameter filePattern: The file pattern to match.
    /// - Returns: A list of all the files matching the pattern.
    ///
    /// - Todo: Globbing is currently not implemented.
    ///
    func listFiles(_ filePattern: String) -> [String]
    {
        let fileNames = directory.keys
        let ret = fileNames.filter{ $0 == filePattern }
        return Array(ret)
    }

    ///
    /// Scratch a file if it exists.
    ///
    /// - Returns: true if a file was deleted.
    ///
    func scratch(_ fileName: String) throws -> Bool
    {
        var ret = false
		if let dirEntry = directory[fileName]
        {
            directory.removeValue(forKey: fileName)
            try writeDirectoryFile()
            let filePath = physicalPath(dirEntry.node)
            let fm = FileManager.default
            do
            {
                try fm.removeItem(atPath: filePath)
            }
            catch
            {
                print("Failed to remove \(filePath), reason: \(error)")
            }
			ret = true
        }
        return ret
    }
}

struct OpenMode: OptionSet, CustomStringConvertible
{
    let rawValue: Int

	static let None: OpenMode = []
    static let Read      = OpenMode(rawValue: 0b01)
    static let Write     = OpenMode(rawValue: 0b10)
    static let ReadWrite = OpenMode(rawValue: 0b11)

    var description: String
    {
        return (self.contains(.Read) ? "R" : "-") + (self.contains(.Write) ? "W" : "-")
    }
}

///
/// A disk channel for sending or retrieving data
///
protocol Channel: class
{
    ///
    /// True if the file is already open
    ///
    var isOpen: Bool { get }
    ///
    /// Open the channel for reading, writing or both
    ///
    /// - Parameter mode: Mode with whuch to open the file (read, write or both)
    /// - Throws: if we can't open the file for any reason
    ///
    func open(_ mode: OpenMode) throws

    ///
    /// Close the channel.
    ///
    /// The function should be written so that it is safe to close an already 
    /// cloased file.  Also, for safety, close should be called in the deinit
    /// to avoid leaks in case something goes wrong.
    ///
    func close()
    ///
    /// Read/write mode of the file.
    ///
    var mode: OpenMode { get }

    ///
    /// Save a chunk of data.
    ///
    /// Note that the save operation need not simply be a "save to disk".  The
    /// command channel uses this to run a command from the PET.
    ///
    /// - Parameter data: data to save
    /// - Throws: If we cannot save the data for any reason
    ///
    func save(_ data: [S65Byte]) throws

    /// Fetch a byte from the channel.  If there are nine left, return nil
    ///
    /// - Returns: The next byte on the channel
    /// - Throws: An error if we fail to get the current byte
    ///
    func currentByte() throws -> S65Byte?

    /// Move on to the next byte in the stream
    ///
    /// - Throws: If something goes wrong reading data from the file.
    func nextByte() throws

    /// True if the last byte fetched was the last byte in the stram (for now).
    /// For a sequential file this gets set at the end of each record.
    var isLastByte: Bool { get }
}

private class FileEntry: Channel
{
    var name: String?
    var mode: OpenMode = OpenMode.None
    var driveNumber: Int?
    var fileType: FileType { fatalError("Need to override file type") }

    weak var driveController: DualDiskDrive?

    init(driveController: DualDiskDrive)
    {
        self.driveController = driveController
    }

    class func entryForType(_ fileType: FileType, driveController: DualDiskDrive) throws -> FileEntry
    {
		switch fileType
        {
        case .program:
            return ProgramFileEntry(driveController: driveController)
        case .sequential:
            return SequentialFileEntry(driveController: driveController)
        case .directory:
            return DirectoryFileEntry(driveController: driveController)
        default:
            throw FileError.fileTypeMismatch("")
        }
    }

    deinit
    {
        close()
    }

    var fileHandle: FileHandle?

    var isOpen: Bool { return fileHandle != nil }

    ///
    /// Open a file for reading or writing
    ///
    /// - Parameter mode: Open for read or write
    ///
    func open(_ mode: OpenMode) throws
    {
        guard name != nil && driveNumber != nil
            else
        {
            throw FileError.fileNameNotSpecified
        }
        self.mode = mode
        if let filePath = try makeSureFileExists()
        {
			switch mode
            {
            case OpenMode.Read:
                fileHandle = FileHandle(forReadingAtPath: filePath)
            case OpenMode.Write:
                fileHandle = FileHandle(forWritingAtPath: filePath)
            case OpenMode.ReadWrite:
                fileHandle = FileHandle(forUpdatingAtPath: filePath)
            default:
                fatalError("Invalid open mode: \(mode.rawValue)")
            }
        }
        else
        {
            if mode.contains(OpenMode.Write)
            {
                throw FileError.cannotCreate("\(self)")
            }
            else
            {
                throw FileError.fileNotFound("\(self)")
            }
        }

    }

    func close()
    {
        fileHandle?.closeFile()
    }
    ///
    /// Save the some data to this file.
    ///
    /// As with most of the save stuff, there is no way to return an error to
    /// the computer, so if the save cannot be completed, we just return.
    ///
    /// - Parameter data: The data to save to the file.
    ///
    fileprivate func save(_ data: [S65Byte]) throws
    {
        guard let fileHandle = fileHandle
        else
        {
            throw FileError.fileNotOpen("\(self)")
        }

        let saveData = Data(data)
        fileHandle.write(saveData)
    }

    fileprivate func disketteForDriveNumber(_ driveNumber: Int) throws -> Diskette
    {
        guard let driveController = driveController else { fatalError("No drive controller") }
        guard let        diskette = driveController.diskettes[driveNumber]
        else
        {
            throw FileError.noDiskInDrive(driveNumber)
        }
        return diskette
    }
    ///
    /// Make a file path as long as there is enough info in the file entry to
    /// do so.
    ///
    /// - Throws: if there is no disk in the disk drive.
    /// - Returns: the file path or nil if it doesn't yet exist.
    fileprivate func makeFilePath() throws -> String?
    {
        guard let     driveNumber = driveNumber     else { fatalError("No drive number") }
        guard let        fileName = name            else { fatalError("No file name") }
        return try disketteForDriveNumber(driveNumber).physicalPath(fileName)
    }

    var fileTypeExtension: String { return "" }

    ///
    /// Ensure that a file exists either for reading or writing.
    ///
    /// If the file is opened for write, we create it if it is not there.  If it
    /// is open for read and does not already exist, we return `false`.  We also
    /// return false if the file info does not have enough information in it to
    /// identify or create the file.
    ///
    /// - Throws: If we fail to make sure the file exists for any reason other than
    ///           it doesn't actually exist.
    /// - Returns: the path to the existing or created file or nil if we fail
    ///            to create it.
    ///
    fileprivate func makeSureFileExists() throws -> String?
    {
        guard let fileName = name else { fatalError("No file name") }

        var ret: String?
        guard let driveNumber = driveNumber else { fatalError("No drive number") }

        if let filePath = try makeFilePath()
        {
            ret = filePath
        }
        else if mode.contains(OpenMode.Write)
        {
            let diskette = try disketteForDriveNumber(driveNumber)
            ret = diskette.createFile(fileName, ofType: fileType)
        }
        return ret
    }

    ///
    /// Fetch data from the file.
    ///
    /// Currently this fetches everything in one go.  The largest file a PET can
    /// load is < 32k anyway.
    ///
    /// - Returns: an array of the fetched data bytes
    /// - Throws: if the fetch fails for any reason e.g. the file is missing or
    ///           unreadable.
    func fetchData() throws
    {
        guard let fileHandle = fileHandle
        else
        {
            throw FileError.fileNotOpen("\(self)")
        }

		let data = fileHandle.readDataToEndOfFile()
        var ret: [S65Byte] = []
        ret = [S65Byte](repeating: 0, count: data.count)
        (data as NSData).getBytes(&ret, length: ret.count)
        if ret.count == 0
        {
            throw FileError.zeroLength("\(self)")
        }
        outputBuffer = ret
        outputPosition = 0
    }

    fileprivate var outputBuffer: [S65Byte]?
    fileprivate var outputPosition: Int = 0

    func currentByte() throws -> S65Byte?
    {
        var ret: S65Byte?
        if outputBuffer == nil
        {
            try nextByte()
        }
        if let outputBuffer = outputBuffer
        {
            if outputPosition < outputBuffer.count
            {
                ret = outputBuffer[outputPosition]
            }
        }
        return ret
    }

    func nextByte() throws
    {
        if outputBuffer == nil
        {
			try fetchData()
        }
        else if outputPosition < outputBuffer!.count
        {
            outputPosition += 1
        }
    }

    var isLastByte: Bool { return outputBuffer == nil || outputPosition == outputBuffer!.count - 1 }

    func pushBackLastByte()
    {
        if outputPosition > 0
        {
            outputPosition -= 1
        }
    }
}

private class ProgramFileEntry: FileEntry
{
    override var fileType: FileType { return FileType.sequential }
}

///
/// File entry for sequential files
///
private class SequentialFileEntry: FileEntry
{
    override var fileType: FileType { return FileType.sequential }
}

private class DirectoryFileEntry: FileEntry
{
    fileprivate var trace = true
    ///
    /// Open a file for reading or writing
    ///
    /// - Parameter mode: Open for read or write
    ///
    override func open(_ mode: OpenMode) throws
    {
        guard name != nil && driveNumber != nil
            else
        {
            throw FileError.fileNameNotSpecified
        }
        self.mode = mode
        _isOpen = true
    }

    fileprivate var _isOpen = false
    override var isOpen: Bool { return _isOpen }

	override func fetchData() throws
    {
        var ret: [S65Byte] = []
        let diskName = "AAAAAAAAAAAAA"
        let diskId = "1"
        let version: S65Byte = 0x2c
        let asciiDiskName = [ 0x22 ] + diskName.asASCII + [ 0x22 ]
        let asciiVersion = version.hexString.uppercased().asASCII

        var line: [S65Byte] = [ 0x0, 0x0 ]
        line += [ 0x20 ]
        line +=  asciiDiskName
        line += [ 0x20 ]
        line += diskId.asASCII
        line += [ 0x20 ]
        line += asciiVersion + [ 0x00 ]
        let base: S65Address = 0x0401
        let link = base + S65Address(line.count)
        ret += [ base.lowByte, base.highByte ]
        ret.append(link.lowByte)
        ret.append(link.highByte)
        ret += line
        ret += [0, 0]
        if trace
        {
            let directoryHex = ret.map{ $0.hexString }
            print("\(self): directory bytes \(directoryHex)")
        }
        outputBuffer = ret
        outputPosition = 0
    }

    override func close()
    {
        _isOpen = false
    }
}

extension FileEntry: CustomStringConvertible
{
    var description: String
    {
        let driveNumber = self.driveNumber != nil ? "\(self.driveNumber!)" : "-"
        let name = self.name ?? ""

        return "\(driveNumber): \(name), \(mode)"
    }
}

class DualDiskDrive :IEEEDevice
{
    fileprivate var diskettes:[Diskette?] = [ Diskette(folderPath: "0.diskette"),
    									  Diskette(folderPath: "1.diskette")]

    /// Command channel, the PET uses this to manage disks and get status info
    let commandChannelNumber = 15

    init()
    {
		openChannels = [Channel?](repeating: nil, count: maxFiles)
        super.init(deviceNumber: CommandByte(8))
    }

    let maxFiles = 16
    fileprivate var openChannels: [Channel?]

    fileprivate var receivingData = false

    var data: [S65Byte] = []

    var dataAsString: String
    {
        let myString = NSString(bytes: data, length: data.count, encoding: String.Encoding.isoLatin1.rawValue)
        guard let ret = myString else { fatalError("Failed to convert bytes to string") }
        return String(ret)
    }

    enum DriveMode
    {
        case quiescent
        case opening
        case saving
        case loading
    }

    fileprivate var driveMode = DriveMode.quiescent
    {
		didSet
        {
            if trace
            {
                print("\(self): driveMode \(oldValue) => \(driveMode)")
            }
        }
    }

    let writeLetter: S65Byte = "W".utf8.first!
    let  readLetter: S65Byte = "R".utf8.first!
    let   seqLetter: S65Byte = "S".utf8.first!
    let  progLetter: S65Byte = "P".utf8.first!

    ///
    /// Parse the drive number and the file name out of the data we have 
    /// received.
    /// The format is
    ///
    /// `N:XXXXXXXXXX[,W|R][,S|P]`
    ///
    /// where N is 0 or 1 and XXXXXXXX is the file name.
    /// - *W* means write
    /// - *R* means read (the default)
    /// - *S* means sequential
    /// - *P* means program (the default for new files)
    ///
    ///
    /// There seems to be mo mechanism for returning an error, so we ignore 
    /// invalid data.
    ///
    /// If the file is a program file, we do not know at this point if we will
    /// be reading or writing because the BASIC 4 disk command doesn't tell us
    /// until it starts sending or asking for data so we open for both read and
    /// write.
    ///
    /// - Parameter fileNumber: The open file number
    ///
    func fillInOpenDetails(_ channelNumber: Int) throws
    {
        guard data.count > 1 else { throw FileError.syntaxError }

        let driveNumber: Int
        let actualFileType: FileType
        let fileName: String
        var openMode: OpenMode

        if data[0] == S65Byte.dollar
        {
            guard data.count == 2 else { throw FileError.syntaxError }
            let driveIdentifier = data[1]
            driveNumber = Int(driveIdentifier - S65Byte.ascii0)
            if let dataAsString = data.asciiString
            {
                fileName = dataAsString
            }
            else
            {
                throw FileError.syntaxError
            }
            openMode = OpenMode.Read
            actualFileType = FileType.directory
        }
        else
        {
            let parsedData = try splitDataForCommand(data)
            if parsedData.count < 2
            {
                throw FileError.syntaxError
            }
            let driveIdentifier = parsedData[0]
            let fileNameBytes = parsedData[1]

            if driveIdentifier.count == 0
            {
                throw FileError.syntaxError
            }
            switch driveIdentifier[0]
            {
            case 0x30:
                driveNumber = 0
            case 0x31:
                driveNumber = 1
            default:
                throw FileError.syntaxError
            }
            openMode = OpenMode.Read
            var fileType: FileType?

            for i in 2 ..< parsedData.count
            {
                let byteSequence = parsedData[i]
                if byteSequence.count != 1
                {
                    throw FileError.syntaxError
                }
                switch byteSequence[0]
                {
                case writeLetter:
                    openMode = OpenMode.Write
                case readLetter:
                    openMode = OpenMode.Read
                case seqLetter:
                    fileType = FileType.sequential
                case progLetter:
                    fileType = FileType.program
                default:
                    throw FileError.syntaxError
                }
            }
            if let fileNameAscii = fileNameBytes.asciiString
            {
                fileName = fileNameAscii
            }
            else
            {
                throw FileError.syntaxError
            }

            if let diskette = diskettes[driveNumber]
            {
                if let existingFileType = diskette.fileTypeOf(fileName)
                {
                    // File exists, use the type on disk unless we have a type, in
                    // which case it must be the same.
                    if fileType != nil && existingFileType != fileType!
                    {
                        throw FileError.fileTypeMismatch("\(driveNumber):\(fileName)")
                    }
                    actualFileType = existingFileType
                }
                else
                {
                    actualFileType = fileType ?? FileType.program
                }
                if actualFileType == FileType.program
                {
                    openMode = OpenMode.ReadWrite
                }
            }
            else
            {
                throw FileError.noDiskInDrive(driveNumber)
            }
       }
        let fileInfo = try FileEntry.entryForType(actualFileType, driveController: self)

        fileInfo.driveNumber = driveNumber
        fileInfo.name = fileName
        openChannels[channelNumber] = fileInfo
        try fileInfo.open(openMode)

    }


    fileprivate func splitDataForCommand(_ inputData: [S65Byte]) throws -> [[S65Byte]]
    {
        var ret: [[S65Byte]] = []

		let driveAndCommands = inputData.splitAt(S65Byte.colon)

        ret.append(driveAndCommands[0])
        if driveAndCommands.count == 2
        {
			ret += driveAndCommands[1].splitAt(S65Byte.comma)
        }
        else
        {
            throw FileError.syntaxError
        }
        return ret
    }

    override func scgDeviceNumber(_ deviceNumber: Int, ieeeRole: IEEERole)
    {
        if ieeeRole == IEEERole.listener
        {
            startSaving(deviceNumber)
        }
        else
        {
            startLoading(deviceNumber)
        }
    }

    override func scgCloseDeviceNumber(_ deviceNumber: Int)
    {
        assert(deviceNumber < maxFiles, "Should not be possible for file number to be greater than 15")
        if let channel = openChannels[deviceNumber]
        {
            channel.close()
            openChannels[deviceNumber] = nil
        }
        driveMode = DriveMode.quiescent
    }

    override func scgOpenDeviceNumber(_ deviceNumber: Int)
    {
        assert(deviceNumber < maxFiles, "Should not be possible for file number to be greater than 15")
        driveMode = DriveMode.opening
    }

    override func receiveByte(_ byte: S65Byte, deviceNumber: Int)
    {
        data.append(byte)
    }

    ///
    /// This override is called when the talker has finished sending data to us.
    /// What we do with it depends on what state we are in.
    ///
    /// - If we were opening a file, this will be the drive name and name of the
    ///   file.
    /// - If we are saving, this will be file data, or, if the command channel,
    ///   a command to execute.
    ///
    /// The above are the only two options, so anything else is fatal and 
    /// requires the programmer to reassess his assumptions.
    ///
    /// - Parameter deviceNumber: The channel we have been accepting data for.
    ///
    override func endInputDataOnDevice(_ deviceNumber: Int)
    {
        switch driveMode
        {
        case .opening:
            if trace
            {
                print("Opening \(dataAsString)")
            }
            do
            {
                try fillInOpenDetails(deviceNumber)
            }
            catch let error as FileError
            {
                lastError = error
            }
            catch
            {
                fatalError("Unhandled error \(error)")
            }
            driveMode = DriveMode.quiescent
        case .saving:
            saveData(deviceNumber)
            driveMode = DriveMode.quiescent
        default:
            fatalError("Can't handle mode \(driveMode) yet")
        }
        if trace
        {
            print("Open file table")
            for fileInfo in openChannels
            {
                print("\(String(describing: fileInfo))")
            }
        }
        data = []
    }

    /// The last file system error we had
    var lastError: FileError?
    fileprivate var outputBuffer: [S65Byte]?
    fileprivate var outputPosition: Int = 0

    fileprivate func putLastErrorInOutputBuffer()
    {
        let result: String
        if let lastError = lastError
        {
            result = lastError.dosErrorString + "\r"
            self.lastError = nil
        }
        else
        {
            result = "0,\"00 OK\",0,0\r"
        }
        outputBuffer = [S65Byte](result.utf8)
        outputPosition = 0
    }


    ///
    /// Start saving a file.
    ///
    /// - Parameter fileNumber: The file to start saving to
    ///
    func startSaving(_ fileNumber: Int)
    {
        if fileNumber == commandChannelNumber && openChannels[commandChannelNumber] == nil
        {
			openChannels[commandChannelNumber] = self
        }
		if let fileInfo = openChannels[fileNumber]
        {
            do
            {
                if !fileInfo.isOpen
                {
                    try fileInfo.open(OpenMode.Write)
                }
                driveMode = DriveMode.saving
            }
            catch
            {
                if let error = error as? FileError
                {
					lastError = error
                }
                else
                {
                    fatalError("Unhandled error \(error)")
                }
            }
        }
    }


    var currentFile: Channel?
//    var outputBuffer: [S65Byte] = []
//    var outputBufferPosition = 0
    ///
    /// Start loading a file.
    ///
    /// - Parameter fileNumber: The file to start loading
    ///
    func startLoading(_ fileNumber: Int)
    {
        if let fileInfo = openChannels[fileNumber]
        {
            do
            {
                if !fileInfo.isOpen
                {
                    try fileInfo.open(OpenMode.Read)
                }
                currentFile = fileInfo
            }
            catch(IEEEError.notTalking)
            {
                fatalError("Trying to send data when not talking")
            }
            catch(IEEEError.notReadyToSend)
            {
                fatalError("Trying to send when we are not ready")
            }
            catch
            {
                if let error = error as? FileError
                {
                    lastError = error
                }
                else
                {
                    fatalError("Unhandled error \(error)")
                }
            }
        }
    }

    override func fetchByte()
    {
        guard let currentFile = currentFile else { return }
        do
        {
            if let byte = try currentFile.currentByte()
            {
                try startSendingByte(byte, isLastByte: currentFile.isLastByte)
                {
					success in
                    if success
                    {
                        do
                        {
                            try currentFile.nextByte()
                        }
                        catch let error as FileError
                        {
                            self.lastError = error
                        }
                        catch
                        {
                            fatalError("Unhandled error \(error)")
                        }
                    }
                }
            }
        }
        catch(IEEEError.notTalking)
        {
            fatalError("Trying to send data when not talking")
        }
        catch(IEEEError.notReadyToSend)
        {
            fatalError("Trying to send when we are not ready")
        }
        catch
        {
            // TODO: Fix zero length files
            fatalError("Something went wrong: \(error)")
        }
    }

    ///
    /// Save the data we have collected to the currently open file.
    ///
    /// As with most of the save stuff, there is no way to return an error to
    /// the computer, so if the save cannot be completed, we just return.
    ///
    /// - Parameter fileNumber: The file to save to.
    ///
    fileprivate func saveData(_ fileNumber: Int)
    {
        do
        {
            if let fileInfo = openChannels[fileNumber]
            {
                try fileInfo.save(data)
            }
            else
            {
                throw FileError.fileNotOpen("Unknown")
            }
        }
        catch
        {
            if let error = error as? FileError
            {
                lastError = error
            }
            else
            {
                fatalError("Unhandled error \(error)")
            }
        }
    }

    fileprivate func isOpen(_ fileName: String) -> Bool
    {
        var ret: Bool = false
        var i = openChannels.makeIterator()
        while let possibleChannel = i.next() , !ret
        {
			if let channel = possibleChannel,
               let fileEntry = channel as? FileEntry,
               let fileEntryName = fileEntry.name

            {
                ret = fileEntryName == fileName && fileEntry.isOpen
            }
        }
        return ret
    }
}

extension DualDiskDrive: Channel
{
    var mode: OpenMode
    {
        get { return OpenMode([.Read, .Write]) }
        set { /* Ignore set */ }
    }

    var isOpen: Bool { return true }

    func open(_ mode: OpenMode) throws
    {
    }

    func close()
    {
    }

    func save(_ data: [S65Byte]) throws
    {
		if let commandString = NSString(bytes: data, length: data.count, encoding: String.Encoding.isoLatin1.rawValue)
        {
            if trace
            {
                print("\(commandString)")
            }
            try runCommand(data)
        }
        else
        {
			throw FileError.syntaxError
        }
    }

    func currentByte() throws -> S65Byte?
    {
        var ret: S65Byte?

        if outputBuffer == nil
        {
            try nextByte()
        }
        guard let outputBuffer = outputBuffer else { fatalError("Failed to fetch outputBuffer") }
        if outputPosition < outputBuffer.count
        {
            ret = outputBuffer[outputPosition]
        }
        return ret
    }

    func nextByte() throws
    {
        if outputBuffer == nil
        {
            putLastErrorInOutputBuffer()
        }
        else
        {
            guard let outputBuffer = outputBuffer else { fatalError("Failed to fetch outputBuffer") }
            if outputPosition < outputBuffer.count
            {
                outputPosition += 1
            }
            else
            {
                self.outputBuffer = nil
            }
        }
    }

    var isLastByte: Bool { return outputBuffer == nil || outputPosition == outputBuffer!.count }
}

///
/// The possible different DOS commands
///
enum DOSCommand: S65Byte
{
    /// Copy file is "C"
    case copy = 0x43
    /// Duplicate disk is "D"
    case duplicate = 0x44
    /// Initialize disk is "I"
    case initialize = 0x49
    /// New disk is "N"
    case new = 0x4E
    /// Rename file is "R"
    case rename = 0x52
    /// Scratch file command is "S"
    case scratch = 0x53
    /// Validate is "V"
    case validate = 0x56
}

///
/// The DOS command interpreter
///
extension DualDiskDrive
{

    func runCommand(_ commandBytes: [S65Byte]) throws
    {
        guard commandBytes.count > 0 else { throw FileError.syntaxError }
        if let command = DOSCommand(rawValue: commandBytes[0])
        {
            switch command
            {
            case .scratch:
                let fileNameToScratch = Array(commandBytes[1 ..< commandBytes.count])
                try scratch(fileNameToScratch)
            default:
                fatalError("Command \(command) is unimplemented")
            }
        }
        else
        {
            throw FileError.unrecognisedCommand(commandBytes[0])
        }
    }

    fileprivate func scratch(_ fileName: [S65Byte]) throws
    {
		let components = fileName.splitAt(S65Byte.colon)
        guard components.count == 2 && components[0].count == 1
            else { throw FileError.syntaxError }
        let driveNumber = Int(components[0][0] - S65Byte.ascii0)
		guard driveNumber > 0 && driveNumber < diskettes.count
            else { throw FileError.syntaxError }
        guard let diskette = diskettes[driveNumber]
            else { throw FileError.noDiskInDrive(driveNumber) }
		guard let fileNamePattern = components[1].asciiString
            else { throw FileError.syntaxError }
        var scratchCount = 0
        for file in diskette.listFiles(fileNamePattern)
        {
            if !isOpen(file)
            {
                let scratched = try diskette.scratch(file)
                if scratched
                {
                    scratchCount += 1
                }
            }
        }
        lastError = FileError.scratched(scratchCount)
    }
}
