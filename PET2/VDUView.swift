//
//  VDUView.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/06/2015.
//
//

import Cocoa
import Swift6502

struct VDUGeometry
{
    let width: Int
    let height: Int

    /// The default geometry of the VSDU screen in characters
    static let defaultGeometry = VDUGeometry(width: 40, height: 25)
}

struct VDUAttribute: OptionSet
{
    let rawValue: Int

    static let Reverse = VDUAttribute(rawValue: 1)
}


/// The delegate is what actually does the drawing. This makes it easy to swap
/// in different methods of drawing e.g. using bitmaps for characters to get an 
/// authentic look.
protocol VDUViewDelegate
{

    /// The background colour of characters. This is usually black.
    var backgroundColour:  NSColor { get set }
    /// The size of a character on screen.
    var characterSize: NSSize { get }


    /// Draw the line of bytes at the given row of the display
    ///
    /// - Parameters:
    ///   - view: The view into which we will be drawing the row.
    ///   - row: The row at which to draw
    ///   - bytes: The bytes to draw
    @available(*, deprecated, message: "MetalVDUView does not call this")
    func draw(view: VDUView, row: Int, bytes: [S65Byte])

    /// Switches bewtween the graphics and lower case character sets.
    ///
    /// - Parameter lowerCase: If true, uses lower case, if false, uses graphics.
    func setCharacterSet(lowerCase: Bool)


    /// Get the character set in one go as a bit map. Thecharacters are arranged
    /// sequentially top row of bits to bottom row of bits
    var characterSetBitmap: [UInt8] { get }
}

@available(*, deprecated, message: "Replaced by MetalVDUView")
class VDUView: NSView
{
    internal let charDelegate: VDUViewDelegate = PETCharDelegate()

    @IBOutlet weak var controller: VDUController!

    override init(frame frameRect: NSRect)
    {
        super.init(frame: frameRect)
    }

    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ dirtyRect: NSRect)
    {
        charDelegate.backgroundColour.set()
        NSBezierPath.fill(dirtyRect)

        let startRow = Int(dirtyRect.origin.y / charDelegate.characterSize.height)
        let endRow = Int((dirtyRect.origin.y + dirtyRect.size.height) / charDelegate.characterSize.height)
        for i in startRow ... endRow
        {
            guard i < geometry.height else { break }

            let row = geometry.height - i - 1
            let theLine = controller.bytesFor(row: row)
            charDelegate.draw(view: self, row: row, bytes: theLine)
        }
    }

    override var isOpaque: Bool { return true }
    override var isFlipped: Bool { return true }

    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!

    var geometry: VDUGeometry = VDUGeometry.defaultGeometry
    {
		didSet
        {
            self.widthConstraint.constant = charDelegate.characterSize.width * CGFloat(geometry.width)
            self.heightConstraint.constant = charDelegate.characterSize.height * CGFloat(geometry.height)
            //self.superview?.layoutIfNeeded()
        }
    }

    override var intrinsicContentSize: NSSize
    {
		return NSSize(width: charDelegate.characterSize.width * CGFloat(geometry.width),
            		 height: charDelegate.characterSize.height * CGFloat(geometry.height))
    }
    
}
