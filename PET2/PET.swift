//
//  PET.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Foundation
import Swift6502

let programStart: S65Address = 0xFD16
let romImageLoad = "petrom-load"
let loadExtension = "6502load"

private class TraceControl: MemoryMappedDevice
{
    func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }

    var block: (S65Address, S65Byte, Bool) -> ()
    var portCount: Int

    fileprivate init(address: S65Address,
               portCount: Int,
                   block: @escaping (S65Address, S65Byte, Bool) -> ())
    {
        self.block = block
        baseAddress = address
        self.portCount = portCount
    }

    fileprivate var baseAddress: S65Address

    private var byte: S65Byte = 0

    fileprivate func willRead(_ portNumber: S65Address) -> S65Byte
    {
        block(portNumber, byte, true)
        return byte
    }

    fileprivate func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        self.byte = byte
        block(portNumber, byte, false)
    }
}

private class ReadTrace: MemoryMappedDevice
{
    private var theByte: S65Byte

    var block: (_ byte: S65Byte) -> ()
    var portCount: Int { return 1 }
    var previousDevice: MemoryMappedDevice?

    fileprivate init(_ address: S65Address, byte: S65Byte, block: @escaping (_ byte: S65Byte) -> ())
    {
        theByte = byte
        self.block = block
        baseAddress = address
    }

    fileprivate var baseAddress: S65Address

    fileprivate func willRead(_ portNumber: S65Address) -> S65Byte
    {
        if let previousDevice = previousDevice
        {
            theByte = previousDevice.willRead(baseAddress - previousDevice.baseAddress)
        }
        block(theByte)
        return theByte
    }

    fileprivate func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        theByte = byte
        if let previousDevice = previousDevice
        {
            previousDevice.didWrite(baseAddress - previousDevice.baseAddress, byte: byte)
        }
    }

    func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        assert(port == 0, "Read trace should only have one port")
        previousDevice = device
    }
}

@available(*, deprecated, message: "Does not maintain its own backing bytes")
private class WriteTrace: MemoryMappedDevice
{
    var block: (_ byte: S65Byte) -> ()
    var portCount: Int { return 1 }
    var previousDevice: MemoryMappedDevice?

    fileprivate init(_ address: S65Address, block: @escaping (_ byte: S65Byte) -> ())
    {
        self.block = block
        baseAddress = address
    }

    fileprivate var baseAddress: S65Address
    private var byte: S65Byte = 0

    fileprivate func willRead(_ portNumber: S65Address) -> S65Byte
    {
        if let previousDevice = previousDevice
        {
            return previousDevice.willRead(baseAddress - previousDevice.baseAddress)
        }
        else
        {
            return byte
        }
    }

    fileprivate func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        if let previousDevice = previousDevice
        {
            previousDevice.didWrite(baseAddress - previousDevice.baseAddress, byte: byte)
        }
        block(byte)
        self.byte = byte
    }

    func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        assert(port == 0, "Write trace should only have one port")
        previousDevice = device
    }
}


@available(*, deprecated, message: "Does not maintain its own backing bytes")
private class Write16BitTrace: MemoryMappedDevice
{
    func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }

    var portCount: Int { return 2 }
    let summary: String

    var value: S65Address = 0
    {
		didSet
        {
            if value != oldValue
            {
                print("$\(baseAddress.hexString): wrote $\(value.hexString), \(summary)")
            }
        }
    }

    fileprivate init(_ address: S65Address, summary: String)
    {
        baseAddress = address
        self.summary = summary
    }

    fileprivate var baseAddress: S65Address

    fileprivate func willRead(_ portNumber: S65Address) -> S65Byte
    {
        return portNumber == 0 ? value.lowByte : value.highByte
    }

    fileprivate func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
		if portNumber == 0
        {
            value.lowByte = byte
        }
        else
        {
            value.highByte = byte
        }
    }
}


private class TapeBufferTrace: MemoryMappedDevice
{
    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }

    static let bufferSize = 0xC0
    let lineCount = 16

    var baseAddress: S65Address
    var portCount: Int { return type(of: self).bufferSize }
    var bytes: [S65Byte] = [S65Byte](repeating: 0, count: bufferSize)

    fileprivate init(_ address: S65Address)
    {
        baseAddress = address
    }

    fileprivate func willRead(_ portNumber: S65Address) -> S65Byte
    {
        return bytes[Int(portNumber)]
    }

    fileprivate func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
		bytes[Int(portNumber)] = byte
        print("--- Tape buffer ($\(baseAddress.hexString)) ---")
        for (i, aByte) in bytes.enumerated()
        {
            let linePosition = i % lineCount
			if linePosition == 0
            {
                print("$\((S65Address(i) &+ baseAddress).hexString):", terminator: "")
            }
            print(" \(aByte.hexString)", terminator: "" )
            if (linePosition == lineCount - 1)
            {
                print("", terminator: "\n")
            }
        }
    }
}

/// This class represents a Commodore PET. It's an abstraction of the real thing
/// containing, as it does, objects that represent some of the real hardware.
/// Support for external peripherals is somewhat incomplete. The tape drive
/// interface is less reliable than the real thing and the disk drive (IEE 488)
/// interface seems a little buggy.
final class PET
{
    var cpu: Mos65xx = make6502()
    var pia1 = PIA6520()
    var pia2 = PIA6520()
    var via = VIA6522()
    /// IRQ line
    var irqAnd = AndGate()
    fileprivate var keyboard = PETKeyboard()
    fileprivate var tapeDeck = PETDatasetteDrive()
    var ieeeBus: IEEEBus
    fileprivate var diskDrive = DualDiskDrive()

    init(vduController: VDUController, tapeDriveController: TapeDriveController?) throws
    {
        ieeeBus = IEEEBus(clock: cpu)
        /*
		 *  Set the reset vector even though the ROMs do this.
		 */
        cpu.set(byte: programStart.lowByte , at: 0xFFFC)
        cpu.set(byte: programStart.highByte, at: 0xFFFD)
        try loadROMImages()
        /*
         *  Map the video RAM
		 */
        vduController.memoryDevice = cpu
        /*
		 *  Wire up the PIAs and VIA
		 */
        pia1.name = "PIA 1"
        pia2.name = "PIA 2"
		pia1.baseAddress = 0xE810
        pia2.baseAddress = 0xE820
        via.baseAddress  = 0xE840
        try cpu.mapDevice(pia1)
		try cpu.mapDevice(pia2)
        try cpu.mapDevice(via)
        cpu.addClockDriver(via)
        irqAnd.outputWire = cpu.irqWire
        irqAnd.addInputWire(pia1.irqB, withName: "pia1.irqB")
        irqAnd.addInputWire(pia1.irqA, withName: "pia1.irqA")
        /*
		 *  Wire up the keyboard
		 */
		pia1.portA.value = 0xff
        keyboard.lineSelect = pia1.portA
        pia1.portB.value = 0xff
        keyboard.selectedRow = pia1.portB
        /*
		 *  Set the diagnostic sense high
         */
        pia1.portA.wireForBit(7).value = true
        /*
		 *  Wire the VDU vsync output to the PIA
		 */
        vduController.vSyncWire = pia1.cb1
        vduController.keyboard = keyboard
        /*
         * Wire up character set control
		 */
        via.ca2.connectNotifier(vduController.setLowerCaseCharacters)
        /*
		 *  The tape drive
         */
        if let tapeDriveController = tapeDriveController
        {
            tapeDriveController.tapeDeck = tapeDeck
            tapeDeck.sense = pia1.portA.wireForBit(4)
            tapeDeck.motorControl = pia1.cb2
            tapeDeck.dataIn = via.portB.wireForBit(3)
            tapeDeck.dataOut = pia1.ca1
            tapeDeck.cpu = cpu
        }
		/*
         * IEEE bus
		 */
        /*
		 *  ATN in on PIA2 CA1 and out on VIA port B bit 2
		 */
        ieeeBus.atn.pipeToWire(pia2.ca1)
        via.portB.wireForBit(2).pipeToWire(ieeeBus.atn)
		/*
		 * DAV in on VIA port B, out on PIA2 CB2
		 */
        ieeeBus.dav.outputWire.pipeToWire(via.portB.wireForBit(7))
        ieeeBus.dav.addInputWire(pia2.cb2, withName: "pia2.cb2")
        /*
         * EOI in on PIA1 port A bit 6 and out on PIA1 A CA2
		 */
        ieeeBus.eoi.pipeToWire(pia1.portA.wireForBit(6))
        pia1.ca2.pipeToWire(ieeeBus.eoi)
        /*
		 *  NDAC in on VIA port B bit 0, out on PIA2 CA2
		 */
        ieeeBus.ndac.outputWire.pipeToWire(via.portB.wireForBit(0))
        ieeeBus.ndac.addInputWire(pia2.ca2, withName: "pia2.ca2")
        /*
		 *  NRFD in on VIA port B and out on VIA port B
		 */
        ieeeBus.nrfd.outputWire.pipeToWire(via.portB.wireForBit(6))
        ieeeBus.nrfd.addInputWire(via.portB.wireForBit(1), withName: "via.portB[1]")
        /*
         * SRQ in on PIA2 CB1
		 */
        ieeeBus.srq.pipeToWire(pia2.cb1)
        /*
		 *  Data in on PIA2 port A and out on PIA2 port B
         */
        ieeeBus.dio.connect(pia2.portA, mask: 0xff, notifier: { value in self.pia2.portA.value = value })
        pia2.portB.connect(ieeeBus.dio, mask: 0xff, notifier: { value in self.ieeeBus.dio.value = value })
        /*
		 * The disk drives
		 */
        diskDrive.bus = ieeeBus

        /*
		 *  Tracing
		 */
		ieeeBus.trace = false
        pia2.traceB = false
        diskDrive.trace = false
    }

    fileprivate func loadROMImages() throws
    {
        let thisBundle = Bundle(for: type(of: self))
        if let fileURL = thisBundle.url(forResource: romImageLoad, withExtension: loadExtension)
        {
            try cpu.loadImagesFromURL(fileURL)
        }
        else
        {
            throw Error6502.fileLoad(message: "No resource for \(romImageLoad).\(loadExtension)")
        }
    }

    func loadProgram(bytes: [S65Byte]) throws
    {
        guard bytes.count >= 2 else { throw PETError.invalidProgram }

        var loadAddress: S65Address = S65Address(bytes[0])
        loadAddress.highByte = bytes[1]
        cpu.set(bytes: bytes[2 ..< bytes.endIndex], at: loadAddress)
    }
}
