//
//  MetalVDUView.swift
//  PET2
//
//  Created by Jeremy Pereira on 22/09/2018.
//

import Cocoa
import MetalKit

class MetalVDUView: MTKView
{
    private let commandQueue: MTLCommandQueue
    private let computePipelineState: MTLComputePipelineState
    private var characterSetbuffer: MTLBuffer?
    private var screenBuffer: MTLBuffer?

    required init(coder: NSCoder)
    {
        guard let device = MTLCreateSystemDefaultDevice()
            else { fatalError("Cannot obtain the GPU device") }
        do
        {
            guard let commandQueue = device.makeCommandQueue()
                else { throw MetalVDUView.Error.unexpectedNil("command queue") }
            self.commandQueue = commandQueue
            computePipelineState = try MetalVDUView.registerShaders(device: device)
        }
        catch
        {
            fatalError("\(error)")
        }
        super.init(coder: coder)
        self.device = device
        framebufferOnly = false
    }

    override init(frame frameRect: CGRect, device: MTLDevice?)
    {
        let notNilDevice = device ?? MTLCreateSystemDefaultDevice()!
        do
        {
            guard let commandQueue = notNilDevice.makeCommandQueue()
                else { throw MetalVDUView.Error.unexpectedNil("command queue") }
            self.commandQueue = commandQueue
            computePipelineState = try MetalVDUView.registerShaders(device: notNilDevice)
        }
        catch
        {
            fatalError("\(error)")
        }
        super.init(frame: frameRect, device: notNilDevice)
        framebufferOnly = false
    }

    static func registerShaders(device: MTLDevice) throws -> MTLComputePipelineState
    {
        let library = device.makeDefaultLibrary()!
        guard let kernel = library.makeFunction(name: "petVDURender")
            else { throw MetalVDUView.Error.unexpectedNil("kernel") }

        return try device.makeComputePipelineState(function: kernel)
    }


    override func draw(_ dirtyRect: NSRect)
    {
        controller.vSync()
        super.draw(dirtyRect)

        guard let drawable = currentDrawable, let device = device
            else { return }
        do
        {
            guard let commandBuffer = commandQueue.makeCommandBuffer()
                else { throw MetalVDUView.Error.unexpectedNil("command buffer") }
            guard let commandEncoder = commandBuffer.makeComputeCommandEncoder()
                else { throw MetalVDUView.Error.unexpectedNil("command encoder") }
            commandEncoder.setComputePipelineState(computePipelineState)
            commandEncoder.setTexture(drawable.texture, index: 0)

            // We need to pass the bitmaps for the characters to display to
            // the shader
            if characterSetbuffer == nil
            {
                let characterBitmap = charDelegate.characterSetBitmap
                assert(characterBitmap.count == 256 * 8, "Don't think our character map is big enough")

                let bufferLength = characterBitmap.count * MemoryLayout<UInt8>.size

				guard let newBuffer = device.makeBuffer(length: bufferLength,
                                                        options: [])
                else { throw MetalVDUView.Error.unexpectedNil("command encoder") }
                memcpy(newBuffer.contents(), characterBitmap, bufferLength)
                characterSetbuffer = newBuffer
            }
			commandEncoder.setBuffer(characterSetbuffer, offset: 0, index: 1)

            // Now we need to copy the bytes into the screen buffer
            let bufferLength = geometry.height * geometry.width * MemoryLayout<UInt8>.size
            if screenBuffer == nil
            {
                guard let newBuffer = device.makeBuffer(length: bufferLength, options: [])
                    else { throw MetalVDUView.Error.unexpectedNil("command encoder") }
                screenBuffer = newBuffer
            }
            guard let screenBuffer = screenBuffer else { fatalError("Screen buffere cannot be nil here") }
            controller.copyScreen(to: screenBuffer.contents())
            commandEncoder.setBuffer(screenBuffer, offset: 0, index: 2)

            let w = computePipelineState.threadExecutionWidth
            let h = computePipelineState.maxTotalThreadsPerThreadgroup / w
            let threadsPerGroup = MTLSizeMake(w, h, 1)
            commandEncoder.dispatchThreads(MTLSizeMake(drawable.texture.width,
                                                       drawable.texture.height,
                                                       1),
                                           threadsPerThreadgroup: threadsPerGroup)
            commandEncoder.endEncoding()
            commandBuffer.present(currentDrawable!)
            commandBuffer.commit()
        }
        catch
        {
            print("\(error)")
        }
    }

    internal let charDelegate: VDUViewDelegate = PETCharDelegate()
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!

    @IBOutlet weak var controller: VDUController!


    /// The geometry of the view i.e. number of rows and columns
    var geometry: VDUGeometry = VDUGeometry.defaultGeometry
    {
        didSet
        {
            self.widthConstraint.constant = charDelegate.characterSize.width * CGFloat(geometry.width)
            self.heightConstraint.constant = charDelegate.characterSize.height * CGFloat(geometry.height)
        }
    }

    func setLowerCaseCharacters(_ lowerCase: Bool)
    {
        charDelegate.setCharacterSet(lowerCase: lowerCase)
        characterSetbuffer = nil
    }


}

extension MetalVDUView
{
    public enum Error: Swift.Error
    {
        case createVertexBufferFailed
        case unexpectedNil(String)
    }
}

