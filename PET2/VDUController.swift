//
//  VDUController.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/06/2015.
//
//

import Cocoa
import Swift6502

let reverseBit = 7

class VDUController: NSWindowController, MemoryMappedDevice
{
    public func pushDevice(_ device: MemoryMappedDevice, port: S65Address)
    {
        fatalError("\(self) does not support chaining devices")
    }

    @IBOutlet weak var vduView: MetalVDUView!
    fileprivate var viewNeedsDisplay = false

    override func windowDidLoad()
    {
        super.windowDidLoad()
    }

    var vSyncThread: Thread?

    @IBOutlet var myTouchbar: NSObject!

    @available(OSX 10.12.2, *)
    override func makeTouchBar() -> NSTouchBar?
    {
        let touchBar = myTouchbar
        // TODO: Make the touchbar customisable
//        touchBar.delegate = self
//        touchBar.customizationIdentifier = .candidateListBar
//        touchBar.defaultItemIdentifiers = [.candidateList, .otherItemsProxy]
//        touchBar.customizationAllowedItemIdentifiers = [.candidateList]

        return touchBar as? NSTouchBar
    }

    @IBAction func runStop(_ sender: AnyObject)
    {
        guard let sender = sender as? NSSegmentedControl
        else
        {
            fatalError("Sender must be a segmented control")
        }
        emulateKeyPress(key: .CtrlC, shifted: sender.selectedSegment == 0)
    }

    @IBAction func offRevs(_ sender: AnyObject)
    {
        guard let sender = sender as? NSSegmentedControl
            else
        {
            fatalError("Sender must be a segmented control")
        }
        emulateKeyPress(key: .CtrlR, shifted: sender.selectedSegment == 0)
    }
    
    @IBAction func clearHome(_ sender: AnyObject)
    {
        guard let sender = sender as? NSSegmentedControl
        else
        {
            fatalError("Sender must be a segmented control")
        }
        emulateKeyPress(key: .Home, shifted: sender.selectedSegment == 0)
    }

    private func emulateKeyPress(key: VirtualKeyCode, shifted: Bool = false)
    {
        guard let keyboard = keyboard else { return }
        if shifted
        {
			keyboard.setMatrixForKeyCode(.LeftShift)
        }
        keyboard.setMatrixForKeyCode(key)
        let delay: UInt64 = 100_000_000 // nonoseconds == 0.1 seconds
        let triggerTime = DispatchTime.init(uptimeNanoseconds: DispatchTime.now().uptimeNanoseconds + delay)

        DispatchQueue.main.asyncAfter(deadline: triggerTime)
        {
            keyboard.resetMatrixForKeyCode(key)
            if shifted
            {
				keyboard.resetMatrixForKeyCode(.LeftShift)
            }
        }
    }

    private var syncs = 0
    private var syncStartTime: DispatchTime = DispatchTime.now()

    func vSync()
    {
        if let vSyncWire = vSyncWire
        {
            vSyncWire.value = false
            vSyncWire.value = true
        }

        if syncs == 0
        {
            syncStartTime = DispatchTime.now()
        }
        syncs += 1
        if syncs % (60 * 30) == 0
        {
            let currentTime = DispatchTime.now()
            let nanoSeconds = currentTime.uptimeNanoseconds - syncStartTime.uptimeNanoseconds
            let frequency = (Double(syncs) * 1_000_000_000) / Double(nanoSeconds)
            print("Vertical sync frequency = \(frequency) Hz")
        }
    }

    var geometry: VDUGeometry
    {
        set
        {
            guard geometry.width == 40 || geometry.height == 25
                else { fatalError("Currently we only support the 40 x 25 sceen") }
            displayMap = [S65Byte](repeating: 0, count: newValue.width * newValue.height)
			vduView.geometry = newValue
        }

        get
        {
            return vduView.geometry
        }
    }

    var memoryDevice: DirectMemoryAccessible?

    /// The sequence of bytes mirroring the screen memory.
    internal private(set) var displayMap: [S65Byte] = []

    func copyScreen(to: UnsafeMutableRawPointer)
    {
        let bufferLength = geometry.height * geometry.width * MemoryLayout<UInt8>.size
        if let memoryDevice = memoryDevice
        {
			memoryDevice.copy(to: to, from: 0x8000, count: bufferLength)
        }
    }
    /// Return the bytes for a row
    ///
    /// - Parameter row: Row for wich to return the bytes
    /// - Returns: The row of bytes
    func bytesFor(row: Int) -> [S65Byte]
    {
        guard row < geometry.height else { return [0] }
        return Array(displayMap[(row * geometry.width) ..< ((row + 1) * geometry.width)])
    }

    // MARK: MemoryMappedDevice

    var portCount: Int
    {
		return geometry.height * geometry.width
    }

    var baseAddress: S65Address = S65Address(0)

    func didWrite(_ portNumber: S65Address, byte: S65Byte)
    {
        displayMap[Int(portNumber)] = byte
    }

    func willRead(_ portNumber: S65Address) -> S65Byte
    {
		return displayMap[Int(portNumber)]
    }

    var vSyncWire: Wire?

    var keyboard: PETKeyboard?

    override func keyDown(with theEvent: NSEvent)
    {
        if let keyboard = keyboard
        {
            keyboard.setKeyCode(theEvent.keyCode, modifierFlags: theEvent.modifierFlags)
        }
    }
    override func keyUp(with theEvent: NSEvent)
    {
        if let keyboard = keyboard
        {
            keyboard.resetKeyCode(theEvent.keyCode, modifierFlags: theEvent.modifierFlags)
        }
    }

    func setLowerCaseCharacters(_ lowerCase: Bool)
    {
        vduView.setLowerCaseCharacters(lowerCase)
        viewNeedsDisplay = true
    }
}
