//
//  OscilloscopeController.swift
//  M6502
//
//  Created by Jeremy Pereira on 26/08/2015.
//
//

import Cocoa
import Swift6502

class OscilloscopeController: NSWindowController
{
    @IBOutlet weak var stackView: NSStackView?
    @IBOutlet weak var startLabel: NSTextField?
    @IBOutlet weak var endLabel: NSTextField?

    var oscilloscope: Oscilloscope?
    {
		didSet
        {
            if let oscilloscope = oscilloscope
            {
                timeRange = Swift6502.Range(firstValue: oscilloscope.minTime)
                populateStackViewFrom(oscilloscope)
            }
            updateUI()
        }
    }

    var controllers: [OscilloscopeSourceController] = []

    fileprivate func populateStackViewFrom(_ oscilloscope: Oscilloscope)
    {
        guard let stackView = stackView else { return }
        controllers.removeAll()
        for source in oscilloscope.source
        {
            let thisController = OscilloscopeSourceController(source: source,
                                              oscilloscopeController: self)!
            controllers.append(thisController)
        }
        let views = controllers.map{ controller in return controller.view }
        for view in views
        {
            stackView.addView(view, in: NSStackView.Gravity.top)
            let constraint = NSLayoutConstraint(item: view,
                                           attribute: .right,
                                           relatedBy: .equal,
                							  toItem: stackView,
                                           attribute: .right,
                						  multiplier: 1,
                							constant: 0)
			stackView.addConstraint(constraint)
        }

    }

    override func windowDidLoad()
    {
        super.windowDidLoad()
        guard let oscilloscope = oscilloscope else { return }
        populateStackViewFrom(oscilloscope)
    }

    var timeRange: Swift6502.Range<Int> = Range(firstValue: 0)
    var zoomedIn = false

    func updateUI()
    {
        guard let oscilloscope = oscilloscope
            ,  oscilloscope.source.count > 0
            else { return }
        if !zoomedIn
        {
            timeRange.include(oscilloscope.time)
        }
        guard let startLabel = startLabel, let endLabel = endLabel
            else { return }
        startLabel.integerValue = timeRange.start
        endLabel.integerValue = timeRange.end
        for controller in controllers
        {
            controller.updateUI()
        }
     }

    func zoomToRange(_ zoomRange: Swift6502.Range<CGFloat>, baseLine: CGFloat)
    {
        let newStartOffset = CycleCount(zoomRange.start / baseLine * CGFloat(timeRange.width))
        let newEndOffset = CycleCount(zoomRange.end / baseLine * CGFloat(timeRange.width))

        var newTimeRange = Swift6502.Range(firstValue: timeRange.start + newStartOffset)
        newTimeRange.include(timeRange.start + newEndOffset)
        timeRange = newTimeRange
        zoomedIn = true
        updateUI()
    }

    @IBAction func zoomOut(_ sender: AnyObject)
    {
        zoomedIn = false
        guard let oscilloscope = oscilloscope else { return }
        timeRange.include(oscilloscope.minTime)
        updateUI()
    }
}
