//
//  HexFormatter.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa

class HexFormatter: Formatter {

}


class Hex16Formatter: Formatter
{
    override func string(for obj: Any?) -> String?
    {
        var ret = "??"
        if let obj = obj as? NSNumber
        {
			ret = obj.intValue.makeHexString(2)
        }
        return ret
    }
}


class Hex32Formatter: Formatter {

}
