//
//  PETKeyboard.swift
//  M6502
//
//  Created by Jeremy Pereira on 30/06/2015.
//
//

import Foundation
import Swift6502


/// Represents Mac OS key codes with some extras to fake PET keys that don't 
/// exist on the Mac.
struct VirtualKeyCode: OptionSet, Hashable
{
    let rawValue: UInt32

    var hashValue: Int { return Int(rawValue) }

    static let ModifierMask = VirtualKeyCode(rawValue: 0xFFF70000) // All modifiers except alt

    static let LineEnd   = VirtualKeyCode([.ModifierMask, VirtualKeyCode(rawValue: 1)])
    static let LeftArrow = VirtualKeyCode([.ModifierMask, VirtualKeyCode(rawValue: 2)])

    static let Shift  = VirtualKeyCode(rawValue: 0x00020000)
    static let Ctrl   = VirtualKeyCode(rawValue: 0x00040000)
    static let Fn     = VirtualKeyCode(rawValue: 0x00080000)
    static let Cursor = VirtualKeyCode(rawValue: 0x000a0000)
    static let Fake   = VirtualKeyCode(rawValue: 0x80000000)	// For virtual key codes that don't really exist

    static let A = VirtualKeyCode(rawValue: 0x0)
    static let S = VirtualKeyCode(rawValue: 0x1)
    static let D = VirtualKeyCode(rawValue: 0x2)
    static let F = VirtualKeyCode(rawValue: 0x3)
    static let H = VirtualKeyCode(rawValue: 0x4)
    static let G = VirtualKeyCode(rawValue: 0x5)
    static let Z = VirtualKeyCode(rawValue: 0x6)
    static let X = VirtualKeyCode(rawValue: 0x7)
    static let C = VirtualKeyCode(rawValue: 0x8)
    static let V = VirtualKeyCode(rawValue: 0x9)
    static let B = VirtualKeyCode(rawValue: 0xb)
    static let Q = VirtualKeyCode(rawValue: 0xc)
    static let W = VirtualKeyCode(rawValue: 0xd)
    static let E = VirtualKeyCode(rawValue: 0xe)
    static let R = VirtualKeyCode(rawValue: 0xf)
    static let Y = VirtualKeyCode(rawValue: 0x10)
    static let T = VirtualKeyCode(rawValue: 0x11)
    static let _1 = VirtualKeyCode(rawValue: 0x12)
    static let _2 = VirtualKeyCode(rawValue: 0x13)
    static let _3 = VirtualKeyCode(rawValue: 0x14)
    static let _4 = VirtualKeyCode(rawValue: 0x15)
    static let _6 = VirtualKeyCode(rawValue: 0x16)
    static let _5 = VirtualKeyCode(rawValue: 0x17)
    static let Equals = VirtualKeyCode(rawValue: 0x18)
    static let _9 = VirtualKeyCode(rawValue: 0x19)
    static let _7 = VirtualKeyCode(rawValue: 0x1a)
    static let Minus = VirtualKeyCode(rawValue: 0x1b)
    static let _8 = VirtualKeyCode(rawValue: 0x1c)
    static let _0 = VirtualKeyCode(rawValue: 0x1d)
    static let RSquare = VirtualKeyCode(rawValue: 0x1e)
    static let O = VirtualKeyCode(rawValue: 0x1f)
    static let U = VirtualKeyCode(rawValue: 0x20)
    static let LSquare = VirtualKeyCode(rawValue: 0x21)
    static let I = VirtualKeyCode(rawValue: 0x22)
    static let P = VirtualKeyCode(rawValue: 0x23)
    static let Return = VirtualKeyCode(rawValue: 0x24)
    static let L = VirtualKeyCode(rawValue: 0x25)
    static let J = VirtualKeyCode(rawValue: 0x26)
    static let Quote = VirtualKeyCode(rawValue: 0x27)
    static let K = VirtualKeyCode(rawValue: 0x28)
    static let SemiColon  = VirtualKeyCode(rawValue: 0x29)
    static let BackSlash  = VirtualKeyCode(rawValue: 0x2a)
    static let Comma      = VirtualKeyCode(rawValue: 0x2b)
    static let Slash      = VirtualKeyCode(rawValue: 0x2c)
    static let N          = VirtualKeyCode(rawValue: 0x2d)
    static let M          = VirtualKeyCode(rawValue: 0x2e)
    static let Dot        = VirtualKeyCode(rawValue: 0x2f)
    static let Space      = VirtualKeyCode(rawValue: 0x31)
    static let BackSpace  = VirtualKeyCode(rawValue: 0x33)
    static let CursorLeft = VirtualKeyCode(rawValue: 0x7b)
    static let CursorDown = VirtualKeyCode(rawValue: 0x7d)

    static let DoubleQuote = VirtualKeyCode([.Shift, .Quote])
    static let Exclamation = VirtualKeyCode([.Shift, ._1])
    static let At 		   = VirtualKeyCode([.Shift, ._2])
    static let Hash        = VirtualKeyCode([.Shift, ._3])
    static let Dollar      = VirtualKeyCode([.Shift, ._4])
    static let Percent     = VirtualKeyCode([.Shift, ._5])
    static let UpArrow     = VirtualKeyCode([.Shift, ._6])
    static let Ampersand   = VirtualKeyCode([.Shift, ._7])
    static let Asterisk    = VirtualKeyCode([.Shift, ._8])
    static let LPar        = VirtualKeyCode([.Shift, ._9])
    static let RPar        = VirtualKeyCode([.Shift, ._0])

    static let QuestionMark   = VirtualKeyCode([.Shift, .Slash])
    static let Plus           = VirtualKeyCode([.Shift, .Equals])
    static let Colon          = VirtualKeyCode([.Shift, .SemiColon])
    static let LAngled        = VirtualKeyCode([.Shift, .Comma])
    static let RAngled        = VirtualKeyCode([.Shift, .Dot])

    static let Home         = VirtualKeyCode([.Fn, VirtualKeyCode(rawValue: 0x73)])
    static let Del          = VirtualKeyCode([.Fn, VirtualKeyCode(rawValue: 0x75)])
    static let CtrlR        = VirtualKeyCode([.Ctrl, .R])	// Reverse
    static let CtrlC        = VirtualKeyCode([.Ctrl, .C])	// Stop
    static let CursorRight  = VirtualKeyCode([.Cursor, VirtualKeyCode(rawValue: 0x7c)])

    static let LeftShift = VirtualKeyCode([.Fake, VirtualKeyCode(rawValue: 0x1)])
    static let RightShift = VirtualKeyCode([.Fake, VirtualKeyCode(rawValue: 0x2)])


    static func fromEventModifiers(_ modifiers: NSEvent.ModifierFlags) -> VirtualKeyCode
    {
        return VirtualKeyCode(rawValue: UInt32(modifiers.rawValue)).intersection(.ModifierMask)
    }

    var hexString: String
    {
        return Int(rawValue).makeHexString(8)
    }
}

private let leftAltMask  = NSEvent.ModifierFlags(rawValue: 0x00080020)
private let rightAltMask = NSEvent.ModifierFlags(rawValue: 0x00080040)


private struct MatrixRow: OptionSet
{
    let rawValue: S65Byte

    var outputByte: S65Byte { return ~rawValue }

    static func bit(_ n: Int) -> MatrixRow
    {
        return MatrixRow(rawValue: S65Byte(1 << n))
	}
}

private struct KeyMapping
{
    let row: Int
    let col: Int
}


/// Reresents a PET keyboard. 
class PETKeyboard
{
    fileprivate var matrix: [MatrixRow] = [MatrixRow](repeating: MatrixRow([]), count: 10)
    fileprivate let keyMap: [ VirtualKeyCode : KeyMapping] =
    [
        .Exclamation : KeyMapping(row: 0, col: 0),
               .Hash : KeyMapping(row: 0, col: 1),
            .Percent : KeyMapping(row: 0, col: 2),
          .Ampersand : KeyMapping(row: 0, col: 3),
               .LPar : KeyMapping(row: 0, col: 4),
          .LeftArrow : KeyMapping(row: 0, col: 5),
               .Home : KeyMapping(row: 0, col: 6),
        .CursorRight : KeyMapping(row: 0, col: 7),

        .DoubleQuote : KeyMapping(row: 1, col: 0),
             .Dollar : KeyMapping(row: 1, col: 1),
              .Quote : KeyMapping(row: 1, col: 2),
          .BackSlash : KeyMapping(row: 1, col: 3),
               .RPar : KeyMapping(row: 1, col: 4),
            .LineEnd : KeyMapping(row: 1, col: 5),
         .CursorDown : KeyMapping(row: 1, col: 6),
          .BackSpace : KeyMapping(row: 1, col: 7),

              .Q : KeyMapping(row: 2, col: 0),
              .E : KeyMapping(row: 2, col: 1),
              .T : KeyMapping(row: 2, col: 2),
              .U : KeyMapping(row: 2, col: 3),
              .O : KeyMapping(row: 2, col: 4),
        .UpArrow : KeyMapping(row: 2, col: 5),
             ._7 : KeyMapping(row: 2, col: 6),
             ._9 : KeyMapping(row: 2, col: 7),

        .W 		   : KeyMapping(row: 3, col: 0),
        .R 		   : KeyMapping(row: 3, col: 1),
        .Y 		   : KeyMapping(row: 3, col: 2),
        .I 		   : KeyMapping(row: 3, col: 3),
        .P 		   : KeyMapping(row: 3, col: 4),
        ._8 		   : KeyMapping(row: 3, col: 6),
        .Slash 	   : KeyMapping(row: 3, col: 7),

        .A 		   : KeyMapping(row: 4, col: 0),
        .D 		   : KeyMapping(row: 4, col: 1),
        .G 		   : KeyMapping(row: 4, col: 2),
        .J 		   : KeyMapping(row: 4, col: 3),
        .L 		   : KeyMapping(row: 4, col: 4),
        ._4 		   : KeyMapping(row: 4, col: 6),
        ._6 		   : KeyMapping(row: 4, col: 7),

        .S 		   : KeyMapping(row: 5, col: 0),
        .F 		   : KeyMapping(row: 5, col: 1),
        .H 		   : KeyMapping(row: 5, col: 2),
        .K 		   : KeyMapping(row: 5, col: 3),
        .Colon 	   : KeyMapping(row: 5, col: 4),
        ._5 		   : KeyMapping(row: 5, col: 6),
        .Asterisk 	   : KeyMapping(row: 5, col: 7),

        .Z 		   : KeyMapping(row: 6, col: 0),
        .C 		   : KeyMapping(row: 6, col: 1),
        .B 		   : KeyMapping(row: 6, col: 2),
        .M 		   : KeyMapping(row: 6, col: 3),
        .SemiColon   : KeyMapping(row: 6, col: 4),
        .Return 	   : KeyMapping(row: 6, col: 5),
        ._1 		   : KeyMapping(row: 6, col: 6),
        ._3 	   	   : KeyMapping(row: 6, col: 7),

        .X 		   : KeyMapping(row: 7, col: 0),
        .V 		   : KeyMapping(row: 7, col: 1),
        .N 		   : KeyMapping(row: 7, col: 2),
        .Comma 	   : KeyMapping(row: 7, col: 3),
        .QuestionMark: KeyMapping(row: 7, col: 4),
        ._2            : KeyMapping(row: 7, col: 6),
        .Plus 	       : KeyMapping(row: 7, col: 7),

        .LeftShift   : KeyMapping(row: 8, col: 0),
        .At 		   : KeyMapping(row: 8, col: 1),
        .RSquare 	   : KeyMapping(row: 8, col: 2),
        .RAngled      : KeyMapping(row: 8, col: 4),
        .RightShift  : KeyMapping(row: 8, col: 5),
        ._0 		   : KeyMapping(row: 8, col: 6),
        .Minus 	   : KeyMapping(row: 8, col: 7),

        .CtrlR 	   	: KeyMapping(row: 9, col: 0),
        .LSquare 	: KeyMapping(row: 9, col: 1),
        .Space 	   	: KeyMapping(row: 9, col: 2),
        .LAngled    : KeyMapping(row: 9, col: 3),
        .CtrlC	 	: KeyMapping(row: 9, col: 4),
        .Dot        : KeyMapping(row: 9, col: 6),
        .Equals 	: KeyMapping(row: 9, col: 7)
   ]
    var lineSelect: Bus?
    {
		didSet
        {
            if let lineSelect = lineSelect
            {
				lineSelect.connect(self, mask: 0xf, notifier: selectLine)
            }
        }
    }

    var selectedRow: Bus?
    {
		didSet
        {
            if let selectedRow = selectedRow
            {
                selectedRow.value = 0xff
            }
        }
    }

    fileprivate func selectLine(_ newByte: S65Byte)
    {
        if let selectedRow = selectedRow
        {
            let selectedLineNumber = Int(newByte & 0xf)
            if selectedLineNumber < matrix.count
            {
                selectedRow.value = matrix[selectedLineNumber].outputByte
            }
            else
            {
                print("Invalid row select byte was $\(selectedLineNumber.makeHexString(2))")
            }
        }
    }

    func setKeyCode(_ newKeyCode: UInt16, modifierFlags: NSEvent.ModifierFlags)
    {
        let modifiers = VirtualKeyCode.fromEventModifiers(modifierFlags)
		let compositeKeyCode = VirtualKeyCode([ VirtualKeyCode(rawValue: UInt32(newKeyCode)),
                                                modifiers])
		setMatrixForKeyCode(compositeKeyCode)
        // Fake shift key presses from alts
        if modifierFlags.contains(leftAltMask)
        {
            setMatrixForKeyCode(.LeftShift)
        }
        else if modifierFlags.contains(rightAltMask)
        {
            setMatrixForKeyCode(.RightShift)
        }
    }

    func resetKeyCode(_ newKeyCode: UInt16, modifierFlags: NSEvent.ModifierFlags)
    {
        let modifiers = VirtualKeyCode.fromEventModifiers(modifierFlags)
        let compositeKeyCode = VirtualKeyCode([ VirtualKeyCode(rawValue: UInt32(newKeyCode)),
            								    modifiers])
        resetMatrixForKeyCode(compositeKeyCode)
        // Fake shift key releases from alts
        if modifierFlags.contains(leftAltMask)
        {
            resetMatrixForKeyCode(.LeftShift)
        }
        else if modifierFlags.contains(rightAltMask)
        {
            resetMatrixForKeyCode(.RightShift)
        }
    }

    func setMatrixForKeyCode(_ keyCode: VirtualKeyCode)
    {
		if let keyMapping = keyMap[keyCode]
        {
			matrix[keyMapping.row].insert(MatrixRow.bit(keyMapping.col))
        }
        else
        {
            print("\(self): set unmapped key code: \(keyCode.hexString)")
        }
    }

    func resetMatrixForKeyCode(_ keyCode: VirtualKeyCode)
    {
        if let keyMapping = keyMap[keyCode]
        {
            matrix[keyMapping.row].remove(MatrixRow.bit(keyMapping.col))
        }
        else
        {
            print("\(self): reset unmapped key code: \(keyCode.hexString)")
        }
    }
}
