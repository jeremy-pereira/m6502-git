//
//  IEEEDevice.swift
//  M6502
//
//  Created by Jeremy Pereira on 29/07/2015.
//
//

import Foundation
import Swift6502

///
/// When we are sending bytes we can raise errors potentially
///
enum IEEEError: Error
{
    /// Tryig to send a byte when we are not the talkier.
    case notTalking
    /// Trying to send a byte when the previous byte has not already been sent
    case notReadyToSend
}

///
/// Structure of an IEEE 488 command byte
///
/// The top three bits represent a command and the bottom five bits represent
/// the device number, except for command 0 which has some special values.
///
/// There is a possible 31 devices with bit pattern `0b11111` representing "all
/// devices"
///
struct CommandByte: OptionSet
{
    /// A command byte is a byte
    let rawValue: S65Byte

    /// Initialise the command byte
    /// 
    /// - Parameter rawValue: The raw value of this command byte
    init(rawValue: S65Byte)
    {
        self.rawValue = rawValue
    }

    ///
    /// A convenience initialiser so we don't have to keep typing the rawValue
    /// name.
    ///
    /// - Parameter rawValue: The raw value of this command byte
    ///
    init(_ rawValue: S65Byte)
    {
        self.init(rawValue: rawValue)
    }

    /// Mask for the command bits
    static let CommandBits = CommandByte(0b11100000)
    /// Mask for the device number
    static let DeviceNumberBits = CommandByte(~0b11100000)
    /// Listen address group
    static let LAG = CommandByte(0b00100000)
    /// Talk address group
    static let TAG = CommandByte(0b01000000)
    /// Secondary command address group
    static let SCG = CommandByte(0b01100000)
    /// Secondary address for close and open
    static let SCGCloseOpen = CommandByte(0b11100000)

    static let OpenAddress = CommandByte(0b00010000)

    /// Broadcast device number
    static let AllDevices = CommandByte(0b00011111)
}

///
/// Base class for any IEEE device
///
class IEEEDevice
{
    var trace = true

    fileprivate let deviceNumber: CommandByte

    fileprivate var initialised = false

    init(deviceNumber: CommandByte)
    {
		self.deviceNumber = deviceNumber


        initialised = true
    }

    /// The IEEE488 bus which connects the drives to a computer
    var bus: IEEEBus?
    {
        didSet
        {
            if let bus = bus
            {
                connectBus(bus)
            }
        }
    }

    fileprivate var myNrfd = Wire()
    fileprivate var myNdac = Wire()
    fileprivate var myDav = Wire()

    fileprivate func connectBus(_ newBus: IEEEBus)
    {
        initialised = false
        defer { initialised = true }

        newBus.nrfd.addInputWire(myNrfd, withName: "\(self).nrfd")
        newBus.dav.outputWire.connectNotifier(davNotifier)
        newBus.dav.addInputWire(myDav, withName: "\(self).dav")
        newBus.ndac.addInputWire(myNdac, withName: "\(self).ndac")
        newBus.nrfd.outputWire.connectNotifier(nrfdNotifier)
        newBus.ndac.outputWire.connectNotifier(ndacNotifier)
        newBus.atn.connectNotifier(atnNotifier)
        myNrfd.value = true
        myNdac.value = true
		myDav.value = true

    }

    ///
    /// The IEEE mode of this device.
    ///
    /// We are either talking, listening or doing nothing.
    ///
    fileprivate enum IEEEMode
    {
        case quiescent
        case talking
        case listening
    }

    /// The role in IEEE terms of his device. We are either the talker, the
    /// listener or none of the above.  This differs from the mode in that it is
    /// set only by IEEE command bytes and can be overridden by ATN
    enum IEEERole
    {
        /// This device is the talker and should be sending bytes on the bus.
        case talker
        /// This device is one of the listeners and should be receiving bytes.
        case listener
        /// This device is neither talker nor listener and is taking no part in
        /// the hand shaking.
        case none
    }

    fileprivate var isCommandByte: Bool { return !bus!.atn.value }

    fileprivate var ieeeMode = IEEEMode.quiescent
    {
		didSet
        {
            if oldValue == .talking && ieeeMode != .talking
            {
                myDav.value = true	// Make sure that DAV isn't low.
                if byteBeingSent != nil
                {
                    byteBeingSent = nil
                    invokeSendCompletionHandler(false)
                }
            }
            switch ieeeMode
            {
            case .quiescent:
                myNdac.value = true
                myNrfd.value = true
            case .talking:
                assert(bus!.atn.value, "Must not talk when ATN is high")
                byteBeingSent = nil
                myNdac.value = true
                myNrfd.value = true
            case .listening:
                myNdac.value = false
                myNrfd.value = true
            }
        }
    }
    fileprivate var ieeeRole: IEEERole = .none

    fileprivate var secondaryAddress: Int = 0

    func davNotifier(_ value: Bool)
    {
        guard initialised else { return }
        guard isCommandByte || ieeeMode == .listening else { return }

        if value
        {
            myNdac.value = false
        }
        else
        {
            myNrfd.value = false
            let theByte = ~bus!.dio.value
            myNdac.value = true
            if isCommandByte
            {
                processCommand(theByte)
            }
            else
            {
                processInputData(theByte)
            }
            myNrfd.value = true
        }
    }

    fileprivate func atnNotifier(_ value: Bool)
    {
        if value
        {
            if trace
            {
                print("\(self): atn high, role is \(ieeeRole)")
            }
            switch ieeeRole
            {
            case .talker:
                ieeeMode = .talking
            case .listener:
                ieeeMode = .listening
            case .none:
                ieeeMode = .quiescent
            }
        }
        else
        {
            ieeeMode = .listening
        }
    }

    fileprivate func nrfdNotifier(_ value: Bool)
    {
        if value && ieeeMode == .talking
        {
            fetchByte()
        }
    }

    fileprivate func ndacNotifier(_ value: Bool)
    {
        if value && ieeeMode == .talking && byteBeingSent != nil
        {
            byteBeingSent = nil
            bus!.dio.value = 0xff		// Clear the data bus
            let isLastByte = !bus!.eoi.value
            if isLastByte
            {
                bus!.eoi.value = true	// Because logic is reversed on IEEE
            }
            if bus!.nrfd.value		// Both nrfd and ndac are high meaning no device
            {
                ieeeMode = .quiescent
            }
            invokeSendCompletionHandler(!bus!.nrfd.value)
            myDav.value = true
        }
    }

    func processCommand(_ byte: S65Byte)
    {
        assert(!bus!.atn.value, "Commands are only issued when ATN is low")
        let commandByte = CommandByte(rawValue: byte)
        let command = commandByte.intersection(CommandByte.CommandBits)
        switch(command)
        {
        case CommandByte.TAG:
            let commandDevice = commandByte.intersection(CommandByte.DeviceNumberBits)
            if commandDevice == self.deviceNumber
            {
                ieeeRole = .talker
                if trace
                {
                    print("\(self): TAG $\(commandDevice.rawValue.hexString) Role is \(ieeeRole)")
                }
            }
            else if commandDevice == CommandByte.AllDevices && ieeeMode == .talking
            {
                ieeeRole = .none
                if trace
                {
                    print("\(self): UNT Role is \(ieeeRole)")
                }
            }
        case CommandByte.LAG:
            let commandDevice = commandByte.intersection(CommandByte.DeviceNumberBits)
            if commandDevice == self.deviceNumber
            {
                ieeeRole = .listener
                if trace
                {
                    print("\(self): LAG $\(commandDevice.rawValue.hexString) Role is \(ieeeRole)")
                }
            }
            else if commandDevice == CommandByte.AllDevices && ieeeMode == .listening
            {
                ieeeRole = .none
                if trace
                {
                    print("\(self): UNL Role is \(ieeeRole)")
                }

            }
        case CommandByte.SCG:
            let commandDevice = commandByte.intersection(CommandByte.DeviceNumberBits)
            secondaryAddress = Int(commandDevice.rawValue)
            if trace
            {
                print ("SCG $\(commandDevice.rawValue.hexString)")
            }
            scgDeviceNumber(Int(secondaryAddress), ieeeRole: ieeeRole)
        case CommandByte.SCGCloseOpen:
            let deviceNumber = commandByte.intersection(CommandByte.DeviceNumberBits)
            if deviceNumber.contains(CommandByte.OpenAddress)
            {
                secondaryAddress = Int(deviceNumber.rawValue - 16)
                if trace
                {
                    print ("SCG open \(secondaryAddress)")
                }
                scgOpenDeviceNumber(secondaryAddress)
            }
            else
            {
                secondaryAddress = Int(deviceNumber.rawValue)
                if trace
                {
                    print ("SCG close \(secondaryAddress)")
                }
                scgCloseDeviceNumber(secondaryAddress)
            }
        default:
            if initialised
            {
                fatalError("Unimplemented IEEE command $\(byte.hexString)")
            }
        }
    }

    func processInputData(_ byte: S65Byte)
    {
        if trace
        {
            print("\(self) received byte \(byte.hexString)")
        }
        receiveByte(byte, deviceNumber: secondaryAddress)
        if !bus!.eoi.value
        {
            endInputDataOnDevice(secondaryAddress)
        }
    }

    ///
    /// Called when we receive an SCG command
    ///
    /// - Parameter deviceNumber: The secondary device the SCG was called on
    /// - Parameter ieeeRole: Are we the talkier or listener
    ///
    func scgDeviceNumber(_ deviceNumber: Int, ieeeRole: IEEERole)
    {
        fatalError("SCG not implemented for this device")
    }

    ///
    /// Called when we receive an SCG close command
    ///
    /// - Parameter deviceNumber: The secondary device the SCG was called on
    ///
    func scgCloseDeviceNumber(_ deviceNumber: Int)
    {
        fatalError("SCG not implemented for this device")
    }

    ///
    /// Called when we receive an SCG open command
    ///
    /// - Parameter deviceNumber: The secondary device the SCG was called on
    ///
    func scgOpenDeviceNumber(_ deviceNumber: Int)
    {
        fatalError("SCG not implemented for this device")
    }

    ///
    /// Called when we receive a byte of data
    ///
    /// - Parameter byte: The byte we received.
    /// - Parameter deviceNumber: The secondary device the SCG was called on
    ///
    func receiveByte(_ byte: S65Byte, deviceNumber: Int)
    {
        fatalError("receiveByte not implemented for this device")
    }

    ///
    /// Called when the talker signals end of input
    ///
    /// - Parameter deviceNumber: The secondary device the SCG was called on
    ///
    func endInputDataOnDevice(_ deviceNumber: Int)
    {
        fatalError("endInputDataOnDevice not implemented for this device")
    }

    fileprivate var byteBeingSent: S65Byte?
    fileprivate var sendCompletionHandler: ((Bool) -> Void)?

    fileprivate func invokeSendCompletionHandler(_ status: Bool)
    {
        if let sendCompletionHandler = sendCompletionHandler
        {
            sendCompletionHandler(status)
        }
    }

    ///
    /// Start sending a byte on the interface
    ///
    /// - Parameter byte: The byte to send
    /// - Parameter isLastByte: true if this is the last byte of data, falsie if not
    /// - Parameter onComplete: Completion handler takes a parameter that
    ///             indicates whether or not the byte was sent successfully.
    /// - Throws: if the device is not the bus talker.
    ///
    func startSendingByte(_ byte: S65Byte,
        			isLastByte: Bool,
        			onComplete: @escaping ((Bool) -> Void)) throws
    {
        guard ieeeMode == .talking else { throw IEEEError.notTalking }
        guard byteBeingSent == nil else { throw IEEEError.notReadyToSend }

		print("\(self): Sending byte $\(byte.hexString), isLast? \(isLastByte)")

        bus!.eoi.value = !isLastByte	// Because logic is reverse on IEEE
        byteBeingSent = byte
        sendCompletionHandler = onComplete
        /*
         *  We can't actually send the byte if somebody else is using the bus.
         *  So we will wait if until everybody is ready for data.
		 */
        if bus!.nrfd.outputWire.value
        {
			putByteBeingSentOnBus()
        }
    }

    fileprivate func putByteBeingSentOnBus()
    {
        if bus!.nrfd.value && bus!.ndac.value
        {
            // There arre apparently no other devices on the bus
            ieeeMode = .quiescent
        }
        else
        {
            bus!.dio.value = ~byteBeingSent!
            myDav.value = false
        }
    }

    func fetchByte()
    {
        fatalError("\(self): does not override fetchByte()")
    }
}
