//
//  FPWindowController.swift
//  M6502
//
//  Created by Jeremy Pereira on 18/12/2016.
//
//

import Cocoa
import M65xxUI

private let pet40ColumnGeometry = VDUGeometry(width: 40, height: 25)

class FPWindowController: NSWindowController
{
    private var thePet: PET!
    private var cpuController: CPUController?
    private var frontPanelControllers: [OperationUpdatable] = []
    private var vduController: VDUController!
    private var oscilloscopeController: OscilloscopeController? = nil


    @IBOutlet var frontPanel: FrontPanelView!
    @IBOutlet var myTouchbar: NSObject!

    override func windowDidLoad()
    {
        super.windowDidLoad()

        guard let window = self.window
        else
        {
            return
        }
        vduController = VDUController(windowNibName: "VDUController")
        _ = vduController.window
        
        buildPet()

        let views = frontPanelControllers.map { controller in return controller.view }
        frontPanel.views = views
        let contentRect = NSRect(origin: window.frame.origin, size: self.frontPanel.frame.size)
        let frameRect = window.frameRect(forContentRect: contentRect)
		window.setFrame(frameRect, display: true)
        vduController.showWindow(self)
        window.makeKeyAndOrderFront(self)
    }

    @available(OSX 10.12.2, *)
    override func makeTouchBar() -> NSTouchBar?
    {
        let touchBar = myTouchbar
        // TODO: Make the touchbar customisable
        //        touchBar.delegate = self
        //        touchBar.customizationIdentifier = .candidateListBar
        //        touchBar.defaultItemIdentifiers = [.candidateList, .otherItemsProxy]
        //        touchBar.customizationAllowedItemIdentifiers = [.candidateList]

        return touchBar as? NSTouchBar
    }

    private func buildPet()
    {
        vduController.geometry = pet40ColumnGeometry
        do
        {
            thePet = try PET(vduController: vduController, tapeDriveController: nil)
        }
        catch
        {
            // TODO: Should present an error to the user
            fatalError("Failed to build the PET, reason: \(error)")
        }
        cpuController = CPUController(cpu: thePet.cpu, uberController: self)
        let ieeeController = IEEEBusViewController(ieeeBus: thePet.ieeeBus)
        frontPanelControllers.append(ieeeController)
        frontPanelControllers.append(cpuController!)
    }
    

    @IBAction func startCPU(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.startCPU(sender)
        }
    }

    @IBAction func stopCPU(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.stopCPU(sender)
        }
    }

    @IBAction func goAtFullSpeed(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.goAtFullSpeed(sender)
        }
    }

    @IBAction func goAtFullUpdates(_ sender: AnyObject)
    {
        if let cpuController = cpuController
        {
            cpuController.goAtFullSpeed(sender)
        }
    }

    func load(program: [UInt8]) throws
    {

        cpuController?.stopCPU
        {
            [weak self] wasRunning in
            guard let controller = self else { return }
            do
            {
                try controller.thePet.loadProgram(bytes: program)
            }
            catch
            {
				controller.frontPanel.presentError(error)
            }
            if wasRunning
            {
                // We are still in the middle of stopping, so start again by 
                // dispatching to the main queue.
                DispatchQueue.main.async
                {
                    controller.cpuController?.startCPU()
                }
            }
        }
    }
}

extension FPWindowController: OperationUpdatable
{
    func update(operation: ExecutionOperation)
    {
        DispatchQueue.main.async
        {
            for controller in self.frontPanelControllers
            {
                controller.update(operation: operation)
            }
            if let oscilloscopeController = self.oscilloscopeController
            {
                oscilloscopeController.updateUI()
            }
        }
    }

    var view: NSView
    {
        return frontPanel
    }


}
