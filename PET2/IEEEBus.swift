//
//  IEEEBus.swift
//  M6502
//
//  Created by Jeremy Pereira on 23/07/2015.
//
//

import Foundation
import Swift6502

private struct ControlState: OptionSet
{
    let rawValue: S65Byte
    static let Dav  = ControlState(rawValue: 0b00000001)
    static let Ndac = ControlState(rawValue: 0b00000010)
    static let Nrfd = ControlState(rawValue: 0b00000100)
    static let Atn  = ControlState(rawValue: 0b00001000)
    static let Eoi  = ControlState(rawValue: 0b00010000)
    static let Ifc  = ControlState(rawValue: 0b00100000)
    static let Ren  = ControlState(rawValue: 0b01000000)
    static let Seq  = ControlState(rawValue: 0b10000000)

    static func fromString(_ name: String) -> ControlState
    {
        switch  name
        {
        case "dav":
            return .Dav
        case "ndac":
            return .Ndac
        case "nrfd":
            return .Nrfd
        case "atn":
            return .Atn
        case "eoi":
            return .Eoi
        case "ifc":
            return .Ifc
        case "ren":
            return .Ren
        case "seq":
            return .Seq
        default:
            fatalError("Invalid control line: \(name)")
        }
    }
}

///
/// Emulates an IEEE bus
///
open class IEEEBus: CustomStringConvertible
{
    open var trace = false
    // MARK: Data bus

    /// Eight bit data bus
    open var dio: Bus = Bus()

    // MARK: Data byte transfer control

    /// Data valid
    open var dav = AndGate()
    /// Not data accepted
    open var ndac = AndGate()
    /// Not ready for data
    open var nrfd = AndGate()

    // MARK: General interface management

    /// Attention distinguishes commands from data.
    ///
    /// If low data is treated as a command.
    open var atn = Wire()
    /// End or identity - end of data
    open var eoi = Wire()
    /// Interface clear - resets whole interface
    open var ifc = Wire()
    /// Remote enable - gives remote device control of the bus
    ///
    /// Not used on the PET
    open var ren = Wire()
    /// Service request - allows other device to request service
    ///
    /// Not used on the PET
    open var srq = Wire()

    fileprivate var initFinished = false

    var clock: MicroSecondClock

    public init(clock: MicroSecondClock)
    {
        self.clock = clock
//        dio.connect(self, mask: 0xff, notifier: ioNotifier)
//        atn.connectNotifier(unimplementedWireNotifier("atn"))
//        dav.outputWire.connectNotifier(unimplementedWireNotifier("dav"))
//        eoi.connectNotifier(unimplementedWireNotifier("eoi"))
//        ifc.connectNotifier(unimplementedWireNotifier("ifc"))
//        ren.connectNotifier(unimplementedWireNotifier("ren"))
//        srq.connectNotifier(unimplementedWireNotifier("srq"))
//        ndac.outputWire.connectNotifier(unimplementedWireNotifier("ndac"))
//        nrfd.outputWire.connectNotifier(unimplementedWireNotifier("nrfd"))

        initFinished = true
    }

    fileprivate func ioNotifier(_ value: S65Byte)
    {
        if trace
        {
            print("\(self), IO changed")
        }
    }

    fileprivate var recursionChecks = ControlState(rawValue: 0)

    fileprivate func unimplementedWireNotifier(_ name: String, _ value: Bool)
    {
        if trace
        {
            print("\(self), \(name) changed")
        }
    }

    fileprivate var wireStates: [String : Bool] = [:]

    open var description: String
    {
		return "IEEE Bus (\(clock.time)): dav=\(dav.value), ndac=\(ndac.value), nrfd=\(nrfd.value), atn=\(atn.value), dio=\(dio.value.hexString)"
    }
}
