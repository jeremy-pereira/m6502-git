//
//  VDU.metal
//  PET2
//
//  Created by Jeremy Pereira on 22/09/2018.
//

#include <metal_stdlib>
using namespace metal;

struct Geometry
{
    uint2 charDims;
    uint2 screenDims;
};

constant struct Geometry pet40Geometry = { uint2(8, 8), uint2(40, 25) };

kernel void petVDURender(texture2d<float, access::write> output [[texture(0)]],
                     	constant uint8_t *characterMaps [[buffer(1)]],
                    	constant uint8_t *screenBuffer [[buffer(2)]],
                     	uint2 gid [[thread_position_in_grid]])
{
    uint2 screenDims = uint2(output.get_width(), output.get_height());
    uint2 petPixelDims = pet40Geometry.charDims * pet40Geometry.screenDims;
    uint2 petPixel = gid * petPixelDims / screenDims;

    // Calculate the index of the character
    uint2 charPos = petPixel / pet40Geometry.charDims;
    uint screenBufferIndex = charPos.x + charPos.y * pet40Geometry.screenDims.x;
    uint charStartIndex = screenBuffer[screenBufferIndex] * pet40Geometry.charDims.y;
    // charStartIndex now points at the byte of the first line of the character
    uint2 bitIndexes = petPixel % pet40Geometry.charDims;
    bool isGreen = (characterMaps[charStartIndex + bitIndexes.y] & (0x80 >> bitIndexes.x)) != 0;

    float3 colour = isGreen ? float3(0, 1, 0) : float3(0);
    output.write(float4(colour, 1), gid);
}
