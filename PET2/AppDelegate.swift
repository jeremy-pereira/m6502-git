//
//  AppDelegate.swift
//  PET2
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa
import Swift6502

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{
    @IBOutlet weak var window: NSWindow!

    var fpController: FPWindowController!

    func applicationDidFinishLaunching(_ aNotification: Notification)
    {
        UserDefaults.standard.set(true, forKey: "NSConstraintBasedLayoutVisualizeMutuallyExclusiveConstraints")
        fpController = FPWindowController(windowNibName: "FPWindowController")
        _ = fpController.window
    }

    func applicationWillTerminate(_ aNotification: Notification)
    {
        // Insert code here to tear down your application
    }

    @IBAction func openFile(_ sender: AnyObject?)
    {
		let openFile = NSOpenPanel()
        openFile.allowsMultipleSelection = false
        openFile.canChooseDirectories = false
        openFile.canChooseFiles = true
        openFile.title = "Load Program"
        openFile.message = "Pick a file to load"
        openFile.prompt = "Load"
        openFile.allowedFileTypes = ["bin", "prg"]
        openFile.allowsOtherFileTypes = true

        openFile.beginSheetModal(for: fpController.window!)
        {
            [weak self] response in
            switch response
            {
            case NSApplication.ModalResponse.OK:
                let urls = openFile.urls
                do
                {
                    let data = try Data(contentsOf: urls[0])
                    let program = data.withUnsafeBytes
                    {
                        return Array($0)
                    }
                    try self?.fpController.load(program: program)
                }
                catch
                {
                    self?.window.presentError(error)
                }
            case NSApplication.ModalResponse.cancel:
                break
            default:
                fatalError("Invalid open panel response")
            }
        }
    }

    // TODO: Connect these actions to the righrt code in the CPU controller
    @IBAction func goAtFullSpeed(_ sender: AnyObject?)
    {

    }
    @IBAction func goAtFullUpdates(_ sender: AnyObject?)
    {

    }


}

