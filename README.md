# README #

## What is this repository for? ##

* This is a 6502 and Commodore PET emulation written for macOS.  
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

There are actually two emulations in here.  The first was written in a combination of Objective-C and pure C.  Almost all of the processor intensive stuff is in pure C with an Objective-C wrapper for ease of interfacing to the UI.  This emulation is the fastest one.  The PET clocks in at about 150 MHz on my MacBook Pro 2.6 Ghz i7.  The 6502 running with no external peripherals runs closer to 200 MHz.

The second emulation is written entirely in Swift 4.2.  This emulation is now far more complete as the C version but the PET clocks at around 83 MHz.  

The main target for the Swift emulation is the PET2 target.


## How do I get set up? ##

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact