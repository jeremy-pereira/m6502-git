# PET Architecture 
## IO chips
### PIA1
<table>
<thead>
	<tr><th>Address</th><th>Name</th><th>7</th><th>6</th><th>5</th><th>4</th><th>3</th><th>2</th><th>1</th><th>0</th></tr>
</thead>
<tbody>
<tr><td>$E810</td><td>Port A</td><td>Diag. sense</td><td>EOI in</td><td>Cassette&nbsp;2 sense</td><td>Cassette&nbsp;1 sense</td><td colspan="4">Keyboard row select</td></tr>

<tr><td>$E811</td><td>Control A</td><td>CA1 transition flag</td><td>CA2 transition flag</td><td colspan="3">CA2 control = EOI out</td><td>Port-DDR Switch</td><td colspan="2">CA1 control = cassette 1 read</td></tr>

<tr><td>$E812</td><td>Port B</td><td colspan="8">Contents of selected keyboard row</td></tr>

<tr><td>$E813</td><td>Control B</td><td>CB1 transition flag</td><td>CB2 transition flag</td><td colspan="3">CB2 control = Cassette 1 motor</td><td>Port-DDR Switch</td><td colspan="2">CB1 control = Screen retrace line</td></tr>

</tbody>
</table>

### PIA2
<table>
<thead>
	<tr><th>Address</th><th>Name</th><th>7</th><th>6</th><th>5</th><th>4</th><th>3</th><th>2</th><th>1</th><th>0</th></tr>
</thead>
<tbody>
<tr><td>$E820</td><td>Port A</td><td colspan="8">Input for the IEEE bus</td></tr>

<tr><td>$E821</td><td>Control A</td><td>CA1 transition flag</td><td>CA2 transition flag</td><td colspan="3">CA2 control = NDAC out</td><td>Port-DDR Switch</td><td colspan="2">CA1 control = ATN in</td></tr>

<tr><td>$E822</td><td>Port B</td><td colspan="8">Output for the IEEE bus</td></tr>

<tr><td>$E823</td><td>Control B</td><td>CB1 transition flag</td><td>CB2 transition flag</td><td colspan="3">CB2 control = DAV out</td><td>Port-DDR Switch</td><td colspan="2">CB1 control = SRQ in</td></tr>

</tbody>
</table>

### VIA
<table>
<thead>
	<tr><th>Address</th><th>Name</th><th>7</th><th>6</th><th>5</th><th>4</th><th>3</th><th>2</th><th>1</th><th>0</th></tr>
</thead>
<tbody>
<tr><td>$E840</td><td>Port B</td><td>DAV in</td><td>NRFD in</td><td>Retrace in</td><td>Tape #2 motor</td><td>Tape data out</td><td>ATN out</td><td>NRFD out</td><td>NDAC in</td></tr>
<tr><td>$E841</td><td>Port A</td><td colspan="8">User port with CA2 handshake</td></tr>
<tr><td>$E842</td><td>DDR B</td><td colspan="8">1 = output, 0 = input</td></tr>
<tr><td>$E843</td><td>DDR A</td><td colspan="8">1 = output, 0 = input</td></tr>
<tr><td>$E844</td><td>Timer 1 low</td><td colspan="8"></td></tr>
<tr><td>$E845</td><td>Timer 1 high</td><td colspan="8"></td></tr>
<tr><td>$E846</td><td>T1 latch lo</td><td colspan="8"></td></tr>
<tr><td>$E847</td><td>T1 latch hi</td><td colspan="8"></td></tr>
<tr><td>$E848</td><td>Timer 2 low</td><td colspan="8"></td></tr>
<tr><td>$E849</td><td>Timer 2 high</td><td colspan="8"></td></tr>
<tr><td>$E84A</td><td>Shift register</td><td colspan="8"></td></tr>
<tr><td>$E84B</td><td>ACR</td><td colspan="2">T1 control</td><td>T2 control</td><td colspan="3">Shift register control</td><td>Port B latch</td><td>Port A latch</td></tr>
<tr><td>$E84C</td><td>PCR</td><td colspan="3">CB2 control</td><td>CB1 control</td><td colspan="3">CA2 control</td><td>CA1 control</td></tr>
<tr><td>$E84D</td><td>IFR</td><td>IRQ on/off</td><td>T1 int</td><td>T2 int</td><td>CB1 int</td><td>CB2 int</td><td>Shift int</td><td>CA1 int</td><td>CA2 int</td></tr>
<tr><td>$E84E</td><td>IER</td><td>Enable-disable</td><td>T1</td><td>T2</td><td>CB1</td><td>CB2</td><td>Shift</td><td>CA1</td><td>CA2</td></tr>
<tr><td>$E84F</td><td>Port A</td><td colspan="8">User port without CA2 handshake</td></tr>

</tbody>
</table>

## IEEE bus
### Command Bytes

<table>
<tr><th colspan="2">Command (xxx-----)</th><th>(---x----)</th><th>(----xxxx)</th></tr>
<tr><td>0</td><td>GTL</td><td>0</td><td>1</td></tr>
<tr><td>0</td><td>LLO</td><td>1</td><td>1</td></tr>
<tr><td>0</td><td>SDC</td><td>0</td><td>4</td></tr>
<tr><td>0</td><td>DCL</td><td>1</td><td>4</td></tr>
<tr><td>0</td><td>PPC</td><td>0</td><td>5</td></tr>
<tr><td>0</td><td>PPU</td><td>1</td><td>5</td></tr>
<tr><td>0</td><td>GET</td><td>0</td><td>8</td></tr>
<tr><td>0</td><td>SPE</td><td>1</td><td>8</td></tr>
<tr><td>0</td><td>TCT</td><td>0</td><td>9</td></tr>
<tr><td>0</td><td>SPD</td><td>1</td><td>9</td></tr>
<tr><td>1</td><td>LAG</td><td colspan="2">device 0 - 30</td></tr>
<tr><td>1</td><td>UNL</td><td colspan="2">all devices 31</td></tr>
<tr><td>2</td><td>TAG</td><td colspan="2">device 0 - 30</td></tr>
<tr><td>2</td><td>UNT</td><td colspan="2">all devices 31</td></tr>
<tr><td>3</td><td>SCG</td><td colspan="2">secondary device 0 - 31</td></tr>
<tr><td>7</td><td>SCG close</td><td>0</td><td>secondary device 0 - 15</td></tr>
<tr><td>7</td><td>SCG open</td><td>1</td><td>secondary device 0 - 15</td></tr>
</table>


## BASIC Program format

Lines are separated by byte $00.  Each line starts with a two byte link address and a two byte line number.  If the link address is $0000, the last line was the last line in the program.
## Disk Drives
### Directory structure
The directory structure sent back by the drives is the same as that of a program.  Format is as follows:

                