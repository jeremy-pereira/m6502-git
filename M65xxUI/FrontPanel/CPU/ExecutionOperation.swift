//
//  ExecutionOperation.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa
import Swift6502


/// An operation to execute a CPU for a given number of cycles. It also has
/// blocks that can be set to give updates to the user interface and to tell the
/// UI when the operation has completed. Note that these blocks run on the
/// thread used by the dispatch queue, su you'll need to have them queue UI
/// updates to the main queue.
///
/// Run one of these by creating it and then using a dispatch queue to execute
/// the `main()` function.
public final class ExecutionOperation
{

    /// Set this to stop the operation before all of its cycles have completed.
    /// Note that this is only checked when the update block runs, so, if you
    /// have a very large value for `cyclesBetweenUpdates`, the operation may
    /// not stop for a while.
    public var isCancelled = false
    var cyclesToRun: CycleCount
    var cyclesBetweenUpdates: CycleCount
    /// The calculated speed of the CPU in MHz. This is calculated by the total
    /// number of cycles executed divided by the elapsed time. 
    public private(set) var speedInMegaHerz: Double = 1
    var cpu: Mos65xx
    private var updateBlock: (ExecutionOperation) -> ()
    private var completionBlock: (ExecutionOperation) -> ()


    /// Create a new execution operation
    ///
    /// - Parameters:
    ///   - cpu: The CPU to run
    ///   - cyclesToRun: The number of cycles to run the CPU for
    ///   - cyclesBetweenUpdates: The number of cycles to run before doing an update
    ///   - updateBlock: A block to run to give updates. This block runs on the 
    ///                  execution thread.
    ///   - completionBlock: A bock that runs when the CPU hasd executed all the
    ///                      cycles. This block runs on the execution thread,
    public init(         cpu: Mos65xx,
                 cyclesToRun: CycleCount,
        cyclesBetweenUpdates: CycleCount,
                 updateBlock: @escaping (ExecutionOperation) -> (),
        	 completionBlock: @escaping(ExecutionOperation) -> ())
    {
        self.cpu = cpu
        self.cyclesToRun = cyclesToRun
        self.cyclesBetweenUpdates = cyclesBetweenUpdates
        self.updateBlock = updateBlock
        self.completionBlock = completionBlock
    }


    public final func main()
    {
		var cyclesToGo = cyclesToRun
        let startNanoseconds = DispatchTime.now().uptimeNanoseconds
        var cyclesUsed: CycleCount = 0
        while cyclesToGo > 0 && !self.isCancelled
        {
            updateBlock(self)
            let startCycles = self.cpu.time
            cpu.runForCycles(cyclesBetweenUpdates)
            cyclesToGo -= cpu.time - startCycles
            let currentTime = DispatchTime.now()
            let elapsedTime = currentTime.uptimeNanoseconds - startNanoseconds
            cyclesUsed = self.cyclesToRun - cyclesToGo
            // To get the speed in Hertz, divide by time / 1E9
            // To get the speed in MHz, divide by 1E6 * (time / 1E9) = time / 1E3
            speedInMegaHerz = Double(cyclesUsed) / (Double(elapsedTime) / 1000)
        }
       	updateBlock(self)
        // TODO: Move to the completion handler
        let elapsedTime = Double(DispatchTime.now().uptimeNanoseconds - startNanoseconds) / 1e09
        print("\(cyclesUsed) cycles in \(elapsedTime) secs for \(Double(cyclesUsed) / (elapsedTime * 1000000)) MHz")
        completionBlock(self)
    }
}
