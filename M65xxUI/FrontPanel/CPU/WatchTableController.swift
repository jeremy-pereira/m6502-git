//
//  WatchTableController.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa
import Swift6502

class WatchTableController: NSObject, NSTableViewDataSource
{
    @IBOutlet weak var watchView: NSTableView!

    fileprivate var watchPoints: [S65Address] = []
    var cpu: Mos65xx?
    {
		didSet
        {
            watchView.reloadData()
        }
    }

    func numberOfRows(in tableView: NSTableView) -> Int
    {
        return watchPoints.count
    }

    func tableView(                 _ tableView: NSTableView,
        objectValueFor tableColumn: NSTableColumn?,
        row: Int) -> Any?
    {
        var ret = "??"

        let item = watchPoints[row]
        if (tableColumn?.identifier)!.rawValue == "address"
        {
            ret = item.hexString
        }
        else
        {
            if let cpu = cpu
            {
                ret = ""
                for i in S65Address(0) ..< S65Address(8)
                {
                    let address = item &+ i
                    let byte = cpu.byte(at: address)
                    ret += byte.hexString + " "
                }
            }
        }

        return ret
    }

    func tableView(_ tableView: NSTableView,
       setObjectValue object: Any?,
  for tableColumn: NSTableColumn?,
                         row: Int)
    {
        if let tableColumn = tableColumn
        {
            if tableColumn.identifier.rawValue == "address"
            {
				let theString = object as! String
                if let theAddress = theString.s65Address
                {
                    watchPoints[row] = theAddress
                    DispatchQueue.main.async(execute: { self.watchView.reloadData() })
                }
            }

        }
    }

    @IBAction func removeWatchPoint(_ sender: AnyObject)
    {
        // TODO: Implement
    }

    @IBAction func addWatchPoint(_ sender: AnyObject)
    {
		watchPoints.append(0)
        watchView.reloadData()
    }


}
