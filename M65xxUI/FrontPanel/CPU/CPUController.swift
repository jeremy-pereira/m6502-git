//
//  CPUController.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa
import Swift6502

let cycleCountMax: CycleCount = Int.max

open class CPUController: NSViewController, OperationUpdatable
{
    fileprivate var cpu: Mos65xx

    @IBOutlet weak var irqLine: NSButton!
    @IBOutlet weak var levelIndicator: NSLevelIndicator!
    @IBOutlet weak var maxLevelLabel: NSTextField!
    @IBOutlet var registerDataSource: RegisterDataSource!
    @IBOutlet weak var singleStep: NSButton!
    @IBOutlet weak var unitsLabel: NSTextField!
    @IBOutlet var watchViewController: WatchTableController!
    @IBOutlet weak var speedMHZ: NSTextField!

    private var uberController: OperationUpdatable!

    /// Initiaslise this CPU controller
    ///
    /// - Parameters:
    ///   - cpu: The CPU that is the controller's model
    ///   - upberController: The top level controller that this controller is
    ///                      subordinsate to. Usually, it is the controller
    ///                      for the container view for all the front panel views.
    public init(cpu: Mos65xx, uberController: OperationUpdatable?)
    {
		self.cpu = cpu
        super.init(nibName: "CPUController", bundle: Bundle(for: type(of: self)))
        self.uberController = uberController ?? self
    }

    required public init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

    override open func viewDidLoad()
    {
        super.viewDidLoad()
        registerDataSource.device = cpu
        watchViewController.cpu = cpu
    }

    @IBAction func setTrace(_ sender: AnyObject)
    {
		if let checkBox = sender as? NSButton
        {
			cpu.trace = checkBox.state == .on
        }
    }

    @IBAction func reset(_ sender: AnyObject)
    {
        cpu.notReset = false
        cpu.notReset = true
    }

    private var operation: ExecutionOperation?


    /// Start the CPU on our execution queue
    open func startCPU()
    {
        guard operation == nil else { return }

        operation = ExecutionOperation(cpu: self.cpu,
                               cyclesToRun: self.singleStep.state == NSControl.StateValue.on ? 2 : cycleCountMax,
                      cyclesBetweenUpdates: updateCycles,
                               updateBlock:
            {
                operation in
                DispatchQueue.main.async
                {
                    [weak self] in
                    self?.uberController.update(operation: operation)
                }
            },
                          completionBlock:
            {
                [weak self] operation in
                if let controller = self
                {
                    DispatchQueue.main.async
                    {
                        controller.complete(operation: operation)
                    }
                }
        	})
        executionQueue.async(execute: operation!.main)
    }

    @IBAction open func startCPU(_ sender: AnyObject)
    {
		startCPU()
    }

    private var operationCompletionActions: [(ExecutionOperation?) -> ()] = []


    /// Adds an action to the operation completion actions. Operations always
    /// run on the main thread. If there is no operation, the action will run 
    /// pretty much straight away, possibly before the caller has finished but the
    /// passed parameter to the action will be nil.
    ///
    /// - Parameter action: The action to add.
    func onOperationComplete(action: @escaping (ExecutionOperation?) -> ())
    {
        DispatchQueue.main.async
        {
            [weak self] in
            guard let controller = self else { return }
            if controller.operation != nil
            {
				controller.operationCompletionActions.append(action)
            }
            else
            {
                action(nil)
            }
        }
    }

    private func complete(operation: ExecutionOperation)
    {
        assert(operation === self.operation, "Operation is the wrong one")
        assert(Thread.current === Thread.main, "complete(operation:) should only run on the main thread")
        for action in operationCompletionActions
        {
			action(operation)
        }
        operationCompletionActions.removeAll()
		self.operation = nil
    }


    /// Stop the CPU and execute the given operation if supplied.
    ///
    /// - Parameter onComplete: A completion actiun that runs on the main thread.
    open func stopCPU(onComplete: @escaping ((Bool) -> ()) = { _ in })
    {
        self.onOperationComplete(action: { operation in onComplete(operation != nil) })
        operation?.isCancelled = true
    }

    fileprivate static let fullSpeedCycles = 300000
    fileprivate static let fullUpdateCycles = 2
    fileprivate var updateCycles = fullSpeedCycles



    /// Makes the CPU run as fast as possible with minimal updates
    ///
    /// - Parameter sender: Object requesting go at full speed.
    @IBAction open func goAtFullSpeed(_ sender: AnyObject)
    {
        updateCycles = CPUController.fullSpeedCycles
    }

    @IBAction func goAtFullUpdates(_ sender: AnyObject)
    {
        updateCycles = CPUController.fullUpdateCycles
    }


    /// Stop the CPU
    ///
    /// - Parameter sender: Object requesting the stop.
    @IBAction open func stopCPU(_ sender: AnyObject)
    {
        stopCPU()
    }

    lazy var executionQueue: DispatchQueue = {
        var queue = DispatchQueue(label: "cpuqueue",
                                    qos: DispatchQoS.default,
                             attributes: DispatchQueue.Attributes(rawValue: 0))
        return queue
    }()

    open func update(operation: ExecutionOperation)
    {
        registerDataSource.tableView.reloadData()
        watchViewController.watchView.reloadData()
		var speed = operation.speedInMegaHerz
        speedMHZ.doubleValue = speed
        let oldUnits = self.unitsLabel.stringValue
        /*
		 *  First set the speed to kilohertz or megahertz
         */
		if speed < 1 && oldUnits != "kHz"
        {
            speed *= 1000 // Get the speed in kHZ instead of MHz
            unitsLabel.stringValue = "kHz"
            levelIndicator.maxValue = 1 // Should force the level to change
        }
        else if speed >= 10 && oldUnits == "kHz"
        {
            unitsLabel.stringValue = "MHz"
            levelIndicator.maxValue = 1	// Should force the level to change
        }
        /*
		 *  Now set the maximum level to a multiple of 20 above the speed
         */
		if speed > levelIndicator.maxValue
        {
            let newMax = ceil(speed / 20) * 20
            levelIndicator.maxValue = newMax
            maxLevelLabel.doubleValue = newMax
        }
		levelIndicator.doubleValue = speed
    }
}
