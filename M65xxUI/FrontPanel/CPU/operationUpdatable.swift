//
//  operationUpdatable.swift
//  M65xxUI
//
//  Created by Jeremy Pereira on 29/09/2018.
//


/// Protocol for a type that can be updated by an execution operation
public protocol OperationUpdatable
{

    /// Called when an execution operation has an update
    ///
    /// - Parameter operation: The operation that has an update
    func update(operation: ExecutionOperation)


    /// Operationsal updatables must have an associated view that gets updated
    var view: NSView { get }
}
