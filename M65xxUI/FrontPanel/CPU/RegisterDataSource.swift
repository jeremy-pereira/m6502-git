//
//  RegisterDataSource.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa
import Swift6502

class RegisterDataSource: NSObject, NSTableViewDataSource
{
    @IBOutlet weak var tableView: NSTableView!

    var device: DeviceWithRegisters?
    {
		didSet
        {
            if device != nil
            {
                tableView.reloadData()
            }
        }
    }

    func numberOfRows(in tableView: NSTableView) -> Int
    {
        return device != nil ? device!.numberOfRegisters : 0
    }

    func tableView(                 _ tableView: NSTableView,
        objectValueFor tableColumn: NSTableColumn?,
                                          row: Int) -> Any?
    {
        var ret = ""
        if let device = device, let tableColumn = tableColumn
        {
            let registerInfo = device.infoForRegister(row)
			switch (tableColumn.identifier.rawValue)
            {
            case "name":
                ret = registerInfo.name
            case "value":
                ret = formatRegisterType(registerInfo, value: device.valueOfRegister(row))
                break
            default:
                break

            }
        }
        return ret
    }

    func formatRegisterType(_ registerInfo: RegisterInfo, value: Int) -> String
    {
        var ret: String = ""

		switch (registerInfo.registerType)
        {
        case .unsigned8:
            ret = value.makeHexString(2)
        case .unsigned16:
            ret = value.makeHexString(4)
        case .flags:
            for i in 0 ..< registerInfo.flagNames.count
            {
                if value & (1 << i) != 0
                {
                    ret = registerInfo.flagNames[i] + ret
                }
                else
                {
                    ret = "-" + ret
                }
            }
        }
        return ret
    }
}
