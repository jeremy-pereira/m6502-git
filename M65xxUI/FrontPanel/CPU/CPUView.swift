//
//  CPUView.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa

class CPUView: NSView
{

    override func draw(_ dirtyRect: NSRect)
    {
        super.draw(dirtyRect)

        // Drawing code here.
    }

    override var intrinsicContentSize: NSSize
    {
        return NSSize(width: 501, height: 203)
    }
}
