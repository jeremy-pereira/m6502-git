//
//  FrontPanelView.swift
//  M6502
//
//  Created by Jeremy Pereira on 24/06/2015.
//
//

import Cocoa

let topBorder: CGFloat = 8
let bottomBorder: CGFloat = 8
let middleBorder: CGFloat = 8

/// This is a view meant to contain various different types of front panel views.
///
/// TODO: MAybe replace with a stack view?
open class FrontPanelView: NSView
{

    /// The set of views that is displayed in this front panel view. When you
    /// set it, it automatically adjusts itself and the views to display them
    /// in order vertically.
    open var views: [NSView] = []
    {
		didSet
        {
            var mySize: NSSize = NSSize(width: 20, height: topBorder + bottomBorder)
            var firstView = true
            for view in views
            {
                let viewSize = view.frame.size
                if firstView
                {
                    firstView = false
                }
                else
                {
					mySize.height += middleBorder
                }
                mySize.height += viewSize.height
                if viewSize.width > mySize.width
                {
                    mySize.width = viewSize.width
                }
            }
            self.setFrameSize(mySize)
            var origin = NSPoint(x: 0, y: bottomBorder)
            for aView in views
            {
				aView.setFrameOrigin(origin)
                addSubview(aView)
                let thisSize = aView.frame.size
                origin.y += thisSize.height + middleBorder
            }
            self.needsDisplay = true
        }
    }
}
